#+title: [230314] my first Hammer Spoon lua code


** first

I haven't used Lua language. but it's simple is that.
I love it.

#+BEGIN_SRC lua
function openApplication(str)
  hs.application.open(str)
end

function bindApp(modifierSet, key, appName)
  hs.hotkey.bind(
    modifierSet,
    key,
    function()
      openApplication(appName)
    end)
end

function bindAppInFnKey(key, appName)
  bindApp({"cnd"}, key, appName)
end

bindAppInFnKey("e", "Emacs")
bindAppInFnKey("i", "Google Chrome")
bindAppInFnKey("k", "KakaoTalk")
#+END_SRC

** second - want a toggle

#+BEGIN_SRC lua
-- 특정 어플리케이션을 열거나 닫는 함수
function toggleApplication(appName)
   local app = hs.application.get(appName)
   if app then
      if app:isFrontmost() then
         app:hide()
      else
         app:activate()
      end
   else
      hs.application.open(appName)
   end
end

-- 주어진 키와 앱 이름으로 토글 바인딩을 설정하는 함수
function bindAppToggle(modifierSet, key, appName)
  hs.hotkey.bind(
    modifierSet,
    key,
    function()
      toggleApplication(appName)
    end)
end

-- 편리하게 함수 키와 앱 이름으로 토글 바인딩을 설정하는 함수
function bindAppInFnKey(key, appName)
  bindAppToggle({"cmd"}, key, appName)
end

bindAppInFnKey("e", "Emacs")
bindAppInFnKey(".", "Slack")
bindAppInFnKey("i", "Google Chrome")
bindAppInFnKey("k", "KakaoTalk")
bindAppInFnKey(",", "iTerm")
bindAppInFnKey("l", "DeepL")

#+END_SRC
