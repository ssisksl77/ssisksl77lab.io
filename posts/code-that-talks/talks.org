#+TITLE: 말들
#+author: Younghwan Name


* Edsger Dijkstra

** in his Turing award lecture

#+BEGIN_QUOTE
The major cause of the software crisis is that the machines have become several orders of magnitude more powerful! To put it quite bluntly: as long as there were no machines, programming was no problem at all; when we had a few weak computers, programming became a mild problem, and now we have gigantic computers, programming has become an equally gigantic problem.
#+END_QUOTE

* Claude Elwood Shannon

#+begin_quote
We know the past but cannot control it. We control the future but cannot know it.
#+end_quote


