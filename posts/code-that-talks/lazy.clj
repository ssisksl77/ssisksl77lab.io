(defn cons [a b]
  (fn [x]
    (case x
      :car a
      :cdr b)))

(defn car [cons]
  (cons :car))

(defn cdr [cons]
  (cons :cdr))

(defn force [stream]
  (let [s (stream)]
    s))

(defn stream-car [stream]
  (car stream))

(defn stream-cdr [stream]
  (force (cdr stream)))

(defn delay [a]
  (fn [] a))

(defn make-stream [a b]
  (cons a (delay b)))


(def stream (make-stream 1 (cons 2 (cons 3 nil))))

(car (cons 1 2))
(cdr (cons 1 2))
(car (cons 1 (cons 2 nil)))
(cdr (cons 1 (cons 2 nil)))
(car (cdr (cons 1 (cons 2 nil))))


(stream-car stream)
(car (stream-cdr stream))
