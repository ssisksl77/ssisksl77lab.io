// functional programming
// https://sookocheff.com/post/fp/representing-pairs-and-lists-in-lambda-calculus/
// https://sookocheff.com/post/fp/typed-lambda-calculus/
var pair = a => b => f => f(a)(b)
var first = a => b => a
var second = a => b => b

pair(1)(2)(first)
pair(1)(2)(second)

var list = pair
