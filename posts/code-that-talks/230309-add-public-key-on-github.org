#+TITLE: github에 rsa키 등록하기 (mac)
#+date: <2023-03-09 Wed>
#+author: Younghwan Nam
#+email: ssisksl77@gmail.com
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../css/site.css" />

#+BEGIN_SRC bash
git clone git@github.com:quarkusio/quarkus.git
'quarkus'에 복제합니다...
git@github.com: Permission denied (publickey).
fatal: 리모트 저장소에서 읽을 수 없습니다

올바른 접근 권한이 있는지, 그리고 저장소가 있는지
확인하십시오.
#+END_SRC

새로운 맥을 사면서 문제가 발생했다.
key를 github에 추가하자

#+BEGIN_SRC bash
ssh-keygen -t rsa -b 4096 -C "남영환 github 계정 이메일"
#+END_SRC

~ssh-keygen~ 명령어에서 ~-t~ 는 암호화 방식을 의미하고, ~-b~ 는 암호화 키의 크기를 의미한다.
~-C~ 는 주석을 의미한다.

위 예제를 예로들면 4096비트의 rsa 암호화 방식으로 키를 생성하고 주석에는 "남영환 github 계정 이메일"을 넣는다.
4096비트로 생성한 이유는 2048비트는 너무 쉽게 해킹이 가능하기 때문이다.

이제 생성된 키를 github에 등록한다.
키는 ~/.ssh/id_rsa.pub에 있다.

#+BEGIN_SRC bash
cat ~/.ssh/id_rsa.pub
#+END_SRC

~cat~ 명령어로 키를 복사하고 github에 등록한다.

#+BEGIN_SRC bash
git clone git@github.com:quarkusio/quarkus.git
'quarkus'에 복제합니다...
Enter passphrase for key '/Users/tom/.ssh/id_rsa':
remote: Enumerating objects: 1079398, done.
remote: Counting objects: 100% (2731/2731), done.
remote: Compressing objects: 100% (1383/1383), done.
remote: Total 1079398 (delta 1242), reused 2621 (delta 1171), pack-reused 1076667
오브젝트를 받는 중: 100% (1079398/1079398), 235.19 MiB | 10.37 MiB/s, 완료.
델타를 알아내는 중: 100% (456587/456587), 완료.
Updating files: 100% (19627/19627), 완료.
#+END_SRC

잘 되는 것 같다.