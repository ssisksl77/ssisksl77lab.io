#+TITLE: 타임리프 404페이지
#+date: <2022-02-05 Sat>
#+author: Younghwan Nam
#+email: ssisksl77@gmail.com
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../css/site.css" />

* Disabling the Whitelabel Error Page
Whitelabel 에러 페이지를 제거해야한다.

#+BEGIN_SRC properties
server.error.whitelabel.enabled=false
#+END_SRC

* Displaying Custom Error Pages
아래 코드를 ~resources/templates/error.html~ 로 저장하면 저절로 연결된다.
#+BEGIN_SRC html
<!DOCTYPE html>
<html>
<body>
<h1>Something went wrong! </h1>
<h2>Our Engineers are on it</h2>
<a href="/">Go Home</a>
</body>
</html>
#+END_SRC

* 레퍼런스
- https://www.baeldung.com/spring-boot-custom-error-page