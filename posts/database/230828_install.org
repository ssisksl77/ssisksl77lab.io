#+date: <2023-08-28 Mon>
#+TITLE: [230828] Postgres 설치
#+author: Younghwan Nam
#+email: ssisksl77@gmail.com
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../css/site.css" />


Postgres를 사용한 적이 없다. 설치하고 사용해보려고 한다.

* 명령어로

#+BEGIN_SRC sh
  docker pull postgres:latest

  docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -p 5432:5432 -v postgres-data:/var/lib/postgresql/data -d postgres

  # 접속하기
  docker exec -it some-postgres psql -U postgres
#+END_SRC

postgres 는 ~postgres~ 유저가 기본적으로 제공된다. 그러므로 ~psql~ 로 접근할 때 ~postgres~ 를 꼭 붙여야함.

* Dockerfile

#+BEGIN_SRC Dockerfile
# 공식 PostgreSQL 이미지를 기반으로 합니다.
FROM postgres:latest

# 추가적인 설정이나 파일이 필요한 경우 이곳에 추가.
# 예: COPY init.sql /docker-entrypoint-initdb.d/
#+END_SRC


* docker-compose.yml

#+BEGIN_SRC yml
version: '3'

services:
  db:
    image: postgres:latest
    volumes:
      - postgres-data:/var/lib/postgresql/data
    environment:
      POSERGRES_DB: mydatabase            # 생성할 데이터베이스 이름
      POSTGRES_USER: user                 # 사용자 이름 
      POSTGRES_PASSWORD: mysecretpassword # 사용자 비밀번호
    ports:
      - "5432:5432"

  pgadmin:
    image: dpage/pgadmin4
    environment:
      PGADMIN_DEFAULT_EMAIL: admin@example.com
      PGADMIN_DEFAULT_PASSWORD: adminpassword
    ports:
      - "8080:80"
    depends_on:
      - postgres

volumes:
  postgres-data:
#+END_SRC

volumn을 확인하기

#+BEGIN_SRC sh
docker volumn inspect pgdata
#+END_SRC







