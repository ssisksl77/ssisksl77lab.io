#+title: SQLite documentation
#+email: ssisksl77@gmail.com
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../css/site.css" />

* Many Small Queries Are Efficient In SQLite

[[https://www.sqlite.org/np1queryprob.html][[Documentation Link]​]]

** N+1 쿼리는 SQLite에서 문제가 되지 않는다.

SQLite는 Client/Server가 아니다.
SQLite는 애플리케이션과 동일한 프로세스 주소 공간에서 실행된다.
쿼리에 message round-trip이 존재하지 않으며 오로지 함수 호출만 포함된다.
단일 SQL 쿼리의 지연 시간은 SQLite에서 훨씬 짧다.
따라서 SQLite에서 많은 수의 쿼리를 사용해도 문제가 되지 않는다.


* 35% Faster Than The Filesystem
