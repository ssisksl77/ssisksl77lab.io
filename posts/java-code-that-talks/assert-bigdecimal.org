#+title: [230518] BigDecimal Junit assert
#+date: <2023-05-18 목>
#+email: ssisksl77@gmail.com
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../css/site.css" />

BigDecimal은 equals 테스트를 하기가 애매하다.


#+BEGIN_SRC
@Test
fun mytest() {
  assertThat(BigDecimal.valueOf(1.0)).isEqualTo(BigDecimal.valueOf(1))
}
--- output ---
org.opentest4j.AssertionFailedError:
expected: 1
 but was: 1.0
Expected :1
Actual   :1.0
#+END_SRC

이렇게 BigDecimal은 테스트할 때 isEqualTo를 사용하는 것은 바람직하지 못하다.
간단한 해킹방법은 compareTo를 이용하는 것이다.


#+BEGIN_SRC
@Test
fun mytest2() {
  assertThat(BigDecimal.valueOf(1.0).compareTo(BigDecimal.valueOf(1))).isZero()
}
#+END_SRC

혹은 isEqualByComparingTo 를 사용하면 깔끔하게 비교가 된다.

#+BEGIN_SRC
@test
fun mytest3() {
  assertThat(BigDecimal.valueOf(1.0)).isEqualByComparingTo(BigDecimal.valueOf(1))
}
#+END_SRC
