(require 'ox-publish)

;; (setq org-agenda-exporter-settings
;;       '((ps-number-of-columns 2)
;;         (ps-landscape-mode t)
;;         (org-agenda-add-entry-text-maxlines 5)
;;         (htmlize-output-type 'css)))

(setq org-image-actual-width nil)

(setq org-publish-project-alist
      '(("posts"
          :base-directory "posts/"
          :base-extension "org"
          :publishing-directory "public/"
          :recursive t
          :publishing-function org-html-publish-to-html
          :auto-sitemap t
          :sitemap-title "I believe that code talks by itself"
          :sitemap-filename "index.org"
          :sitemap-style tree
          ;:sitemap-function org-list-to-subtree
          :author "Younghwan Nam"
          :email "ssisksl77@gmail.com"
          :with-creator t
          :sitemap-sort-files anti-chronologically
          :htmlize-source t
          :sitemap-sort-files alphabetically
          :sitemap-sort-folders first
          )
        ("css"
         :base-directory "css/"
         :base-extension "css"
         :publishing-directory "public/css"
         :publishing-function org-publish-attachment
         :recursive t)
        ("images"
         :base-directory "images/"
         :base-extension "png"
         :publishing-directory "public/images"
         :publishing-function org-publish-attachment
         :recursive t)
        ("all" :components ("posts" "css" "images"))))
