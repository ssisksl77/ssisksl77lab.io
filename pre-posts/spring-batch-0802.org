#+title: Spring Batch 0802

* 동기

Spring Batch를 사용하면서 실패에 대한 핸들링을 어떻게 하는지 좀 더 코드를 보면서 이해해보는 시간이 필요.
해당 내용은 나의 개인 기록이며, 두서가 없음.


* ApplicationContext

이 내용은 실패 핸들링에 대한 것과 큰 관련은 없지만 알면 좋은 내용이라 디버깅하면서 본 내용을 기록함.
SpringBatch를 수행하면 main메서드에 args와 함께 스프링어플리케이션을 수행할 것이다.

#+BEGIN_SRC java
fun main(args: Array<String>) {
  runApplication<App>(*args)
}
#+END_SRC

** JobLauncherApplicationRunner
스프링은 바로 사용할 수 있는 ApplicationRunner를 가져온다.
우리는 배치를 사용하니 배치용 ApplicationRunner가 필요하다.

#+BEGIN_SRC java
// SpringApplication.java
private void callRunners(ApplicationContext context, ApplicationArguments args) {
  List<Object> runners = new ArrayList<>();
  // 우리는 이곳에서 JobLaunchApplicationRunner
  runners.addAll(context.getBeansOfType(ApplicationRunner.class).values());
  runners.addAll(context.getBeansOfType(ApplicationRunner.class).values());
  AnnotationAwareOrderComparator.sort(runners);
  for (Object runner : new LinkedHashSet<>(runners)) {
    if (runner instanceof ApplicationRunner) {
      callRunner((ApplicationRunner) runner, args);
    }
    if (runner instanceof CommandLineRunner) {
      callRunner((CommandLineRunner) runner, args);
    }
  }
}

private void callRunner(ApplicationRunner runner, ApplicationArguments args) {
  try {
    (runner).run(args);
  }
  catch (Exception e) {
    throw new IllegalStateException("Failed to execute ApplicationRunner", ex)
  }
}
#+END_SRC

JobApplicationRunner의 run을 보면 매개변수를 배열로 쪼개서 내부적인 run메서드로 넣어준다.

#+BEGIN_SRC java
// JobLauncherApplicationRunner.java
public void run(ApplicationArguments args) throws Exception {
  String[] jobArguments = args.getNonOptionArgs().toArray(new String[0]);
  run(jobArguments);
}

public void run(String... args) throws JobExecutionException {
  logger.info("Running default command line with: " + Arrays.asList(args));
  launchJobFromProperties(StringUtils.splitArrayElementsIntoProperties(args, "="));
}

protected void launchJobFromProperties(Properties properties) throws JobExecutionException {
  // 이곳에서 jobName과 매개변수를 만들어준다.
  JobParameters jobParameters = this.converter.getJobParameters(properties);
  executeLocalJobs(jobParameters);
  executeRegisteredJobs(jobParameters);
}
#+END_SRC
일반적으로 executeLocalJobs가 수행될 것이다. this.job은 Job이라는 객체로 Autowired 된 것을 말한다.

#+BEGIN_SRC java
private Collection<Job> jobs = Collections.emptySet();
,,,
@Autowired(required = false)
public void setJobs(Collection<Job> jobs) {
  this.jobs = jobs;
}

private void executeLocalJobs(JobParameters jobParameters) throws JobExecutionException {
  for (Job job : this.jobs) {
    if (StringUtils.hasText(this.jobNames)) {
      String[] jobsToRun = this.jobNames.split(",");
      if (!PatternMatchUtils.simpleMatch(jobsToRun, job.getName())) {
        loger.debug(LogMessage.format("Skipped job: %s", job.getName()));
        continue;
      }
    }
    // 여기서 실행됨.
    execute(job, jobParameters);
  }
}

protected void execute(Job job, jobParameters jobParameters) throws ,,, {
  JobParameters parameters = getNextJobParameters(job, jobParameters);
  JobExecution execution = this.jobLauncher.run(job, parameters);
  if (this.publisher != null) {
    this.publisher.publishEvent(new JobExecutionEvent(execution));
  }
}
#+END_SRC

this.jobLauncher 는 특별한 구현체가 없으면 SimpleJobLauncher가 실행된다. (나는 현재 jobLauncher를 이용해서 job을 수행하고 있다.)
#+BEGIN_SRC java
public JobExecution run(final Job job, final JobParameters jobParameters) throws ,, {
  final JobExecution jobExecution;
  // Repository에서 해당 잡의 마지막 Execution 정보를 가져온다.
  JobExecution lastExecution = jobRepository.getLastJobExecution(job.getName(), jobParameters);
  // lastExecution이 null 이 아니면 그에 따른 처리를 한다.
  jobExecution = jobRepository.createJobExecution(job.getName(), jobParameters);

  try {
    taskExecution.execute(new Runnable() {
      try {
        // debug모드면 이곳저곳에서 로그를 찍어낸다.
        job.execute(jobExecution);
      } catch(Throwable t) {
        rethrow(t);
      }
    });
  } catch (TaskRejectedException e) {
    // 실패시 jobRepository에 update한다.
    jobExecution.upgradeStatus(BatchStatus.FAILED);
    if (jobExecution.getExitStatus().equals(ExitStatus.UNKNOWN)) {
      jobExecution.setExitStatus(ExitStatus.FAILED.addExitDescription(e));
    }
    jobRepository.update(jobExecution);
  }
  return jobExecution;
}
#+END_SRC

여기서 job.execute(jobExecution) 이 AbstractJob으로 갔다하면:

#+BEGIN_SRC java
public final void execute(JobExecution execution) {
  try {
    listener.beforeJob(execution);
    try {
      doExecute(execution);
    } catch (RepeatException e) {
      throw e.getCause();
    }
    ,,,
  } catch(Throwable t) {
	logger.error("Encountered fatal error executing job", t);
	execution.setExitStatus(getDefaultExitStatusForFailure(t, execution));
	execution.setStatus(BatchStatus.FAILED);
	execution.addFailureException(t);
  } finally {
    ,,,
    listener.afterJob(execution);
    jobRepository.update(execution)
  }
}
#+END_SRC

기본적인 Job 구현체는 FlowJob, SimpleJob 있다. 뭐든 상관없다.
#+BEGIN_SRC java
// SimpleJob.java
protected void doExecute(JobExecution) ,,, {
  StepExecution stepExecution = null;
  for (Step step : steps) {
	stepExecution = handleStep(step, execution);
	if (stepExecution.getStatus() != BatchStatus.COMPLETED) {
	  //
	  // Terminate the job if a step fails
	  //
  	  break;
  	}
  }
  //
  // Update the job status to be the same as the last step
  //
  if (stepExecution != null) {
	if (logger.isDebugEnabled()) {
  	  logger.debug("Upgrading JobExecution status: " + stepExecution);
	}
	execution.upgradeStatus(stepExecution.getStatus());
	execution.setExitStatus(stepExecution.getExitStatus());
  }
}

// FlowJob.java
@Override
protected void doExecute(final JobExecution execution) throws JobExecutionException {
  try {
	JobFlowExecutor executor = new JobFlowExecutor(getJobRepository(), new SimpleStepHandler(getJobRepository()), execution);
    executor.updateJobExecutionStatus(flow.start(executor).getStatus());
  }
  catch (FlowExecutionException e) {
    if (e.getCause() instanceof JobExecutionException) {
      throw (JobExecutionException) e.getCause();
    }
    throw new JobExecutionException("Flow execution ended unexpectedly", e);
  }
}
#+END_SRC

뭐 둘다 수행하는 코드가 있다. Tasklet의 경우는 FlowJob은 SimpleFlow 라는 구현체를 이용하여 Step을 실행한다:

#+BEGIN_SRC java
// SimpleFlow.java
public FlowExecution resume(String stateName, FlowExecutor executor) {
  // map에서 가져옴.
  State state = stateMap.get(stateName);  // stateName은 Job의 이름을 말하는데 my-job-name.step0 이런식으로 시작함.
  ,,,
  status = state.handler(executor);
}
#+END_SRC

드디어 status(StepState.java) 에서 FlowExecutor를 실행한다.
#+BEGIN_SRC java
// JobFlowExecutor 를 일반적으로 사용하는듯.
	@Override
	public FlowExecutionStatus handle(FlowExecutor executor) throws Exception {
		/*
		 ,* On starting a new step, possibly upgrade the last execution to make
		 ,* sure it is abandoned on restart if it failed.
		 ,*/
		executor.abandonStepExecution();
		return new FlowExecutionStatus(executor.executeStep(step));
	}
#+END_SRC

JobFlowExecutor.java 에서 stepHandler.handlerStep(step, execution); 으로 스텝을 실행한다.

AbstractStep.java의 doExecute(stepExecution); 으로 TaskletStep.java의 doExecute을 수행한다.
TODO : 여기서 예외를 받는걸 다음장에서 해야할듯.

#+BEGIN_SRC java
// AbstractStep.java
public final void execute(StepExecution stepExecution) throws ,,, {
  // JobRepository 갱신.
  getJobRepository().update(stepExecution);
  // 시작
  ExitStatus exitStatus = ExitStatus.EXECUTING;
  doExecutionRegistration(stepExecution);
  try {
    getCompositeListener().beforeStep(stepExecution);
    open(stepExecution.getExecutionContext());  // Tasklet이면 ItemStream.open을 해야함.

    try {
      doExecute(stepExecution);  //
    } catch(RepeatException e) {
      throw e.getCause();
    }

    // and 연산은 여기서 심각도가 높은 녀석의 값으로 덮어짐.
    // EXECUTING have severity 1
    // COMPLETED have severity 2
    // NOOP have severity 3
    // STOPPED have severity 4
    // FAILED have severity 5
    // UNKNOWN have severity 6
    exitStatus = ExitStatus.COMPLETED.and(stepExecution.getExitStatus());
    // 강제로 멈추려고 한 경우
    if (stepExecution.isTerminateOnly()) {
        throw new JobInterruptedException("JobExecution interrupted.");
    }

    // 완료됨
    stepExecution.upgradeStatus(BatchStatus.COMPLETED);
  } catch (Throwable e) {
    stepExecution.upgradeStatus(determineBatchStatus(e));  // STOP인지 FAIL인지 확인하기위함. 예외던지면 FAIL
    exitStatus = exitStatus.and(getDefaultExitStatusForFailure(e));
    stepExecution.addFailureException(e);
  }

}
#+END_SRC

* Step - Exception

스텝(Step)에서 예외를 던지면 다음 스텝을 수행하지 않는다.
기본적으로 간단한 구현체은 Tasklet을 사용했다면 TaskletStep이 생성된다. 아래코드처럼 스텝을 생성코드를 보자:

#+BEGIN_SRC java
@Bean
fun myTaskletStep(
  return stepBuildferFactory["my-tasklet-step"]
    .tasklet(MyTasklet)
    .build()
)
#+END_SRC

build() 함수는 TaskletStep을 생성한다:

#+BEGIN_SRC java
public abstract class AbstractTaskletStepBuilder {
  ,,,
  public TaskletStep build() {
    ,,,
    TaskletStep step = new TaskletStep(getName());
    ,,,
    return step;
  }
}
#+END_SRC

아까도 말했듯이. Step이 오기까지는 AbstractJob -> FlowJob(SimpleJob일 수 있음) -> AbstractStep -> TaskletStep을 거쳐도 다가온다.
그리고 예외를 던지면 AbstractStep에서 catch로 잡아낸다.

만약 내가 로직상에서 RuntimeException을 던진 것이라면 determineBatchStatus(e) 는 FAILED 가 나올 것이다.
저장되는 exitStatus도 "FAILED" 가 된다.
이후 stepExecution.addFailjureException 에 예외를 저장하는 점을 기억하자.

#+BEGIN_SRC java
catch(Throwable e) {
  stepExecution.upgradeStatus(determineBatchStatus(e));
  exitStatus = exitStatus.and(getDefaultExitStatusForFailure(e));
	stepExecution.addFailureException(e);
}
#+END_SRC

다음 finally 코드를 보자. 이제 exitStatus를 stepExecution에 할당하고, afterStep을 수행한다. (그 이후는 Tasklet을 닫는 작업들이다.)
#+BEGIN_SRC java
try {
  // Update the step execution to the latest known value so the
	// listeners can act on it
	exitStatus = exitStatus.and(stepExecution.getExitStatus());
	stepExecution.setExitStatus(exitStatus);
	exitStatus = exitStatus.and(getCompositeListener().afterStep(stepExecution));
}
#+END_SRC

이로써 AbstractJob에 있는 handleStep(StepHandler) 메서드가 끝난다.

다음 스텝이 존재하면 다음 스텝을 실행시킨다.
#+BEGIN_SRC java
// SimpleJob.java
protected void doExecute(JobExecution execution) throws ,,, {
  StepExecution stepExecution = null;
  for (Step step : steps) {
    stepExecution = handleStep(step, execution);
    if (stepExecution.getStatus() != BatchStatus.COMPLETED) {
      // Terminate the job if a step fails
      break;
    }
  }
  if (stepExecution != null) {
    execution.upgradeStatus(stepExecution.getStatus());
    execution.setExitStatus(stepExecution.getExitStatus());
  }
}
#+END_SRC

완료하면 이제 호출의 반대로 돌아온다 AbstractJob -> SimpleJobLauncher

내가 수행하고 있는 jobLauncher는 이런식의 코드가 나온다. 여기서 아까 저장한 예외 리스트를 보고 예외를 던져야한다.
#+BEGIN_SRC java
var result = jobLauncher.run(arg1, arg2);

if (result.status != BatchStatus.COMPLETED) {
  result.allFailureException.forEach {throwable ->
    logger.error {"실패 {}", throwable.toString()}
  }
  throw RuntimeException("실패")
}
#+END_SRC


그리고 Job이 끝나면 JobLauncherApplicationRunner 에서 이벤트를 발생시킨다.

#+BEGIN_SRC java
protected void execute(Job job, JobParameters jobParameters)
			throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException,
	JobParametersInvalidException, JobParametersNotFoundException {
	JobParameters parameters = getNextJobParameters(job, jobParameters);
	JobExecution execution = this.jobLauncher.run(job, parameters);
	if (this.publisher != null) {
		this.publisher.publishEvent(new JobExecutionEvent(execution));
	}
}
#+END_SRC

JobExecutionEvent를 핸들링하는 녀석이 이벤트를 받을 것이다.

#+BEGIN_SRC java
// SimpleApplicationEventMulticaster.java
	public void multicastEvent(final ApplicationEvent event, @Nullable ResolvableType eventType) {
		ResolvableType type = (eventType != null ? eventType : resolveDefaultEventType(event));
		Executor executor = getTaskExecutor();
		for (ApplicationListener<?> listener : getApplicationListeners(event, type)) {
			if (executor != null) {
				executor.execute(() -> invokeListener(listener, event));
			}
			else {
				invokeListener(listener, event);
			}
		}
	}
#+END_SRC

~ResolvableType type~ 에 저장된 값은 org.springframework.boot.autoconfigure.batch.JobExecutionEvent 이다.
~getApplicationListeners(event, type)~ 에서 이 이벤트를 핸들링하는 녀석들을 가져온다.

나의 경우 3개의 리스너가 나왔다.
#+BEGIN_SRC java
0 = {TaskLifecycleListener@19936}
1 = {DelegatingApplicationListener@19952}
2 = {JobExecutionExitCodeGenerator@19953}
#+END_SRC

이곳에서 내가 눈여겨 본 것은 ~JobExecutionExitCodeGenerator~ 이다.

#+BEGIN_SRC java
public class JobExecutionExitCodeGenerator implements ApplicationListener<JobExecutionEvent>, ExitCodeGenerator {
  private final List<JobExecution> executions = new CopyOnWriteArrayList<>();

  @Override
  public void onApplicationEvent(JobExecution event) {
    this.executions.add(event.getJobExecution());
  }

  @Override
  public int getExitCode() {
    for (JobExecution execution : this.executions) {
      if (execution.getStatus().ordinal() > 0) {
        return execute.getStatus().ordinal();
      }
    }
  }
}
#+END_SRC

이것으로 Job은 실패했지만 배치어플리케이션 자체는 실패했는지 알 수 없는 경우가 있는데
이럴 때, ExitCodeGenerator와 ExitCodeGenerator 빈들을 가져다가 실패한 값을 가지는 녀석들이 있는지 전수조사를 한다.
#+BEGIN_SRC java
fun main(args: Array<String>) {
    val applicationContext = runApplication<App>(*args)
    exitProcess(SpringApplication.exit(applicationContext))

}
#+END_SRC

이렇게하면 시스템이 종료될 때 무조건 0만 리턴되지는 않게된다.

만약에 RuntimeException으로 실패가 난경우는 리턴값이 5가 된다.
그런데 만약 리턴값을 내가 컨트롤하고 싶다면?

JobBuilder를 만들 때 리스너를 넣어서 처리할 수 있다.


#+BEGIN_SRC java
jobBuilderFactory["my-job"]
    .start(step1).on(ExitStatus.COMPLETED.exitCode).to(step2)
    .from(step1).on("*").fail()
    .end()
    .listener(object : JobExecutionListnerSupport() {
        override fun afterJob(jobExecution: JobExecution) {
          if (jobExecution.status != BatchStatus.COMPLETED) {
            exitProcess(1)
          }
        }
    })
#+END_SRC


* 참고자료

소스코드.
