

https://corfield.org/blog/2021/02/23/deps-edn-monorepo/


* 우리의 모노레포는 어떻게 생겼나?

우리의 main 깃레포 안에 ~build~ 폴더에 script, tooling, configuration이 있으며, ~clojure~ 폴더에 클로저 소스코드와 테스트코드가 있다.
~clojure~ 폴더 안에 재사용가능한 library, application이 서브 프로젝트로 존재한다.
우리는 이 코드베이스를 이용하여 12개 이상의 빌드를하여 ~uberjar~ 로 배포한다.

우린 111,000줄의 Clojure코드가 있다.
88,000줄은 소스코드이며 나머지 230,000줄은 테스트코드이다.

* 왜 우리는 모노레포를 쓰는가?

비록 우리가 자체적으로 애플리케이션 아티팩트를 빌드하고 배포할 수 있지만(때로는 그렇게 할 수 있지만)
우리는 단일 ~git tag/SHA~ 에 연결된 모든 애플리케이션 아티팩트를 함께 빌드하고 배포하려는 경향이 있습니다.

REPL에서 작업할 때 우리는 편집기에서 모든 소스 코드와 모든 테스트 코드(및 모든 종속성)를 사용할 수 있기를 원합니다.
원하는 경우 여러 하위 프로젝트에서 변경할 수 있으며 편집기에서 테스트 조합을 실행할 수 있습니다.

* Clojure CLI 와 deps.edn 이 어떤 도움을 주는가?

Leiningen은 점점 더 많은 개발/테스트/빌드 프로세스를 자동화하고 싶었기 때문에 제약이 따르기 시작했습니다.

2018년 이후 Clojure CLI로 모두 마이그레이션을 완료함.

우리는 Clojure CLI의 단순성과 성능을 좋아합니다.
필요한 경우에만 종속성 및 명령줄 옵션의 캐시를 계산하므로 
대부분 프로젝트의 코드 및 종속성만으로 단일 JVM(Leiningen과 달리)만 실행합니다.

일반 Clojure 코드를 사용하여 dev/test/build 파이프라인에 대해 원하는 모든 도구를 빌드할 수 있습니다.
(Boot의 접근 방식과 매우 유사하지만 Boot 자체의 작은 "프레임워크" 조차 없는 방식이 Clojure CLI)
이것은 또한 Leiningen이나 Boot를 사용하는 것보다 훨씬 더 원활하게 필요에 따라 도구를 혼합할 수 있습니다.

우리는 Clojure CLI가 공식적이며 Cognitect(Nubank)에서 지원되는 도구이며 
Clojure 자체와 함께 전체적인 방식으로 신중하게 고려되는 정기적인 업데이트를 받는 것을 좋아합니다.

~/.clojure/deps.edn에서 사용자 수준 구성을 제외하는 "reproducible" 방식으로 
프로젝트 기반 CLI 작업을 수행하도록 선택할 수 있습니다.
그러나 원하는 경우 개인 사용자 정의의 모든 기능을 사용하여 작업을 수행할 수도 있으므로 
개발자가 원하는 대로 환경을 설정할 수 있습니다.

우리는 의존성이 로컬일 수 있다는 점을 좋아합니다 ( ~:local/root~ )
이것으로 하위 프로젝트는 소스 코드 수준에서 서로 매우 쉽게 의존할 수 있습니다.
전체 모노레포에서 이미 편집기의 모든 소스 코드로 작업하는 방식을 반영합니다.

우리는 CLI를 호출할 때 단일 별칭을 포함하여 모든 하위 프로젝트에서 종속성의 버전을 "고정"할 수 있도록 특히 :override-deps를 좋아합니다.
(2021-04-21 우리는 그것을 좋아했지만 많은 CLI 도구로 잘 작동하지 않습니다.)

* How did the Clojure CLI and deps.edn hinder us?

CLI는 세 개의 deps.edn 파일이 있다고 가정합니다.
1. the root one baked into the CLI installation (실제로는 ~org.clojure/tools.deps.alpha~ 버전을 말하는가봄)
2. the user-level one (일반적으로 ~/.clojure/~)
3. and the project-level one.

우리의 초기 접근 방식은 CLI가 지원하는 ~CLJ_CONFIG~ 환경 변수를 활용하여 
사용자레벨(user-level) deps.edn 파일이 환경변수에 따라 다른 디렉토리를 선택하는 것이었다.

이를 통해 :override-deps를 통한 pinning dependencies(의존성 고정), 
testing tools, JAR 파일 구축 등을 위해 우리가 원하는 모든 "control" alias를 포함하는 단일 deps.edn을 가질 수 있었습니다.

그런 다음 각 하위 프로젝트에 고유한 프로젝트 수준 deps.edn 파일이 있어야 합니다. 이것은 잘 작동했습니다.

#+BEGIN_SRC sh
$ cd monorepo/subproject
$ CLJ_CONFIG=../versions clojure -M:defaults:other:aliases -m some.tooling
#+END_SRC

우리는 이것을 ~build~ 셸 스크립트로 래핑하여 다음을 실행할 수 있고
~cd~ 를 처리하고 명령줄의 다른 비트를 추가할 수 있습니다.

#+BEGIN_SRC sh
$ build other:aliases subproject -m some.tooling
#+END_SRC

...중략...

모노레포를 사용하고 있던 여러 클로저리안들과 이야기를 나눈 후,
일반적인 옵션은 프로젝트 수준의 deps.edn 파일을 프로그래밍 방식으로 생성하는 것 같았습니다.

...중략...

* The Third Way

결론적으로, 우리들의 모노레포 구조는 다음과 같다. 
#+BEGIN_SRC 
<repo>
|____build # our shell scripts / config / etc
...
|____clojure # our Clojure code "root"
| |____activator # a subproject
| | |____deps.edn # bare dependencies: no versions, no test
. . .    [2021-04-21: src dependencies with versions, no test]
| | |____src
| | | |____ws
| | | | |____activator.clj
| | |____test
| | | |____ws
| | | | |____activator_expectations.clj
...
| |____classes
| | |____.keep
...
| |____deps.edn # control deps.edn file
...
| |____worldsingles # another subproject
| | |____deps.edn
| | |____resources
...
| | |____src
...
| | |____test
...
#+END_SRC  

그리고 ~clojure/activator/deps.edn~ 은 다음과 같다.
#+BEGIN_SRC 
{:deps
 {worldsingles/worldsingles {:local/root "../worldsingles"}
  ;; originally:
  camel-snake-kebab/camel-snake-kebab {}
  com.stuartsierra/component {}
  seancorfield/next.jdbc {}
  ;; 2021-04-21:
  camel-snake-kebab/camel-snake-kebab {:mvn/version "0.4.2"}
  com.stuartsierra/component {:mvn/version "1.0.0"}
  seancorfield/next.jdbc {:mvn/version "1.1.646"}
  }}
#+END_SRC

~clojure/deps.edn~ 안에는 아래가 
#+BEGIN_SRC 
 ;; for each subproject, we have two aliases:
  :activator {:extra-deps {worldsingles/activator {:local/root "activator"}}}
  :activator-test
  {:extra-paths ["activator/test"]
   :extra-deps {worldsingles/worldsingles-test {:local/root "worldsingles-test"}}}
#+END_SRC

이 ~clojure/deps.edn~ 에는 tooling alias도 존재한다.
#+BEGIN_SRC 
  :test ; for a testing context
  {:extra-deps {com.gfredericks/test.chuck {:mvn/version "0.2.10"}
                expectations/clojure-test {:mvn/version "1.2.1"}
                org.clojure/test.check {}}
   :jvm-opts ["-Dclojure.core.async.go-checking=true"
              "-Dclojure.tools.logging.factory=clojure.tools.logging.impl/log4j2-factory"
              "-Dlogged-future=synchronous"
              "-XX:-OmitStackTraceInFastThrow"
              "--illegal-access=warn"]}

  :runner ; to run tests (test:<subproject>-test:runner) -- the <task> called test
  {:extra-deps {org.clojure/tools.namespace {}
                org.clojure/tools.reader {}
                com.cognitect/test-runner
                {:git/url "https://github.com/cognitect-labs/test-runner"
                 :sha "b6b3193fcc42659d7e46ecd1884a228993441182"}}
   :jvm-opts ["-Dlog4j2.configurationFile=log4j2-silent.properties"]
   :main-opts ["-m" "cognitect.test-runner"
               "-r" ".*[-\\.](expectations|test)(\\..*)?$"]}
#+END_SRC

이제 다음처럼 실행할 수 있다.

#+BEGIN_SRC 
$ clojure -M:defaults:activator:activator-test:test:runner -d activator/test
# or just
$ build test activator
#+END_SRC

모든 ~{}~ 버전은 ~:override-deps~ 의 ~:defaults~ 에 지정됨. (이후 사라짐 모노레포 2탄에 나오는듯)

#+BEGIN_SRC 
  ;; "pinned" versions for all cross-project dependencies
  :defaults
  {:override-deps
   {...
    camel-snake-kebab/camel-snake-kebab {:mvn/version "0.4.2"}
    clj-time/clj-time {:mvn/version "0.15.2"}
    clojure.java-time/clojure.java-time {:mvn/version "0.3.2"}
    ...
    org.clojure/tools.logging {:mvn/version "1.1.0"}
    org.clojure/tools.namespace {:mvn/version "1.0.0"}
    org.clojure/tools.reader {:mvn/version "1.3.3"}
    ...}}
#+END_SRC

빌드는 아래처럼

#+BEGIN_SRC 
$ clojure -X:uberjar :aliases '[:defaults :activator]' :jar '"test.jar"' :main-class ws.activator :aot true
#+END_SRC

~uberjar~ alias도 보자
#+BEGIN_SRC 
:uberjar
  {:replace-deps {seancorfield/depstar {:mvn/version "2.0.187"}}
   :exec-fn hf.depstar/uberjar}
#+END_SRC

~:everything~ 알리아스도 보자.
#+BEGIN_SRC 
  :everything
  {:extra-deps {worldsingles/activator {:local/root "activator"}
                ...
                worldsingles/worldsingles {:local/root "worldsingles"}
                ...}
                worldsingles/wsseogeo {:local/root "wsseogeo"}}
   :extra-paths ["activator/test"
                 ...
                 "worldsingles/test"
                 "worldsingles-test/test"
                 ...]}
#+END_SRC

REPL을 띄울 때는?
#+BEGIN_SRC 
$ clj -A:defaults:everything:test
Clojure 1.10.3-rc1
user=> (require 'ws.activator)
nil
user=>
#+END_SRC

그 이후로 여러가지 설명을 했지만 2탄에서 더 나은 방식으로 변경되었다니 그걸 구경가자.