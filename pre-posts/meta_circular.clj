;; 환경 계산법.
;; 1. special form을 제외하고, 식(combination)의 값을 구하려면 부분식의 값부터 모두 구해야 한다.
;;    그 다음에 연산자를 피연산자에 적용한다.
;; 2. 프로시저를 인자에 적용하려면, 프로시저의 몸(식)을 계산하기 위해 새 환경부터 만든다.


;; 4.1.1 언어실행기의 알짜배기
;; eval
;; primitive expression
;; - number같이 값을 나타내는 식(expression)은 곧바로 리턴.
;; - 변수가 오면 환경에서 찾아봄.
;; special form
;; - 따옴표가 있는 식 -> 따옴표 업애서 리턴.
;; - 정의하는 식 -> 새 변수를 eval, 이후 환경을 고친다.
;; - if -> 특별한 과정이 필요.
;; - lambda -> arguments, body, environment를 묶은 프로시저 생성.
;; - begin -> 차례대로 계산.
;; - cond -> if로 바꿈.
;; combination(엮은식)
;; - eval을 여러돌림. 이후 apply로 프로시저 계산을 맡는다.





(defn eval [exp env]
  (cond
    (self-evaluating? exp) exp
    (variable? exp)        (loopkup-variable-value exp env)
    (quoted? exp)          (text-of-quotation exp)
    (assignment? exp)      (eval-assignment exp env)
    (definition? exp)      (eval-definition exp env)
    (if? exp)              (eval-if)
    (lambda? exp)          (make-procedure (lambda-parameters exp)
                                           (lambda-body exp)
                                           env)
    (begin? exp)
    (eval-sequence (begin-actions exp) env)

    (cond? exp)
    (eval (cond->if exp) env)

    (application? exp)
    (apply (eval (operator exp) env)
           (list-of-values (operands exp) env))

    :else
    (error "Unknown expressino type --EVAL" exp)))


(defn apply [procedure arguments]
  (cond
    (primitive-procedure? procedure)
    (apply-primitive-procedure procedure arguments)

    (compound-procedure? procedure)
    (eval-sequence
     (procedure-body procedure)
     (extend-environment
      (procedure-parameters procedure)
      arguments
      (procedure-environment procedure)))

    :else
    (error "Unknown type -- APPLY" procedure)))

;; 잇단식 sequence of expressions
(defn list-of-values [exps env]
  (if (no-operand? exps)
    '()
    (cons (do (eval (first-operand exps) env)
              (list-of-values (rest-operands exprs) env)))))

(defn eval-if [exp env]
  (if (true? (eval (if-predicate exp) env))
    (eval (if-consequent exp) env)
    (eval (if-alternative exp) env)))
;; procedure argument
(defn eval-sequence [exps env]
  (cond
    (last-exp? exps) (eval (first-exp exprs) env)
    :else (eval (first-exp exps) env)))
;; 덮어쓰기와 정의.
(defn eval-assignment [exp env]
  )
