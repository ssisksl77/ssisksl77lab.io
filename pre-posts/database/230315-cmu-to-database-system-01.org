#+title: 230315 Cmu To Database System 01


이번 내용은 database storage 에 대한 이야기이다.

* disk-based architecture
the dbms assumes that the primary storage location of the database is on non-volatile disk.

the dbms's components manage the movement of data between non-volatile and volatile storage.

DBMS는 데이터가 휘발되지 않는 디스크에 저장되는 것을 기대한다.
DBMS의 컴포넌트들은 휘발되는 스토리지와 휘발되지 않는 스토리지 사이의 이동을 관리한다.


* storage hierachy
** volatile
- random access, byte-addressable
- cpu register > cpu caches > dram
** non-volatile
- sequential access, block-addressable
- ssd > hdd > network storage

알아야 할 점은 일반적으로 시퀀셜 엑세스는 랜덤엑세스보다 훨씬 빠르다.
그것이 ssd 처럼 디스크가 있는 것이 아니라 할지라도

새로 등장하는 저장장치로 ~persistent memory~ 라고 있으며, 이것은 디스크 저장공간이 있으면서 DRAM의 속도가 난다고 한다. 이 강의에서는 커버하지 않는다.
인텔이 Optane에 적용하였지만 돈이 안되어서 버림.

** ACCESS TIMES
Latency Numbers Every Programmer Should know

#+BEGIN_SRC
1 ns - L1 Cache Ref  -> 0.000000001 s    -> 1초(라고 가정)
4 ns - L2 Cache Ref  -> 0.000000004 s    -> 4초
100 ns - DRAM        -> 0.0000001 s      -> 100초
16,000 ns - SSD      -> 0.000016 s       -> 4.4시간
2,000,000 ns - HDD   -> 0.002 s          -> 3.3주
~50,000,000 ns Network Storage -> 0.05 s -> 1.5년
1,000,000,000 ns Tape Archives -> 1 s    -> 31.7년

ns = 1 x 10^-9
#+END_SRC

* sequential vs random access
random access on non-volatile storage is almost always much slower than sequential access.

DBMS will want to maximize sequential access.
- algorithms try to reduce number of writes to random pages so that data is stored in contiguous blocks.
- allocating multiple pages at the same time is called an extent.

* system design goal
allow the DBMS to manage databases that exceed the amount of memory available.

Reading/writing to disk is expensive, so it must be managed carefully to avoid
large stalls and performance degradation.

random access on disk is usually much slower than sequential access, so the DBMS will
want to maximize sequential access.

* disk-oriented DBMS
데이터베스는 모두 디스크에 저장된다. 데이터베이스 파일의 데이터는 페이지로 구성되며 첫 번째 페이지는 디렉토리 페이지이다.
데이터에 대한 작업을 수행하려면 DBMS가 데이터를 메모리로 가져와야한다. 이를 위해 디스크와 메모리 사이 데이터 이동을 관리하는 버퍼 풀(buffer pool)이 존재한다.
또한 DBMS에는 쿼리를 실행하는 실행엔진이 있다. 실행 엔진은 버퍼 풀에 특정 페이지를 요청하고, 버퍼 풀은 해당 페이지를 메모리로 가져오고 실행엔진에 메모리의 해당 페이지에 대한 포인터를 제공한다.
버퍼 풀 관리자는 실행 엔진이 메모리의 해당 부분에서 작동하는 동안 페이지가 있는지 확인한다.

* 왜 운영체제 기능을 이용하지 않는지?
DMBS의 높은 수준의 설계 목표는 사용 가능한 메모리 양을 초과하는 데이터베이스를 지원하는 것이다.
디스크 읽기/쓰기는 비용이 많이 들기 때문에 디스크 사용을 신중하게 관리해야한다.
디스크에서 무언가를 가져오느라 다른 모든 작업 속도가 느려지는 것을 원치 않는다.
DBMS가 디스크에서 데이터를 가져오기 위해 대기하는 동안 다른 쿼리를 처리할 수 있기를 원한다.

이런 높은 수준의 설계 목표는 큰 주소 공간과 OS가 디스크에서 페이지를 가져올 수 있는 장소가 있는 가상 메모리를 말하는 것 같다.

DBMS는 메모리 매핑(nmap)을 사용할 수도 있을 것이다. 이것으로 파일의 내용을 프로그램의 주소 공간에 저장할 수 있다.

OS는 파일의 페이지를 메모리 안팎으로 이동하는 역할을 하므로 DBMS는 이제 이것으로 걱정할 필요가 없다.

nmap으로 프로세스의 주소 공간에 있는 파일의 내용을 매핑한다. 그러면 OS가 디스크와 메모리 사이에서 페이지를 앞 뒤로 이동시킬 책임이 있게된다.
안타깝게도 이는 nmap이 페이지 폴트에 도달하면 프로세스가 차단됨을 의미한다. (페이지 폴트란 자신의 주소 공간에는 존재하지만 시스템의 RAM에는 현재 없는 데이터나 코드에 접근 시도하였을 때 발생하는 현상.)
- 쓰기를 하고 싶으면 nmap을 DBMS에 도입하는 것은 좋지 않다.
- DBMS는 항상 스스로 컨트롤하고 싶을 것이다. 왜냐하면 DBMS가 더 많은 정보를 갖고 있다. 무엇 때문에 데이터를 접근하는지 어떤 쿼리가 수행되는지 같은 것들을 알고 있기 때문이다. OS는 무엇 때문에 필요한지 모른다.

즉, 정리하면
memory mapped i/o problems
1. transaction safety : OS can flush dirty pages at any time
   언제 디스크에 쓰기를 해야 하는가. 이상적으로는 commit을 수행한 다음일 것이다.
   하지만 OS는 언제나 쓸 수 있다. 또 만약에 내가 지금 업데이트를 했지만 당장 쓰기를 하지 않는 경우도 있다. (여러 페이지를 수정하는 중이라) OS가 이럴 때 다른 업데이트를 수행하기 전에 내 페이지를 플러시하면 크래시가 날 것이다.
2. I/O Stalls : DBMS doesn't know which pages are in memory. The OS will stall a thread on page fault. (OS가 페이지 오류 시 스레드를 중단시킴)
3. Error Handling : Difficult to validate pages. Any access can cause a SIGBUS that the DBMS must handle.
   너가 직접 문제가 생기면 signal들을 아 핸들링 해야 한다.
4. performance issues : OS data structure contention. TLB shootdowns.
   작은 데이터베이스면 괜찮을 것이다.
   하지만 고성능 시스템을 만들려고 한다면 OS는 자신만의 내부 데이터 구조가 자기자신만을 위한 페이지 테이블이 있기에 이것이 보틀넥이 될 수 있다.

nmap의 이런 문제를 해결하는 방식이 있다. 실제로 많은 데이터베이스가 nmap을 사용한다.
처음에는 쉬운 방법으로 도입하지만 이후에는 점점 후회하는 양상을 보인다는 듯.

nmap를 full usage하는 곳. elasticsearch, leveldb(구글 키밸류라는 듯)
nmap을 부분적으로 사용하는 곳. mongoDB -> (나중에 없앴다는 듯), sqlite

DMBS는 결국 자신이 컨트롤 하고 싶어한다 그리고 OS보다 더 잘 할 수 있다.
- Flushing dirty pages to disk in the correct order
- specialized prefetching
- buffer replacement policy
- thread/process scheduling


* 데이터베이스 저장소
1. How the DBMS represents the database in files on disk
2. How the DBMS manages its memory and moves data back-and-forth from disk.

** File Storage
the DBMS stores a database as one or more files on disk typically in a proprietary format.
- The OS doesn't know anything about the contents of these files.

Early system in the 1980s used custom filesystems on raw storage.
- Some "enterprise" DBMSs still support this.
- Most newer DBMSs do not do this.

일반적으로 DBMS는 파일을 디스크에 저장한다. 파일을 계층화하여(디렉토리를 이용한다는 말인듯) 저장하기도 하고, 하나의 파일에 다 넣기도한다(ex SQLite).

OS는 이 파일들의 컨텐츠를 알 수 없다. DBMS 만이 내용물을 해독할 수 있다. 특정 DBMS가 데이터를 인코딩한 방식이니까!

*** storage manager
the storage manager is responsible for maintaining a database's files.
- some do their own scheduling for reads and writes to improve spatial and temporal locality of pages.

It organizes the files as a collection of pages.
- Tracks data read/written to pages.
- Tracks the available space.

DBMS의 storage manager는 데이터베이스 파일 관리를 맡는다. 파일을 페이지 모음으로 표시한다.
또한 페이지에 어떤 데이터가 읽히고 쓰였는지, 얼마나 많은 여유공간이 있는지 추적한다.

** database pages
A page is a fixed-size block of data
- it can contain tuples, meta-data, indexes, log records...
- most systems do not mix page types.
- some systems require a page to be self-contained.

Each page is given a unique identifier.
- the DBMS uses an indirection layer to map page IDs to physical location.

DBMS는 페이지라는 고정 크기의 데이터 블록으로 데이터베이스를 구성한다. 이 페이지는 하나 이상의 파일을 의미한다.
페이지에는 다양한 종류의 데이터(튜플, 인덱스 등)가 포함될 수 있다. 대부분의 시스템은 페이지 내에서 이러한 유형을 혼합하지 않는다.
일분 시스템에서는 각 페이지를 읽는데 필요한 모든 정보가 페이지 자체에 있어야 한다.
각 페이지를 읽는데 필요한 모든 정보가 페이지 자체에 있어야 한다.

각 페이지는 유니크한 식별자다. 만약 데이터베이스가 파일 하나로 구성된다면, 페이지 아이디는 파일 오프셋일 수도 있다. 대부분 DBMS들은 페이지ID를 파일 경로와 오프셋에 매핑하는 방향지정 레이어(indirection layer)가 있다.

대부분 DBMS는 fixed-sized page를 사용한다. variable-sized pages(사이즈가 변하는 페이지)

*

* reference
- https://www.youtube.com/watch?v=df-l2PxUidI&list=PLSE8ODhjZXjaKScG3l0nuOiDTTqpfnWFf&index=4&ab_channel=CMUDatabaseGroup
- https://15445.courses.cs.cmu.edu/fall2022/notes/03-storage1.pdf
- https://15445.courses.cs.cmu.edu/fall2022/slides/03-storage1.pdf
