#+TITLE: Lacinia
#+date: 
#+author: Younghwan Nam
#+email: ssisksl77@gmail.com
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../css/site.css" />


* Graphql 파싱 하기


* alumbra.parser

https://github.com/alumbra/alumbra.parser/blob/master/src/alumbra/parser.clj

** 사용법

    Query 날리기

    #+BEGIN_SRC clj
    (graphql/parse-document
        "query People($limit: Int = 10, $offset: Int = 0) {
            people(limit: $limit, offset: $offset) {
            name
            friends { name }
            }
        }")
    #+END_SRC

    타입 시스템

    #+BEGIN_SRC clj
    (graphql/parse-schema
        "type Person {
            name: String!
        }

        type QueryRoot {
            people(limit: Int, offset: Int): [Person]
        }

        schema { query: QueryRoot }")
    #+END_SRC

*** parse-document

    #+BEGIN_SRC  clj
    (defn parse-document 
      [document]
      (let [result (document/parse document)]
        (if-not (antlr/error? result)
          (document/transform result)
          (collect-errors))))
    #+END_SRC
**** document/parse

#+BEGIN_SRC clj
(ns alumbra.parse.document
  (:require [alumbra.parser.antlr :as antlr]
            [alumbra.parser.traverse :as t]
            [alumbra.parser.utils :refer :all]))

(antlr/defparser parse
  {:grammar "alumbra/GraphQL.g4"
   :root "document"
   :aliases
   {:valueWithVariable :value
    :arrayValueWithVariable :arrayValue
    :objectValueWithVariable :objectValue
    :objectFieldWIthVariable :objectField
    :objectFieldValueWithVariable :objectFieldValue}})
#+END_SRC
