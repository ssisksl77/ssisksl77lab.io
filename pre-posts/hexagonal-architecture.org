#+TITLE: Hexagonal Architecture (헥사고널 아키텍처 사용기)
#+date: <2022-02-03 Thu>
#+author: Younghwan Nam
#+email: ssisksl77@gmail.com
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../css/site.css" />

* 헥사고널 아키텍처

헥사고널 아키텍처는 처음 접한 것은 2020년 즈음이었다. 
퇴근시간이 되면 종종 유튜브에 있는 기술세미나 한편을 읽다 집에 돌아간 적이 있는데 그때 우연찮게 헥사고널 아키텍처를 알게 되었다.
사실 해당 영상에서는 플러그인 방식 아키텍처라고 설명했지만, 여하튼 비즈니스 로직을 추상화하는 점은 아주 비슷했다.

팀원들과 들뜬나머지 공부하고 논쟁하고 또 공부하고 논쟁하고 하면서도 개발이 느려지지 않게 하기위해 노력했다.
그러니 우리 팀원들은 그때, 어떻게 보면 열정이고 낭만이었지만, 참으로 몸이 성하지 않게 일했다.

헥사고널은 기본적으로 레이어방식의 아키텍처를 비판하면서 시작한다. 
예를들어 Spring MVC는 Controller, Service, Persistence 의 계층이 있다. 
이 아키텍처는 아주 견고하다. 안정감이 있어서 많은 사람들이 좋아하는 구조이다.

MVC 패턴은 아주 좋은 패턴이다. 문제는 항상 다른 곳에 있다.
나의 경험상 프로그래밍을 하면서 생기는 문제는 항상 다른 곳에 있다. (일정, 사람, 돈)

* MVC 패턴의 견고함
MVC 패턴은 기본적으로 ~요청 -> Controller -> Service -> Persistence~ 로 진행되서 다시 역방향으로 되돌아가서 응답하면서 라이프사이클이 끝난다.

** (사례1) 서비스가 사라진다.
MVC패턴의 문제는 사용자에 있다. 
어김없이 개발자는 변경된 요구사항을 보고 고민에 빠진다. 
계속 되는 변경사항에 (그것이 숙명이지만) 지칠대로 지친 그는 머릿속에서 '서비스까진 만들 필요는 없겠는데?'
라는 소리가 들리기 시작한다.

안그래도 프로젝트에 객체가 너무 많다고 생각이 들기 시작한 그는(그것이 객체지향이지만 말이다)
컨트롤러계층에서 퍼시스턴스계층의 DB접근을 직접하는 코드를 추가했다. 
그는 잘 동작하는 것을 보고 흡족했다. '그래~ 간단하잖아~'

그는 테스트를 작성하기 시작한다. 
컨트롤러계층에 테스트를 추가했다. 
원래 도메인에 관련된 테스트가 없던 곳에 추가되어서 그런지 테스트가 더 잘 보이는 것 같기도 하고 좋다.

** (사례2) 계층은 불편하다.
사람들이 결국 컨트롤러, 서비스는 그저 퍼시스턴스로 통하는 통로라는 말하는 이유가 여기에 있다.
나는 이런 가르침을 첫번째,두번째 회사의 시니어들에게 자주 들었다.

나쁜 코드인가? 상대적 유지보수를 논한다면 그렇다. 
바꿔야 하는가? 이건 모르겠다. 
왜냐하면 이 회사들은 유지보수를 잘 해내고 있었기 때문이다. 그중 하나는 IT회사로서 10년이 넘었다.

계층은 불편하다.
결국 계층을 끌어내리기 시작하면서 퍼시스턴스레이어에서 대부분의 로직이 수행되기도 한다.

억지스럽지만 예를 하나 만들어보자.

회원가입시 저절로 로그인이 되도록 하고자 한다.


https://hackernoon.com/how-it-feels-to-learn-javascript-in-2016-d3a717dd577f
