#+TITLE: superlifter (Dataloader for clojurian)
#+date: <2022-01-24 Mon>
#+author: Younghwan Nam
#+email: ssisksl77@gmail.com
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../css/site.css" />



* 소개
https://github.com/oliyh/superlifter

superlifter는 다음과 같은 특징을 주장한다.
- 빠르고 간단한 DataLoader 패턴 구현
- 시간 혹은 큐 사이즈에 따른 Bucketing(값을 바가지로 퍼낼 수 있다)
- 비동기로 가져오기(async fetching)
- 배치로 가져오기(batch fetching)
- 동일 세션에서 가져오는 모든 것들을 캐시로 공유할 수 있다.
  - 일관된 결과를 보증
  - 중복된 일을 피한다.
- 캐시접근으로 장기적으로 데이터가 지속됨 (long-term persistence)