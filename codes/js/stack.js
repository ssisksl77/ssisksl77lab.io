const stack = (...items) => ({
  push: item => stack(...items, item),
  pop: () => {
    // create a item list
    const newItems = [...items]

    // remove the last item from the list and
    // assign it to a variable
    const [item] = newItems.splice(-1)

    // return the pair
    return [item, stack(...newItems)]
  },
  toString: () => `stack(${ items.join(',') })`
})
const push = (item, stack) => stack.push(item)
const pop = stack => stack.pop()


// or
// const stack = (...elements) => [...elements]
// const push = (a, stack) => stack.concat([a])
// const pop = stack => {
//   const newStack = stack.slice(0)
//   const item = newStack.pop()
//   return [item, newStack]
// }
