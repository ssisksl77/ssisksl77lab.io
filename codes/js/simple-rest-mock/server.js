const express = require('express');
const app = express();
const port = 9999;

app.use(express.json()); // for parsing application/json

app.get('/papi/v1/settlement/trade-company', (req, res) => {
    res.json([{ id: 1, name: 'Item 1' }, { id: 2, name: 'Item 2' }, { id: 3, name: 'Item 3'}, { id: 225, name: "수정한 데이터?"}]);
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}/`);
});
