(ns sql-generator
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(def constant-values
  {:channel "SHOP"
   :shop "GL"
   :artist "artist"
   :category_id "100"
   :closing_date "'2024-10-06'"
   :goods_code "'goods_code'"
   :goods_name "'goods_name'"
   :goods_option_code "'goods_option_code'"
   :goods_option_name "'goods_option_name'"
   :goods_option_type "'DELIVERY'"
   :sale_id 1
   :sale_price 100
   :sale_price_sdc 1
   :tax_type "'TAXABLE'"
   :tax "'VAT'"
   :supply_price_conversion_rate 1.10
   :sale_stock_id 100
   :inflow_from "''"})

(defn number->order-item-id [n]
  (format "'zz%03d'" n))

(defn create-value-string [n order-sheet-id]
  (format "(%s, '%s', '%s', NOW(), '%s', %s, %s, %s, %s, %s, %s, %s, '%s', %d, %d, %d, %s, %s, %.2f, %d, %s)"
          (number->order-item-id n)
          (:channel constant-values)
          (:shop constant-values)
          (:artist constant-values)
          (:category_id constant-values)
          (:closing_date constant-values)
          (:goods_code constant-values)
          (:goods_name constant-values)
          (:goods_option_code constant-values)
          (:goods_option_name constant-values)
          (:goods_option_type constant-values)
          order-sheet-id
          (:sale_id constant-values)
          (:sale_price constant-values)
          (:sale_price_sdc constant-values)
          (:tax_type constant-values)
          (:tax constant-values)
          (:supply_price_conversion_rate constant-values)
          (:sale_stock_id constant-values)
          (:inflow_from constant-values)))

(defn generate-sql []
  (let [column-names "order_item_id, channel, shop, created_at, artist, category_id, closing_date, goods_code, goods_name, goods_option_code, goods_option_name, goods_option_type, order_sheet_id, sale_id, sale_price, sale_price_sdc, tax_type, tax, supply_price_conversion_rate, sale_stock_id, inflow_from"
        order-sheet-ids (with-open [rdr (io/reader "order_sheet_ids.txt")]
                          (vec (line-seq rdr)))
        values (map-indexed (fn [idx id]
                              (create-value-string (inc idx) id))
                            order-sheet-ids)
        sql-parts [(str "INSERT INTO settlement.order_item\n")
                   (str "       (" column-names ")\n")
                   "       VALUES\n"
                   (str "       " (str/join ",\n       " values) ";")]]

    (with-open [writer (io/writer "insert_statements.sql")]
      (.write writer (str/join "" sql-parts)))))

(generate-sql)