var Router = require('./router');
var http = require('http');
var methods = require('./methods')
var finalhandler = require('./finalhandler');

var hasOwnProperty = Object.prototype.hasOwnProperty
var slice = Array.prototype.slice;

var app = exports = module.exports = {};

app.init = function init() {
  this.lazyrouter()
}

app.handle = function handle(req, res, callback) {
  var router = this._router;

  var done = callback || finalhandler(req, res, {});
  console.log(`app.handle : router=${router}, done=${done}`)
  router.handle(req, res, done);
}

app.listen = function listen() {
  var server = http.createServer(this);
  return server.listen.apply(server, arguments);
}

app.lazyrouter = function lazyrouter() {
  if (!this._router) {
    this._router = new Router();
  }
  return this._router;
}

methods.forEach(function (method) {
  app[method] = function (path) {
    var route = this.lazyrouter();
    route[method].apply(route, slice.call(arguments));
    return this;
  }
})
