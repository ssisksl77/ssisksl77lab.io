module.exports = pathtoRegexp;

/**
 * Match matching groups in a regular expression.
 * 가져와야하는 파라미터명이 있는 곳에 이 정규식으로 대체되어 들어간다.
 * 정규식 설명
 *  - \(: 이 부분은 리터럴 괄호 '('를 찾습니다. 괄호는 정규 표현식에서 그룹을 나타내는 데 사용되므로, 이는 매칭 그룹의 시작을 의미합니다.
 *  - (?:...): 이는 비캡처 그룹을 나타냅니다. 즉, 이 그룹 내에서 매칭된 내용은 결과로 캡처되지 않습니다.
 *  - \?<: 이는 리터럴 문자 '?' 뒤에 '<'가 오는 패턴을 찾습니다. 이 패턴은 이름이 지정된 캡처 그룹을 나타내기 위해 사용됩니다 (예: (?<name>...)).
 *  - (.*?): 이 부분은 캡처 그룹입니다. .*?는 가능한 가장 짧은 문자열을 비탐욕적(non-greedy) 방식으로 매칭하며, 이는 이름이 지정된 그룹에서 괄호 '<'와 '>' 사이의 이름을 추출하는 데 사용됩니다.
 *  - >: 이는 리터럴 '>' 문자를 찾습니다. 이것은 이름이 지정된 그룹의 끝을 나타냅니다.
 *  - (?!\?): 이 부분은 부정형 전방 탐색(negative lookahead)입니다. 즉, 바로 뒤에 '?' 문자가 오지 않는 위치를 찾습니다. 이는 (?...) 형태의 다른 정규 표현식 구성 요소 (예: 전방 탐색, 후방 탐색, 조건부 표현 등)를 제외하기 위해 사용됩니다.
 * 
 * 
 */
var MATCHING_GROUP_REGEXP = /\((?:\?<(.*?)>)?(?!\?)/g;

/**
 * 주어진 경로 문자열을 정규화합니다,
 * 정규식을 반환합니다.
 *
 * 빈 배열을 전달해야 합니다,
 * 자리 표시자를 포함하는
 * 키 이름이 포함된 빈 배열을 전달해야 합니다. 예를 들어 "/user/:id"는
 * ["id"]를 포함합니다.
 *
 * @param {String|RegExp|Array} 경로
 * @param {Array} 키
 * @param {Object} 옵션
 * @return {RegExp}
 * @api 비공개
 */
function pathtoRegexp(path, keys, options) {
  options = options || {};
  keys = keys || [];

  // strict, end, flags: 경로 매칭의 엄격함, 끝 처리, 대소문자 구분 여부를 설정합니다.
  var strict = options.strict;
  var end = options.end !== false;
  var flags = options.sensitive ? '' : 'i';
  var extraOffset = 0;
  var keysOffset = keys.length;
  var i = 0;
  var name = 0;
  var m;

  if (path instanceof RegExp) {
    while (m = MATCHING_GROUP_REGEXP.exec(path.source)) {
      keys.push({
        name: name++,
        optional: false,
        offset: m.index
      });
    }

    return path;
  }

  if (Array.isArray(path)) {
    // 배열 부분을 정규식으로 매핑하고 그 소스를 반환합니다. 또한 모든 세대에
    // 동일한 키와 옵션 인스턴스를 모든 세대에 전달하여
    // 소스를 결합하기 전에 일관된 매칭 그룹을 얻습니다.
    path = path.map(function (value) {
      return pathtoRegexp(value, keys, options).source;
    });

    return new RegExp('(?:' + path.join('|') + ')', flags);
  }
  path = ('^' + path + (strict ? '' : path[path.length - 1] === '/' ? '?' : '/?'))
    .replace(/\/\(/g, '/(?:')
    .replace(/([\/\.])/g, '\\$1')
    .replace(/(\\\/)?(\\\.)?:(\w+)(\(.*?\))?(\*)?(\?)?/g, function (match, slash, format, key, capture, star, optional, offset) {
      slash = slash || '';
      format = format || '';
      capture = capture || '([^\\/' + format + ']+?)';
      optional = optional || '';

      keys.push({
        name: key,
        optional: !!optional,
        offset: offset + extraOffset
      });

      var result = ''
        + (optional ? '' : slash)
        + '(?:'
        + format + (optional ? slash : '') + capture
        + (star ? '((?:[\\/' + format + '].+?)?)' : '')
        + ')'
        + optional;

      extraOffset += result.length - match.length;

      return result;
    })
    .replace(/\*/g, function (star, index) {
      var len = keys.length

      while (len-- > keysOffset && keys[len].offset > index) {
        keys[len].offset += 3; // Replacement length minus asterisk length.
      }

      return '(.*)';
    });

  // This is a workaround for handling unnamed matching groups.
  while (m = MATCHING_GROUP_REGEXP.exec(path)) {
    var escapeCount = 0;
    var index = m.index;

    while (path.charAt(--index) === '\\') {
      escapeCount++;
    }

    // It's possible to escape the bracket.
    if (escapeCount % 2 === 1) {
      continue;
    }

    if (keysOffset + i === keys.length || keys[keysOffset + i].offset > m.index) {
      keys.splice(keysOffset + i, 0, {
        name: name++, // Unnamed matching groups must be consistently linear.
        optional: false,
        offset: m.index
      });
    }

    i++;
  }

  // If the path is non-ending, match until the end or a slash.
  path += (end ? '$' : (path[path.length - 1] === '/' ? '' : '(?=\\/|$)'));

  return new RegExp(path, flags);
}

// TEST
// console.log(pathtoRegexp('/user/:id', []));  // /^\/user\/([^\/]+?)(?=\/|$)/i