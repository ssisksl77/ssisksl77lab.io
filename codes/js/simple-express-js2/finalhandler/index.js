module.exports = finalhandler;

function createHtmlDocument(message) {
  return `<!DOCTYPE html><html><head><title>Error</title></head>
  <body><h1>Error</h1><p>${message}</p></body></html>`;
}

function finalhandler(_, res, _) {
  // simple implementation log error and send response

  // get the error from the options
  return function (err) {
    res.statusCode = 404;
    res.setHeader('Content-Type', 'text/plain; charset=utf-8');

    res.end(createHtmlDocument(err || 'Not Found'));
  }
}