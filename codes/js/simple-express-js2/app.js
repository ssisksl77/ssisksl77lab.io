var express = require('./express');

var app = express()

// TODO : Implement use function
// app.use(function (req, res, next) {
//   console.log('middleware 1')
//   next()
// })

app.get('/', function (req, res) {
  res.end('Hello World')
})

app.get('/users/:name', function (req, res) {
  console.log(req.params)
  res.end("HELLO " + req.params.name)
})

app.listen(3001, function () {
  console.log('Server is running on port 3001')
})

// querystring node module is used to parse query strings
// const querystring = require('querystring');
// console.log(querystring.parse('foo=bar&abc=xyz&abc=123'))