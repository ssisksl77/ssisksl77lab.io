var Route = require('./route')
var Layer = require('./layer')
var methods = require('../methods')
var flatten = require('../array-flatten')
var objectRegExp = /^\[object (\S+)\]$/;
var slice = Array.prototype.slice;
var toString = Object.prototype.toString;
var parseUrl = require('../parseurl');

var proto = module.exports = function router() {

  function router(req, res, next) {
    router.handle(req, res, next);
  }

  // mixin Router class functions
  router.__proto__ = proto;

  router.params = {};
  router._params = [];
  router.stack = [];

  return router;
}

/**
 * Map the given param placeholder `name`(s) to the given callback.
 *
 * Parameter mapping is used to provide pre-conditions to routes
 * which use normalized placeholders. For example a _:user_id_ parameter
 * could automatically load a user's information from the database without
 * any additional code,
 *
 * The callback uses the same signature as middleware, the only difference
 * being that the value of the placeholder is passed, in this case the _id_
 * of the user. Once the `next()` function is invoked, just like middleware
 * it will continue on to execute the route, or subsequent parameter functions.
 *
 * Just like in middleware, you must either respond to the request or call next
 * to avoid stalling the request.
 *
 *  app.param('user_id', function(req, res, next, id){
 *    User.find(id, function(err, user){
 *      if (err) {
 *        return next(err);
 *      } else if (!user) {
 *        return next(new Error('failed to load user'));
 *      }
 *      req.user = user;
 *      next();
 *    });
 *  });
 *
 * @param {String} name
 * @param {Function} fn
 * @return {app} for chaining
 * @public
 */
proto.param = function param(name, fn) {
  // param logic
  if (typeof name === 'function') {
    console.log('router.param(fn): Refactor to use path params');
    this._params.push(name);
    return;
  }

  // apply param functions
  var params = this._params;
  var len = params.length;
  var ret;

  if (name[0] === ':') {
    deprecate('router.param(' + JSON.stringify(name) + ', fn): Use router.param(' + JSON.stringify(name.slice(1)) + ', fn) instead')
    name = name.slice(1)
  }

  for (var i = 0; i < len; ++i) {
    if (ret = params[i](name, fn)) {
      fn = ret;
    }
  }

  // ensure we end up with a
  // middleware function
  if ('function' !== typeof fn) {
    throw new Error('invalid param() call for ' + name + ', got ' + fn);
  }

  (this.params[name] = this.params[name] || []).push(fn);
  return this;
};

proto.handle = function handle(req, res, out) {
  var self = this;

  var idx = 0;
  var protohost = getProtohost(req.url) || '';
  var stack = self.stack;
  var removed = '';
  var paramcalled = {};
  var parentUrl = req.baseUrl || '';
  var done = restore(out, req, 'baseUrl', 'next', 'params');

  next();

  function next(err) {
    var layerError = (err === 'route' ? null : err);

    // restore altered req.url
    if (removed.length !== 0) {
      req.baseUrl = parentUrl;
      req.url = protohost + removed + req.url.slice(protohost.length)
      removed = '';
    }

    // // signal to exit router
    // if (layerError === 'router') {
    //   setImmediate(done, null)
    //   return
    // }

    // no more matching layers
    if (idx >= stack.length || layerError) {
      console.log('no more matching layers')
      return done(layerError);
    }

    var path = getPathname(req);
    if (path == null) {
      return done(layerError);
    }
    // console.log('path', path)
    // find next matching layer
    var layer;
    var match;
    var route;

    while (match !== true && idx < stack.length) {
      layer = stack[idx++];
      match = matchLayer(layer, path);
      // console.log('====== match', match, layer.path, path, '======')
      route = layer.route;

      if (typeof match !== 'boolean') {
        // hold on to layerError
        layerError = layerError || match;
      }

      if (match !== true) {
        continue;
      }

      if (!route) {
        // process non-route handlers normally
        continue;
      }

      if (layerError) {
        match = false;
        continue;
      }
    }

    // no match
    if (match !== true) {
      return done(layerError);
    }

    // store route for dispatch on change
    if (route) {
      req.route = route;
    }

    // Capture one-time layer values
    // 이전 레이어에서 사용한 파라미터를 저장
    req.params = layer.params;

    var layerPath = layer.path;
    // this should be done for the layer
    self.process_params(layer, paramcalled, req, res, function (err) {
      if (err) {
        return next(layerError || err);
      }

      if (route) {
        return layer.handle_request(req, res, next);
      }

      trim_prefix(layer, layerError, layerPath, path);
    });
  }

  function trim_prefix(layer, layerError, layerPath, path) {
    // console.log(`===== trim_prefix: ${layer}, ${layerError}, ${layerPath}, ${path} =====`)
    if (layerPath.length !== 0) {
      // Validate path breaks on a path separator
      if (layerPath !== path.slice(0, layerPath.length)) {
        next(layerError)
        return
      }

      // Validate path breaks on a path separator
      var c = path[layerPath.length]
      if (c && c !== '/' && c !== '.') return next(layerError)
      // layerPath가 path의 접두사인 경우, layerPath를 제거하고 req.url을 업데이트
      removed = layerPath;
      req.url = protohost + req.url.slice(protohost.length + removed.length)
      // console.log(`===== protohost: ${protohost}, req.url: ${req.url} =====`)
      // Ensure leading slash
      if (!protohost && req.url[0] !== '/') {
        req.url = '/' + req.url;
        slashAdded = true;
      }

      // Setup base URL (no trailing slash)
      req.baseUrl = parentUrl + (removed[removed.length - 1] === '/'
        ? removed.substring(0, removed.length - 1)
        : removed);
    }
    if (layerError) {
      layer.handle_error(layerError, req, res, next);
    } else {
      layer.handle_request(req, res, next);
    }
  }
}

proto.process_params = function process_params(layer, called, req, res, done) {
  var params = this.params;

  // captured parameters from the layer
  var keys = layer.keys;

  // fast track
  if (!keys || keys.length === 0) {
    return done();
  }

  var i = 0;
  var name;
  var paramIndex = 0;
  var key;
  var paramVal;
  var paramCallbacks;
  var paramCalled;

  // process params in order
  // param callbacks can be async
  function param(err) {
    if (err) {
      return done(err);
    }

    if (i >= keys.length) {
      return done();
    }

    paramIndex = 0;
    key = keys[i++];
    name = key.name;
    paramVal = req.params[name];
    paramCallbacks = params[name];
    paramCalled = called[name];

    if (paramVal === undefined || !paramCallbacks) {
      return param();
    }

    // param previously called with same value or error occurred
    if (paramCalled && (paramCalled.match === paramVal
      || (paramCalled.error && paramCalled.error !== 'route'))) {
      // restore value
      req.params[name] = paramCalled.value;

      // next param
      return param(paramCalled.error);
    }

    called[name] = paramCalled = {
      error: null,
      match: paramVal,
      value: paramVal
    };

    paramCallback();
  }

  // single param callbacks
  function paramCallback(err) {
    var fn = paramCallbacks[paramIndex++];

    // store updated value
    paramCalled.value = req.params[key.name];

    if (err) {
      // store error
      paramCalled.error = err;
      param(err);
      return;
    }

    if (!fn) return param();

    try {
      fn(req, res, paramCallback, paramVal, key.name);
    } catch (e) {
      paramCallback(e);
    }
  }

  param();
}


proto.use = function use(fn) {
  var offset = 0;
  var path = '/';

  // default path to '/'
  // disambiguate router.use([fn])
  if (typeof fn !== 'function') {
    var arg = fn;

    while (Array.isArray(arg) && arg.length !== 0) {
      arg = arg[0];
    }

    // first arg is the path
    if (typeof arg !== 'function') {
      offset = 1;
      path = fn;
    }
  }

  var callbacks = flatten(slice.call(arguments, offset));

  if (callbacks.length === 0) {
    throw new TypeError('Router.use() requires a middleware function')
  }

  for (var i = 0; i < callbacks.length; i++) {
    var fn = callbacks[i];

    // non-use functions
    if (typeof fn !== 'function') {
      throw new TypeError('Router.use() requires a middleware function but got a ' + gettype(fn))
    }

    // add the middleware
    var layer = Layer(path, {
      sensitive: this.caseSensitive,
      strict: this.strict,
      end: false
    }, fn);

    layer.route = undefined;

    this.stack.push(layer);
  }

  return this;
}

/**
 * Create a new Route for the given path.
 *
 * Each route contains a separate middleware stack and VERB handlers.
 *
 * See the Route api documentation for details on adding handlers
 * and middleware to routes.
 *
 * @param {String} path
 * @return {Route}
 * @public
 */
proto.route = function route(path) {
  var route = new Route(path);

  var layer = Layer(path, {}, route.dispatch.bind(route));

  layer.route = route;

  this.stack.push(layer);
  return route;
}

methods.concat('all').forEach(function (method) {
  proto[method] = function (path) {
    var route = this.route(path);
    route[method].apply(route, slice.call(arguments, 1));
    return this;
  }
})

function getPathname(req) {
  try {
    // https://github.com/pillarjs/parseurl/blob/master/index.js
    return parseUrl(req).pathname;
  } catch (err) {
    return undefined;
  }
}

function matchLayer(layer, path) {
  try {
    return layer.match(path);
  } catch (err) {
    return err
  }
}


// get protocal and host
function getProtohost(url) {
  if (typeof url !== 'string' || url.length === 0 || url[0] === '/') {
    return undefined
  }

  var searchIndex = url.indexOf('?')
  var pathLength = searchIndex !== -1
    ? searchIndex
    : url.length
  var fqdnIndex = url.slice(0, pathLength).indexOf('://')

  return fqdnIndex !== -1
    ? url.substring(0, url.indexOf('/', 3 + fqdnIndex))
    : undefined
}

// get type for error message
function gettype(obj) {
  var type = typeof obj;

  if (type !== 'object') {
    return type;
  }

  // inspect [[Class]] for objects
  return toString.call(obj)
    .replace(objectRegExp, '$1');
}

// restore obj props after function
// out 함수를 실행할 때, req.baseUrl, req.next, req.params 를 복원하기 위한 함수
// done을 이후에 호출하면 out({req.baseUrl, req.next, req.params}) 가 실행된다.
function restore(fn, obj) {
  var props = new Array(arguments.length - 2);
  var vals = new Array(arguments.length - 2);

  for (var i = 0; i < props.length; i++) {
    props[i] = arguments[i + 2];
    vals[i] = obj[props[i]];
  }

  return function () {
    // restore vals
    for (var i = 0; i < props.length; i++) {
      obj[props[i]] = vals[i];
    }

    return fn.apply(this, arguments);
  }
}

// TEST
// var router = proto();
// router.use('/', function (req, res, next) {
//   console.log('middleware 1');
//   next();
// })

// router.get('/', function (req, res) {
//   console.log('get 1');
//   res.status(200).send('get 1');
// })

// router.get('/test', function (req, res, next) {
//   console.log('get 1');
//   next();
// })

// router.use('/test', function (req, res, next) {
//   console.log('middleware 2');
//   next();
// })

// router.use('/test', function (req, res, next) {
//   console.log('middleware 3');
//   next();
// })

// router.get('/test/:name', function (req, res) {
//   console.log(`get 2 ${req.params.name}`)
//   res.status(200).send('get 2');
// })

// // middleware 1 TEST
// router.handle({
//   url: '/',
//   method: 'GET'
// }, {}, function () {
//   console.log('handler 1 done');
// })

// console.log('--- router --- params : ', router.params)


// // middleware 2, 3 TEST
// router.handle({ url: '/test', method: 'GET' }, {}, function () {
//   console.log('handler 2, 3 done');
// })



// // param TEST
// router.handle({ url: '/test/foo', method: 'GET' }, {}, function () {
//   console.log('handler 2 done');
// })


// console.log('--- router --- params : ', router.params)


////

var proto2 = proto();

// 파라미터에 대한 처리 함수 등록
proto2.param(function (param, option) {
  return function (req, res, next, val) {
    console.log('name=', param, ' value', val)
    next()
  }
});

proto2.param('name', function (req, res, next, name) {
  console.log('Processing name parameter:', name);
  // 여기서 데이터베이스 조회 등의 작업을 수행할 수 있습니다.
  req.name = name.toUpperCase();
  next();
});

// 기존 라우트 설정...

proto2.get('/test/:name', function (req, res) {
  console.log(`get 2 ${req.name}`); // 대문자로 변환된 이름 출력
  res.status(200).send(`Hello, ${req.name}!`);
});

// 파라미터 테스트
proto2.handle({ url: '/test/john', method: 'GET' }, {}, function () {
  console.log('handler with params done');
});

console.log('--- proto2 --- params : ', proto2.params);
console.log('--- proto2 --- _params : ', proto2._params);

// 출력: --- router --- params : { name: [Function] }


// 기존 코드 아래에 추가