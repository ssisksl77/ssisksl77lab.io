var flatten = require('../array-flatten')
var Layer = require('./layer')
var methods = require('../methods')

var slice = Array.prototype.slice;
var toString = Object.prototype.toString;

module.exports = Route;

function Route(path) {
  this.path = path;
  this.stack = [];
  this.methods = {};
}

// This function is called when the route is registered with app
// It will create a new Layer with the given path, and add it to the stack
Route.prototype._handles_method = function _handles_method(method) {
  if (this.methods._all) {
    return true;
  }

  var method = method.toLowerCase();

  return Boolean(this.methods[method]);
}

Route.prototype.dispatch = function dispatch(req, res, done) {
  var idx = 0;
  var stack = this.stack;
  if (stack.length === 0) {
    return done();
  }

  req.route = this;

  next();

  function next(err) {
    if (err && err === 'route') {
      return done();
    }

    if (err && err === 'router') {
      return done(err);
    }

    if (idx >= stack.length) {
      return done(err);
    }

    var layer = stack[idx++];
    if (layer.method && layer.method !== req.method.toLowerCase()) {
      return next(err);
    }

    if (err) {
      layer.handle_error(err, req, res, next);
    } else {
      layer.handle_request(req, res, next);
    }
  }
}

Route.prototype.all = function all() {
  var handles = flatten(slice.call(arguments));

  for (var i = 0; i < handles.length; i++) {
    var handle = handles[i];

    var layer = Layer('/', {}, handle);
    layer.method = undefined;

    this.methods._all = true;
    this.stack.push(layer);
  }

  return this;
}

methods.forEach(function (method) {
  Route.prototype[method] = function () {
    var handles = flatten(slice.call(arguments));

    for (var i = 0; i < handles.length; i++) {
      var handle = handles[i];

      var layer = Layer('/', {}, handle);
      layer.method = method;

      this.methods[method] = true;
      this.stack.push(layer);
    }

    return this;
  }
});

// TEST Route class

// add get and post methods to the route each method can have multiple handlers for example : 
// var route = new Route('/foo/:bar');
// route.get(function (req, res, next) {
//   console.log('get');
//   next();
// }, function (req, res, next) {
//   console.log('get2');
//   next();
// })
// route.post(function (req, res, next) {
//   console.log('post');
//   next();
// })

// console.log(route._handles_method('get'));  // true
// console.log(route.dispatch({ method: 'get' }, {}, function () { console.log('done') })); // two middleware functions will be called

// console.log(route._handles_method('post'));
// console.log(route.dispatch({ method: 'post' }, {}, function () { console.log('done') })); // one middleware function will be called

// console.log(route._handles_method('delete')); // false

// add all method to the route

// var route2 = new Route('/foo/:bar');
// route2.all(function (req, res, next) {
//   console.log('all' + req.method);
//   next();
// })

// console.log(route2._handles_method('get'));  // true
// console.log(route2._handles_method('post')); // true
// console.log(route2._handles_method('delete')); // true
// console.log(route2.dispatch({ method: 'get' }, {}, function () { console.log('done') })); // one middleware function will be called
// console.log(route2.dispatch({ method: 'post' }, {}, function () { console.log('done') })); // one middleware function will be called
// console.log(route2.dispatch({ method: 'delete' }, {}, function () { console.log('done') })); // one middleware function will be called
