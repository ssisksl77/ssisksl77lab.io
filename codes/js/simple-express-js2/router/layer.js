var pathRegexp = require('../path-to-regexp')

var hasOwnProperty = Object.prototype.hasOwnProperty;

module.exports = Layer

function Layer(path, options, fn) {

  if (!(this instanceof Layer)) {
    return new Layer(path, options, fn);
  }

  opts = options || {};

  this.handle = fn;
  this.name = fn.name || '<anonymous>';
  this.params = undefined;
  this.path = undefined;
  this.regexp = pathRegexp(path, this.keys = [], options);
  this.regexp.fast_star = path === '*'
  this.regexp.fast_slash = path === '/' && opts.end === false
}

Layer.prototype.match = function match(path) {
  var match

  if (path != null) {
    if (this.regexp.fast_slash) {
      this.params = {};
      this.path = '';
      return true;
    }

    if (this.regexp.fast_star) {
      this.params = { '0': decode_param(path) };
      this.path = '';
      return true;
    }

    match = this.regexp.exec(path);
  }

  if (!match) {
    this.params = undefined;
    this.path = undefined;
    return false;
  }

  this.params = {};
  this.path = match[0];

  var keys = this.keys;
  var params = this.params;

  for (var i = 1; i < match.length; i++) {
    var key = keys[i - 1];
    var prop = key.name;
    var val = decode_param(match[i]);

    if (val !== undefined || !(hasOwnProperty.call(params, prop))) {
      params[prop] = val;
    }
  }

  return true;
}

function decode_param(val) {
  if (typeof val !== 'string' || val.length === 0) {
    return val;
  }

  return decodeURIComponent(val);
}

// handle error for the layer
// this function is added to the layer by the Router class
// this funcction will be called when the layer handles a request, but an error occurs
// on index.js, this function is called by the Route.dispatch function
Layer.prototype.handle_error = function handle_error(error, req, res, next) {
  var fn = this.handle;

  // not a standard error handler
  if (fn.length !== 4) {
    return next(error);
  }

  try {
    fn(error, req, res, next);
  } catch (err) {
    next(err);
  }
}

// handle request for the layer
// this function is added to the layer by the Router class
// this function will be called when the layer matches the request path
// on index.js, this function is called by the Route.dispatch function
Layer.prototype.handle_request = function handle_request(req, res, next) {
  var fn = this.handle;

  if (fn.length > 3) {
    return next();
  }

  try {
    fn(req, res, next);
  } catch (err) {
    next(err);
  }
}


// TEST
// var layer = new Layer('/foo/:bar', {}, function () { 'hello' });
// console.log(layer.match('/foo/test'));
// console.log(layer.handle())
// console.log(layer.params);
// console.log(layer.path);
// console.log(layer.match('/foo/test/123'));

// var layer = new Layer('/foo', {}, function () { 'hello' });
// console.log(layer.match('/foo/bar'));