var proto = require('./application');

exports = module.exports = createApplication;

function createApplication() {
  var app = function (req, res, next) {
    app.handle(req, res, next);
  };

  Object.assign(app, proto);
  app.init();
  return app;
}


// TEST
// var app = createApplication();
// console.log(app)
// app.listen(3001, function () {
//   console.log('Server is running on port 3001')
// })
