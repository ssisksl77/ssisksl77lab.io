const EventEmitter = require('node:events');

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();

function myevent(a, b) {
  console.log('an event occurred!', a, b);
}

myEmitter.on('my-event', myevent);
myEmitter.on('my-event', myevent);

myEmitter.emit('my-event', 'hello', 'world');

myEmitter.removeListener('my-event', myevent);
myEmitter.removeListener('my-event', myevent);

myEmitter.emit('my-event', 'hello', 'world again!');
myEmitter.emit('my-event', 'hello', 'world again!');
myEmitter.emit('my-event', 'hello', 'world again!');

// 출력결과:
// an event occurred! hello world
// an event occurred! hello world


// function myevent2() {
//   console.log(this.target)
// }

// var wrapped = myevent2.bind({target: 'my-event2'})

// console.log(wrapped)
// console.log(wrapped())