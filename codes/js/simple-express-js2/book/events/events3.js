const EventEmitter = require('node:events');

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();
myEmitter.on('heavyTask', () => {
  setImmediate(() => {
    let sum = 0;
    for (let i = 0; i < 1e9; i++) {
      sum += i;
    }
    console.log(`Task completed async with sum: ${sum}`);
  });
});

console.log('Before emit');
myEmitter.emit('heavyTask');
console.log('After emit');