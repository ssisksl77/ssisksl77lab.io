const EventEmitter = require('node:events');

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();

myEmitter.once('my-event', (a, b) => {
  console.log('an event occurred once!', a, b);
});

myEmitter.emit('my-event', 'hello', 'world');  // Prints: an event occurred! hello world
myEmitter.emit('my-event', 'hello', 'world again!');  
myEmitter.emit('my-event', 'hello', 'world again!');  