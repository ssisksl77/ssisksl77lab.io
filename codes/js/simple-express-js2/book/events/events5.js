
function EventEmitter() {
    this._events = {}
  }
  
function _addListener(target, type, listener, prepend) {
    let events = target._events
    let existing = events[type]
    
    if (existing === undefined) {
        // 이벤트 타입이 처음 등록되는 경우, 리스너 배열을 초기화합니다.
        existing = events[type] = [listener]
    } else {
        // 이벤트 타입이 이미 등록된 경우, 배열에 새 리스너를 추가합니다.
        if (prepend) {
            // prepend가 true인 경우, 리스너를 배열의 맨 앞에 추가합니다.
            existing.unshift(listener)
        } else {
            // prepend가 false인 경우, 리스너를 배열의 맨 뒤에 추가합니다.
            existing.push(listener)
        }
    }
    return target
}

EventEmitter.prototype.addListener = function addListener(type, listener) {
    return _addListener(this, type, listener, false)
}

EventEmitter.prototype.on = EventEmitter.prototype.addListener

EventEmitter.prototype.prependListener = 
    function prependListener(type, listener) {
    return _addListener(this, type, listener, true)
}

/**
 * 이벤트 리스너가 한 번만 실행되도록 감싸는 함수입니다.
 */
function onceWrapper() {
    if (!this.fired) {
        this.target.removeListener(this.type, this.wrapFn)
        this.fired = true

        if (arguments.length === 0) {
            return this.listener.call(this.target)
        }

        return this.listener.apply(this.target, arguments)
    } 
}


/**
 * 리스너를 onceWrapper로 래핑합니다.
 */
function _onceWrap(target, type, listener) {
    const state = { fired: false, wrapFn: undefined, target, type, listener }
    // state를 this로 바인딩한 onceWrapper 함수를 생성합니다.
    const wrapped = onceWrapper.bind(state)
    wrapped.listener = listener
    state.wrapFn = wrapped
    return wrapped
}

EventEmitter.prototype.once = function once(type, listener) {
    // onceWrap으로 래핑된 객체를 리스너로 추가합니다.
    this.on(type, _onceWrap(this, type, listener))
    return this
}

EventEmitter.prototype.emit = function emit(type, ...args) {
    let events = this._events
    const handler = events[type]

    if (handler === undefined) {
        return false
    }

    // 복사본을 만들어 원본 배열의 수정을 방지합니다.
    const listeners = Array.prototype.slice.call(handler)
    const len = listeners.length
    for (let i = 0; i < len; i++) {
        listeners[i].apply(this, args)
    }

    return true
}

EventEmitter.prototype.removeListener = function removeListener(type, listener) {
    const events = this._events
    if (events === undefined) {
        return this
    }

    const list = events[type]
    if (list === undefined) {
        return this
    }

    for (let i = 0; i < list.length; i++) {
        if (list[i] === listener) {
            // 배열의 기존 요소를 삭제, 교체 또는 새 요소를 추가하여 배열의 내용을 변경합니다.
            list.splice(i, 1)
            break
        }
    }

    return this
}


// Usage
const myEmitter = new EventEmitter();

function myevent(a, b) {
    console.log('an event occurred!', a, b);
}

myEmitter.on('my-event', myevent);
myEmitter.on('my-event', myevent);

myEmitter.emit('my-event', 'hello', 'world');
// 출력결과:
// an event occurred! hello world
// an event occurred! hello world

function myOnceEvent(a, b) {
    console.log('an event occurred once!', a, b);
}

myEmitter.once('my-event-once', myOnceEvent);

myEmitter.emit('my-event-once', 'hello', 'world again!');  
myEmitter.emit('my-event-once', 'hello', 'world again!');

// 출력: an event occurred! hello world

// removeListener Test
myEmitter.removeListener('my-event', myevent);
myEmitter.removeListener('my-event', myevent);

myEmitter.emit('my-event', 'hello', 'world');
myEmitter.emit('my-event', 'hello', 'world');
myEmitter.emit('my-event', 'hello', 'world');
myEmitter.emit('my-event', 'hello', 'world');