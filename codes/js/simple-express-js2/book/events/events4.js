
const EventEmitter = require('events');
const emitter = new EventEmitter();

emitter.on('heavyTask', () => {
  let sum = 0;
  for (let i = 0; i < 1e9; i++) {
    sum += i;
  }
  console.log(`Task completed with sum: ${sum}`);
});

console.log('Before emit');
emitter.emit('heavyTask');
console.log('After emit');
