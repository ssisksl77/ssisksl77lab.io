const express = require('express');
const app = express();

// 클로저를 사용하여 미들웨어 생성
function checkRole(role) {
  // 'role' 파라미터는 클로저에 의해 기억됨
  return function (req, res, next) {
    const userRole = req.headers['user-role']; // 실제 환경에서는 인증 시스템을 통해 설정
    if (userRole && userRole === role) {
      next(); // 역할이 일치하면 다음 미들웨어로 진행
    } else {
      res.status(403).send('Access Denied'); // 역할이 다르면 접근 거부
    }
  };
}

// 특정 역할을 요구하는 라우트
app.get('/admin', checkRole('admin'), (req, res) => {
  res.send('Admin Dashboard');
});

app.listen(3000, () => {
  console.log('Server running on port 3000');
});