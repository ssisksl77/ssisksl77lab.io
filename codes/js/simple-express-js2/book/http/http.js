const http = require('http');

// HTTP 서버 생성
const server = http.createServer((req, res) => {
  // 요청에 대한 URL과 HTTP 메서드 로깅
  console.log(`URL: ${req.url}, Method: ${req.method}`);

  // HTTP 헤더 설정
  res.writeHead(200, { 'Content-Type': 'text/html' });

  // 응답 본문 전송
  res.end('<h1>Hello, World!</h1>');
});

// 3000번 포트에서 서버 시작
server.listen(3000, function () {
  console.log('Server is listening on port 3000');
});

server.on('listening', function () {
  console.log('This is listening event handler');
})
