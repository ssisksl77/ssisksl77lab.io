var url = require('url')
var parse = url.parse
var Url = url.Url

module.exports = parseurl
module.exports.original = originalurl

function parseurl(req) {
  var url = req.url

  if (url === undefined) {
    // URL is undefined
    return undefined
  }

  var parsed = req._parsedUrl

  if (fresh(url, parsed)) {
    // Return cached URL parse
    return parsed
  }

  // Parse the URL
  parsed = fastparse(url)
  parsed._raw = url

  return req._parsedUrl = parsed
}

/**
 * Parse the `req` original url with fallback and memoization.
 *
 * @param {ServerRequest} req
 * @return {Object}
 * @public
 */
function originalurl(req) {
  var url = req.originalUrl

  if (typeof url !== 'string') {
    // Fallback
    return parseurl(req)
  }

  var parsed = req._parsedOriginalUrl

  if (fresh(url, parsed)) {
    // Return cached URL parse
    return parsed
  }

  // Parse the URL
  parsed = fastparse(url)
  parsed._raw = url

  // _parsedOriginalUrl은 커스텀 캐시 프로퍼티임.
  // fresh() 함수가 이 프로퍼티를 사용하여 URL 파싱 결과를 캐싱하고, 이를 재사용.
  return (req._parsedOriginalUrl = parsed)
}

/**
 * Parse the `str` url with fast-path short-cut.
 *
 * @param {string} str
 * @return {Object}
 * @private
 */
function fastparse(str) {
  if (typeof str !== 'string' || str.charCodeAt(0) !== 0x2f /* / */) {
    return parse(str)
  }

  var pathname = str
  var query = null
  var search = null

  // This takes the regexp from https://github.com/joyent/node/pull/7878
  // Which is /^(\/[^?#\s]*)(\?[^#\s]*)?$/
  // And unrolls it into a for loop
  for (var i = 0; i < str.length; i++) {
    switch (str.charCodeAt(i)) {
      case 0x3f: // ?
        if (search === null) {
          // 물음표 이전까지의 문자열을 pathname으로 설정하고, 이후 문자열을 query로 설정합니다.
          pathname = str.substring(0, i)
          // 물음표 이후 문자열
          query = str.substring(i + 1)
          // 물음표 포함한 검색 문자열 전부. 
          search = str.substring(i)
        }
        break
      case 0x09: // \t
      case 0x0a: // \n
      case 0x0c: // \f
      case 0x0d: // \r
      case 0x20: // <space>
      case 0x23: // #
      case 0xfeff:
        // 특수문자 처리 : 복잡한 문자열 처리는 url.parse()로 넘깁니다.
        return parse(str)
    }
  }

  // fastparse는 Url 객체는 만들지만 아주 간단한 정보만 설정하고 리턴한다.
  //  - Url에 null로 설정된 속성들 protocol, slashes, auth, host, port, hostname, hash
  //  - Url에 설정된 속성들 search, query, pathname, path, href, _raw
  var url = Url !== undefined
    ? new Url()
    : {}

  url.path = str
  url.href = str
  url.pathname = pathname

  if (search !== null) {
    url.query = query
    url.search = search
  }

  return url
}

/**
 * Determine if parsed is still fresh for url.
 *
 * @param {string} url - 확인하고자 하는 원본 URL 문자열입니다.
 * @param {object} parsedUrl - 해당 URL의 파싱 결과를 담은 객체입니다.
 * @return {boolean}
 * @private
 */
function fresh(url, parsedUrl) {
  // parsedUrl이 null이 아닌 객체인지 확인합니다. 이는 parsedUrl이 유효한 객체로 존재해야 한다는 기본 조건을 충족시킵니다.
  return typeof parsedUrl === 'object' && parsedUrl !== null &&
    // 이는 선택적으로 parsedUrl의 타입이 특정 클래스의 인스턴스여야 하는 경우를 처리하기 위함입니다.
    (Url === undefined || parsedUrl instanceof Url) &&
    // _raw 는 URL 문자열을 저장하는 속성입니다. 이 속성은 URL 문자열이 변경되었는지 여부를 확인하는 데 사용됩니다.
    parsedUrl._raw === url
}

// TEST
// console.log(parseurl({ url: 'http://localhost:3000' }))
// console.log(parseurl({ url: 'http://localhost:3000/' }))
// console.log(parseurl({ url: 'http://localhost:3000/foo/bar' }))
// console.log(parseurl({ url: 'http://localhost:3000/foo/bar?name=foo' }))
// console.log(parseurl({ url: '/foo/bar?name=foo' }))
