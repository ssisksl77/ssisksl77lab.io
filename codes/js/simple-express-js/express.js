const mixin = require('merge-descriptors')
const proto = require('./application')


function createApplication() {
  var app = function(req, res, next) {
    app.handle(req, res, next)
  }

  mixin(app, proto, false)
  
  // app.init()
  return app;
}


module.exports = createApplication
