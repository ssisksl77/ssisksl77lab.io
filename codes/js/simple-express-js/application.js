var Router = require('./router');
var http = require('http')
var methods = require('./methods')

var app = exports = module.exports = {};

// 이미 다른 곳에서 실행해서 만들어짐.
app.lazyrouter = function lazyrouter() {
  // base router
  if (!this._router) {
    this._router = new Router({})
  }
}

// finalhandler 는 예외를 핸들링하는 마지막 핸들러
function finalhandler(req, res) {
  return function (err) {
    // 에러 발생 시 상태 코드와 메시지 설정
    var status = err ? (err.status || 500) : 404
    var message = err ? (err.message || 'Internal Server Error') : 'Not Found'

    // 이미 헤더가 전송된 경우 추가 동작을 중지
    if (res.headersSent) {
      return req.socket.destroy()
    }

    // 응답 준비
    res.statusCode = status
    res.statusMessage = message

    // 응답 헤더 설정
    res.setHeader('Content-Type', 'text/html; charset=utf-8')

    // 간단한 HTML 응답 생성
    var body = `<html>
  <head><title>${message}</title></head>
  <body><h1>${status} - ${message}</h1></body>
</html>`

    // Content-Length 설정
    res.setHeader('Content-Length', Buffer.byteLength(body))

    // 응답 전송
    res.end(body)
  }
}

// router.handle 로 위임.
app.handle = function (req, res, callback) {
  this.lazyrouter();  // 원래 여기서 안하는데 일단 함.
  var router = this._router

  console.log(`application.js - app.handle - router ${router}`)
  var done = callback || finalhandler(req, res)
  router.handle(req, res, done);
}

app.listen = function listen() {
  var server = http.createServer(this);
  return server.listen.apply(server, arguments)
}

app.use = function use(fn) {
  this.lazyrouter();
  this._router.use(fn);
  return this;
}

app.route = function route(path) {
  this.lazyrouter();
  return this._router.route(path);
}


// ADD METHODS
// route 함수 대신 사용해서 메서드 추가 
methods.forEach(function (method) {
  app[method] = function (path) {
    console.log(`application.js - app[method] - path ${path}, method ${method}, this ${this}`)
    this.lazyrouter();
    // _router.route(path) 를 통해 Router.route 객체를 생성하고
    // 해당 객체에 method 를 통해 라우트 핸들러를 추가한다.
    this._router.route(path)[method].apply(this._router.route(path), Array.prototype.slice.call(arguments, 1));
    return this;
  }
})

