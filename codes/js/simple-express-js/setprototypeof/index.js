'use strict'
// javascript 객체의 프로토타입을 설정하는 폴리필(Polyfill)을 제공한다.
// 폴리필이란 웹 브라우저에서 기본적으로 지원하지 않는 기능을 사용할 수 있도록 하는 코드 또는 플러그인을 말한다.
// 이 코드는 특히 `Object.setPrototypeOf` 함수가 지원되지 않는 오래된 브라우저에서 객체의 프로토타입을 설정할 수 있도록 돕는다.
/* eslint no-proto: 0 */

// - Object.setPrototypeOf 가 있으면 그대로 사용한다.
// - 그렇지않은 경우 `__proto__` 속성을 지원하는지 확인한다. `({ __proto__: [] } instanctof Arrray)` 는 `__proto__` 속성을 이용해
//   배열의 프로토타입을 설정하고, 이 객체가 `Array` 인스턴스이면 `setProtoOf` 를
// - false 면 mixinProperties 를 사용한다.
module.exports = Object.setPrototypeOf || ({ __proto__: [] } instanceof Array ? setProtoOf : mixinProperties)

// 객체의 `__proto__` 객체로 설정한다. 많은 브라우저에서 널리 지원된다. 표준은 아님.
function setProtoOf (obj, proto) {
  obj.__proto__ = proto
  return obj
}

// 이 방법은 객체의 실제 프로토타입 체인을 변경하지 않고, 상속과 유사한 효과를 내려고할 때 사용한다.
function mixinProperties (obj, proto) {
  for (var prop in proto) {
    if (!Object.prototype.hasOwnProperty.call(obj, prop)) {
      obj[prop] = proto[prop]
    }
  }
  return obj
}
