var express = require('./express')

var app = express()

app.use(function middleware_1(req, res, next) {
    console.log('hi this is middleware 1')
    next()
})

app.use(function middleware_2(req, res, next) {
    console.log('hi this is middleware 2 ')
    next()
})

app.get('/', function(req, res) {
    res.end('hi this is handle! hello world')
})
app.listen(3000, function() { console.log('server is running...')})
