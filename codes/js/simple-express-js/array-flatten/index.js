module.exports = flatten

function flatten(array) {
  const result = []
  for (let i = 0; i < array.length; i++) {
    if (Array.isArray(array[i])) {
      result.push(...flatten(array[i]))
    } else {
      result.push(array[i])
    }
  }
  return result
}