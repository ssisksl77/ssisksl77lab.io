const flatten = require('../array-flatten');
const Layer = require('./layer')
var slice = Array.prototype.slice;
var methods = require('../methods')

module.exports = Route;

function Route(path) {
  // 라우트 경로
  this.path = path;
  // 라우트 핸들러를 저장하는 배열
  this.stack = [];

  // debug('new %o', path)

  // route handlers for various http methods
  this.methods = {};
}

Route.prototype._handles_method = function _handles_method(method) {
  if (this.methods._all) {
    return true;
  }

  // 존재하는 메서드인지 확인
  return Boolean(this.methods[method]);
};

Route.prototype.dispatch = function dispatch(req, res, done) {
  var idx = 0;
  var stack = this.stack;

  // 라우트 핸들러가 없으면 바로 종료
  if (stack.length === 0) {
    return done();
  }

  req.route = this;

  // 라우트 핸들러를 순차적으로 실행
  next();

  function next(err) {
    // skip rest of route on error
    if (err && err === 'route') {
      return done();
    }

    // skip non-route handlers
    if (err && err === 'router') {
      return done(err);
    }

    // no more matching layers
    if (idx >= stack.length) {
      return done(err);
    }

    var layer = stack[idx++];
    // 라우트 핸들러가 아니면 다음 핸들러로 이동
    if (layer.method && layer.method !== req.method) {
      return next(err);
    }

    // 에러가 발생하면 에러 핸들러로 이동
    if (err) {
      // layer에게 에러 핸들러로 이동하라고 알려줌
      layer.handle_error(err, req, res, next);
    } else {
      // layer에게 요청 핸들러로 이동하라고 알려줌
      layer.handle_request(req, res, next);
    }
  }
}

// 라우트 핸들러를 추가하는 메서드
Route.prototype.all = function all() {
  var handles = flatten(slice.call(arguments));

  // 모든 메서드에 대해 핸들러를 추가
  for (var i = 0; i < handles.length; i++) {
    var handle = handles[i];

    // 핸들러를 추가
    var layer = Layer('/', {}, handle);
    layer.method = undefined;

    this.methods._all = true;
    this.stack.push(layer);
  }

  return this;
};


// ADD METHODS
// route 함수 대신 사용해서 메서드 추가
methods.forEach(function (method) {
  Route.prototype[method] = function () {
    var handles = flatten(slice.call(arguments));

    for (var i = 0; i < handles.length; i++) {
      var handle = handles[i];

      // 핸들러를 추가
      var layer = Layer('/', {}, handle);
      layer.method = method;

      this.methods[method] = true;
      this.stack.push(layer);
    }

    return this;
  };
});