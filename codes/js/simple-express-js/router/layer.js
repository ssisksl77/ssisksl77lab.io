var pathRegexp = require('path-to-regexp');

var hasOwnProperty = Object.prototype.hasOwnProperty;

module.exports = Layer;

function Layer(path, options, fn) {
  if (!(this instanceof Layer)) {

    return new Layer(path, options, fn);
  }

  var opts = options || {};

  this.handle = fn;
  this.name = fn.name || '<anonymous>';
  this.params = undefined;
  this.path = undefined;
  this.regexp = pathRegexp(path, this.keys = [], opts);

  // set fast path flags
  this.regexp.fast_star = path === '*'
  this.regexp.fast_slash = path === '/' && opts.end === false

  // Layer created with
  console.log(`Layer created with path ${path} regexp ${this.regexp} fast_slash ${this.regexp.fast_slash} fast_star ${this.regexp.fast_star}`)
}

Layer.prototype.handle_error = function handle_error(error, req, res, next) {
  var fn = this.handle;

  // 4개의 인자를 받는 함수가 아니면 next(error)를 호출한다.
  if (fn.length !== 4) {
    return next(error);
  }

  try {
    fn(error, req, res, next);
  } catch (err) {
    next(err);
  }
};

Layer.prototype.handle_request = function handle_request(req, res, next) {
  var fn = this.handle;

  // 4개의 인자를 받는 함수가 아니면 next()를 호출한다.
  if (fn.length > 3) {
    return next();
  }

  try {
    fn(req, res, next);
  } catch (err) {
    next(err);
  }
}

// simple route matching
Layer.prototype.match = function match(path) {
  var match;

  if (path !== null) {
    // fast path non-ending match for / (any path matches)
    if (this.regexp.fast_slash) {
      this.params = {};
      this.path = '';
      return true;
    }

    // fast path for * (everything matched in a param)
    if (this.regexp.fast_star) {
      this.params = { '0': decode_param(path) };
      this.path = path;
      return true;
    }

    // match the path
    match = this.regexp.exec(path);
  }

  if (!match) {
    this.params = undefined;
    this.path = undefined;
    return false;
  }

  // store values
  this.params = {};
  this.path = match[0];

  var keys = this.keys;
  var params = this.params;

  // param에 매칭되는 값을 decode 하여 저장.
  for (var i = 1; i < match.length; i++) {
    var key = keys[i - 1];
    var prop = key.name;
    var val = decode_param(match[i])

    if (val !== undefined || !(hasOwnProperty.call(params, prop))) {
      params[prop] = val;
    }
  }

  return true;

}

/**
 * Decode param value.
 *
 * @param {string} val
 * @return {string}
 * @private
 */
function decode_param(val) {
  if (typeof val !== 'string' || val.length === 0) {
    return val;
  }

  return decodeURIComponent(val);

}