var setPrototypeOf = require('../setprototypeof')
var parseUrl = require('../parseurl')
var Layer = require('./layer')
var flatten = require('../array-flatten')
var Route = require('./route')
const mixin = require('merge-descriptors')


var proto = module.exports = function (options) {
  var opts = options || {}

  function router(req, res, next) {
    router.handle(req, res, next);
  }


  // mixin Router class functions
  // 이걸 넣어야 아래 proto.handle ,,, 을 사용할 수 있다.
  setPrototypeOf(router, proto)

  router.params = {};
  router._params = [];
  router.caseSensitive = opts.caseSensitive;
  router.mergeParams = opts.mergeParams;
  router.strict = opts.strict;
  router.stack = [];

  return router;
}

// IMPORTANT !!!
proto.handle = function handle(req, res, out) {
  var self = this;

  var idx = 0
  var protohost = getProtohost(req.url) || ''
  var removed = ''
  var sync = 0
  var paramcalled = {}

  // middleware and routes
  var stack = self.stack

  // manage inter-router variables
  var parentParams = req.params
  var parentUrl = req.baseUrl || ''
  // out 함수를 실행할 때, req.baseUrl, req.next, req.params 를 복원하기 위한 함수
  // done을 이후에 호출하면 out({req.baseUrl, req.next, req.params}) 가 실행된다.
  var done = restore(out, req, 'baseUrl', 'next', 'params')

  // setup next layer
  req.next = next
  //   console.log(`여기에서 실제로 router객체가 라우팅을 하는 곳이다.
  // protohost=${protohost}, stack=${stack}, req=${req}, res=${res}, parentParams=${parentParams}, parentUrl=${parentUrl}
  // `)

  req.baseUrl = parentUrl
  req.originalUrl = req.originalUrl || req.url

  next()

  // 요청이 각 미들웨어와 라우트 핸들러를 순차적으로 통과하도록한다.
  // `next` 함수는 재귀적으로 호출되어 라우터 스택의 각 레이어를 탐색하고, 일치하는 레이어를 실행한다. 
  // 각 레이어는 미들웨어 또는 라우트 핸들러 일 수 있다.
  function next(err) {
    console.log(`next() called. err=${err}, idx=${idx}, stack.length=${stack.length}`)
    var layerError = err === 'route' ? null : err

    // restore altered req.url
    if (removed.length !== 0) {
      req.baseUrl = parentUrl
      req.url = protohost + removed + req.url.slice(protohost.length)
      removed = ''
    }

    // no more matching layers
    if (idx >= stack.length) {
      done()
      // setImmediate(done, layerError)
      return
    }

    // max sync stack
    if (sync >= 100) {
      done()
      // return setImmediate(next, err)
    }

    var path = getPathname(req)

    if (path == null) {
      console.log(`next() path is null`)
      return done(layerError)
    }

    // find next matching layer
    var layer
    var match
    var route

    while (match !== true && idx < stack.length) {
      console.log(`next() while loop idx=${idx}, stack.length=${stack.length}`)
      layer = stack[idx++]
      match = matchLayer(layer, path)
      route = layer.route
      console.log(`next() match=${match}, route=${route}`)

      if (typeof match !== 'boolean') {
        // hold on to layerError
        layerError = layerError || match;
      }

      if (match !== true) {
        continue;
      }

      if (!route) {
        console.log('next() route is null')
        // process non-route handlers normally
        continue;
      }

      if (layerError) {
        // routes do not match with a pending error
        match = false;
        continue;
      }

      var method = req.method;
      var has_method = route._handles_method(method)

      // don't even bother matching route
      // if route doesn't have a method handler
      if (!has_method && method !== 'HEAD') {
        match = false;
      }
    }
    // no match
    if (match !== true) {
      return done(layerError)
    }

    // store route for dispatch on change
    // 현재 일지하는 라우트를 `req.route` 에 저장하여 이후 요청 처리에서 사용할 수 있도록 한다.
    // `route` 변수는 현재 요청 경로와 일치하는 라우트 객체를 가리킴.
    // `req.route` 는 라우트 핸들러에서 사용할 수 있는 라우트 객체를 가리킴.
    // req.route 에 저장하여 이후 요청 처리에서 사용할 수 있도록 한다.
    if (route) {
      req.route = route
    }

    // Capture one-time layer values
    // 경로 파라미터 병합 및 설정.
    // 현재 레이어의 파라미터를 `req.params` 에 병합한다.
    // `req.params` 는 라우트 핸들러에서 사용할 수 있는 파라미터 객체를 가리킴.
    //  - `self.mergeParams` 가 `true` 인 경우
    //    - 현재 레이어의 파라미터와 부모 레이어의 파라미터를 병합한다.
    req.params = self.mergeParams
      ? mergeParams(layer.params, parentParams)
      : layer.params
    // 현재 레이어의 경로를 저장.
    var layerPath = layer.path

    // this should be done or the layer
    // 레이어의 파라미터를 처리하고, 해당 레이어의 요청 핸들러를 호출합니다.
    // - `process_params` 함수는 레이어의 파라미터를 처리한다. 이는 파라미터에 매핑된 미들웨어 함수들을 실행하는 과정이다.
    // - `paramcalled` 객체는 파라미터 콜백 함수들이 호출된 상태를 추적하는데 사용된다.
    // - 에러가 발생하면 `next` 함수가 호출되어 에러를 처리한다.
    // - 에러가 발생하지 않았고 라우트가 존재하면, 라우트의 요청 핸들러(`layer.handle_request`)를 호출한다.
    // - 라우트가 없으면, `trim_prefix` 함수가 호출되어 경로의 접두사를 제거하고 요청을 계속 처리한다.
    // - `sync` 변수는 스택 오버플로우를 방지하기 위해 사용된다.
    self.process_params(layer, paramcalled, req, res, function (err) {
      console.log(`process_params() called. err=${err}`)
      if (err) {
        next(layerError || err)
      } else if (route) {
        layer.handle_request(req, res, next)
      } else {
        console.log(layer)
        console.log(`layer=${layer}, layerError=${layerError}, layerPath=${layerPath}, path=${path}`)
        trim_prefix(layer, layerError, layerPath, path)
      }

      sync = 0
    })
  }
  // trim_prefix 함수는 express.js 라우터의 `handle` 메서드 내에서 호출되는 함수.
  // 요청 경로에서 레이어의 경로 접두사를 제거하고, 적절한 미들웨어 또는 라우트 핸들러를 실행한다.
  // 이 과정에서 경로가 일치하지 않으면, `next` gㅏㅁ수를 호출하여 다음 레이어로 이동.
  // - 
  /**
   * 예제 시나리오
   * app.use('/api', ...)
   * app.get('/api/users', (req, res) => { ... }
   * 
   * 1. 레이어의 경로가 `/api` 이고, 요청 경로가 `/api/users` 인 경우
   * 2. `trim_prefix` 함수는 `/api`를 요청 경로에서 제거하고, `/users`를 새로운 요청 경로로 설정한다.
   * 3. 이후 미들웨어 또는 라우트 핸들러를 실행한다.
   * 
   * 이것을 해야 하는 이유
   * - 이를 통해 라우트 핸들러가 레이어의 경로 접두사를 인식하지 않고, 요청 경로만을 처리할 수 있도록 한다.
   */
  function trim_prefix(layer, layerError, layerPath, path) {
    // layerPath가 비어 있지 않은 경우, path의 접두사로 layerPath가 일치하는지 확인한다.
    // 일치하지 않으면 next(layerError)를 호출하여 다음 레이어로 이동한다.
    if (layerPath.length !== 0) {
      // Validate path has a prefix match
      if (layerPath !== path.slice(0, layerPath.length)) {
        next(layerError)
        return
      }

      // Validate path break on a path separator
      // `path`의 `layerPath.length` 위치에 있는 문자가 `/` 또는 `.` 이어야 한다.
      // 그렇지 않으면 next(layerError)를 호출하여 다음 레이어로 이동한다.
      var c = path[layerPath.length]
      if (c && c !== '/' && c !== '.') return next(layerError)

      // Trim off the part of the url that matches the route
      // middleware (.use stuff) needs to have the path stripped
      console.log(`trim_prefix() before path=${path}, layerPath=${layerPath}`)
      // 요청 URL에서 `layerPath`를 접두사를 제거한다.
      // 제거된 접두사는 `removed` 변수에 저장.
      removed = layerPath
      req.url = protohost + req.url.slice(protohost.length + removed.length)
      console.log(`trim_prefix() after path=${req.url}`)

      // Ensure leading slash
      // protohost가 없고, URL이 슬래시로 시작하지 않는 경우, 슬래시를 추가한다.
      // 이를 통해 URL이 항상 슬래시로 시작하도록 보장.
      if (!protohost && req.url[0] !== '/') {
        req.url = '/' + req.url
        // slashAdded = true
      }

      // Setup base URL (no trailing slash)
      // 기본 URL(baseUrl)을 설정합니다.
      // removed가 슬래시로 끝나는 경우, 슬래시를 제거하여 기본 URL을 설정합니다.
      req.baseUrl = parentUrl + (removed[removed.length - 1] === '/'
        ? removed.substring(0, removed.length - 1)
        : removed)
    }

    if (layerError) {
      layer.handle_error(layerError, req, res, next)
    } else {
      layer.handle_request(req, res, next)
    }
  }
}


// get pathname of request
function getPathname(req) {
  console.log(`getPathname() req=${req}, req.url=${req.url}, parseUrl(req)=${parseUrl(req)}`)
  try {
    return parseUrl(req).pathname
  } catch (err) {
    return undefined
  }
}

// get protocol + host for a URL
// example
// URL: http://example.com/path?query=1
//  - searchIndex : 20
//  - pathIndex : 20
//  - fqdnIndex: 4 (http:// 에서 :// 의 위치)
//  - return http://example.com
function getProtohost(url) {
  var searchIndex = url.indexOf('?')
  var pathLength = searchIndex !== -1 ? searchIndex : url.length
  // Fully Qualified Domain Name
  var fqdnIndex = url.slice(0, pathLength).indexOf('://')
  console.log(`searchindex=${searchIndex}, pathLength=${pathLength}, fqdnIndex=${fqdnIndex}, url=${url}`)
  // str.indexOf(searchValue, fromIndex)
  return fqdnIndex !== -1
    ? url.substring(0, url.indexOf('/', 3 + fqdnIndex))
    : undefined

}

proto.use = function use(fn) {
  var offset = 0
  var path = '/'

  // default path to '/'
  // disambiguate router.use([fn])
  if (typeof fn !== 'function') {
    var arg = fn

    while (Array.isArray(arg) && arg.length !== 0) {
      arg = arg[0]
    }

    // first arg is the path
    if (typeof arg !== 'function') {
      offset = 1
      path = fn
    }
  }

  var callbacks = flatten(Array.prototype.slice.call(arguments, offset))

  if (callbacks.length === 0) {
    throw new TypeError('Router.use() requires a middleware function')
  }

  for (var i = 0; i < callbacks.length; i++) {
    var fn = callbacks[i]

    if (typeof fn !== 'function') {
      throw new TypeError('Router.use() requires a middleware function but got a ' + gettype(fn))
    }

    // add the middleware
    console.log(`use called [fn=${fn.name} on path=${path}]`)
    var layer = Layer(path, {
      sensitive: this.caseSensitive,
      strict: false,
      end: false
    }, fn)

    layer.route = undefined

    this.stack.push(layer)
  }

  return this
}

// restore obj props after function
// 주어진 함수 `fn` 를 호출하기 전에 특정 객체(`obj`)의 속성을 저장해두고, 
// 함수 호출 후에 원래의 값으로 복원하는 역할을 한다.
// 이를 통해 함수 실행 중에 객체의 특정 속성 값이 변경되더라도, 함수가 종료된 후에는 원래의 값으로 되돌릴 수 있다.
// 객체가 변경될 수도 있으니 closure 로 저장해두고, 나중에 함수를 실행할 때 복원해서 사용한다.
function restore(fn, obj) {
  var props = new Array(arguments.length - 2)
  var vals = new Array(arguments.length - 2)

  for (var i = 0; i < props.length; i++) {
    props[i] = arguments[i + 2];
    vals[i] = obj[props[i]];
  }

  return function () {
    // restore vals
    for (var i = 0; i < props.length; i++) {
      obj[props[i]] = vals[i];
    }

    return fn.apply(this, arguments);
  };
}

function matchLayer(layer, path) {
  try {
    return layer.match(path);
  } catch (err) {
    return err
  }
}

// merge params with parent params
function mergeParams(params, parent) {
  // parent가 객체가 아니거나 null/undefined 이면 params를 그대로 반환
  if (typeof parent !== 'object' || !parent) {
    return params
  }

  // mixin을 사용하여 parent 객체를 복사한다.
  var obj = mixin({}, parent)

  // 숫자형 키가 없는 경우 간단하게 병합.
  if (!(0 in params) || !(0 in parent)) {
    return mixin(obj, params)
  }

  var i = 0
  var o = 0

  // determine numeric gaps
  // params와 parent의 숫자형 키 중 가장 큰 값(최대 인덱스)를 각각 찾는다.
  while (i in params) {
    i++
  }

  while (o in parent) {
    o++
  }

  // offset numeric indices in params before merge
  // params의 숫자형 키를 parent의 인덱스 오프셋에 맞춰 재배치.
  for (i--; i >= 0; i--) {
    params[i + o] = params[i]

    // 0보다 작은 인덱스는 삭제한다.
    if (i < o) {
      delete params[i]
    }
  }

  // parent 복사본과 재배치된 params를 병합한다.
  return mixin(obj, params);
}

proto.process_params = function process_params(layer, called, req, res, done) {
  console.log(`process_params() layer=${layer}, called=${called}, req=${req}, res=${res}, done=${done}`)
  var params = layer.keys

  // captured parameters from the layer
  var keys = layer.keys

  // fast track
  if (!keys || keys.length === 0) {
    return done()
  }

  var i = 0
  var name
  var paramIndex = 0
  var key
  var paramVal
  var paramCallbacks
  var paramCalled

  // process params in order
  // 순서대로 파라미터를 처리한다.
  // param callbacks can be async
  function param(err) {
    if (err) {
      return done(err)
    }

    if (i >= keys.length) {
      return done()
    }

    paramIndex = 0
    key = keys[i++]
    name = key.name
    paramVal = req.params[name]
    paramCallbacks = params[name]
    paramCalled = called[name]

    if (paramVal === undefined || !paramCallbacks) {
      return param()
    }

    // param previously called with same value or error occurred
    if (paramCalled && (paramCalled.match === paramVal
      || (paramCalled.error && paramCalled.error !== 'route'))) {
      // restore value
      req.params[name] = paramCalled.value

      // next param
      return param(paramCalled.error)
    }

  }
}

/**
 * Create a new Route for the given path.
 *
 * Each route contains a separate middleware stack and VERB handlers.
 *
 * See the Route api documentation for details on adding handlers
 * and middleware to routes.
 *
 * @param {String} path
 * @return {Route}
 * @public
 */
proto.route = function route(path) {
  var route = new Route(path)

  var layer = new Layer(path, {
    sensitive: this.caseSensitive,
    strict: this.strict,
    end: true
  }, route.dispatch.bind(route));  // bind 는 this가 route를 가리키도록 한다.

  layer.route = route;

  this.stack.push(layer);
  return route;
}