(defn cons [a b]
  #(case %
     "car" a
     "cdr" b))

(defn car [cons] (cons "car"))
(defn cdr [cons] (cons "cdr"))

(def stream-car car)
(defn stream-cdr [stream] (-> stream cdr force))

(defn force [stream] (stream))

(defmacro delay [stream]
  `(fn [] ~stream))

(defn repeat [n] (cons n (delay (repeat n))))

(defn range [start end]
  (when (< start end)
    (cons start (delay (range (inc start)
                              end)))))

(defn take [n, stream]
  (when (< 0 n)
    (cons (car stream)
          (delay (take (dec n)
                       (stream-cdr stream))))))
(defn print-all-stream [stream]
  (loop [s stream]
    (when s
      (println (stream-car s))
      (recur (stream-cdr s)))))

(defn print-all-stream-recur [stream]
  (when stream
    (println (stream-car stream))
    (print-all-stream-recur (stream-cdr stream))))

(print-all-stream (take 50000 (range 1 10000000)))
(print-all-stream-recur (take 50000 (range 1 1000000)))
;=> java.lang.StackOverflowError
