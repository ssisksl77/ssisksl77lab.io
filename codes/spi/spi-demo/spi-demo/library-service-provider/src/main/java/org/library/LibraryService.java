package org.library;

import org.library.spi.Book;
import org.library.spi.Library;

import java.util.Optional;
import java.util.ServiceLoader;

public class LibraryService {
    private static LibraryService libraryService;
    private final ServiceLoader<Library> loader;

    private LibraryService() {
        loader = ServiceLoader.load(Library.class);
    }

    public static synchronized LibraryService getInstance() {
        if (libraryService == null) {
            libraryService = new LibraryService();
        }
        return libraryService;
    }

    public void refresh() {
        loader.reload();
    }

    public Optional<Book> getBook(String category, String name) {
        for (Library library : loader) {
            if (library.getCategory().equals(category)) {
                return Optional.ofNullable(library.getBook(name));
            }
        }
        return Optional.empty();
    }


    public Optional<Book> getBook(String name) {
        for (Library library : loader) {
            Optional<Book> book = Optional.ofNullable(library.getBook(name));
            if (book.isPresent()) {
                return book;
            }
        }
        return Optional.empty();
    }
}
