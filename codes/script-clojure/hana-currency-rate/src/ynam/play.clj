(ns ynam.play
    (:require [clojure.java.io :as io]
              [clojure.string :as str]
              [honey.sql :as sql]))

(defn make-querymap [rows]
  (let [now  (new java.util.Date)
        rows' (map #(conj % now) rows)] 
    {:insert-into :settlement.jelly_currency_rate
     :columns [:base-date :currency-code :rate :created-at]
     :values rows'}))

(let [lines (->> (slurp (io/resource "현재환율조회_20230522.txt") :encoding "EUC-KR")
                 (str/split-lines)
                 (drop 3)
                 (map #(str/split % #"\s+")))
      titles (first lines)
      context (rest lines)
      xx (->> context
              (map #(zipmap titles %))
              (map #(select-keys % ["통화", "현찰사실때", "매매기준율"]))
              (map (fn [{:strs [통화 현찰사실때 매매기준율]}]
                     [현찰사실때 매매기준율])))
      xx' (-> xx
              (make-querymap)
              (sql/format {:parameterizer false}))]
  xx'
#_  (spit "insert-query.sql" xx')

  )



(sql/format {:insert-into :settlement.jelly_currency_rate
             :columns [:base-date :currency-code :rate :created-at]
             :values [[1 2 3 4]]}
            )
