(ns jvm-analyzer-demo
  (:require [clojure.tools.analyzer :as ana]
            [clojure.tools.analyzer.env :as env]
            [clojure.tools.analyzer.ast :as ast]
            [clojure.tools.analyzer.jvm :as ana.jvm]
            [clojure.tools.analyzer.passes :refer [schedule]]
            [clojure.tools.analyzer.env :as env]
            [clojure.tools.analyzer.passes.jvm.emit-form :as e]
            [clojure.pprint :as pp]))


(ana.jvm/analyze '(+ 1 1))
'{:method add
  :op :static-call
  :form (. clojure.lang.Numbers (add 1 1))
  :env {}
  :children [:args]
  :args [{:op :const} {:op :const}]}

;; analyzer 는 기본적으로 entry point 를 설정해야 사용할 수 있다.
;; 동작하지 않음
(ana/analyze 'a {})

;; 이유
;; analyze 함수는 macroexpand-1, create-var, parse 등의 핵심 엔트리 포인트 함수가 설정되어 있어야 동작합니다.
;; 이러한 엔트리 포인트는 Clojure 코드의 구문 분석, 매크로 확장 등을 담당합니다.
;; 따라서, 직접 사용하기보다는 tools.analyzer.jvm 같은 상위 레벨 분석기를 통해 사용하는 것이 일반적입니다.

;; Calling analyze on the form is all it takes to get its AST (the output has been pretty printed for clarity):
(ana.jvm/analyze 1)

;; To get a clojure form out of an AST, use the emit-form pass:
(e/emit-form (ana.jvm/analyze '(let [a 1] a)))
;; (let* [a 1] a)

;; Note that the output will be fully macroexpanded. You can also get an hygienic form back, using the emit-hygienic-form pass:
(e/emit-hygienic-form (ana.jvm/analyze '(let [a 1 a a] a)))
;; (let* [a__#0 1 a__#1 a__#0] a__#1)

;; The analyze function can take an environment arg ( not provided it uses the default empty-env) which allows for more advanced usages, like injecting locals from an outer scope:
;; env를 추가하여 외부스코프에서 로컬 변수를 주입하는 것 같은 효과를 만들 수 있다.
(-> '(let [a a] a)
    (ana.jvm/analyze (assoc (ana.jvm/empty-env)
                            :locals '{a {:op :binding
                                         :name a
                                         :form a
                                         :locals :let}}))
    e/emit-hygienic-form)

;; analyze+eval 분석 후 양식을 평가하여 결과값을 ast :result 필드에 저장하는 함수
;; 이 함수는 여러 form을 분석할 때 사용해야한다.
;; clojure form 을 분석 할 때는, 이전 form을 평가해야 의미있을 수있다.
;; (ana.jvm/analyze '(defmacro x []))
(ana.jvm/analyze+eval '(defmacro  x  []))
(ana.jvm/analyze+eval '(x))

;;; + 를 * 로 변환하는 패스를 추가

(defn var-name [v]
  (let [nm (:name (meta v))
        nsp (.getName ^clojure.lang.Namespace (:ns (meta v)))]
    (symbol (name nsp) (name nm))))

(defn transition-pass
  {:pass-info {:walk :post :depends #{} :after an-jvm/default-passes}}
  [ast]
  (let [transitions (-> (env/deref-env) :passes-opts :transitions)]
    (if (and (= (:op ast) :invoke)       ;; AST 노드가 함수 호출
             (= (-> ast :fn :op) :var)     ;; 변수인가
             (contains? transitions (var-name (-> ast :fn :var))))  ;; 변환해야 하는 녀석인가.
      (do
        (merge ast
               {:op :transitions
                :name (get transitions (var-name (-> ast :fn :var)))}))
      ast))) ;; 다른 노드는 변경하지 않음

(meta #'transition-pass)
(meta (var transition-pass))

;; 사용자 정의 패스를 추가한 패스 목록 생성
;; schedule 은 meta 함수의 메타데이터를 이용해서 설정값을 가진다.
;; {:after    #{...}  ; 이 패스 이전에 실행되어야 하는 패스들
;;  :before   #{...}  ; 이 패스 이후에 실행되어야 하는 패스들
;;  :depends  #{...}  ; 이 패스가 의존하는 패스들
;;  :walk     :none/:post/:pre/:any  ; 트리 순회 방식
;;  :affects  #{...}  ; 이 패스와 같은 트리 순회에 포함되어야 하는 패스들
;;  :state    fn      ; 패스 상태를 초기화하는 함수}
(def custom-passes
  (schedule (into an-jvm/default-passes #{#'transition-pass})))

;; 분석할 코드
(defn special [] "fOO")
(defn boom! [] "FFOO")
(def code '(special))

;; plus-to-multiply-pass  도입
(binding [ana.jvm/run-passes custom-passes]
  (-> (ana.jvm/analyze  code
                        (ana.jvm/empty-env)
                        {:passes-opts (merge ana.jvm/default-passes-opts {:transitions {`special `boom!}})})))
{:args [],
 :children [:fn :args],
 :fn
 {:op :var,
  :assignable? false,
  :var #'jvm-analyzer-demo/special,
  :meta
  {:arglists ([]),
   :line 92,
   :column 1,
   :file "/Users/a1234/git/ssisksl77.gitlab.io/codes/deep-walking-macro/src/jvm_analyzer_demo.clj",
   :name special,
   :ns #namespace[jvm-analyzer-demo]},
  :env
  {:context :ctx/expr,
   :locals {},
   :ns jvm-analyzer-demo,
   :column 12,
   :line 93,
   :file "/Users/a1234/git/ssisksl77.gitlab.io/codes/deep-walking-macro/src/jvm_analyzer_demo.clj"},
  :form special,
  :o-tag java.lang.Object,
  :arglists ([])},
 :meta {:line 93, :column 12},
 :name jvm-analyzer-demo/boom!,
 :op :transitions,
 :env
 {:context :ctx/expr,
  :locals {},
  :ns jvm-analyzer-demo,
  :column 12,
  :line 93,
  :file "/Users/a1234/git/ssisksl77.gitlab.io/codes/deep-walking-macro/src/jvm_analyzer_demo.clj"},
 :o-tag java.lang.Object,
 :top-level true,
 :form (special)}
;;;;
;; 이렇게 분석된 AST를 이용해서 코드를 변환할 수 있다.
;;;;