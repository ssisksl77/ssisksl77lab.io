(ns go-macro
  (:require [clojure.core.async :as a]
            [clojure.core.async.impl.ioc-macros :as ioc-macros]
            [clojure.core.async.impl.go :as go]
            [clojure.tools.analyzer.jvm :as ana.jvm]))

;; https://www.youtube.com/watch?v=R3PZMIwXN_g&t=1379s&ab_channel=TimothyBaldridge

(go-macro/go-impl {} '(println "Hello, world!"))

(macroexpand-1 '(a/go (println "Hello, world!")))

(macroexpand-1
 '(go-macro/gen-plan
 [a (assoc-in-plan [:x] 1)
  b (assoc-in-plan [:y] 2)]
 [a b]))


(go-macro/gen-plan
 [a (assoc-in-plan [:x] 1)
  b (assoc-in-plan [:y] 2)]
 [a b])

;;

(defn r-assoc [k v m]
  (assoc m k v))

(def add-me (partial r-assoc :name "timothy"))

;; state monad
(add-me {})

(defn thread-it 
  "모든 함수가 acc(hashmap)를 받아서 실행된다. 
   그리고 리턴한다. 
   acc에는 상태가 저장된다."
  [& fns]
  (reduce (fn [acc f]
            (f acc)) 
          {} ;; composable 하지 않다.
          fns))

(thread-it (partial r-assoc :name "timothy")
           (partial r-assoc :age 30))

(defn thread-it2
  "모든 함수가 acc(hashmap)를 받아서 실행된다. 
   그리고 리턴한다. 
   acc에는 상태가 저장된다."
  [& fns]
  (fn [initial]
    (reduce (fn [acc f]
              (f acc))
            initial
            fns)))

((thread-it2 (partial r-assoc :name "timothy")
            (partial r-assoc :age 30)
            (partial r-assoc :job "programmer"))
 {})
;; 이제 composable 하다.

(defn add-personal-info []
  (thread-it2 (partial r-assoc :name "timothy")
              (partial r-assoc :age 30)))

(defn add-job-info []
  (thread-it2 (partial r-assoc :job "programmer")))


((thread-it2 (add-personal-info)
            (add-job-info))
 {})
;; 이렇게 함수로 합쳐진다. 
;; 이것을 state monad 라고 부른다.
;; 함수적으로 순수하게 상태를 변경할 수 있다.
;; core.async 에서는 이렇게 더하는 것에서 더 나아간다.
;; deep walking macro 에서 context 를 추가하기도 하고 pop 하기도 한다.
;; 15:13

;; gen-plan은 이 monad 를 바인딩 하는데 사용한다.

(go-macro/assoc-in-plan [:x] 1) ;; return a function
((go-macro/assoc-in-plan [:y] 2) {})
;; => [2 {:y 2}]

(macroexpand-1 '(go-macro/gen-plan
                 [assoc-v [go-macro/assoc-in-plan [2] 4]]
                 ["Hello, " assoc-v]))
((go-macro/gen-plan
  [assoc-v (go-macro/assoc-in-plan [:x] 1)]
  (str "Hello, " assoc-v))
 {})
;; => ["Hello, 1" {:x 1}]

((go-macro/gen-plan
  [assoc-v (go-macro/all (for [x (range 5)]
             (go-macro/assoc-in-plan [(keyword (str "x_" x))] x)))]
  (str "Hello, " assoc-v))
 {})
;; ["Hello, [0 1 2 3 4]" {:x_0 0, :x_1 1, :x_2 2, :x_3 3, :x_4 4}]


(-> (go-macro/gen-plan
     [assoc-v (go-macro/all (for [x (range 5)]
                              (go-macro/assoc-in-plan [(keyword (str "x_" x))] x)))]
     (str "Hello, " assoc-v))
    (go-macro/get-plan))
;; ["Hello, [0 1 2 3 4]" {:x_0 0, :x_1 1, :x_2 2, :x_3 3, :x_4 4}]

;; push-binding, pop-binding
;; 22:56
;; work with context
;; push-binding update the context (update-in [:bindings key] conj value)
;; pop-binding go back to the old state ()
(-> (go-macro/gen-plan
     [assoc-v (go-macro/all (for [x (range 5)]
                              (go-macro/assoc-in-plan [(keyword (str "x_" x))] x)))]
     (str "Hello, " assoc-v))
    (go-macro/get-plan))

;; 25:25
(-> (go-macro/parse-to-state-machine
 '[(if (= x 1) :true :false)] {}) 
    (clojure.pprint/pprint))
; [inst_20129
;  {:bindings {:terminators ()},
;   :block-id 1,
;   :blocks
;   {1
;    [{:ast [(if (= x 1) :true :false)], 
;      :locals nil, 
;      :id inst_20128}
;     {:value inst_20128, 
;      :id inst_20129}]},
;   :block-catches {1 nil},
;   :start-block 1,
;   :current-block 1}]


;; 뭔가 잘못되었다. 코드가 많이 바꼈어.
;; 기존 강의에서는 코드를 그대로 item-to-ssa 로 변환하지만
;; 변경된 코드는 jvm-analyzer 를 사용해용해서 s-expression 을 만들고
;; 그것을 ssa -> state-machine 으로 변환한다.
;; state-machine 코드를 만들어야 한다.
(defn state-machine [body num-user-params [crossing-env env] user-transitions]
  (binding [ana.jvm/run-passes go/run-passes]
    (-> (ana.jvm/analyze `(let [~@(if (go/nested-go? env)
                                   (mapcat (fn [[l {:keys [tag]}]]
                                             (go/emit-hinted l tag crossing-env))
                                           env)
                                   (mapcat (fn [[l ^clojure.lang.Compiler$LocalBinding lb]]
                                             (go/emit-hinted l (when (.hasJavaClass lb)
                                                              (some-> lb .getJavaClass .getName))
                                                          crossing-env))
                                           env))]
                           ~body)
                        (go/make-env env crossing-env)
                        {:passes-opts (merge ana.jvm/default-passes-opts
                                             {:uniquify/uniquify-env true
                                              :mark-transitions/transitions user-transitions})})
        ;; (go/parse-to-state-machine user-transitions)
        ;; second
        ;; (go/emit-state-machine num-user-params user-transitions)
        )))

(defn parse-to-state-machine
  "Takes an sexpr and returns a hashmap that describes the execution flow of the sexpr as
   a series of SSA style blocks."
  [body terminators]
  (-> (go/gen-plan
       [a (go/push-binding :terminators terminators)
        blk (go/add-block)
        _ (go/set-block blk)
        id (go/item-to-ssa body)
        term-id (go/add-instruction (go/->Return id))
        _ (go/pop-binding :terminators)
        ]
       id) 
      go/get-plan))


(let [expr '(let [x 1]
              (if (= x 1) :true :false))
      result (-> (state-machine '(let [x 1]
                                   (if (= x 1) :true :false))
                                0
                                [[:x {:tag :int}]]
                                {})
                 (parse-to-state-machine {})
                 second)]
  ;; (println (clojure.pprint/pprint (second sm)))
  (println "\nInput Expression:")
  (println expr)
  (println "\nGenerated State Machine:")
  (println "Start Block:" (:start-block result))
  (println "\nBlocks:" (:blocks result))
  (doseq [[block-id instructions] (:blocks result)]
    (println "\nBlock" block-id ":")
    (doseq [inst instructions]
      (println "  -" (dissoc inst :id)))))

(defn test-transformation [expr]
  (let [ast (ana.jvm/analyze expr)  ; AST로 변환
        terminators {}          ; 기본 terminators
        result (parse-to-state-machine ast terminators)] ; 상태 기계로 변환

    (println "\nInput Expression:")
    (println expr)
    (println "\nGenerated State Machine:")
    (println "Start Block:" (:start-block result))
    (println "\nBlocks:")
    (doseq [[block-id instructions] (:blocks result)]
      (println "\nBlock" block-id ":")
      (doseq [inst instructions]
        (println "  -" (dissoc inst :id))))))


(let [expr '(if (= 1 1) :true :false)
      result (-> (state-machine expr
                                0
                                [[:x {:tag :int}]]
                                {})
                 (parse-to-state-machine {})
                 second)]
  ;; (println (clojure.pprint/pprint (second sm)))
  (println "\nInput Expression:")
  (println expr)
  (println "\nGenerated State Machine:")
  ;; (doseq [[block-id instructions] (:blocks result)]
  ;;   (println "\nBlock" block-id ":")
  ;;   (doseq [inst instructions]
  ;;     (clojure.pprint/pprint inst))
  
  ;; )
  (clojure.pprint/pprint (:blocks result))
  )

(let [expr '(println 1)
      result (-> (state-machine expr
                                0
                                []
                                {})
                 )]
  (-> result 
      (select-keys [:op :fn :args])
      (update-in [:fn] select-keys [:op :var])
      (update-in [:args] (partial map (fn [x] (select-keys x [:op :var :val :type]))))
      (clojure.pprint/pprint))
)
; {:op :invoke,
;  :fn {:op :var, :var #'clojure.core/println},
;  :args ({:op :const, :val 1, :type :number})}

(let [expr '(println 1)
      result (-> (state-machine expr
                                0
                                []
                                {}))]
  (-> result
      (select-keys [:op :fn :args])
      (update-in [:fn] #(select-keys % [:op :var]))
      (update-in [:args] (partial map #(select-keys % [:op :var :val :type])))
      (parse-to-state-machine {})))
;; [inst_21229
;;  {:bindings {:terminators ()},
;;   :block-id 1,
;;   :blocks
;;   {1
;;    [{:ast {:op :invoke, :fn {:op :var, :var #'clojure.core/println}, :args ({:op :const, :val 1, :type :number})},
;;      :locals nil,
;;      :id inst_21229}
;;     {:value inst_21229, :id inst_21230}]},
;;   :block-catches {1 nil},
;;   :start-block 1,
;;   :current-block 1}]

(def if-ast
  {:op :if
   :test {:op :const
          :val true}
   :then {:op :const
          :val "then-branch"}
   :else {:op :const
          :val "else-branch"}})
(let [expr '(if true "then-branch" "else-branch")
      result (-> (state-machine expr
                                0
                                []
                                {})
                 )]
  (-> result
      ;; 이게 있어야 변환하고 없으면, RawCode 를 그대로 사용한다.
      (assoc :clojure.core.async.impl.go/transform? true)
      (parse-to-state-machine {})))
      


(go-macro/state-machine '(if true "then-branch" "else-branch")
               0
               []
               {})

(go-macro/state-machine '(let [c (a/chan)]
                           (loop [a 0]
                                (if (< a 10)
                                  (do 
                                    (a/go (a/>! c a))
                                    (recur (inc a)))
                                  a)
                             ))
                        0
                        []
                        {})

(state-machine '(if true "then-branch" "else-branch")
               0
               []
               {})

;; 정리하면
;; state-machine 은 jvm-analyzer 를 사용해서 s-expression 을 만들고
;; 그것을 ssa로 변환한다.
;; 그리고 그것을 state-machine 으로 변환한다.
;; 그런데 s-expression 을 만들고 ssa로 변환 후에 어떻게 state-machine 로 변환되는지가 모르겠다.