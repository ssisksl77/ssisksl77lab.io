    ;; simple exrpession
(+ (* a a)
   (* b b))

;; 우리는 이 폼을 보고 list 데이터임을 알고 수정할 수 있다고 우리는 알고 있다.
;; 매크로는 이 폼을 수정하여 컴파일러에게 보낼 수 있다.

```clj
(only-ints 1 2 3 4) => [1 2 3 4]
```

```clj
(defmacro only-ints [& args]
    (assert (every? integer? args))
    (vec args))
```

defmacro 로 작성한 코드는 컴파일 타임에 수행된다.

```clj
(defn test-fn [x]
    (only-hints x))
```
이는 수행되지 않는다. 왜냐하면 only-hints 는 컴파일 타임에 매크로확장(평가) 되기 때문에 `x` 자체에 대한 검사를 하기 때문이다.

```clj
(defn  only-ints-fn  [&  args]
    (assert (every? integer? args))
    (vec args))

(defn test-fn  [x]
    (only-hints x))
```

이렇게 하면 런타임 테스트가 수행되어 성공한다.

매크로 확장은 대체 무엇인가? (05:03)
if, when, loop 같은 곳에서 clojure 는 모두 매크로를 사용한다.

```clj
(macroexpand '(only-ints 1 2 3 4)) => [1 2 3 4]
(macroexpand '(only-ints-fn 1 2 3 4)) => (only-ints-fn 1 2 3 4)
```
매크로가 아닌 것은 자료구조포맷을 그대로 리턴하고 매크로는 평가해버린다.


```clj
(defmacro when [test & body]
    `(if ~test
        (do ~body)
        nil))
```

매크로 확장을 해보자.

```clj
(macroexpand '(when true (println "hello") 42))
```

여기까지가 일반적인 매크로 형태이다.

---

### Environment

매크로에서는 Environment 로 환경을 가져올 수 있음
```clj
(macroexpand '(defmacro foo [] 42))
=>
(do 
    (clojure.core/defn foo ([&form &env] 42))
    (. (var foo) (setMacro))
    (var foo)
)
```

그러므로 아래 매크로를 실행하면 environment를 출력할 수 있다.

```clj
(defmacro when [test & body]
    (println &env)
    `(if ~test
        (do ~body)
        nil))
(fn [x]
  (when true (println "hello" 42)))
=> {x #<LocalBinding clojure.lang.Compiler$LocalBinding@194527c6>}
```

이번엔 
```clj
(meta #'when)
{:arglists ([test & body]), :ns #<Namespace deep-walking-macro.core>, :name when
 :column 1 :line 22
 :macro true  ;; 이게 중요
}
```

매크로와 함수의 차이는 `meta` 키가 true, false 의 차이만 있다.
여기서 작성자의 조언, deep walking macro 에  대해 알게 되면 
점점 작은 컴파일러를 만들어내려고 하지만!
이것이 단순이 도구임을 명심하기  바란닥고 조언

### Deep Walking Macro 17:22

```clj
(defmulti parse-item (fn [form ctx]
                        (cond 
                          (seq? form) :seq
                          (int? form) :int
                          (symbol? form) :symbol)))

(defmethod parse-item :seq
  [form ctx]
  (parse-sexpr form ctx))

(defmethod parse-item :int
  [form ctx]
  {:type :int
   :value form})

(defmethod parse-item :symbol
  [form ctx]
  {:type :symbol
   :value form})

(defmulti parse-sexpr (fn [[sym & rest] ctx] 
                        sym))

(defmethod parse-sexpr 'if
  ;; 첫번째 인자는 필요없음 if 인걸 알고 있음.
  [[_ test then else] ctx]
  {:type :if
   :test (parse-item test ctx)
   :then (parse-item then ctx)
   :else (parse-item else ctx)})
   ;; 이를 보면 우리는 각 recursive 하게 해시맵을 만들고 있고
   ;; 컴파일러는 배우고 있는 사람에게는 이건 AST 와 같음을 알 수 있다.

(parse-item 42 nil)
(parse-item 's nil)
(parse-item '(if x 42 41) nils)
```

하지만 이제 `when` 을 시행하면 어떨까? 

```
(parse-item '(when  x 42 1) nil)
; Execution error (IllegalArgumentException) at core/eval10119$fn (REPL:16).
; No method in multimethod 'parse-sexpr' for dispatch value: when
```

when은 내부적으로 if를 쓰지만 매크로이므로 확장을 할 필요가 있다.

```clj
(defmethod parse-expr 'seq
  [form ctx]
  (let [form (macroexpand form)]
    (parse-sexpr form ctx)))
```

parse-sexpr 이 수행되기전에 parse-expr 에서 seq 형태인 것은 매크로,함수,자료구조 가 있을 것이다.
여기서 매크로는 미리 확장해놓아야  parse-sexpr 에서 분리하기  좋을 것이다.

하지만 다시 실행해도 실패한다.
when은 do도 함께 사용하기 때문.
do 안에는 수행해야될 expression 들이 많기 때문에 순회하면서 모두 parse 함.

```clj
(defmethod parse-sexpr 'do
  [[_ & body]  ctx]
  {:type :do
   :body (map (fn [x] (parse-item x ctx)) body)})
```

실행해도 다시 실패할 것인데 when에는 nil을 리턴하고 있다. 
parse-item 에서 nil을 체크하는 부분이 없어서 문제가된다.
```clj
(defmulti parse-item (fn [form ctx]
                        (cond 
                          (seq? form) :seq
                          (int? form) :int
                          (symbol? form) :symbol
                          (nil? form) :nil)))
(defmethod parse-item :nil
  [form ctx]
  {:type :nil
  })
```


### ctx 에는 무엇을 넣을 수 있는가. counting integers

```clj
(defmethod parse-item :int
  [form ctx]
  (swap! ctx inc)
  {:type :int
   :value  form})
```

```

(let [a (atom 0)]
  (parse-item '(when t 42) a)
  (println @a))

=> 
0
nil


(def b (atom 0))
(parse-item '(when t 42) b)
@b
=>
1
```

이러면 안되는데 왜 이런 것일까?
그 이유는 parse-sexpr 에서 'do  를 다룰 때 문제가 있다.
map 은 천성적으로 lazy하다. 
그래서 리턴값을 출력하기  전까지 확장하지 않을 것이다. 평생 출력되지 않았으니 확장되지도 않았을 것.
```clj
(defmethod parse-sexpr 'do
  [[_ & body]  ctx]
  {:type :do
   :body (doall (map (fn [x] (parse-item x ctx)) body))})
```
이제 정상적으로 수행된다.

resolve 는 심볼을 받아서, 해당  심볼에 해당하는 Var 를  찾아준다.

```clj
(resolve '+)
clojure.core/+
```
이 값은 네임스페이스에 따라 다를 수 있다.
  - 매크로를 작성할 때 심볼이 실제로 존재하는지 확인할 때
  - 동적으로 함수나  변수를 찾을 때
  - 리플렉셔이 필요할 때
  
사용한다. 이것으로 한번 parse-sexpr :default 를 만들어보자.
```clj
(defmethod  parse-sexpr :default
  [[f &  body] ctx]
  {:type  :call
   :fn (parse-item f ctx)
   :args (doall (map (fn [x] (parse-item x ctx))
                     body))})


(parse-item '(+ 2 3) (atom 0))
;; {:type :call, :fn {:type :symbol, :value +}, :args ({:type :int, :value 2} {:type :int, :value 3})}

(parse-item '((comp pos? +) 2 3) (atom 0))
;; {:type :call,
;;  :fn {:type :call, :fn {:type :symbol, :value comp}, :args ({:type :symbol, :value pos?} {:type :symbol, :value +})},
;;  :args ({:type :int, :value 2} {:type :int, :value 3})}
```
이렇게 comp 의 경우는 default로 분류되고 그 안에 포맷은 심볼 pos?, 심볼 + 가 존재함을 보여주게 된다. 

우리는 이것을 to-ast로 부르려고 한다.

```clj
(defmacro to-ast [form]
  (parse-item form (atom 0)))

(to-ast (+ 1 2))
=> {:type :call, :fn {:type :symbol, :value #function[clojure.core/+]}, :args nil}
```

go 매크로는 위 코드와 아주 비슷한 일을 한다.


