(ns core)
;;; https://www.youtube.com/watch?v=HXfDK1OYpco&t=14s&ab_channel=TimothyBaldridge

(defmulti parse-item (fn [form ctx]
                       (cond
                         (seq? form) :seq
                         (int? form) :int
                         (symbol? form) :symbol
                         (nil? form) :nil)))

(defmulti parse-sexpr (fn [[sym & rest] ctx]
                        sym))


#_(defmethod parse-item :seq
    [form ctx]
    (parse-sexpr form ctx))

(defmethod parse-item :seq
  [form ctx]
  (let [form (macroexpand form)]
    (parse-sexpr form ctx)))


#_(defmethod parse-item :int
    [form ctx]
    {:type :int
     :value form})
(defmethod parse-item :int
  [form ctx]
  (swap! ctx inc)
  {:type :int
   :value form})


(defmethod parse-item :symbol
  [form ctx]
  {:type :symbol
   :value form})

(defmethod parse-item :nil
  [form ctx]
  {:type :nil})

(defmethod parse-sexpr 'if
  ;; 첫번째 인자는 필요없음 if 인걸 알고 있음.
  [[_ test then else] ctx]
  {:type :if
   :test (parse-item test ctx)
   :then (parse-item then ctx)
   :else (parse-item else ctx)})
   ;; 이를 보면 우리는 각 표현식을 recursive 하게 해시맵을 만들고 있고
   ;; 컴파일러는 배우고 있는 사람에게는 이건 AST 와 같음을 알 수 있다.

(defmethod parse-sexpr 'do
  [[_ & body]  ctx]
  {:type :do
   :body (doall (map (fn [x] (parse-item x ctx)) body))})

(comment
  (parse-item 42 nil)
  ;; {:type :int, :value 42}
  (parse-item 's nil)
  ;; {:type :symbol, :value s}
  (parse-item '(if x 42 41) nil)
  ;; {:type :if, :test {:type :symbol, :value x}, :then {:type :int, :value 42}, :else {:type :int, :value 41}}

  (parse-item '(when  x 42 1) nil)
  ; Execution error (IllegalArgumentException) at core/eval10119$fn (REPL:16).
  ; No method in multimethod 'parse-sexpr' for dispatch value: when

  (parse-item '(when  x 42 1) nil)
  ;; 두번째 개선 후.
  ; Execution error (IllegalArgumentException) at core/eval10424$fn (REPL:21).
  ; No method in multimethod 'parse-sexpr' for dispatch value: do

  (parse-item '(when  x 42) {})
  #_{:type :if,
     :test {:type :symbol, :value x},
     :then {:type :do, :body ({:type :int, :value 42} {:type :int, :value 1})},
     :else {:type :nil}}


  #_(defmacro when [test & body]
      `(if ~test
         (do ~body)
         nil))
  ;;


  (let [a (atom 0)]
    (parse-item '(when t 42) a)
    (println @a))

  (def b (atom 0))
  (parse-item '(when t 42) b)
  @b
  
  (defmethod  parse-sexpr :default
    [[f &  body] ctx]
    {:type  :call
     :fn (parse-item f ctx)
     :args (doall (map (fn [x] (parse-item x ctx))
                       body))})
  
  (parse-item '(+ 2 3) (atom 0))
  ;; {:type :call, :fn {:type :symbol, :value +}, :args ({:type :int, :value 2} {:type :int, :value 3})}
  
  (parse-item '((comp pos? +) 2 3) (atom 0))
  ;; {:type :call,
  ;;  :fn {:type :call, :fn {:type :symbol, :value comp}, :args ({:type :symbol, :value pos?} {:type :symbol, :value +})},
  ;;  :args ({:type :int, :value 2} {:type :int, :value 3})}
  
  
  (parse-item '(+ 2 3) (atom 0))
  ;; {:type :call, :fn {:type :symbol, :value +}, :args ({:type :int, :value 2} {:type :int, :value 3})}
  
  (parse-item '((comp pos? +) 2 3) (atom 0))
  ;; {:type :call,
  ;;  :fn {:type :call, :fn {:type :symbol, :value comp}, :args ({:type :symbol, :value pos?} {:type :symbol, :value +})},
  ;;  :args ({:type :int, :value 2} {:type :int, :value 3})}
  
  
  (defmacro to-ast [form]
    (parse-item form (atom 0)))
  
  (to-ast (+ 1 2))
  
  )




