(ns poc)

(require '[xtdb.api :as xt])

(def node (xt/start-node {}))

(def manifest
  {:xt/id :manifest
   :pilot-name "Johanna"
   :id/rocket "SB002-sol"
   :id/employee "22910x2"
   :badges "SETUP"
   :cargo ["stero" "gold fish" "slippers" "secret note"]})

(xt/submit-tx node [[::xt/put manifest]])

(xt/entity (xt/db node) :manifest)

;; ch02

(def node (xt/start-node {}))
(xt/submit-tx
 node
 [[::xt/put
   {:xt/id :commodity/Pu
    :common-name "Nitrogen"
    :type :element/gas
    :density 1.2506
    :radioactive false}]

  [::xt/put
   {:xt/id :commodity/N
    :common-name "Nitrogen"
    :type :element/gas
    :density 1.2506
    :radioactive false}]

  [::xt/put
   {:xt/id :commodity/CH4
    :common-name "Methane"
    :type :molecule/gas
    :density 0.717
    :radioactive false}]])
;; => #:xtdb.api{:tx-id 1, :tx-time #inst "2023-08-20T13:46:27.799-00:00"}

(xt/submit-tx
 node
 [[::xt/put
   {:xt/id :stock/Pu
    :commod :commodity/Pu
    :weight-ton 21 }
   #inst "2115-02-13T18"]

  [::xt/put
   {:xt/id :stock/Pu
    :commod :commodity/Pu
    :weight-ton 23 }
   #inst "2115-02-14T18"]

  [::xt/put
   {:xt/id :stock/Pu
    :commod :commodity/Pu
    :weight-ton 22.2 }
   #inst "2115-02-15T18"]

  [::xt/put
   {:xt/id :stock/Pu
    :commod :commodity/Pu
    :weight-ton 24 }
   #inst "2115-02-18T18"]

  [::xt/put
   {:xt/id :stock/Pu
    :commod :commodity/Pu
    :weight-ton 24.9 }
   #inst "2115-02-19T18"]])

(xt/sync node)


(xt/submit-tx
 node
 [[::xt/put
   {:xt/id :stock/N
    :commod :commodity/N
    :weight-ton 3 }
   #inst "2115-02-13T18"
   #inst "2115-02-19T18"]

  [::xt/put
   {:xt/id :stock/CH4
    :commod :commodity/CH4
    :weight-ton 92 }
   #inst "2115-02-15T18"
   #inst "2115-02-19T18"]])
;; => #:xtdb.api{:tx-id 4, :tx-time #inst "2023-08-20T14:07:27.905-00:00"}


(xt/entity (xt/db node #inst "2115-02-14") :stock/Pu)
;; => {:commod :commodity/Pu, :weight-ton 21, :xt/id :stock/Pu}


(xt/entity (xt/db node #inst "2115-02-18") :stock/Pu)
;; => {:commod :commodity/Pu, :weight-ton 22.2, :xt/id :stock/Pu}


(defn easy-ingest
  "Uses XTDB put transaction to add a vector of documents to a specified node"
  [node docs]
  (xt/submit-tx node
                (vec (for [doc docs]
                       [::xt/put doc])))
  (xt/sync node))



(xt/submit-tx
 node
 [[::xt/put
   {:xt/id :manifest
    :pilot-name "Johanna"
    :id/rocket "SB002-sol"
    :id/employee "22910x2"
    :badges ["SETUP" "PUT"]
    :cargo ["stereo" "gold fish" "slippers" "secret note"]}]])
;; => #:xtdb.api{:tx-id 5, :tx-time #inst "2023-08-20T14:10:16.993-00:00"}


;; ch03
(defn easy-ingest
  "Uses XTDB put transaction to add a vector of
  documents to a specified system"
  [node docs]
  (xt/submit-tx node
    (vec (for [doc docs]
              [::xt/put doc])))
  (xt/sync node))

(def data
  [{:xt/id :commodity/Pu
    :common-name "Plutonium"
    :type :element/metal
    :density 19.816
    :radioactive true}

   {:xt/id :commodity/N
    :common-name "Nitrogen"
    :type :element/gas
    :density 1.2506
    :radioactive false}

   {:xt/id :commodity/CH4
    :common-name "Methane"
    :type :molecule/gas
    :density 0.717
    :radioactive false}

   {:xt/id :commodity/Au
    :common-name "Gold"
    :type :element/metal
    :density 19.300
    :radioactive false}

   {:xt/id :commodity/C
    :common-name "Carbon"
    :type :element/non-metal
    :density 2.267
    :radioactive false}

   {:xt/id :commodity/borax
    :common-name "Borax"
    :IUPAC-name "Sodium tetraborate decahydrate"
    :other-names ["Borax decahydrate" "sodium borate" "sodium tetraborate" "disodium tetraborate"]
    :type :mineral/solid
    :appearance "white solid"
    :density 1.73
    :radioactive false}])

(easy-ingest node data)
;; => #inst "2023-08-20T14:44:21.972-00:00"


;; datalog tutorial
(xt/q (xt/db node)
      '{:find [element]
        :where [[element :type :element/metal]]})
;; => #{[:commodity/Pu] [:commodity/Au]}

(=
 (xt/q (xt/db node)
         '{:find [element]
           :where [[element :type :element/metal]]})

 (xt/q (xt/db node)
         {:find '[element]
          :where '[[element :type :element/metal]]})

 (xt/q (xt/db node)
         (quote
          {:find [element]
           :where [[element :type :element/metal]]})))
;; => true

(xt/q  (xt/db node)
       '{:find [name]
         :where [[e :type :element/metal]
                 [e :common-name name]]})
;; => #{["Gold"] ["Plutonium"]}

(xt/q (xt/db node)
        '{:find [name rho]
          :where [[e :density rho]
                  [e :common-name name]]})
;; => #{["Nitrogen" 1.2506]
;;      ["Carbon" 2.267]
;;      ["Methane" 0.717]
;;      ["Borax" 1.73]
;;      ["Gold" 19.3]
;;      ["Plutonium" 19.816]}

(xt/q (xt/db node)
      '{:find [name]
        :where [[e :type type]
                [e :common-name name]]
        :in [type]}
      :element/metal)


(defn filter-type
  [type]
  (xt/q (xt/db node)
        '{:find [name]
          :where [[e :common-name name]
                  [e :type type]]
          :in [type]}
        type))

(defn filter-appearance
  [description]
  (xt/q (xt/db node)
        '{:find [name IUPAC]
          :where [[e :common-name name]
                  [e :IUPAC-name IUPAC]
                  [e :appearance appearance]]
          :in [appearance]}
        description))

(filter-type :element/metal)
(filter-appearance "white solid")


(xt/submit-tx
 node
 [[::xt/put
   {:xt/id :manifest
    :pilot-name "Johanna"
    :id/rocket "SB002-sol"
    :id/employee "22910x2"
    :badges ["SETUP" "PUT" "DATALOG-QUERIES"]
    :cargo ["stereo" "gold fish" "slippers" "secret note"]}]])
;; => #:xtdb.api{:tx-id 6, :tx-time #inst "2023-08-20T15:38:10.368-00:00"}

(xt/sync node)
;; => #inst "2023-08-20T15:38:10.368-00:00"

(xt/submit-tx
 node
 [[::xt/put
   {:xt/id :consumer/RJ29sUU
    :consumer-id :RJ29sUU
    :first-name "Jay"
    :last-name "Rose"
    :cover? true
    :cover-type :Full}
   #inst "2114-12-03"]])

(xt/sync node)
;; => #inst "2023-08-20T16:29:22.142-00:00"

(xt/submit-tx
 node
 [[::xt/put
   {:xt/id :consumer/RJ29sUU
    :consumer-id :RJ29sUU
    :first-name "Jay"
    :last-name "Rose"
    :cover? true
    :cover-type :Full}
   #inst "2113-12-03" ;; Valid time start
   #inst "2114-12-03" ;; Valid time end
   ]
  [::xt/put
   {:xt/id :consumer/RJ29sUU
    :consumer-id :RJ29sUU
    :first-name "Jay"
    :last-name "Rose"
    :cover? true
    :cover-type :Full}
   #inst "2112-12-03"
   #inst "2113-12-03"]

  [::xt/put
   {:xt/id :consumer/RJ29sUU
    :consumer-id :RJ29sUU
    :first-name "Jay"
    :last-name "Rose"
    :cover? false}
   #inst "2112-06-03"
   #inst "2112-12-02"]

  [::xt/put
   {:xt/id :consumer/RJ29sUU
    :consumer-id :RJ29sUU
    :first-name "Jay"
    :last-name "Rose"
    :cover? true
    :cover-type :Promotional}
   #inst "2111-06-03"
   #inst "2112-06-03"]])

(xt/sync node)

(xt/q (xt/db node #inst "2114-01-01")
      '{:find [cover type]
        :where [[e :consumer-id :RJ29sUU]
                [e :cover? cover]
                [e :cover-type type]]})


(xt/q (xt/db node #inst "2111-07-03")
        '{:find [cover type]
          :where [[e :consumer-id :RJ29sUU]
                  [e :cover? cover]
                  [e :cover-type type]]})


(xt/q (xt/db node #inst "2112-07-03")
      '{:find [cover type]
        :where [[e :consumer-id :RJ29sUU]
                [e :cover? cover]
                [e :cover-type type]]})

(xt/submit-tx
 node
 [[::xt/put
   {:xt/id :manifest
    :pilot-name "Johanna"
    :id/rocket "SB002-sol"
    :id/employee "22910x2"
    :badges ["SETUP" "PUT" "DATALOG-QUERIES" "BITEMP"]
    :cargo ["stereo" "gold fish" "slippers" "secret note"]}]])

(xt/sync node)

;; 5: Match with XTDB - Saturn Assginment
(require '[xtdb.api :as xt])

(def node (xt/start-node {}))

(defn easy-ingest
  "Users XTDB put transaction to add a vector of documents to a specified node"
  [node docs]
  (xt/submit-tx node
                (vec (for [doc docs]
                       [::xt/put doc])))
  (xt/sync node))

(def data
  [{:xt/id :gold-harmony
    :company-name "Gold Harmony"
    :seller? true
    :buyer? false
    :units/Au 10211
    :credits 51}

   {:xt/id :tombaugh-resources
    :company-name "Tombaugh Resources Ltd."
    :seller? true
    :buyer? false
    :units/Pu 50
    :units/N 3
    :units/CH4 92
    :credits 51}

   {:xt/id :encompass-trade
    :company-name "Encompass Trade"
    :seller? true
    :buyer? true
    :units/Au 10
    :units/Pu 5
    :units/CH4 211
    :credits 1002}

   {:xt/id :blue-energy
    :seller? false
    :buyer? true
    :company-name "Blue Energy"
    :credits 1000}])

(easy-ingest node data)
;; => #inst "2023-08-23T11:31:57.490-00:00"

;; 트레이드 이후 주식 및 펀드 레벨을 쉽게 보여주는 함수를 만들고자 함.
(defn stock-check
  [company-id item]
  {:result (xt/q (xt/db node)
                 {:find '[name funds stock]
                  :where ['[e :company-name name]
                          '[e :credits funds]
                           ['e item 'stock]]
                  :in '[e]}
                 company-id)
   :item item})

(defn format-stock-check
  [{:keys [result item] :as stock-check}]
  (for [[name funds commod] result]
    (str "Name: " name ", Funds: " funds ", " item " " commod)))

(xt/submit-tx
 node
 [[::xt/match
   :blue-energy
   {:xt/id :blue-energy
    :seller? false
    :buyer? true
    :company-name "Blue Energy"
    :credits 1000}]
  [::xt/put
   {:xt/id :blue-energy
    :seller? false
    :buyer? true
    :company-name "Blue Energy"
    :credits 900
    :units/CH4 10}]

  [::xt/match
   :tombaugh-resources
   {:xt/id :tombaugh-resources
    :company-name "Tombaugh Resources Ltd."
    :seller? true
    :buyer? false
    :units/Pu 50
    :units/N 3
    :units/CH4 92
    :credits 51}]
  [::xt/put
   {:xt/id :tombaugh-resources
    :company-name "Tombaugh Resources Ltd."
    :seller? true
    :buyer? false
    :units/Pu 50
    :units/N 3
    :units/CH4 82
    :credits 151}]])
;; => #:xtdb.api{:tx-id 3, :tx-time #inst "2023-08-23T12:10:16.665-00:00"}

(xt/sync node)
;; => #inst "2023-08-23T12:10:16.665-00:00"
;; match 를 이용하여 transaction 유효성을 맞춘다.

(format-stock-check (stock-check :tombaugh-resources :units/CH4))
;; => ("Name: Tombaugh Resources Ltd., Funds: 151, :units/CH4 82")
(format-stock-check (stock-check :blue-energy :units/CH4))
;; => ("Name: Blue Energy, Funds: 900, :units/CH4 10")

;; 정상 transaction.

;; 실패시 어떤 현상을 보이는지 보여주기.
(xt/submit-tx
 node
 [[::xt/match
   :gold-harmony
   {:xt/id :gold-harmony
    :company-name "Gold Harmony"
    :seller? true
    :buyer? false
    :units/Au 10211
    :credits 51}]
  [::xt/put
   {:xt/id :gold-harmony
    :company-name "Gold Harmony"
    :seller? true
    :buyer? false
    :units/Au 211
    :credits 51}]

  [::xt/match
   :encompass-trade
   {:xt/id :encompass-trade
    :company-name "Encompass Trade"
    :seller? true
    :buyer? true
    :units/Au 10
    :units/Pu 5
    :units/CH4 211
    :credits 100002}]
  [::xt/put
   {:xt/id :encompass-trade
    :company-name "Encompass Trade"
    :seller? true
    :buyer? true
    :units/Au 10010
    :units/Pu 5
    :units/CH4 211
    :credits 1002}]])

(xt/sync node)

(format-stock-check (stock-check :gold-harmony :units/Au))
(format-stock-check (stock-check :encompass-trade :units/Au))


;; 가장 최근 적하목록(badge)를 업데이트 하라.
(xt/submit-tx
 node
 [[::xt/put
   {:xt/id :manifest
    :pilot-name "Johanna"
    :id/rocket "SB002-sol"
    :id/employee "22910x2"
    :badges ["SETUP" "UP" "DATALOG-QUERIES" "BITEMP" "MATCH"]
    :cargo ["stereo" "gold fish" "slippers" "secret note"]}]])

(xt/sync node)

;; 그러는 동안 포터가 지구에 있는 카알랑에게 준 메모를 아직 가지고 있는지 확인한다.
(xt/q (xt/db node)
      '{:find [belongings]
        :where [[e :cargo belongings]]
        :in [belongings]}
      "secret note")
;; belongings에 "secret node" 가 있으면 리턴함.


;; 6: Delete with XTDB - Jupiter Assignment
;; 지정된 유효 시간에 문서를 삭제한다. 문서의 이전 버전은 계속 사용할 수 있다.
;; 삭제 작업에는 시작 및 종료 유효 시간을 포함하는 옵션과 함께 유효한 Entity id가 필요하다.
;; 문서는 시작기준 또는 시작 및 종료 유효시간(제공한 경우) 사이에 삭제됨.
;; 유효시간 창을 벗어난 문서의 이전 버전은 보존된다.
;; 형태는 [::xt/delete eid valid-time-start valid-time-end]
(xt/submit-tx node
              [[::xt/put {:xt/id :kaarlang/clients
                          :clients [:encompass-trade]}
                #inst "2110-01-01T09"
                #inst "2111-01-01T09"]
               [::xt/put {:xt/id :kaarlang/clients
                          :clients [:encompass-trade :blue-energy]}
                #inst "2111-01-01T09"
                #inst "2113-01-01T09"]
               [::xt/put {:xt/id :kaarlang/clients
                          :clients [:blue-energy]}
                #inst "2113-01-01T09"
                #inst "2114-01-01T09"]
               [::xt/put {:xt/id :kaarlang/clients
                          :clients [:blue-energy :gold-harmony :tombaugh-resources]}
                #inst "2114-01-01T09"
                #inst "2115-01-01T09"]])

(xt/sync node)

(xt/entity-history
 (xt/db node #inst "2116-01-01T09")
 :kaarlang/clients
 :desc
 {:with-docs? true})
;; => [#:xtdb.api{:tx-time #inst "2023-08-23T12:59:18.452-00:00",
;;                :tx-id 4,
;;                :valid-time #inst "2115-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "0000000000000000000000000000000000000000",
;;                :doc nil}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T12:59:18.452-00:00",
;;                :tx-id 4,
;;                :valid-time #inst "2114-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "d4bca6c78409d9d40ee42319a8aec32bffad9030",
;;                :doc
;;                {:clients
;;                 [:blue-energy :gold-harmony :tombaugh-resources],
;;                 :xt/id :kaarlang/clients}}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T12:59:18.452-00:00",
;;                :tx-id 4,
;;                :valid-time #inst "2113-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "000e5b775b55d06f0bddc77d736184284aa1e4e9",
;;                :doc {:clients [:blue-energy], :xt/id :kaarlang/clients}}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T12:59:18.452-00:00",
;;                :tx-id 4,
;;                :valid-time #inst "2111-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "cd71551fe21219db59067ce7483370fdebaae8b0",
;;                :doc
;;                {:clients [:encompass-trade :blue-energy],
;;                 :xt/id :kaarlang/clients}}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T12:59:18.452-00:00",
;;                :tx-id 4,
;;                :valid-time #inst "2110-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "5ec42ea653288e01e1a9d7d2068b4658416177e0",
;;                :doc
;;                {:clients [:encompass-trade], :xt/id :kaarlang/clients}}]


;; kaarlang을 특정 기간에 전체기록을 삭제한다.
(xt/submit-tx
 node
 [[::xt/delete :kaarlang/clients #inst "2110-01-01" #inst "2116-01-01"]])

(xt/sync node)

(xt/entity-history
 (xt/db node #inst "2115-01-01T08")
 :kaarlang/clients
 :desc
 {:with-docs? true})
;; => [#:xtdb.api{:tx-time #inst "2023-08-23T13:00:52.815-00:00",
;;                :tx-id 5,
;;                :valid-time #inst "2114-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "0000000000000000000000000000000000000000",
;;                :doc nil}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T13:00:52.815-00:00",
;;                :tx-id 5,
;;                :valid-time #inst "2113-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "0000000000000000000000000000000000000000",
;;                :doc nil}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T13:00:52.815-00:00",
;;                :tx-id 5,
;;                :valid-time #inst "2111-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "0000000000000000000000000000000000000000",
;;                :doc nil}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T13:00:52.815-00:00",
;;                :tx-id 5,
;;                :valid-time #inst "2110-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "0000000000000000000000000000000000000000",
;;                :doc nil}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T13:00:52.815-00:00",
;;                :tx-id 5,
;;                :valid-time #inst "2110-01-01T00:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "0000000000000000000000000000000000000000",
;;                :doc nil}]

;; 삭제된 것이 보일 것이다.
;; 하지만 삭제된 문서는 다시 검색해야 하는 경우 언제든 검색할 수 있다.
;; 이러한 유효시간 삭제는 소프트 삭제이므로 언제든지 검색할 수 있다.
(xt/entity-history
 (xt/db node #inst "2115-01-01T08")
 :kaarlang/clients
 :desc
 {:with-docs? true
  :with-corrections? true})
;; => [#:xtdb.api{:tx-time #inst "2023-08-23T13:00:52.815-00:00",
;;                :tx-id 5,
;;                :valid-time #inst "2114-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "0000000000000000000000000000000000000000",
;;                :doc nil}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T12:59:18.452-00:00",
;;                :tx-id 4,
;;                :valid-time #inst "2114-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "d4bca6c78409d9d40ee42319a8aec32bffad9030",
;;                :doc
;;                {:clients
;;                 [:blue-energy :gold-harmony :tombaugh-resources],
;;                 :xt/id :kaarlang/clients}}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T13:00:52.815-00:00",
;;                :tx-id 5,
;;                :valid-time #inst "2113-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "0000000000000000000000000000000000000000",
;;                :doc nil}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T12:59:18.452-00:00",
;;                :tx-id 4,
;;                :valid-time #inst "2113-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "000e5b775b55d06f0bddc77d736184284aa1e4e9",
;;                :doc {:clients [:blue-energy], :xt/id :kaarlang/clients}}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T13:00:52.815-00:00",
;;                :tx-id 5,
;;                :valid-time #inst "2111-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "0000000000000000000000000000000000000000",
;;                :doc nil}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T12:59:18.452-00:00",
;;                :tx-id 4,
;;                :valid-time #inst "2111-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "cd71551fe21219db59067ce7483370fdebaae8b0",
;;                :doc
;;                {:clients [:encompass-trade :blue-energy],
;;                 :xt/id :kaarlang/clients}}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T13:00:52.815-00:00",
;;                :tx-id 5,
;;                :valid-time #inst "2110-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "0000000000000000000000000000000000000000",
;;                :doc nil}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T12:59:18.452-00:00",
;;                :tx-id 4,
;;                :valid-time #inst "2110-01-01T09:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "5ec42ea653288e01e1a9d7d2068b4658416177e0",
;;                :doc
;;                {:clients [:encompass-trade], :xt/id :kaarlang/clients}}
;;     #:xtdb.api{:tx-time #inst "2023-08-23T13:00:52.815-00:00",
;;                :tx-id 5,
;;                :valid-time #inst "2110-01-01T00:00:00.000-00:00",
;;                :content-hash
;;                #xtdb/id "0000000000000000000000000000000000000000",
;;                :doc nil}]

;; 7. Evict with XTDB - The secret mission
(def node (xt/start-node {}))

(xt/submit-tx node
              [[::xt/put
                {:xt/id :person/kaarlang
                 :full-name "Kaarlang"
                 :origin-planet "Mars"
                 :identity-tag :KA01299242093
                 :DOB #inst "2040-11-23"}]

               [::xt/put
                {:xt/id :person/ilex
                 :full-name "Ilex Jefferson"
                 :origin-planet "Venus"
                 :identity-tag :IJ01222212454
                 :DOB #inst "2061-02-17"}]

               [::xt/put
                {:xt/id :person/thadd
                 :full-name "Thad Christover"
                 :origin-moon "Titan"
                 :identity-tag :IJ01222212454
                 :DOB #inst "2101-01-01"}]

               [::xt/put
                {:xt/id :person/johanna
                 :full-name "Johanna"
                 :origin-planet "Earth"
                 :identity-tag :JA012992129120
                 :DOB #inst "2090-12-07"}]])

(xt/sync node)

;; 삭제 전, 
(defn full-query
  [node]
  (xt/q
   (xt/db node)
   '{:find [(pull e [*])]
     :where [[e :xt/id id]]}))

(full-query node)

;; evict operation에 잊혀질 권리를 행사할 수 있는 유일한 사람은 Kaarlang뿐이라고 한다.
(xt/submit-tx node [[::xt/evict :person/kaarlang]])

(xt/sync node)
(full-query node)

;; 지정된 :xt/id 와 관련된 모든 데이터는 eid 자체와 함께 XTDB에서 제거되었다.
;; transaction history는 불변이다. 즉, transaction은 절대 제거되지 않는다.
;; document가 XTDB에서 완전히 제거되었는지 확인하려면 각 사람에 대한
;; 기록 내림차순(history-descending) 정보를 확인하여 이를 확인할 수 있다.
(xt/entity-history (xt/db node)
                   :person/kaarlang
                   :desc
                   {:with-docs? true})
;; 보이지 않음 이제.

;; 8. Await with XTDB - Depra-5 Assignment
;; 우주를 돌아다니는 동안 몸이 무거워짐을 ㄷ느낌.
;; 오랫동안ㄱ 극저온 상태에서 몸이 약해진 것인지 새로운 행성의 중력이 더 강한 것인지 궁금해진다.

(def node (xt/start-node {}))
(def stats
  [{:body "Sun"
    :type "Star"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 109.3
    :volume 1305700
    :mass 33000
    :gravity 27.9
    :xt/id :Sun}
   {:body "Jupiter"
    :type "Gas Giant"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 10.97
    :volume 1321
    :mass 317.83
    :gravity 2.52
    :xt/id :Jupiter}
   {:body "Saturn"
    :type "Gas Giant"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius :volume
    :mass :gravity
    :xt/id :Saturn}
   {:body "Saturn"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 9.14
    :volume 764
    :mass 95.162
    :gravity 1.065
    :type "planet"
    :xt/id :Saturn}
   {:body "Uranus"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 3.981
    :volume 63.1
    :mass 14.536
    :gravity 0.886
    :type "planet"
    :xt/id :Uranus}
   {:body "Neptune"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 3.865
    :volume 57.7
    :mass 17.147
    :gravity 1.137
    :type "planet"
    :xt/id :Neptune}
   {:body "Earth"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 1
    :volume 1
    :mass 1
    :gravity 1
    :type "planet"
    :xt/id :Earth}
   {:body "Venus"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 0.9499
    :volume 0.857
    :mass 0.815
    :gravity 0.905
    :type "planet"
    :xt/id :Venus}
   {:body "Mars"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 0.532
    :volume 0.151
    :mass 0.107
    :gravity 0.379
    :type "planet"
    :xt/id :Mars}
   {:body "Ganymede"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 0.4135
    :volume 0.0704
    :mass 0.0248
    :gravity 0.146
    :type "moon"
    :xt/id :Ganymede}
   {:body "Titan"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 0.4037
    :volume 0.0658
    :mass 0.0225
    :gravity 0.138
    :type "moon"
    :xt/id :Titan}
   {:body "Mercury"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 0.3829
    :volume 0.0562
    :mass 0.0553
    :gravity 0.377
    :type "planet"
    :xt/id :Mercury}])

(xt/submit-tx node (mapv (fn [stat] [::xt/put stat]) stats))


(xt/submit-tx
 node
 [[::xt/put
   {:body "Kepra-5"
    :units {:radius "Earth Radius"
            :volume "Earth Volume"
            :mass "Earth Mass"
            :gravity "Standard gravity (g)"}
    :radius 0.6729
    :volume 0.4562
    :mass 0.5653
    :gravity 1.4
    :type "planet"
    :xt/id :Kepra-5}]])


(xt/sync node)

(sort
 (xt/q
  (xt/db node)
  '{:find [g planet]
    :where [[planet :gravity g]]}))

(defn ingest-and-query
  [traveller-doc]
  (xt/submit-tx node [[::xt/put traveller-doc]])
  (xt/q
   (xt/db node)
   '{:find [n]
     :where [[id :passport-number n]]
     :in [id]}
   (:xt/id traveller-doc)))

(ingest-and-query
 {:xt/id :origin-planet/test-traveller
  :chosen-name "Test"
  :given-name "Test Traveller"
  :passport-number (java.util.UUID/randomUUID)
  :stamps []
  :penalties []})


(defn ingest-and-query
  "Ingests the given traveller's document into XTDB, returns the passport number 
  once the transaction is complete."
  [traveller-doc]
  (xt/await-tx node
               (xt/submit-tx node [[::xt/put traveller-doc]]))
  (xt/q
   (xt/db node)
   '{:find [n]
     :where [[id :passport-number n]]
     :in [id]}
   (:xt/id traveller-doc)))

(ingest-and-query
 {:xt/id :origin-planet/test-traveller
  :chosen-name "Testy"
  :given-name "Test Traveller"
  :passport-number (java.util.UUID/randomUUID)
  :stamps []
  :panalties []})

;; XTDB는 비동기가 기본이다. transaction을 central transaction log에 제출하면,
;; 나중에 색인을 생성해준다. 색인이 없으면 읽을 수 없는데, 생성될 때까지 기다리는
;; await-tx를 써야한다.
;; 대량의 배치를 ingesting 하는 경우, await-tx는 상당히 느려지므로,
;; you only need to await the final transaction to know that all of the preceding transactions are
;; available.

(ingest-and-query
 {:xt/id :earth/ioelena
  :chosen-name "Ioelena"
  :given-name "Johanna"
  :passport-number (java.util.UUID/randomUUID)
  :stamps []
  :penalties []})

