(ns core
  (:require [aleph.http :as http]))

(defn handler [req]
  {:status 200
   :header {"content-type" "text/plain"}
   :body "hello!"})

(http/start-server handler {:port 8888})


(require
 '[manifold.deferred :as d]
 '[byte-streams :as bs])

(-> @(http/get "http://google.com/")
    :body
    bs/to-string
    prn)

(d/chain (http/get "https://google.com")
         :body
         bs/to-string
         prn)
