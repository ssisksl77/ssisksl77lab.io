(ns wdm
  (:require [clojure.string :as str]
))
;; wdm metadata excel -> csv -> format


(def columns [:start-date :trade-company-name :trade-company-no
              :community :artist-member :wbs-code :merchant-id :product-id :product-name :settlement-category-no :settlement-category-name :settlement-category-code :add-time])
(def wdm-csv (-> (slurp "wdm-meta.csv")
                 (str/split-lines)))

(->> wdm-csv
     first)
(->> wdm-csv 
     (map #(str/split % #",")))



