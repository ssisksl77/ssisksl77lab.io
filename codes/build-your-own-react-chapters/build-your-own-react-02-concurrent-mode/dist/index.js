function createElement(type, props, ...children) {
  return {
    type,
    props: {
      ...props,
      children: children.map(child => typeof child == "object" ? child : createTextElement(child))
    }
  };
}
function createTextElement(text) {
  return {
    type: "TEXT_ELEMENT",
    props: {
      nodeValue: text,
      children: []
    }
  };
}
function createDom(fiber) {
  const dom = fiber.type == "TEXT_ELEMENT" ? document.createTextNode("") : document.createElement(fiber.type);
  const isProperty = key => key !== "children";
  Object.keys(fiber.props).filter(isProperty).forEach(name => {
    dom[name] = fiber[name];
  });
  return dom;
}
const isEvent = key => key.startsWith("on");
const isProperty = key => key !== "children" && !isEvent(key);
const isNew = (prev, next) => key => prev[key] !== next[key];
const isGone = (prev, next) => key => !(key in next);
function updateDom(dom, prevProps, nextProps) {
  // Remove old or changed event listeners
  Object.keys(prevProps).filter(isProperty).filter(isGone(prevProps, nextProps)).forEach(name => {
    dom.removeEventListener(name, prevProps[name]);
  });

  // Remove old properties
  Object.keys(prevProps).filter(isProperty).filter(isGone(prevProps, nextProps)).forEach(name => {
    dom[name] = "";
  });

  // Set new or changed properties
  Object.keys(nextProps).filter(isProperty).filter(isNew(prevProps, nextProps)).forEach(name => {
    dom[name] = nextProps[name];
  });

  // Add event listeners
  Object.keys(nextProps).filter(isProperty).filter(isNew(prevProps, nextProps)).forEach(name => {
    dom.addEventListener(name, nextProps[name]);
  });
}
function commitRoot() {
  commitWork(wipRoot.child);
  // 수정 & 제거를 위한 비교를 위해 현재 루트를 저장한다.
  currentRoot = wipRoot;
  wipRoot = null;
  deletions = [];
}
function commitWork(fiber) {
  if (!fiber) {
    return;
  }
  let domParentFiber = fiber.parent;
  while (!domParentFiber.dom) {
    // DOM node의 부모를 찾으려면, fiber tree를 거슬러 올라가야 한다.
    domParentFiber = domParentFiber.parent;
  }
  const domParent = domParentFiber.dom;

  // 새로만드는 거
  if (fiber.effectTag === "PLACEMENT" && fiber.dom != null) {
    domParent.appendChild(fiber.dom);
  } else if (fiber.effectTag === "UPDATE" && fiber.dom != null) {
    updateDom(fiber.dom, fiber.alternate.props, fiber.props);
  } else if (fiber.effectTag === "DELETION") {
    // 함수컴포넌트의 경우, fiber.dom이 없으므로 
    // 삭제할 때도 DOM node의 부모를 찾아야 한다. 
    commitDeletion(fiber, domParent);
  }
  commitWork(fiber.child);
  commitWork(fiber.sibling);
}
function commitDeletion(fiber, domParent) {
  if (fiber.dom) {
    domParent.removeChild(fiber.dom);
  } else {
    // fiber.dom 을 만날 때까지 올라감.
    commitDeletion(fiber.child, domParent);
  }
}
function render(element, container) {
  // In the render function we set `nextUnitOfWork` to the root of the fiber tree.
  wipRoot = {
    dom: container,
    props: {
      children: [element]
    },
    alternate: currentRoot // 이전 fiber tree를 가리키는 포인터
  };

  deletions = [];
  nextUnitOfWork = wipRoot;
}
let nextUnitOfWork = null;
let currentRoot = null;
let wipRoot = null;
let deletions = null;
function workLoop(deadline) {
  let shouldYield = false;
  while (nextUnitOfWork && !shouldYield) {
    nextUnitOfWork = performUnitOfWork(nextUnitOfWork);
    shouldYield = deadline.timeRemaining() < 1;
  }

  // 다음 단위 작업이 없고, wipRoot가 있으면 commitRoot를 호출한다.
  // we commit the whole fiber tree at once.
  if (!nextUnitOfWork && wipRoot) {
    commitRoot();
  }

  // setTimeout으로 생각할 수도 있지만 실행 시점을 알려주는 대신 기본 스레드가 유휴 상태일 때 콜백을 실행함.
  requestIdleCallback(workLoop);
}
function performUnitOfWork(fiber) {
  const isFunctionComponent = fiber.type instanceof Function;
  if (isFunctionComponent) {
    // 함수 컴포넌트의 경우, 함수를 호출하여 자식을 가져온다.
    updateFunctionComponent(fiber);
  } else {
    // 일반 컴포넌트의 경우, 자식은 props.children이다.
    updateHostComponent(fiber);
  }
  if (fiber.child) {
    return fiber.child;
  }
  let nextFiber = fiber;
  while (nextFiber) {
    if (nextFiber.sibling) {
      return nextFiber.sibling;
    }
    nextFiber = nextFiber.parent;
  }
}
function updateFunctionComponent(fiber) {
  // 함수 컴포넌트의 경우, 함수를 호출하여 자식을 가져온다.
  // 함수 컴포넌트의 경우 type 프로퍼티가 함수가된다. 그곳에 props 인자를 전파해서 실행하여, JSX(createElement리턴값)를 리턴한다.
  const children = [fiber.type(fiber.props)];
  reconcileChildren(fiber, children);
}
function updateHostComponent(fiber) {
  // add dom node
  if (!fiber.dom) {
    fiber.dom = createDom(fiber);
  }
  reconcileChildren(fiber, fiber.props.children);
}
function reconcileChildren(wipFiber, elements) {
  let index = 0;
  let oldFiber = wipFiber.alternate && wipFiber.alternate.child;
  let prevSibling = null;
  while (index < elements.length || oldFiber != null) {
    const element = elements[index];
    let newFiber = null;

    // TODO compare oldFiber to element
    const sameType = oldFiber && element && element.type == oldFiber.type;
    if (sameType) {
      // update the node
      newFiber = {
        type: oldFiber.type,
        dom: oldFiber.dom,
        parent: wipFiber,
        props: element.props,
        alternate: oldFiber,
        effectTag: "UPDATE"
      };
    }
    if (element && !sameType) {
      // add this node
      newFiber = {
        type: element.type,
        dom: null,
        parent: wipFiber,
        props: element.props,
        alternate: null,
        effectTag: "PLACEMENT"
      };
    }
    if (oldFiber && !sameType) {
      // delete the oldFiber's node
      oldFiber.effectTag = "DELETION";
      deletions.push(oldFiber);
    }
    if (oldFiber) {
      oldFiber = oldFiber.sibling;
    }
    if (index === 0) {
      wipFiber.child = newFiber;
    } else if (element) {
      prevSibling.sibling = newFiber;
    }
    prevSibling = newFiber;
    index++;
  }
}
const Diact = {
  createElement,
  render
};

/** @jsx Diact.createElement */
function App(props) {
  return Diact.createElement("h1", null, "Hi ", props.name);
}
const element = Diact.createElement(App, {
  name: "foo"
});
// 아래처럼 변할 것
// function App(props) {
//     return Diact.createElement(
//         "h1",
//         null,
//         "Hi ",
//         props.name
//     )
// }

// 함수 컴포넌트의 경우, type이 함수컴포넌트가 들어옴(문자가 아님)
// const element = Diact.createElement(
//     App,
//     { name: "foo" }
// )

const container = document.getElementById("root");
Diact.render(element, container);

// when brower is ready, it will call our workLoop and we'll start working on the root.
requestIdleCallback(workLoop);