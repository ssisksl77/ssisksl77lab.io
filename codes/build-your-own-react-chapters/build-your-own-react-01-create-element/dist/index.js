function createElement(type, props, ...children) {
  return {
    type,
    props: {
      ...props,
      children: children.map(child => typeof child == "object" ? child : createTextElement(child))
    }
  };
}
function createTextElement(text) {
  return {
    type: "TEXT_ELEMENT",
    props: {
      nodeValue: text,
      children: []
    }
  };
}
const Didact = {
  createElement
};

// const element = Didact.createElement(
//     "div",
//     { id: "foo"},
//     Didact.createElement("a", null, "bar"),
//     Didact.createElement("b")
// )
// 아래 코드가 위로 변형되는 것
/** @jsx Didact.createElement */
const element = Didact.createElement("div", {
  id: "foo"
}, Didact.createElement("a", null, "bar"), Didact.createElement("b", null));
function render(element, container) {
  const dom = element.type == "TEXT_ELEMENT" ? document.createTextNode("") : document.createElement(element.type);

  // 가상 엘리먼트에 매개변수로 있는 property들을 진짜 node에 주입한다.
  const isProperty = key => key !== "children";
  Object.keys(element.props).filter(isProperty).forEach(name => {
    dom[name] = element.props[name];
  });
  element.props.children.forEach(child => render(child, dom));
  container.appendChild(dom);
}
const Diact = {
  createElement,
  render
};
const container = document.getElementById("root");
Diact.render(element, container);