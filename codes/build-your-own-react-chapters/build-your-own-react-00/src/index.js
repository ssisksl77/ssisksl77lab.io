// const element = <h1 title="foo">Hello</h1>
// React.createElement 는 그저 아래 인자로 객체를 만들뿐이다.
const element = {
    type: "h1",
    props: {
        title: "foo",
        children : "Hello MyFriend",
    },
}

const container = document.getElementById("root")

const node = document.createElement(element.type)
node["title"] = element.props.title

const text = document.createTextNode("")
text["nodeValue"] = element.props.children

node.appendChild(text)

container.appendChild(node)

