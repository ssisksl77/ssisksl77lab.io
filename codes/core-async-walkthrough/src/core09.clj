(ns core09)


;;; ioc-macro
go 매크로를 통해 스레드를 명시적으로 관리하지않고 비동기 채널을 사용할 수 있다.

protocols:
 - `IInstruction` : instruction 타입은 `reads-from`, `write-to`, `block-references` 메서드를 구현해야함.
 - `IEmittableInstruction` : instruction을 표현하는 clojure code를 리턴하는 경우 `emit-instruction` 메소드를 구현해야함.
 - `ITerminator` : 블록을 종료하는 명령을 가지려면 `terminator-code` `terminate-block` 을 구현해야함.
instruction:
Const, RawCode, CustomTerminator, Recur, Call, StaticCall, InstanceInterop, Case, Fn, Dot, Jmp, Return, CondBr, PushTry, PopTry, CatchHandler 그리고 EndFinally
와 같은 다양한 명령어 유형이 정의도어 있다.
각 명령어 유형은 위에서 언급한 프로토콜 중 하나 이상을 구현하며, go 매크로 내에서 수행할 수 있는 다양한 연산을 나타낸다.
interaction
core.async 라이브러리는 go 매크로 내의 Clojure코드를 이러한 명령어 시퀀스로 변환한다.
instruction (명령어)는 달느 명령어에서 값을 읽고 쓰고, 다른 코드 블록을 참조하고, 클로저 코드를 방출할 수 있다.
이러한 instructions 은 명시적인 스레드 관리 없이도 core.async가 실행할 수 있는 상태 머신형태를 가진다.

;; We're going to reduce Clojure expressions to a ssa format,
;; and then translate the instructions for this
;; virtual-virtual-machine back into Clojure data.

;; Here we define the instructions:

(defprotocol IInstruction
  (reads-from [this] "Returns a list of instructions this instruction reads from")
  (writes-to [this] "Returns a list of instructions this instruction writes to")
  (block-references [this] "Returns all the blocks this instruction references"))

(defprotocol IEmittableInstruction
  (emit-instruction [this state-sym] "Returns the clojure code that this instruction represents"))

(defprotocol ITerminator
  (terminator-code [this] "Returns a unique symbol for this instruction")
  (terminate-block [this state-sym custom-terminators] "Emites the code to terminate a given block"))

1. `Const` 상수 값을 나타내는 레코드이다.
     `value` 값이 `::value` 인 경우 런타임 상태에 보관된 값에 대한 Clojure 코드를 반환함.
     `value` 값이 있으면 상수를 리턴하는 Clojure 코드를 반환함.
(defrecord Const [value]
  IInstruction
  (reads-from [_this] [value])
  (writes-to [this] [(:id this)])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [this state-sym]

    (if (= value ::value)
      `[~(:id this) (rt/aget-object ~state-sym ~rt/VALUE-IDX)]
      `[~(:id this) ~value])))

2. `RawCode` : 임의의 클로저 코드를 나타내는 레코드입니다.
    `read-from` 메서드는 AST에서 사용된 로컬 변수를 기반으로 읽은 명령어 목록을 반환한다. (AST?)
    `emit-instruction` 메서드는 `read-from`에 필요한 추가 바인딩을 포함하여 AST에 대한 Clojure 코드를 반환합니다.
(defrecord RawCode [ast locals]
  IInstruction
  (reads-from [_this]
    (for [local (map :name (-> ast :env :locals vals))
          :when (contains? locals local)]
      (get locals local)))
  (writes-to [this] [(:id this)])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [this _state-sym]
    (if (not-empty (reads-from this))
      `[~@(into []
                (comp
                 (map #(select-keys % [:op :name :form]))
                 (filter (fn [local]
                           (contains? locals (:name local))))
                 (distinct)
                 (mapcat
                  (fn [local]
                    `[~(:form local) ~(get locals (:name local))])))
                (-> ast :env :locals vals))
        ~(:id this) ~(:form ast)]
      `[~(:id this) ~(:form ast)])))


3. `CustomTerminator` : 커스텀 종료 명령을 나타내는 레코드.
    블록종료 메서드는 제공되는 함수 `f` , 상태 심볼 (state symbol), 기타 값을 사용하여 블록을 종료하는 클로저 코드를 생성.
(defrecord CustomTerminator [f blk values meta]
  IInstruction
  (reads-from [_this] values)
  (writes-to [_this] [])
  (block-references [_this] [])
  ITerminator
  (terminate-block [_this state-sym _]
    (with-meta `(~f ~state-sym ~blk ~@values)
      meta)))

4. Recur 재귀 명령 레코드
    emit-instruction 메서드는 `recur` node value를 지정된ID에 바인딩하고, 필요한 경우 임시 바인딩으로 충돌하는 바인딩을 처리하는 Clojure코드를 방출한다.
    재귀는 `recur` node에 ID만 넣으면 되나보다.
(defn- emit-clashing-binds
  [recur-nodes ids clashes]
  (let [temp-binds (reduce
                    (fn [acc i]
                      (assoc acc i (gensym "tmp")))
                    {} clashes)]
    (concat
     (mapcat (fn [i]
               `[~(temp-binds i) ~i])
             clashes)
     (mapcat (fn [node id]
               `[~node ~(get temp-binds id id)])
             recur-nodes
             ids))))

(defrecord Recur [recur-nodes ids]
  IInstruction
  (reads-from [_this] ids)
  (writes-to [_this] recur-nodes)
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [_this _state-sym]
    (if-let [overlap (seq (set/intersection (set recur-nodes) (set ids)))]
      (emit-clashing-binds recur-nodes ids overlap)
      (mapcat (fn [r i]
                `[~r ~i]) recur-nodes ids))))

5. Call : 함수와 인수를 읽어오는 함수 호출을 나타냄?
    emit-instruction은 주어진 인수를 사용하여 함수를 호출하는 Clojure 코드를 방출함.
(defrecord Call [refs]
  IInstruction
  (reads-from [_this] refs)
  (writes-to [this] [(:id this)])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [this _state-sym]
    `[~(:id this) ~(seq refs)]))

6. StaticCall : 제공된 인수를 가진 클래스의 정적 메소드 호출을 나타낸다.
    emit-instruction 메서드는 주어진 인수를 사용하여 클래스의 정적 메서드를 호출하는 클로저를 방출한다.

(defrecord StaticCall [class method refs]
  IInstruction
  (reads-from [_this] refs)
  (writes-to [this] [(:id this)])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [this _state-sym]
    `[~(:id this) (. ~class ~method ~@(seq refs))]))

7. InstanceInterop : 객체에 대한 인스턴스 메서드 호출 또는 필드 엑세스를 나타낸다.
    emit-instruction 메서드는 인스턴스 메서드를 호출하거나 지정된 인수를 사용하여 객체의 필드에 엑세스하는 Clojure코드를 방출한다.
(defrecord InstanceInterop [instance-id op refs]
  IInstruction
  (reads-from [_this] (cons instance-id refs))
  (writes-to [this] [(:id this)])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [this _state-sym]
    `[~(:id this) (. ~instance-id ~op ~@(seq refs))]))
8. Case : case표현식을 나타냄. 여러 test value -> jump block 모음으로 이루어짐.
    terminate-block 메서드는 각 테스트 값에 대해 값을 테스트하고 테스트 값이 일치하지 않으면 해당 블록 또는 기본 블록으로 점프하는 Clojure코드를 방출.
(defrecord Case [val-id test-vals jmp-blocks default-block]
  IInstruction
  (reads-from [_this] [val-id])
  (writes-to [_this] [])
  (block-references [_this] [])
  ITerminator
  (terminate-block [_this state-sym _]
    `(do (case ~val-id
           ~@(concat (mapcat (fn [test blk]
                               `[~test (rt/aset-all! ~state-sym ~rt/STATE-IDX ~blk)])
                             test-vals jmp-blocks)
                     (when default-block
                       `[(do (rt/aset-all! ~state-sym ~rt/STATE-IDX ~default-block)
                             :recur)])))
         :recur)))
9. Fn : 지정된 body와 로컬 변수 바인딩이 있는 함수 정의를 나타냄.
    emit-instruction 메서드는 주어진 지역 변수 바인딩으로 함수를 정의하는 Clojure 코드를 방출함.
(defrecord Fn [fn-expr local-names local-refs]
  IInstruction
  (reads-from [_this] local-refs)
  (writes-to [this] [(:id this)])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [this _state-sym]
    `[~(:id this)
      (let [~@(interleave local-names local-refs)]
        ~@fn-expr)]))
10. Dot : 객체 또는 클래스에 대한 메서드 호출을 나타냄.
    emit-instruction 메서드는 메서드 호출, 지정된 인수를 사용하여 객체 또는 클래스 필드에 엑세스하는 Clojure코드 방출.
(defrecord Dot [cls-or-instance method args]
  IInstruction
  (reads-from [_this] `[~cls-or-instance ~method ~@args])
  (writes-to [this] [(:id this)])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [this _state-sym]
    `[~(:id this) (. ~cls-or-instance ~method ~@args)]))
11. Jmp : 다른 블록으로 이동.
    terminate-block 메서드는 지정된 블록으로 점프하도록 런타임 상태를 설정하는 클로저 코드 방출.
(defrecord Jmp [value block]
  IInstruction
  (reads-from [_this] [value])
  (writes-to [_this] [])
  (block-references [_this] [block])
  ITerminator
  (terminate-block [_this state-sym _]
    `(do (rt/aset-all! ~state-sym ~rt/VALUE-IDX ~value ~rt/STATE-IDX ~block)
         :recur)))
12 Return : go block을 종료하는 return 명령을 나타냄.
    `terminate-block` 은 런타임 상태의 값을 설정하고 상태를 완료로 표시하는 클로저 코드 방출.
(defrecord Return [value]
  IInstruction
  (reads-from [_this] [value])
  (writes-to [_this] [])
  (block-references [_this] [])
  ITerminator
  (terminator-code [_this] :Return)
  (terminate-block [this state-sym custom-terminators]
    (if-let [f (get custom-terminators (terminator-code this))]
      `(~f ~state-sym ~value)
      `(do (rt/aset-all! ~state-sym ~rt/VALUE-IDX ~value ~rt/STATE-IDX ::finished)
           nil))))
13 CondBr : 조건 분기를 나타냄.
    terminate-block은 값을 테스트하고 결과에 따라 then block 또는 else 블록으로 점프하는 클로저 코드 방출.
(defrecord CondBr [test then-block else-block]
  IInstruction
  (reads-from [_this] [test])
  (writes-to [_this] [])
  (block-references [_this] [then-block else-block])
  ITerminator
  (terminate-block [_this state-sym _]
    `(do (if ~test
           (rt/aset-all! ~state-sym ~rt/STATE-IDX ~then-block)
           (rt/aset-all! ~state-sym ~rt/STATE-IDX ~else-block))
         :recur)))
14 PushTry : 예외 처리기 스택에 캐치 블록을 추시하는 것을 나타냄.
    emit-instruction 메서드는 런타임 상태에서 캐치 블록을 예외 처리기 스택으로 푸시하는 Clojure코드 방출.
(defrecord PushTry [catch-block]
  IInstruction
  (reads-from [_this] [])
  (writes-to [_this] [])
  (block-references [_this] [catch-block])
  IEmittableInstruction
  (emit-instruction [_this state-sym]
    `[~'_ (rt/aset-all! ~state-sym ~rt/EXCEPTION-FRAMES (cons ~catch-block (rt/aget-object ~state-sym ~rt/EXCEPTION-FRAMES)))]))

15 PopTry : 스택에서 캐치 블록을 pop함하는 것을 타나냄.
    emit-instruction 메서드는 런타임 상태에서 예외 처리기 스택에서 캐치 블록을 pop 하는 클로저 코드 방출.
(defrecord PopTry []
  IInstruction
  (reads-from [_this] [])
  (writes-to [_this] [])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [_this state-sym]
    `[~'_ (rt/aset-all! ~state-sym ~rt/EXCEPTION-FRAMES (rest (rt/aget-object ~state-sym ~rt/EXCEPTION-FRAMES)))]))


16 CatchHandler : 예외 처리하는 캐치 블록을 나타냄.
    terminate-block 메서드는 예외를 포착하고 예외 유형에 따라 적절한 핸들러 블록으로 점프하는 Clojure코드를 방출.
    지정된 핸들러 블록 중 예외 유형과 일치하는 블럭이 없으면 예외가 다시 발생.
(defrecord CatchHandler [catches]
  IInstruction
  (reads-from [_this] [])
  (writes-to [_this] [])
  (block-references [_this] (map first catches))
  ITerminator
  (terminate-block [_this state-sym _]
    (let [ex (gensym 'ex)]
      `(let [~ex (rt/aget-object ~state-sym ~rt/VALUE-IDX)]
         (cond
           ~@(for [[handler-idx type] catches
                   i [`(instance? ~type ~ex) `(rt/aset-all! ~state-sym ~rt/STATE-IDX ~handler-idx)]]
               i)
           :else (throw ~ex))
         :recur))))

17 EndFinally : Represents the end of a finally block, where an exception should be rethrown. 예외를 다시 던져야 하는 finally 블록의 마지막을 나타냄 (?)
    emit-instruction 은 지정된 지역 변수에 저장된 예외를 다시 던지는 클로저 코드를 방출.

(defrecord EndFinally [exception-local]
  IInstruction
  (reads-from [_this] [exception-local])
  (writes-to [_this] [])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [_this _state-sym]
    `[~'_ (throw ~exception-local)]))

요약하면 ioc_macro.clj는 core.async에서 go 블록에 대한 코드의 중간 표현을 구축하는데 사용되는 명령어 유형 집합을 정의한다. (record)
그런 다음 이러한 명령어는 비동기 작업을 활성화하기 위해 상태 머신에서 실행되는 Clojure 코드로 방출된다. (IEmittableInstruction/emit-instruction)
Clojure 코드로 방출되는 방시긍ㄹ 정의하는 하나 이상의 프로토콜 (IInstruction, IEmittableInstruction, ITerminator)



### ImitabbleInstruction에 대해서 알아보자.
방출되는 코드는 일반적으로 Clojure 구문을 따르는 기호, 키워드, 벡터 및 목록의 조합이다.
이 코드를 나중에 평가하면 함수 호출, 필드 엑세스, 다른 블록으로의 점프 등 원하는 명령의 효과를 생성한다.

예시
(emit-instruction [this state-sym]
  (if (= value ::value)
    `[~(:id this) (rt/aget-object ~state-sym ~rt/VALUE-IDX)]
    `[~(id:this) ~value]))
이 경우 emit-instruction 메서드는 명령어 ID에 값을 할당하는 것을 나타내는 클로저 코드를 방출함.
만약 `value` 가 `::value` 인 경우 런타임 상태에서 값을 검색한다. (`rt/aget-object`) . 아니면 직접할당한다.

값의 형태는 아래와 같다.
`[(:id this) <value_expression>]`

emit-instruction 반환값을 사용하려면, 생성된 코드가 일반적으로 let바인딩 또는 함수 본문 내의 적절한 위치에 삽입된다.
생성도니 코드를 평가하는 동안 방출된 명령어가 나타내는 순서대로 실행되어 블록 간 이동, 예외 처리, 함수 호출 수행 등 프로그램이 원하는 작업을 수행할 수 있다.

명렁어 방출 프로세스는 core.async 라이브러리에 구현된 상태 머신과 함께 작동한다.
상태 머신은 방출된 명령어의 실행을 관리하며 메인 스레드를 차단하지 않고 비동기 연산을 원활하게 수행할 수 있도로 한다.

(require '[clojure.core.async :as async])

(async/go
  (let [c (chan 10)]
    (println "HELLO")
    (async/>! c "MAN")
    (println (async/<! c))
    (println (str "is this ") "possible")))

(comment
  (def c-const (->Const 0 (async/chan 10)))

  (def println-hello (->RawCode 1 (quote (println "HELLO")) {}))

  (def put-c-man (->Call 2 [c-const "MAN"]))

  (def take-c (->Call 3 [c-const]))

  (def str-is-this (->RawCode 4 (list 'str "is this " (:id take-c)) {}))

  (def println-concat (->RawCode 5 (list 'println (:id str-is-this)) {}))

  (def insts [c-const
              println-hello
              put-c-man
              take-c
              str-is-this
              println-concat])
  (defn emit-inst [inst state-sym]
    (if (satisfies? IEmittableInstruction inst)
      (emit-instruction inst state-sym)
      '()))
  (def state-sym (gensym "state")))



STATE-SYM에 대해서 알아보자.
Jmp는 이것을 사용해서 점프를 수행함.
Jmp:
  (rt/aset-all! ~state-sym ~rt/VALUE-IDX ~value ~rt/STATE-IDX ~block): 값과 상태 인덱스를 상태머신에 씀
    state-sym: 상태머신 심볼
    rt/VALUE-IDX: 상태머신에 값이 저장되어 있는 상태임
    value: 저장할 값.
    rt/STATE-IDX: 상태 머신에 저장되어있는 상태 인덱
    block: 다음으로 점프할 인덱스
PushTry:
  (rt/aset-all! ~state-sym ~rt/EXCEPTION-FRAMES (cons ~catch-block (rt/aget-object ~state-sym ~rt/EXCEPTION-FRAMES))): 예외 프레임 스택에 캐치 블록을 푸시한다.
    state-sym: 상태머신 심볼
    rt/EXCEPTION-FRAMES: 상태 머신에서 예외 프레임이 저장되는 인덱스
    catch-block: 예외 프레임 스택에 푸시할 캐치 블록 인덱스
    (rt/aget-object ~state-sym ~rt/EXCEPTION-FRAMES): 현재 예외 프레임 스택을 가져온다.
Case:
  (rt/aset-all! ~state-sym ~rt/STATE-IDX ~blk): 상태 머신의 상태 인덱스를 설정한다.
    state-sym: 상태 머신 심볼.
    rt/STATE-IDX: 상태 인덱스가 상태 머신에 저장되는 인덱스
    blk: 점프할 블록 인덱스

cheat sheet :
상태 머신의 여러 인덱스에 값을 설정한다.
