(ns core13)

parse-to-state-machine은 S-표현식 본문과 종결자 집합을 받는 함수입니다. 이
함수는 주어진 본문을 해시맵으로 변환하여 S-표현식의 실행 흐름을 일련의 SSA(정적
단일 할당) 스타일 블록으로 설명합니다. 다음은 함수를 한 줄씩 분석한 내용입니다:
(defn parse-to-state-machine
  "Takes an sexpr and returns a hashmap that describes the execution flow of the sexpr as
   a series of SSA style blocks."
  [body terminators]
  (-> (gen-plan
       [_ (push-binding :terminators terminators)
        ;;
        blk (add-block)
        _ (set-block blk)
        ;;
        id (item-to-ssa body)
        term-id (add-instruction (->Return id))
        ;; 이제 불필요해진 바인딩은 제거해야함.
        _ (pop-binding :terminators)]
       term-id)
      get-plan))

(gen-plan ,,,)
1. The gen-plan macro is used to generate a plan. The plan is a set of
sequential steps, and each step in the plan is a pair of a function and its
arguments. The gen-plan macro allows you to describe complex plans in a more
readable and concise way.
-> gen-plan 매크로는 plan을 생성한다. plan은 순차적인 단계의 집합이며, 각 계획은 함수와 그 인수의 쌍으로 구성된다.

[_ (push-binding :terminators terminators)]
2. The first step in the plan is to push the terminators (user transitions) into
the binding. This makes the terminators available throughout the transformation.

-> 계획의 첫 번째 단계는 터미네이터(사용자 전환)를 바인딩에 푸시하는 것입니다.
   이렇게 하면 전환 전반에 걸쳐 터미네이터를 사용할 수 있게 됩니다.

[blk (add-block)]
3. The second step is to add a new block to the plan using the add-block
function. A block is a basic unit in SSA representation, which groups a series
of instructions together. The add-block function returns a block identifier blk.

-> 두 번째 단계는 블록 추가 기능을 사용하여 계획에 새 블록을 추가하는 것입니다.
   블록은 일련의 명령어를 함께 그룹화하는 SSA 표현의 기본 단위입니다. 블록 추가
   함수는 블록 식별자 blk를 반환합니다.

_ (set-block blk)
4. The third step is to set the current block to the newly created block blk.
This makes the block blk the target for any subsequent instructions added to the
plan.

-> 세 번째 단계는 현재 블록을 새로 생성된 'blk' 블럭으로 설정하는 것입니다.
   이렇게 하면 'blk' 블록이 계획에 추가되는 모든 후속 지침 (instructions)의 대상이 됩니다.

[id (item-to-ssa)]
5. The fourth step is to convert the body S-expression into SSA form using the
item-to-ssa function. This function recursively processes the body and returns
an identifier id for the last instruction in the SSA form.

-> 네 번째 단계는 item-to-ssa 함수를 사용하여 본문 S-표현식을 SSA 형식으로
   변환하는 것입니다. 이 함수는 본문을 재귀적으로 처리하고 SSA 형식의 마지막
   인스트럭션에 대한 식별자 ID를 반환합니다.

[term-id (add-instruction (->Return id))]
6. The fifth step is to add a return instruction to the plan using the
add-instruction function. The ->Return constructor creates a new return
instruction, which uses the id from the previous step as its value. The return
instruction is added to the current block, and its identifier is stored in
term-id.

-> 다섯 번째 단계는 추가 명령어 함수를 사용하여 플랜에 반환 명령어를 추가하는
   것입니다. 반환 생성자는 이전 단계의 ID를 값으로 사용하는 새 반환 인스트럭션을
   생성합니다. 반환 명령어가 현재 블록에 추가되고 해당 식별자는 term-id에
   저장됩니다.

[_ (pop-binding :terminators)]
7. The sixth and final step is to pop the terminators binding, as it is no longer needed.

-> 여섯 번째이자 마지막 단계는 터미네이터 바인딩이 더 이상 필요하지 않으므로 터미네이터 바인딩을 해제하는 것입니다.

get-plan
8. The get-plan function is called on the result of the gen-plan macro to
actually generate and return the final plan, which is a hashmap that represents
the execution flow of the body as a series of SSA style blocks.

-> get-plan 함수는 gen-plan 매크로의 결과에 대해 호출되어 실제로 본문의 실행
   흐름을 일련의 SSA 스타일 블록으로 표현하는 해시맵인 최종 계획을 생성하고
   반환합니다.


add-block과 set-block으로 만든 blk는 직접적으로는 사용되지 않고 있다.
하지만 간접적으로 사용하고 있다고 한다. 이에 대한 예시를 요청했다.

(comment
  (ns example.core
  (:require [clojure.core.async :refer [go chan <! >! timeout]])
  (:gen-class))

(defn simple-example []
  (let [c (chan)
        d (chan)]
    (go
      (println "Sending 42 to channel c")
      (>! c 42)
      (println "Sent 42 to channel c"))
    (go
      (println "Receiving from channel c")
      (let [val (<! c)]
        (println "Received" val "from channel c")
        (>! d val)
        (println "Sent" val "to channel d")))))

(defn -main [& args]
  (simple-example)
  (Thread/sleep 1000))
;;
)

이 예제에는 채널 c와 d를 통해 통신하는 두 개의 이동 블록이 있습니다. 첫 번째
이동 블록은 값 42를 채널 c로 보내고, 두 번째 이동 블록은 채널 c에서 값을 수신한
다음 채널 d로 보냅니다.


이제 core.async가 내부적으로 이 코드를 parse-to-state-machine을 사용하여 SSA 표현으로 변환하는 방법을 살펴봅시다.

1. core.async는 go 블록의 코드를 분석하고 clojure.tools.analyzer 라이브러리를 사용하여 AST를 생성합니다.
2. 그런 다음 코드의 다른 부분 간의 전환을 처리하도록 AST를 수정합니다(예: 채널에서 값을 보내고 받을 때).
3. 마지막으로, 파스 투 스테이트 머신을 호출하여 수정된 AST를 SSA 표현으로 변환합니다.

`parse-to-state-machine` 함수는 처음에 새 블록을 초기화하여 현재 블록으로 설정한
다음 item-to-ssa를 사용하여 바둑 블록의 본문을 처리합니다. 그 결과 명령어가 기본
블록으로 그룹화되는 SSA 표현을 생성합니다.


(defmacro gen-plan
  "Allows a user to define a state monad binding plan.
  (gen-plan
    [_ (assoc-in-plan [:foo :bar] 42)
     val (get-in-plan [:foo :bar])]
    val)"
  [binds id-expr]
  (let [binds (partition 2 binds)
        psym (gensym "plan_")
        forms (reduce
               (fn [acc [id expr]]
                 (concat acc `[[~id ~psym] (~expr ~psym)]))
               []
               binds)]
    `(fn [~psym]
       (let [~@forms]
         [~id-expr ~psym]))))

(gen-plan
 [_ (push-binding :terminators terminators)
  ;;
  blk (add-block)
  _ (set-block blk)
  ;;
  id (item-to-ssa body)
  term-id (add-instruction (->Return id))
  ;; 이제 불필요해진 바인딩은 제거해야함.
  _ (pop-binding :terminators)]
 term-id)

;; 이거 실제 ioc_macro.clj에 가서 실행해보면 잘 나옴.
(-> (gen-plan
     [_ (push-binding :terminators {})
      blk (add-block)
      _ (set-block blk)
      id (item-to-ssa {:op :const :form 1})
      term-id (add-instruction (->Return id))]
     term-id)
    get-plan)
;; inst_22729는 내부적으로 item-to-ssa가 만든 (gensym 값임)
;; gen-plan의 리턴값임
;; (fn [psym]
;;   (let [~form]
;;     [~id ~psym]))
;; 이 형태를 보면 벡터를 리턴하는데 바로 이게 끝임.
;; ~form, ~id는 모두 gen-plan할 때 만든거임. id는 무슨 소용인지
;; 모르겠음. 일단 나중에 `second` 한 후에 emit 하는 코드가 있는 거로 봐서
;; 예상대로 별 쓸모 없는 값임.(현재로는 다른 곳에선 중요한듯 id가 위치기 때문)
;; id또한 다른 곳에서 만들어줌 (block-id 를 만들면 생기는 아이디인듯)
;;
;; 그렇다면 psym은 뭐냐 get-plan에서는 {}을 넣음 (상태가 없다는 뜻)
;; 이것은 상태 모나드라고 하는듯. 이제 리턴하면서 상태가 개 많아짐
;; 왜냐면 받아서 psym을 리턴하니까...
;; 상태가 많아지는 경우는 다른 item-to-ssa를 수행할 때 인듯함.
;; => [inst_22729
;;     {:bindings {:terminators ({})},
;;      :block-id 1,
;;      :blocks {1 [{:value 1, :id inst_22729}]},
;;      :block-catches {1 nil},
;;      :start-block 1,
;;      :current-block 1}]

getplan에 forms 가 이거임 이것이 함수 내 let 반딩을 한 함수를 리턴함. 예를들
([_ plan_24516] ((push-binding :terminators {}) plan_24516)
 [blk plan_24516] ((add-block) plan_24516)
 [_ plan_24516] ((set-block blk) plan_24516)
 [id plan_24516] ((item-to-ssa {:op :const, :form 1}) plan_24516)
 [term-id plan_24516] ((add-instruction (->Return id)) plan_24516)
 ;;
 )

(fn []
  (let [[_ plan_24516] ((push-binding :terminators {}) plan_24516)
        [blk plan_24516] ((add-block) plan_24516)
        [_ plan_24516] ((set-block blk) plan_24516)
        [id plan_24516] ((item-to-ssa {:op :const, :form 1}) plan_24516)
        [term-id plan_24516] ((add-instruction (->Return id)) plan_24516)
        ]
    ;; term-id는 마지막 리턴 id
    ;; plan_은 해당 함수에서 만든 (gensym "plan_")
    [term-id plan_26412]))


(require '[clojure.inspector :refer [inspect inspect-tree]])
(inspect-tree (-> (gen-plan
                   [_ (push-binding :terminators {})
                    blk (add-block)
                    _ (set-block blk)
                    id (item-to-ssa {:op :const :form 1})
                    id2 (item-to-ssa {:op :const :form 5})
                    term-id (add-instruction (->Return id2))]
                   term-id)
                  ((fn [x] (println x) x))
                  get-plan))


이 다음에 second한 이후 emit-state-machine을 수행한다.

다시 아래 코드로 오자
(go
  (let [c (chan 2)]
    (>! c "VALUE")
    (println (+ 1 2))
    (println "HELLO " (<! c))))
;; 아래 출력값은 내가 state-machine 함수 맨 위에 인자들을 출력한 것.
{:body (do (let [c (chan 2)]
             (>! c VALUE)
             (println (+ 1 2))
             (println HELLO  (<! c)))),
 :num-user-params 1,
 :crossing-env {},
 :env nil,
 :user-transitions {clojure.core.async/<! clojure.core.async.impl.ioc-macros/take!, clojure.core.async/>! clojure.core.async.impl.ioc-macros/put!, clojure.core.async/alts! clojure.core.async/ioc-alts!, :Return clojure.core.async.impl.ioc-macros/return-chan}}

state-machine에서 (parse-to-state-machine user-transition) 리턴값을 출력해보자.
[inst_27991
 {:bindings {:terminators (), :locals ()},
  :block-id 3,
  :blocks
  {1
   [{:ast
     {:op :invoke,
      :form (chan 2),
      :env
      {:context :ctx/expr,
       :locals {},
       :ns clojure.core.async,
       :file
       "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
       :column 11,
       :line 472},
      :fn
      {:op :var,
       :assignable? false,
       :var #'clojure.core.async/chan,
       :meta
       {:arglists
        ([] [buf-or-n] [buf-or-n xform] [buf-or-n xform ex-handler]),
        :doc
        "Creates a channel with an optional buffer, an optional transducer\n  (like (map f), (filter p) etc or a composition thereof), and an\n  optional exception-handler.  If buf-or-n is a number, will create\n  and use a fixed buffer of that size. If a transducer is supplied a\n  buffer must be specified. ex-handler must be a fn of one argument -\n  if an exception occurs during transformation it will be called with\n  the Throwable as an argument, and any non-nil return value will be\n  placed in the channel.",
        :line 81,
        :column 1,
        :file
        "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
        :name chan,
        :ns #namespace[clojure.core.async]},
       :env
       {:context :ctx/expr,
        :locals {},
        :ns clojure.core.async,
        :file
        "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
        :column 11,
        :line 472},
       :form chan,
       :o-tag java.lang.Object,
       :arglists
       ([] [buf-or-n] [buf-or-n xform] [buf-or-n xform ex-handler])},
      :args
      [{:op :const,
        :env
        {:context :ctx/expr,
         :locals {},
         :ns clojure.core.async,
         :file
         "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
         :column 11,
         :line 472},
        :type :number,
        :literal? true,
        :val 2,
        :form 2,
        :o-tag long,
        :tag java.lang.Long}],
      :meta {:line 472, :column 11},
      :children [:fn :args],
      :o-tag java.lang.Object},
     :locals nil,
     :id inst_27982}
    {:value inst_27982, :id inst_27983}
    {:f clojure.core.async.impl.ioc-macros/put!,
     :blk 2,
     :values [inst_27983 "VALUE"],
     :meta {:line 473, :column 5},
     :id inst_27984}],
   2
   [{:value :clojure.core.async.impl.ioc-macros/value, :id inst_27985}
    {:ast
     {:op :invoke,
      :form (println (+ 1 2)),
      :env
      {:context :ctx/statement,
       :locals
       {c
        {:op :binding,
         :name c__#0,
         :init
         {:op :invoke,
          :form (chan 2),
          :env
          {:context :ctx/expr, :locals {}, :ns clojure.core.async},
          :fn
          {:op :var,
           :assignable? false,
           :var #'clojure.core.async/chan,
           :meta
           {:arglists
            ([]
             [buf-or-n]
             [buf-or-n xform]
             [buf-or-n xform ex-handler]),
            :doc
            "Creates a channel with an optional buffer, an optional transducer\n  (like (map f), (filter p) etc or a composition thereof), and an\n  optional exception-handler.  If buf-or-n is a number, will create\n  and use a fixed buffer of that size. If a transducer is supplied a\n  buffer must be specified. ex-handler must be a fn of one argument -\n  if an exception occurs during transformation it will be called with\n  the Throwable as an argument, and any non-nil return value will be\n  placed in the channel.",
            :line 81,
            :column 1,
            :file
            "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
            :name chan,
            :ns #namespace[clojure.core.async]},
           :env
           {:context :ctx/expr, :locals {}, :ns clojure.core.async},
           :form chan},
          :args
          [{:op :const,
            :env
            {:context :ctx/expr, :locals {}, :ns clojure.core.async},
            :type :number,
            :literal? true,
            :val 2,
            :form 2}],
          :meta {:line 472, :column 11},
          :children [:fn :args]},
         :form c,
         :local :let,
         :children [:init]}},
       :ns clojure.core.async,
       :file
       "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
       :column 5,
       :line 474},
      :fn
      {:op :var,
       :assignable? false,
       :var #'clojure.core/println,
       :meta
       {:added "1.0",
        :ns #namespace[clojure.core],
        :name println,
        :file "clojure/core.clj",
        :static true,
        :column 1,
        :line 3734,
        :arglists ([& more]),
        :doc "Same as print followed by (newline)"},
       :env
       {:context :ctx/expr,
        :locals
        {c
         {:op :binding,
          :name c__#0,
          :init
          {:op :invoke,
           :form (chan 2),
           :env
           {:context :ctx/expr, :locals {}, :ns clojure.core.async},
           :fn
           {:op :var,
            :assignable? false,
            :var #'clojure.core.async/chan,
            :meta
            {:arglists
             ([]
              [buf-or-n]
              [buf-or-n xform]
              [buf-or-n xform ex-handler]),
             :doc
             "Creates a channel with an optional buffer, an optional transducer\n  (like (map f), (filter p) etc or a composition thereof), and an\n  optional exception-handler.  If buf-or-n is a number, will create\n  and use a fixed buffer of that size. If a transducer is supplied a\n  buffer must be specified. ex-handler must be a fn of one argument -\n  if an exception occurs during transformation it will be called with\n  the Throwable as an argument, and any non-nil return value will be\n  placed in the channel.",
             :line 81,
             :column 1,
             :file
             "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
             :name chan,
             :ns #namespace[clojure.core.async]},
            :env
            {:context :ctx/expr, :locals {}, :ns clojure.core.async},
            :form chan},
           :args
           [{:op :const,
             :env
             {:context :ctx/expr, :locals {}, :ns clojure.core.async},
             :type :number,
             :literal? true,
             :val 2,
             :form 2}],
           :meta {:line 472, :column 11},
           :children [:fn :args]},
          :form c,
          :local :let,
          :children [:init]}},
        :ns clojure.core.async,
        :file
        "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
        :column 5,
        :line 474},
       :form println,
       :o-tag java.lang.Object,
       :arglists ([& more])},
      :args
      [{:args
        [{:op :const,
          :env
          {:context :ctx/expr,
           :locals
           {c
            {:op :binding,
             :name c__#0,
             :init
             {:op :invoke,
              :form (chan 2),
              :env
              {:context :ctx/expr, :locals {}, :ns clojure.core.async},
              :fn
              {:op :var,
               :assignable? false,
               :var #'clojure.core.async/chan,
               :meta
               {:arglists
                ([]
                 [buf-or-n]
                 [buf-or-n xform]
                 [buf-or-n xform ex-handler]),
                :doc
                "Creates a channel with an optional buffer, an optional transducer\n  (like (map f), (filter p) etc or a composition thereof), and an\n  optional exception-handler.  If buf-or-n is a number, will create\n  and use a fixed buffer of that size. If a transducer is supplied a\n  buffer must be specified. ex-handler must be a fn of one argument -\n  if an exception occurs during transformation it will be called with\n  the Throwable as an argument, and any non-nil return value will be\n  placed in the channel.",
                :line 81,
                :column 1,
                :file
                "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
                :name chan,
                :ns #namespace[clojure.core.async]},
               :env
               {:context :ctx/expr,
                :locals {},
                :ns clojure.core.async},
               :form chan},
              :args
              [{:op :const,
                :env
                {:context :ctx/expr,
                 :locals {},
                 :ns clojure.core.async},
                :type :number,
                :literal? true,
                :val 2,
                :form 2}],
              :meta {:line 472, :column 11},
              :children [:fn :args]},
             :form c,
             :local :let,
             :children [:init]}},
           :ns clojure.core.async,
           :file
           "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
           :column 14,
           :line 474},
          :type :number,
          :literal? true,
          :val 1,
          :form 1,
          :o-tag long,
          :tag long}
         {:op :const,
          :env
          {:context :ctx/expr,
           :locals
           {c
            {:op :binding,
             :name c__#0,
             :init
             {:op :invoke,
              :form (chan 2),
              :env
              {:context :ctx/expr, :locals {}, :ns clojure.core.async},
              :fn
              {:op :var,
               :assignable? false,
               :var #'clojure.core.async/chan,
               :meta
               {:arglists
                ([]
                 [buf-or-n]
                 [buf-or-n xform]
                 [buf-or-n xform ex-handler]),
                :doc
                "Creates a channel with an optional buffer, an optional transducer\n  (like (map f), (filter p) etc or a composition thereof), and an\n  optional exception-handler.  If buf-or-n is a number, will create\n  and use a fixed buffer of that size. If a transducer is supplied a\n  buffer must be specified. ex-handler must be a fn of one argument -\n  if an exception occurs during transformation it will be called with\n  the Throwable as an argument, and any non-nil return value will be\n  placed in the channel.",
                :line 81,
                :column 1,
                :file
                "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
                :name chan,
                :ns #namespace[clojure.core.async]},
               :env
               {:context :ctx/expr,
                :locals {},
                :ns clojure.core.async},
               :form chan},
              :args
              [{:op :const,
                :env
                {:context :ctx/expr,
                 :locals {},
                 :ns clojure.core.async},
                :type :number,
                :literal? true,
                :val 2,
                :form 2}],
              :meta {:line 472, :column 11},
              :children [:fn :args]},
             :form c,
             :local :let,
             :children [:init]}},
           :ns clojure.core.async,
           :file
           "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
           :column 14,
           :line 474},
          :type :number,
          :literal? true,
          :val 2,
          :form 2,
          :o-tag long,
          :tag long}],
        :children [:args],
        :method add,
        :op :static-call,
        :env
        {:context :ctx/expr,
         :locals
         {c
          {:op :binding,
           :name c__#0,
           :init
           {:op :invoke,
            :form (chan 2),
            :env
            {:context :ctx/expr, :locals {}, :ns clojure.core.async},
            :fn
            {:op :var,
             :assignable? false,
             :var #'clojure.core.async/chan,
             :meta
             {:arglists
              ([]
               [buf-or-n]
               [buf-or-n xform]
               [buf-or-n xform ex-handler]),
              :doc
              "Creates a channel with an optional buffer, an optional transducer\n  (like (map f), (filter p) etc or a composition thereof), and an\n  optional exception-handler.  If buf-or-n is a number, will create\n  and use a fixed buffer of that size. If a transducer is supplied a\n  buffer must be specified. ex-handler must be a fn of one argument -\n  if an exception occurs during transformation it will be called with\n  the Throwable as an argument, and any non-nil return value will be\n  placed in the channel.",
              :line 81,
              :column 1,
              :file
              "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
              :name chan,
              :ns #namespace[clojure.core.async]},
             :env
             {:context :ctx/expr, :locals {}, :ns clojure.core.async},
             :form chan},
            :args
            [{:op :const,
              :env
              {:context :ctx/expr, :locals {}, :ns clojure.core.async},
              :type :number,
              :literal? true,
              :val 2,
              :form 2}],
            :meta {:line 472, :column 11},
            :children [:fn :args]},
           :form c,
           :local :let,
           :children [:init]}},
         :ns clojure.core.async,
         :file
         "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
         :column 14,
         :line 474},
        :o-tag long,
        :class clojure.lang.Numbers,
        :form (. clojure.lang.Numbers (add 1 2)),
        :tag java.lang.Long,
        :validated? true,
        :raw-forms ((+ 1 2))}],
      :meta {:line 474, :column 5},
      :children [:fn :args],
      :o-tag java.lang.Object},
     :locals {c__#0 inst_27983},
     :id inst_27986}
    {:ast
     {:op :var,
      :assignable? false,
      :var #'clojure.core/println,
      :meta
      {:added "1.0",
       :ns #namespace[clojure.core],
       :name println,
       :file "clojure/core.clj",
       :static true,
       :column 1,
       :line 3734,
       :arglists ([& more]),
       :doc "Same as print followed by (newline)"},
      :env
      {:context :ctx/expr,
       :locals
       {c
        {:op :binding,
         :name c__#0,
         :init
         {:op :invoke,
          :form (chan 2),
          :env
          {:context :ctx/expr, :locals {}, :ns clojure.core.async},
          :fn
          {:op :var,
           :assignable? false,
           :var #'clojure.core.async/chan,
           :meta
           {:arglists
            ([]
             [buf-or-n]
             [buf-or-n xform]
             [buf-or-n xform ex-handler]),
            :doc
            "Creates a channel with an optional buffer, an optional transducer\n  (like (map f), (filter p) etc or a composition thereof), and an\n  optional exception-handler.  If buf-or-n is a number, will create\n  and use a fixed buffer of that size. If a transducer is supplied a\n  buffer must be specified. ex-handler must be a fn of one argument -\n  if an exception occurs during transformation it will be called with\n  the Throwable as an argument, and any non-nil return value will be\n  placed in the channel.",
            :line 81,
            :column 1,
            :file
            "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
            :name chan,
            :ns #namespace[clojure.core.async]},
           :env
           {:context :ctx/expr, :locals {}, :ns clojure.core.async},
           :form chan},
          :args
          [{:op :const,
            :env
            {:context :ctx/expr, :locals {}, :ns clojure.core.async},
            :type :number,
            :literal? true,
            :val 2,
            :form 2}],
          :meta {:line 472, :column 11},
          :children [:fn :args]},
         :form c,
         :local :let,
         :children [:init]}},
       :ns clojure.core.async,
       :file
       "/Users/tom/github/core.async/src/main/clojure/clojure/core/async.clj",
       :column 5,
       :line 475},
      :form println,
      :o-tag java.lang.Object,
      :arglists ([& more])},
     :locals {c__#0 inst_27983},
     :id inst_27987}
    {:f clojure.core.async.impl.ioc-macros/take!,
     :blk 3,
     :values [inst_27983],
     :meta {:line 475, :column 23},
     :id inst_27988}],
   3
   [{:value :clojure.core.async.impl.ioc-macros/value, :id inst_27989}
    {:refs [inst_27987 "HELLO " inst_27989], :id inst_27990}
    {:value inst_27990, :id inst_27991}]},
  :block-catches {1 nil, 2 nil, 3 nil},
  :start-block 1,
  :current-block 3}]

-> second를 조지고 inspect-tree를 하면 inst_28065키 하나에 해시맵 하나가 있다.
  {inst_28065 {:bindings
               :block-id 3
               :blocks {1 [RawCode{:ast {:op :invoke
                                         :form (chan 2)
                                         :fn {:op :var :assignable? false
                                              :var #'clojure.core.async/chan
                                              :form chan}
                                         :args [{:op :const
                                                 :form 2
                                                 :val 2
                                                 :literal? true
                                                 :type :number
                                                 :tag java.lang>Long}]}
                                   :locals nil
                                   :id inst_28056}
                           Const{:value inst_28056, :id inst_28057}
                           Terminator{:f ioc-macros/put!
                                      :blk 2
                                      :values [inst_28057 "VALUE"]
                                      :id inst_27058}]
                        2 [Const{:value :clojure.core.async.impl.ioc-macros/value
                                 :id inst_28059}
                           RawCode{:ast {:op :invoke :form (println (+ 1 2))}
                                   :locals {c__#0 inst_28057}
                                   :id inst_28060}
                           RawCode{:ast {:op :var :assignable? false
                                         :var #'clojure.core/println
                                         :form println}
                                   :locals {c__#0 inst_28057}
                                   :id inst_28061}
                           CustomTerminator{:f clojure.core.async.impl.ioc-macros/take!
                                            :blk 3
                                            :values [inst_28057]
                                            :id inst_28062}]
                        3 [Const{:value :clojure.core.async.impl.ioc-macros/value
                                 :id inst_28063}
                           Call{:refs [inst_28061 "Hello" inst_28063]
                                :id inst_28064}
                           Return{:value inst_28064
                                  :id inst_28065}]}
               :start-block 1
               :current-block 3}}

;;; 이게
 (emit-state-machine num-user-params user-transitions) 값도 훑어보자.
근데 이미 이것은 함수가 되어 있다. 상태머신이 되어버린듯...
근데 받아서 바로 실행하는 코드가 있음.
(defmacro go
  [& body]
  (let [crossing-env (zipmap (keys &env) (repeatedly gensym))]
    `(let [c# (chan 1)
           captured-bindings# (clojure.lang.Var/getThreadBindingFrame)]
       (dispatch/run
         (^:once fn* []
          (let [~@(mapcat (fn [[l sym]] [sym `(^:once fn* [] ~(vary-meta l dissoc :tag))]) crossing-env)

                f# ~(ioc/state-machine `(do ~@body) 1 [crossing-env &env] ioc/async-custom-terminators)
                ;; 여기 상태머신을 바로 실행함.
                ;; 해보니까 이러면 바로 아래 값이 나옴
                ;; 첫번쨰 인자는 역시 자기 자신인가... 상태머신함수인가봄
                ;; 세팅하는게 USER-START-IDX == 5에다가 c#을 넣는데 이게 리턴채널일 것임
                ;; ioc/BINDINGS-IDX(3) captured-bindings 미리 바인딩된 값을 넣음
                ;; #object[java.util.concurrent.atomic.AtomicReferenceArray 0xb47496a [clojure.core.async$eval29001$fn__29002$state_machine__26276__auto____29003@64891eca, 1, null, clojure.lang.Var$Frame@7efdd5a1, null, clojure.core.async.impl.channels.ManyToManyChannel@18f0934b, null, null, null, null]]

                state# (-> (f#)
                           (ioc/aset-all! ioc/USER-START-IDX c#
                                          ioc/BINDINGS-IDX captured-bindings#)
                           ((fn [x#] (println x#) x#)))]
            (ioc/run-state-machine-wrapped state#))))
       c#)))

;; 여기서 run-state-machine-wrapped를 수행하면

(defn run-state-machine [state]
  ((aget-object state FN-IDX) state))

(defn run-state-machine-wrapped [state]
  (try
    (run-state-machine state)
    (catch Throwable ex
      ;; 실패하면 클로즈 하는 가봄.
      ;; 아까 알듯이 USER-START-IDX 에는 리턴 채널이 있음 얘를 닫으면 끝나나봄.
      (impl/close! (aget-object state USER-START-IDX))

      (throw ex))))

일단 state의 첫번째 함수 (상태함수)를 자겨와서 state(환경그자체)를 상태함수에다가 주입하면서
드디어 머신이 실행된다.
심심하네 aget-object 구경할까
(defn aget-object [^AtomicReferenceArray arr idx]
  (.get arr idx))
배열에서 값을 가져오는 것이다.
