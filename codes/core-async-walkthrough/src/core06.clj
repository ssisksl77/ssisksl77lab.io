(ns core06)

;;;; SSA example

#_(defn example-function [x y]
  (let [a x
        b y]
    (if (> a b)
      (do
        (set! a (- a b))
        (set! b (+ b a)))
      (do (set! a (+ a b))
          (set! b (- b a))))
    [a b]))
;; 위 코드를 ssa 형식으로 바꾸면
(defn example-function-ssa [x y]
  (let [a_1 x
        b_1 y]
    (if (> a_1 b_1)
      (let [a_2 (- a_1 b_1)
            b_2 (+ b_1 a_2)]
        [a_2 b_2])
      (let [a_3 (+ a_1 b_1)
            b_3 (- b_1 a_3)]
        [a_3 b_3]))))
;; go macro

SSA (static single assignment)를 생성하기 위한 상태모나드를 정의하는 코드들을 보자.
상태 모나드는 값을 변경하지 않고 상태를 유지하는 함수형 프로그래밍 기법이다.


1. `gen-plan` : 상태 모나드를 정의하기 위한 매크로, 상태 변경 계획을 정의하고. 그 결과를 반환한다.

(defmacro gen-plan
  "Allows a user to define a state monad binding plan.
  (gen-plan
    [_ (assoc-in-plan [:foo :bar] 42)
     val (get-in-plan [:foo :bar])]
    val)"
  [binds id-expr]
  (let [binds (partition 2 binds)
        psym (gensym "plan_")
        forms (reduce
               (fn [acc [id expr]]
                 (concat acc `[[~id ~psym] (~expr ~psym)]))
               []
               binds)]
    `(fn [~psym]
       (let [~@forms]
         [~id-expr ~psym]))))

(defn assoc-in-plan
  "Same as assoc-in, but for state hash map"
  [path val]
  (fn [plan]
    [val (assoc-in plan path val)]))

(defn get-in-plan
  "Same as get-in, but for a state hash map"
  [path]
  (fn [plan]
    [(get-in plan path) plan]))

(defn get-plan
  "Returns the final [id state] from a plan. "
  [f]
  (f {}))

;;;; 예시 1
(def my-plan
  (gen-plan
   [_ (assoc-in-plan [:a] 1)
    b (get-in-plan [:a])]
   b))

(get-plan my-plan)
;; => [1 {:a 1}]


;;;; 예시 2
(defn add-number [num]
  (fn [plan]
    [nil (update-in plan [:numbers] (fnil conj []) num)]))

(defn sum-numbers []
  (fn [plan]
    [(apply + (get-in plan [:numbers])) plan]))

(def my-number-plan
  (gen-plan
    [_ (add-number 5)
     _ (add-number 7)
     _ (add-number 3)  ;; 이게 상태 모나드인듯?
     total (sum-numbers)]
    total))

(macroexpand-1 '(gen-plan
                [_ (add-number 5)
                 _ (add-number 7)
                 _ (add-number 3)  ;; 이게 상태 모나드인듯?
                 total (sum-numbers)]
                total))
;; => (clojure.core/fn
;;     [plan_20405]
;;     (clojure.core/let
;;      [[_ plan_20405]
;;       ((add-number 5) plan_20405)
;;       [_ plan_20405]
;;       ((add-number 7) plan_20405)
;;       [_ plan_20405]
;;       ((add-number 3) plan_20405)
;;       [total plan_20405]
;;       ((sum-numbers) plan_20405)]
;;      [total plan_20405]))
;; 이렇게 보니까 이해가 된다.
;; get plan은 내부 코드를 보면 {}를  `gen-plan` 인자로 넣어줌.
;; 그러면 그것을 기반으로 add-number가 수행됨. 함수들을 잘 보면 바로 실행되는 것이 아니라, plan을 받을 때 수행됨. plan에 넣으면 해야할 일을 수행하는데
;; 보니까 plan에 값을 넣기만 하고 리턴하지 않는 `add-number`
;; plan의 값을 받아서 계산하고 값을 리턴하는 `sum-numbers`가 있는거임.
;; vector에서 오른쪽은 그러니까 계산 한 리턴값인데 없을 수도 있고, 있을 수도 있음. 하지만 오른쪽은 `plan` 즉, 상태이기 때문에 이것은 계속 유지되어야함.

(get-plan my-number-plan)
;; => [15 {:numbers [5 7 3]}]

2. get-plan: 상태 모나드의 최종 결과를 반환하는 함수입니다.

4. push-binding, get-binding, pop-binding: 상태를 변경하거나 검색하기 위한 함수들입니다.
        push-binding은 새로운 상태를 추가하고, get-binding은 현재 상태를 가져오며, pop-binding은 최근 상태를 제거합니다.
5. assoc-in-plan, update-in-plan, get-in-plan:
        상태 해시맵에서 값을 가져오거나 변경하는 함수입니다.
6. set-block, get-block, add-block: 블록을 설정하거나 검색하거나 추가하는 함수들입니다.
7. add-instruction: 현재 블록에 명령을 추가하는 함수입니다.

위의 함수들을 사용하여 상태 모나드를 정의하고 작업할 수 있습니다.
이러한 상태 모나드를 사용하면 클로저에서 SSA를 구성하고 관리할 수 있습니다.
SSA는 중간 코드 최적화를 수행할 때 주로 사용되는 프로그래밍 표현입니다. 이 코드는 core.async의 구현에 필요한 중요한 부분입니다.


(defn all
  "Assumes that itms is a list of state monad function results, threads the state map
  through all of them. Returns a vector of all the results."
  [itms]
  (fn [plan]
    (reduce
     (fn [[ids plan] f]
       (let [[id plan] (f plan)]
         [(conj ids id) plan]))
     [[] plan]
     itms)))

이건 뭐지... 예시
(defn add-one [n]
  (fn [plan]
    [(+ n 1) plan]))

(defn add-two [n]
  (fn [plan]
    [(+ n 2) plan]))

(defn add-three [n]
  (fn [plan]
    [(+ n 3) plan]))

(let [[results final-state] ((all [(add-one 1) (add-two 1) (add-three 1)]) {})]
  (println "Results:" results)
  (println "Final state:" final-state))
;; Results : [2 4 4]
;; Final state: {}

;; 용도 : 작업이 서로를 의존하면(변수로 사용하고 있으면) all을 이용해서 한 번에 받을 수 있다?
;; 변수 바인딩 여러 변수 바인딩이 필요하면 all로 한번에 가능!
(defn push-binding
  "Sets the binding 'key' to value. This operation can be undone via pop-bindings.
   Bindings are stored in the state hashmap."
  [key value]
  (fn [plan]
    [nil (update-in plan [:bindings key] conj value)]))

(defn pop-binding
  "Removes the most recent binding for key"
  [key]
  (fn [plan]
    [(first (get-in plan [:bindings key]))
     (update-in plan [:bindings key] pop)]))

(let [[results final-state] ((all [(push-binding :x 1)
                                   (push-binding :y 2)
                                   (pop-binding :x)])
                             {})]
  (println "Results:" results)
  (println "Final state:" final-state))
;; Results [nil nil 1]
;; Final state : {bindings {:x () :y (2)}}

;; 용도 2 : 복잡한 계산순서 : 여러 단계를 거쳐 값을 계산하거나 처리해야 하는 경우
;;         각 단계를 상태 모나드 함수로 정의한 다음 `all` 로 순차적 실행을 도모함.
(let [[results final-state] ((all [(add-number 3) (add-number 5) (sum-numbers)]) {})]
  (println "Results:" results)
  (println "Final state:" final-state))
;; Results [nil nil 8]
;; Final state: {:numbers [3 5]}

(defn push-binding
  "Sets the binding 'key' to value. This operation can be undone via pop-bindings.

;; Bindings are stored in the state hashmap."
  [key value]
  (fn [plan]
    [nil (update-in plan [:bindings key] conj value)]))

;; 현재 값을 바꿔서 push-binding 하는 듯?
(defn push-alter-binding
  "Pushes the result of (apply f old-value args) as current value of binding key"
  [key f & args]
  (fn [plan]
    [nil (update-in plan [:bindings key]
                    #(conj % (apply f (first %) args)))]))


(require '[clojure.core.async.impl.ioc-macros :refer [gen-plan get-plan push-binding get-binding push-alter-binding pop-binding print-plan no-op]])

(defn example-alter-binding []
  (let [my-plan (gen-plan
                  [_ (push-binding :y 0)
                   y1 (get-binding :y)
                   _ (push-alter-binding :y + 5)
                   y2 (get-binding :y)
                   _ (push-alter-binding :y * 3)
                   y3 (get-binding :y)
                   _ (pop-binding :y)
                   y4 (get-binding :y)]
                  [y1 y2 y3 y4])]
    (get-plan my-plan)))

(println (example-alter-binding))
;; =>[[0 5 15 5] {:bindings {:y (5 0)}}]

;;;
(defn print-plan []
  (fn [plan]
    (pprint plan)
    [nil plan]))

(defn add-number [n]
  (fn [plan]
    [n (update-in plan [:numbers] (fnil conj []) n)]))

(def my-number-plan
  (gen-plan
    [x (add-number 5)
     _ (print-plan)
     y (add-number 7)
     _ (print-plan)
     z (add-number 3)
     _ (print-plan)]
    [x y z]))

(get-plan my-number-plan)
;; => [[5 7 3] {:numbers [5 7 3]}]
;; 아래처럼 출력됨.
;; {:numbers [5]}
;; {:numbers [5 7]}
;; {:numbers [5 7 3]}

;;;;;;;;;;;; block????
1. `set-block` : 현재 작성 중인 블록을 설정하는 함수. `add-instruction` 호출은 이 블록에 추가됨 (??).
        블록ID를 인수로 사용하여 계획의 `:current-block` 키에 블록ID를 할당.
2. `get-block` : 현재 작성 중인 블록을 가져오는 함수. `:current-block` 키에서 블록ID를 가져와 반환한다.
3. `add-block` : 새로운 블록을 추가하는 함수. 블록ID를 반환하지만, 현재 작성 중인 블록을 변형하지 않음.
        이 함수는 `gen-plan` 을 사용하여 계획을 생성하며, 새로운 블록을 추가하고 필요한 정보를 설정한다.
        추가로 발생하는 catch를 가져와 블록에 연결하고 현재 블록이 설정되어 있지 않은 경우 시작블록으로 설정함.
`add-block`을 단계별로 훑어보자
(defn add-block
  "Adds a new block, returns its id, but does not change the current block (does not call set-block)."
  []
  (gen-plan
   [_ (update-in-plan [:block-id] (fnil inc 0))
    blk-id (get-in-plan [:block-id])
    cur-blk (get-block)
    _ (assoc-in-plan [:blocks blk-id] [])
    catches (get-binding :catch)
    _ (assoc-in-plan [:block-catches blk-id] catches)
    _ (if-not cur-blk
        (assoc-in-plan [:start-block] blk-id)
        (no-op))]
   blk-id))

1. `gen-plan`을 사용하여 상태 계획을 생성할 것이다.
2. [_ (update-in-plan [:block-id] (fnil inc 0))] - `:block-id`를 1 증가시키거나 처음 초기화함.
3. [blk-id (get-in-plan [:block-id]) - `:block-id` 에 할당된 새로운 블록ID를 가져와서 `blk-id` 에 넣어줌.] -
4. [cur-blk (get-block)] -  현재 작성중인 블록을 가져옴.
5. [_ (assoc-in-plan [:blocks blk-id] [])] - 새로운 블록을 상태 계획의 `:blocks` 키에 추가하고 빈 목록으로 초기화함. {:blocks {1 []}} 이런식으로.
6. [catches (get-binding :catch)
    _ (assoc-in-plan [:block-catches blk-id] catches)] - `catch` 바인딩 정보를 가져와서 새로운 블록의 `:block-catches` 에 추가한다.
7. [_ (if-not cur-blk
        (assoc-in-plan [:start-block] blk-id)
        (no-op))] - 현재 작성 중인 블록이 설정되어 있지 않다면, 새로운 블록을 시작블록으로 설정한다. 그렇지 않으면 아무 작업도 수행하지 않는다. (`no-op`)

예시1 - 두 개의 블록을 생성하고 각각에 명령 추가
(require '[clojure.core.async.impl.ioc-macros :refer [gen-plan get-plan push-binding get-binding push-alter-binding pop-binding print-plan no-op
                                                      add-block set-block add-instruction]])
(require '[clojure.pprint :refer [pprint]])
(def my-block-plan
  (gen-plan
   [block1 (add-block)
    _ (set-block block1)
    _ (add-instruction {:op :print, :text "Hello, block1!"})
    block2 (add-block)
    _ (set-block block2)
    _ (add-instruction {:op :print, :text "Hello, block 2!"})]
   nil))

(get-plan my-block-plan)
;; => [nil
;;     {:block-id 2,
;;      :blocks
;;      {1 [{:op :print, :text "Hello, block1!", :id inst_17648}],
;;       2 [{:op :print, :text "Hello, block 2!", :id inst_17649}]},
;;      :block-catches {1 nil, 2 nil},
;;      :start-block 1,
;;      :current-block 2}]

예시 2: 분기처리
(defn conditionally-print [condition]
  (gen-plan
   [block1 (add-block)
    _ (set-block block1)
    _ (add-instruction {:op :print, :text "Block 1"})
    block2 (add-block)
    _ (set-block block2)
    _ (add-instruction {:op :print, :text "Block 2"})
    _ (if condition
        (assoc-in-plan [:start-block] block1)
        (assoc-in-plan [:start-block] block2))]
   nil))

(def initial-state {:blocks {}
                    :block-id 0
                    :current-block nil
                    :start-block nil})

(defn execute-blocks [state]
  (let [state (second state)]
    (loop [block-id (:start-block state)]
      (let [block (get-in state [:blocks block-id])]
        (when block
          (doseq [instr block]
            (when (= (:op instr) :print)
              (println (:text instr))))
          (recur (get-in state [:next-blocks block-id])))))))

(def my-print-plan (conditionally-print false))
(def final-state (my-print-plan initial-state))
(execute-blocks final-state)

;;; TIP gen-plan 클로저 코드만으로 이해하기
(defn add-number [state n]
  (update state :numbers conj n))

(defn sum-numbers [state]
  (assoc state :sum (reduce + (:numbers state))))

(defn example []
  (-> {}
      (add-number 5)
      (add-number 7)
      (add-number 3)
      sum-numbers))

(println (example))

;; my-block-plan을 대체하는 함수
(defn my-basic-plan [state]
  (-> state
      (assoc :current-block 1)
      (assoc-in [:blocks 1] [])
      (assoc-in [:block-catches 1] [])
      (assoc :start-block 1)))

;; conditionally-print를 대체하는 함수
(defn basic-conditionally-print [state condition message]
  (if condition
    (do (println message)
        state)
    state))

;; loop-blocks를 대체하는 함수
(defn basic-loop-blocks [state f]
  (let [start-block (get state :start-block)]
    (loop [cur-block start-block]
      (when cur-block
        (let [block-data (get-in state [:blocks cur-block])]
          (f block-data)
          (recur (get-in state [:next-block cur-block])))))))

(def state (-> {}
               my-basic-plan))

(def new-state (basic-conditionally-print state true "Hello, world!"))

(basic-loop-blocks new-state (fn [block-data] (println "Block data:" block-data)))

이건 뭔지 모르겠는데 또 알려줘.
(defn conditionally-print [state condition]
  (let [block1 (assoc-in state [:blocks 1] [{:op :print, :text "Block 1"}])
        block2 (assoc-in state [:blocks 2] [{:op :print, :text "Block 2"}])]
    (if condition
      (assoc block1 :current-block 1)
      (assoc block2 :current-block 2))))

(defn execute-blocks [state]
  (let [current-block (get state :current-block)
        block (get-in state [:blocks current-block])]
    (doseq [instr block]
      (case (:op instr)
        :print (println (:text instr))))))

(def state (-> {}
               (assoc :current-block 1)
               (assoc :blocks {})))

(def new-state (conditionally-print state true))

(execute-blocks new-state)

;;; 뭔지 모르지만 또 알려줌.
(defn conditionally-print [condition]
  (let [block1 [{:op :print, :text "Block 1"}]
        block2 [{:op :print, :text "Block 2"}]
        selected-block (if condition block1 block2)]
    (for [instr selected-block]
      (when (= (:op instr) :print)
        (:text instr)))))

(println (conditionally-print true))
