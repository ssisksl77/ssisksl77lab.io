(ns core10)

;;;;;;; SSA
이 코드에는 최적화 및 코드 생성을 수행하기에 적합한 중간 표현인 SSA(정적 단일 할당) 형식으로 Clojure 구문 트리를 변환하기 위한 다양한 메서드의 구현이 포함되어 있습니다.
각 메서드는 defmethod를 사용하여 정의되며 :do, :case, :quote, :try 등과 같은 특정 Clojure 표현식 유형에 대해 작동합니다.
몇 가지 표현식 유형에 대한 -item-to-ssa 메서드의 구현을 세분화하고 작동 방식을 설명하겠습니다:

이 코드는 주어진 추상 구문 트리(AST)에 대한 정적 단일 할당(SSA) 변환의 Clojure 구현입니다.
함수는 함께 작동하여 AST를 최적화 및 분석을 간소화하는 코드의 중간 표현인 SSA 형식으로 변환합니다.
다음은 각 함수에 대한 간략한 설명입니다:


`-item-to-ssa` : 입력 AST의 :op 키워드를 기반으로 디스패치하는 다중 메서드입니다. AST의 다양한 부분을 SSA 형식으로 변환하는 중심점 역할을 합니다.
(defmulti -item-to-ssa :op)
(defmethod -item-to-ssa :default
  [ast]
  (gen-plan
   [locals (get-binding :locals)
    id (add-instruction (->RawCode ast locals))]
   id))

item-to-ssa 함수 : `-item-to-ssa` 를 호출하는 래퍼함수. 인풋으로 받은 AST를 변환함.변환이 필요한 것이 아니면 `gen-plan` 을 호출하여 새 계획을 생성하고 AST 및 로컬 바인딩을 기반으로 raw-code instruction을 추가한다.
(defn item-to-ssa [ast]
  (if (or (::transform? ast)
          (contains? #{:local :const :quote} (:op ast)))
    (-item-to-ssa ast)
    (gen-plan
     [locals (get-binding :locals)
      id (add-instruction (->RawCode ast locals))]
     id)))

함수 호출을 나타내는 :invoke AST 노드를 SSA 형식으로 변환한다.
(defmethod -item-to-ssa :invoke
  [{f :fn args :args}]
  (gen-plan
   [arg-ids (all (map item-to-ssa (cons f args)))
    inst-id (add-instruction (->Call arg-ids))]
   inst-id))

키워드 호출을 나타내는 :keyword-invoke AST노드를 SSA 형식으로 반환한다.
(defmethod -item-to-ssa :keyword-invoke
  [{f :keyword target :target}]
  (gen-plan
   [arg-ids (all (map item-to-ssa (list f target)))
    inst-id (add-instruction (->Call arg-ids))]
   inst-id))

뭐 대게 이런식임.

let을 보자.
1. let-binding-to-ssa 를 이용하여 let-ids를 받음.
2. locals에 있는 기존바인딩들을 pop 함.
3. let-id 각각을 Const로 바꾸고 add-instruction을 하는 것으로 보아 상수 인스트럭션을 추가하고 local-ids에 저장.
4. `push-alter-bindings` 로 `:locals` 에 새로운 바인딩으로 병합하여 로컬 바인딩을 푸시하여 변경. (locals는 다 없애도 되나...? 기존 값들은 다른 bindings 같은 곳에 있나보다. 해당 인스트럭션에서만의 locals 라면 없애야 할지도)
5. let 표현식의 본문을 SSA 형식으로 변환하고 결과를 body-id에 저장한다.
6. 로컬 바인딩을 이전으로 돌린다. (pop-bindings 사용)
7. 변환된 `:let` 표현식을 SSA형식으로 나타내는 body-id를 반환한다.
(defmethod -item-to-ssa :let
  [{:keys [bindings body]}]
  (gen-plan

   [let-ids (all (map let-binding-to-ssa bindings))
    _ (all (map (fn [_] (pop-binding :locals)) bindings))

    local-ids (all (map (comp add-instruction ->Const) let-ids))
    _ (push-alter-binding :locals merge (into {} (map (fn [id {:keys [name form]}]
                                                        [name (vary-meta id merge (meta form))])
                                                      local-ids bindings)))

    body-id (item-to-ssa body)
    _ (pop-binding :locals)]
   body-id))


(defn let-binding-to-ssa
  [{:keys [name init form]}]
  (gen-plan
   [bind-id (item-to-ssa init)
    _ (push-alter-binding :locals assoc (vary-meta name merge (meta form)) bind-id)]
   bind-id))

loop
a. let-binding-to-ssa 함수로 루프 바인딩을 변환하고, local-val-ids에 저장.
b. 각 바인딩에 대해 로컬 바인딩 (:locals)을 팝하여 (pop-bindigns) 이전 바인딩을 복원합니다. 이 작업은 팝 바인딩 함수를 사용하여 수행합니다.
c. 각 local-val-ids에 대해 상수 인스트럭션을 추가하고 결과를 local-ids에 저장합니다.
d. 루프 본문(body-blk)을 위한 새 블록과 최종 루프 결과(final-blk)를 위한 다른 블록을 생성합니다.
e. 점프 인스트럭션을 추가하여 body-blk으로 점프합니다.
f. 현재 블록을 body-blk로 설정합니다.
g. 현재 바인딩을 local-ids에서 생성된 새 바인딩과 병합하여 로컬 바인딩을 푸시하고 변경합니다.
h. 루프에 대한 :recur-point 및 :recur-nodes 바인딩을 푸시합니다.
i. 루프 본문을 SSA 형식으로 변환하고 결과를 ret-id에 저장합니다.
j. recur-nodes, :recur-point 및 로컬 바인딩을 이전 바인딩으로 다시 팝합니다.
k. 루프가 종료되지 않으면 점프 명령을 추가하여 final-blk로 점프합니다. 그렇지 않으면 no-op을 실행합니다.
l. 현재 블록을 final-blk로 설정합니다.
m. 루프 값에 대한 상수 인스트럭션을 추가하고 ret-id에 저장합니다.
n. 변환된 :loop 표현식을 SSA 형식으로 나타내는 ret-id를 반환합니다.
(defmethod -item-to-ssa :loop
  [{:keys [body bindings] :as ast}]
  (gen-plan
   [local-val-ids (all (map let-binding-to-ssa bindings))
    _ (all (for [_ bindings]
             (pop-binding :locals)))
    local-ids (all (map (comp add-instruction ->Const) local-val-ids))
    body-blk (add-block)
    final-blk (add-block)
    _ (add-instruction (->Jmp nil body-blk))

    _ (set-block body-blk)
    _ (push-alter-binding :locals merge (into {} (map (fn [id {:keys [name form]}]
                                                        [name (vary-meta id merge (meta form))])
                                                      local-ids bindings)))
    _ (push-binding :recur-point body-blk)
    _ (push-binding :recur-nodes local-ids)

    ret-id (item-to-ssa body)

    _ (pop-binding :recur-nodes)
    _ (pop-binding :recur-point)
    _ (pop-binding :locals)
    _ (if (not= ret-id ::terminated)
        (add-instruction (->Jmp ret-id final-blk))
        (no-op))
    _ (set-block final-blk)
    ret-id (add-instruction (->Const ::value))]
   ret-id))

;;; SSA format
SSA(정적 단일 할당)는 컴파일러 최적화에 사용되는 프로그램의 중간 표현(IR)입니다.
SSA 형식에서는 모든 변수가 정확히 한 번만 할당되고 모든 변수가 사용되기 전에 정의됩니다.
이를 위해 SSA는 제어 흐름 합류 지점에서 서로 다른 변수 버전을 병합하는 φ(파이) 함수라는 특수 함수를 도입합니다.

SSA 양식 사용의 주요 이점은 다음과 같습니다:
1. 데이터 흐름 분석 간소화: 각 변수는 정확히 한 번만 할당되므로, 하나의 할당만 추적하면 되므로 데이터 흐름 분석이 더 쉬워집니다.

2. 다양한 최적화가 가능합니다: SSA 형식은 상수 전파, 데드 코드 제거, 전역 값 번호 지정 등 다양한 최적화를 가능하게 합니다.

3. 더 쉬운 레지스터 할당: SSA를 사용하면 컴파일러가 레지스터에 저장해야 할 값과 메모리에 저장해야 할 값을 더 쉽게 결정할 수 있습니다. 이는 보다 효율적인 머신 코드 생성으로 이어질 수 있습니다.

제공된 Clojure 코드의 컨텍스트에서 SSA 변환은 프로그램의 AST(추상 구문 트리) 표현을 가져와서 SSA 형식으로 변환합니다.
이 과정에는 복잡한 표현식을 분해하고, 중간 결과를 위한 새로운 변수를 생성하고, 제어 흐름 구조를 SSA 요구 사항에 맞게 업데이트하는 작업이 포함됩니다.

제공된 코드는 다양한 유형의 AST 노드를 SSA 형식으로 변환하기 위한 함수 집합을 정의합니다.
이러한 함수는 AST 노드의 :op 키에 따라 디스패치하는 -item-to-ssa 멀티메서드의 메서드로 구현됩니다.
이 접근 방식을 사용하면 새로운 유형의 AST 노드를 지원하기 위해 SSA 변환을 쉽게 확장할 수 있습니다.

SSA와 SSA가 아닌 코드차이

(let [x 1
      y (+ x 2)
      x (* x y)]
  (+ x y))
위 코드는 x를 두번 할당하므로 SSA가 아님.

(let [x_1 1
      y_1 (+ x_1 2)
      x_2 (* x_1 y_1)]
  (+ x_2 y_1))
위 코드는 x를 두번 할당하는 대신 x_1, x_2 로 할당함.


Q.- SSA의 목적은 그저 이름 충돌을 해결하기 위함인가?
이름 충돌을 방지하는 것도 SSA의 장점 중 하나이지만, SSA의 주된 목적은 컴파일러 및 기타 프로그램 분석 도구를 위해 코드를 단순화하고 최적화하는 것입니다.
각 변수를 정확히 한 번만 할당하면 코드 내에서 데이터 흐름과 제어 흐름 종속성을 추적하고 분석하기가 더 쉬워집니다.

SSA를 사용하면 상수 전파, 데드 코드 제거, 공통 하위 표현식 제거 등 다양한 컴파일러 최적화를 수행할 수 있습니다.
이러한 최적화를 통해 컴파일된 코드의 성능을 크게 향상시킬 수 있습니다.

따라서 SSA는 이름 충돌을 방지하는 데 도움이 되지만, 주된 목표는 코드 분석을 간소화하고 컴파일 프로세스 중에 최적화를 가능하게 하는 것입니다.
