(ns core04)


;;;; runtime.clj
;;;; 내부 상태 머신 런타임과 관련된 함수 및 매크로가 포함되어 있다.
;;;; 상태머신은 비동기 작업의 실행과 채널과의 상호작용을 관리하는데 사용된다.
(ns ^{:skip-wiki true}
    clojure.core.async.impl.runtime
    (:require [clojure.core.async.impl.protocols :as impl])
    (:import [java.util.concurrent.locks Lock]
             [java.util.concurrent.atomic AtomicReferenceArray]))
;; 이 상수들은 상태머신을 다루기 위한 정보들임.
;; 1. `FN-IDX` : index of the function to be executed in the state machine. This function represents the current operation or step in the asynchronous task.
;;               이 인덱스는 현재 스텝 또는 블록 식별자를 상태 머신에 저장하는 데 사용됩니다.
;;               실제 함수를 저장하는 데 사용되는 것이 아니라 현재 실행 중인 블록의 식별자를 저장하는 데 사용됩니다.
;;               스테이지 머신이 단계를 진행함에 따라 FN-IDX의 값은 다음 단계 또는 블록을 가리키도록 변경됩니다.
;;
;; 2. `STATE-IDX` : index of the current state of the state machine. it can hold information about the next operation or a special state, like a finished state.
;;                  이 인덱스는 현재 블록 실행이 완료되면 상태 머신이 전환할 다음 상태 또는 블록 식별자를 저장하는 데 사용됩니다.
;;                  상태 전환이 발생하면 새로운 상태 또는 블록을 반영하도록 STATE-IDX의 값이 업데이트됩니다.
;;                  전환이 끝나면 FN-IDX의 값이 STATE-IDX의 값으로 설정되어 스테이트 머신이 다음 단계 또는 블록으로 이동합니다.
;;
;;                  Jmp 명령어의 경우, 코드 블록(do (rt/aset-all! ~state-sym ~rt/VALUE-IDX ~value ~rt/STATE-IDX ~block) :recur))이 지정된 블록으로 STATE-IDX를 업데이트합니다.
;;                  즉, 다음 실행될 상태 또는 블록은 ~블록이 됩니다.
;;                  Jmp 명령이 실행되면 스테이트 머신은 FN-IDX를 STATE-IDX 값으로 업데이트하여 스테이트 머신을 다음 단계 또는 블록으로 효과적으로 전환합니다.
;;
;;                  요약하면, FN-IDX는 현재 실행 중인 블록의 식별자를 저장하는 데 사용되며, STATE-IDX는 다음에 실행할 블록의 식별자를 저장하는 데 사용됩니다.
;;                  상태 전환이 발생하면 상태 머신이 다음 단계 또는 블록으로 이동하기 위해 FN-IDX의 값이 STATE-IDX의 값으로 업데이트됩니다.
;; 3. `VALUE-IDX` : index of the value that is being transferred or processed in the state machine(전송 또는 처리 중인 값의 인덱스)
;;                      예를들어, 채널에서 가져온 값일 수도 있고, 채널에 입력되는 값일 수도 있다.
;; 4. `BINDINGS-IDX` : index of the bindings associated with the current state of the state machine(상태 머신의 현재 상태와 연관된 바인딩의 인덱스).
;;                     이러한 바인딩은 추가 컨텍스트를 보유하여 비동기 작업의 여러 단계에 걸쳐 상태를 관리하는데 유용할 수 있다.
;; 5. `EXCEPTION-FRAMES` : 상태 배열 내 예외 프래임의 인덱스. 예외 핸들링에 대한 정보를 상태 머신에 저장함. `try-catch` 블록 같은 것을
;; 6. `CURRENT-EXCEPTION` : 상태 머신에서 처리 중인 현재 예외의 인덱스 : 상태머신이 실행되는 동안 예외가 발생하면, 이 인덱스에 저장되어 상태머신이 예외를 처리하고 실행을 계속하거나 정상 종료를 할 수 있다.
;; 7. `USER-START-IDX` : 이 상수는 사용자의 시작 지점 또는 상태 머신의 진입 지점의 인덱스.
;;     일반적으로 initial channel or a reference to the initial state of the user's code that is being executed within the state machiine.
;;     이 인덱스는 사용자 정의 상태 정보를 저장하기 위한 시작점. USER-START-IDX 앞의 인덱스 FN-IDX, STATE-IDX 같이 상태 머신의 내부 작동을 위해 예약된 인덱스임.
;;     USER-START-IDX 부터 시작하는 인덱스는 프로그램에 필요한 추가 상태 정보를 저장하는데 사용할 수 있다.
;;     이를 통해 상태 머신의 내부 작동을 방해하지 않고 사용자 지정 상태 데이터를 저장하고 관리할 수 있다.

;; 다른 블로그에 따르면
;; 0 FN-IDX : 상태머신함수.
;; 1 STAET-IDX : 현재상태
;; 2 VALUE-IDX : the value the expression in the go block evaluate to, 고블록 표현식이 평가하는 값?
;; 3 BINDINGS-IDX : 캡처링한 바인딩들
;; 6 USER-START-IDX : go블록이 반환할 채널
;; 오래된 거라 바뀌었을 수도 있음.
(def ^:const FN-IDX 0)
(def ^:const STATE-IDX 1)
(def ^:const VALUE-IDX 2)
(def ^:const BINDINGS-IDX 3)
(def ^:const EXCEPTION-FRAMES 4)
(def ^:const CURRENT-EXCEPTION 5)
(def ^:const USER-START-IDX 6)

;; 실제 AtomicReferenceArray는 go block에서 생성됨.
;; clojure.core.async.impl.ioc-macro 에서 `state-array` 함수가 있음.
(comment (defn state-array
           [size]
           (AtomicReferenceArray. size)))

(defn aset-object [^AtomicReferenceArray arr idx ^Object o]
  (.set arr idx o))

(defn aget-object [^AtomicReferenceArray arr idx]
  (.get arr idx))

(defmacro aset-all!
  [arr & more]
  (assert (even? (count more)) "Must give an even number of args to aset-all!")
  (let [bindings (partition 2 more)
        arr-sym (gensym "statearr-")]
    `(let [~arr-sym ~arr]
       ~@(map
          (fn [[idx val]]
            `(aset-object ~arr-sym ~idx ~val))
          bindings)
       ~arr-sym)))

;; unused fn
(defn finished?
  "Returns true if the machine is in a finished state"
  [state-array]
  (identical? (aget-object state-array STATE-IDX) ::finished))
;; finished 안쓰는대신 전용 채널을 사용하여 완료 시그널을 보낸다?
(comment
  (require '[clojure.core.async :refer [go-loop <! >! timeout chan close!]])

  (defn process-data [data]
    ;; Process the data here
    (println "Processing data:" data))

  (def input-chan (chan))
  (def done-chan (chan))

  (go-loop []
    (let [data (<! input-chan)]
      (if (nil? data)
        (do
          (println "Finished processing data")
          (close! done-chan))
        (do
          (process-data data)
          (recur)))))

  ;; Put some data into the input channel
  (>! input-chan "data1")
  (>! input-chan "data2")
  (>! input-chan "data3")

  ;; Close the input channel to signal the end of data
  (close! input-chan)

  ;; Block until the done-chan is closed, signaling that the go-loop has finished
  (<! done-chan)
  ;;
  )
;; take!, put! 함수에 대한 핸들러 생성에 사용.
(defn- fn-handler
  [f]
  (reify
    Lock
    (lock [_])
    (unlock [_])

    impl/Handler
    (active? [_] true)
    (blockable? [_] true)
    (lock-id [_] 0)
    (commit [_] f)))

;; 인자 state를 받아서 해당 배열의 `FN-IDX (0)` 위치에 있는 함수를 호출함. 이 함수는 상태 기계를 작동시키는 역할을 함.
(defn run-state-machine [state]
  ((aget-object state FN-IDX) state))

;; 이 함수는 상태 배열(state)를 인자로 받음.
;; `run-state-machine`을 호출하고 예외를 처리함. 예외가 발생하면 사용자가 제공한 채널을 닫고 예외를 다시 던짐.
;; 이 함수는 상태 기계를 실행하면서 발생할 수 있는 예외를 처리하는 역할을 함.
(defn run-state-machine-wrapped [state]
  (try
    (run-state-machine state)
    (catch Throwable ex
      (impl/close! (aget-object state USER-START-IDX))
      (throw ex))))

;; take! 경우,
;; fn-handler는 채널에서 값을 가져올 때 상태 배열의 `VALUE-IDX`를 수신한 값으로 설정하고
;; 상태 배열의 `STATE-IDX`를 제공된 블록(`blk`)으로 업데이트 하는 핸들러를 생성한다.
;; 마지막으로 업데이트된 상태 배열로 상태 기계를 실행한다.
(defn take! [state blk c]
  (if-let [cb (impl/take! c (fn-handler
                             (fn [x]
                               (aset-all! state VALUE-IDX x STATE-IDX blk)  ;; VALUE-IDX를 x로 STATE-IDX를 blk에 설정함.
                               (run-state-machine-wrapped state))))]  ;; dㅣ건 머임:
    (do (aset-all! state VALUE-IDX @cb STATE-IDX blk)
        :recur)
    nil))
;; `fn-handler`는 값을 채널에 성공적으로 넣을 때,
;; 상태 배열의 `VALUE-IDX` 를 결과 값(성공한 경우 true)으로 업데이트하고
;; 상태 배열의 `STATE-IDX`를 제공된 블록 blk로 업데이트하는 핸들러 생성
(defn put! [state blk c val]
  (if-let [cb (impl/put! c val (fn-handler (fn [ret-val]
                                             (aset-all! state VALUE-IDX ret-val STATE-IDX blk)
                                             (run-state-machine-wrapped state))))]
    (do (aset-all! state VALUE-IDX @cb STATE-IDX blk)
        :recur)
    nil))

;; impl/take! 및 impl/put!은 clojure.core.async.impl.protocols 네임스페이스의 프로토콜 메소드입니다. 이들은 실제 채널 구현에 대한 호출을 위임합니다.
;;  실제로, clojure.core.async.impl.channels 네임스페이스의 채널 구현에서 clojure.core.async.impl.protocols의 프로토콜 메소드인 impl/take! 및 impl/put!을 확인할 수 있습니다.
;;  이러한 구현은 사용되는 채널 구현에 따라 달라질 수 있습니다.
;; runtime.clj의 take! 및 put! 함수는 상태 기계를 작동시키는 데 사용되는 함수이며, 이러한 함수는 채널에서 값을 가져오거나 채널에 값을 넣을 때 상태 기계를 업데이트하고 다시 실행합니다.
;; 따라서 runtime.clj에 있는 take! 및 put!은 상태 기계의 일부로서 작동하며, impl/take! 및 impl/put!는 실제 채널 구현과 상호 작용하는 프로토콜 메소드입니다.
;;  이 두 함수 집합은 서로 다른 목적으로 사용됩니다.






;; 주어진 state를 받아서, 사용자가 제공한 `USER-START-IDX` 값을 가져옴. (이거 채널인가봄)
;; 인자 value가 nil이 아니면 channel에 이 값을 넣음. 그 다음 채널을 닫고 반환함.
;; 이 함수는 go 블록의 리턴값을 처리하고 채너릉ㄹ 반환하는데 사용함.
(defn return-chan [state value]
  (let [c (aget-object state USER-START-IDX)]
    (when-not (nil? value)
      (impl/put! c value (fn-handler (fn [] nil))))
    (impl/close! c)
    c))


async-custom-terminators는 clojure.core.async.impl.ioc-macros 네임스페이스에서 사용됩니다.
이 네임스페이스는 core.async의 매크로 구현에 대한 내부적인 지원을 제공합니다.
매크로 구현은 go, go-loop, thread, thread-call과 같은 고수준 매크로의 코드 변환을 처리합니다.
일반적으로 개발자가 직접 이 네임스페이스를 사용할 필요는 없으며,
대신 clojure.core.async 네임스페이스에서 제공되는 고수준 매크로를 사용하여 비동기 코드를 작성하고 관리합니다.
그러나 이 네임스페이스를 통해 async-custom-terminators가 어떻게 사용되는지 이해하는 것이 도움이 될 수 있습니다.
(def async-custom-terminators
  {'clojure.core.async/<! `take!
   'clojure.core.async/>! `put!
   'clojure.core.async/alts! 'clojure.core.async/ioc-alts!
   :Return `return-chan})

runtime.clj 내의 함수들은 주로 상태 기계를 처리하고 실행하는 데 사용되며, 상호 작용하는 방식을 다음과 같이 설명할 수 있습니다.
run-state-machine: 상태 배열을 받아서 해당 상태에서 실행할 함수를 가져와 실행합니다. 이 함수는 주로 다른 함수에서 호출되어 상태 기계를 실행합니다.
run-state-machine-wrapped: run-state-machine을 감싸서 예외 처리를 추가합니다. 이 함수는 상태 기계를 실행하면서 발생한 예외를 처리합니다.
take! 및 put!: 이 함수들은 상태 기계에서 채널에서 값을 가져오거나 채널에 값을 넣는 작업을 수행할 때 사용됩니다. 각각의 함수는 상태 기계를 업데이트하고 run-state-machine-wrapped를 호출하여 상태 기계를 다시 실행합니다.
return-chan: 상태 기계가 완료되면 결과 값을 반환하고 사용자 시작 채널을 닫습니다. 이 함수는 상태 기계의 종료 시 호출됩니다.
fn-handler: 주로 take! 및 put! 함수에서 사용되는 커스텀 핸들러를 생성합니다. 이 핸들러는 상태 기계를 업데이트하고 다시 실행하는 데 필요한 콜백 함수를 제공합니다.

상호 작용의 개요는 다음과 같습니다:
상태 기계가 실행되면, run-state-machine 또는 run-state-machine-wrapped 함수를 통해 실행됩니다.
상태 기계가 채널에서 값을 가져오거나 채널에 값을 넣어야 하는 경우, take! 또는 put! 함수가 호출됩니다.
take! 및 put! 함수는 상태 기계를 업데이트한 다음, run-state-machine-wrapped를 호출하여 상태 기계를 다시 실행합니다.
상태 기계가 완료되면, return-chan 함수를 호출하여 사용자 시작 채널에 결과를 반환하고 채널을 닫습니다.
이러한 함수들은 함께 작동하여 상태 기계를 처리하고, 채널에서 값을 가져오거나 채널에 값을 넣는 동작을 처리하며, 상태 기계가 완료되면 결과를 반환합니다.

;;;; reference
;;;; http://hueypetersen.com/posts/2013/08/02/the-state-machines-of-core-async/
;;;; https://www.zorched.net/2014/05/18/using-a-core-async-routine-as-a-state-machine/
;;;; https://www.braveclojure.com/core-async/
;;;; https://nakkaya.com/2010/06/22/finite-state-machine-implementation-in-clojure/
;;;; http://jedahu.github.io/spaghetti/
;;;; https://github.com/metosin/tilakone
