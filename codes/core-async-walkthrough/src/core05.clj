(ns core05)

;;;; https://github.com/clojure/core.async/blob/master/src/main/clojure/clojure/core/async/impl/ioc_macros.clj
변수 및 함수 설명:

state-machine-vars: 상태 머신에서 사용되는 일반적인 변수들을 정의합니다.
async-custom-terminators: 비동기 함수들의 내부 처리에 사용되는 특수한 종료자들을 정의합니다.
state-idx: 주어진 상태 배열에서 특정 상태에 대한 인덱스를 가져옵니다.
meta-idx: 주어진 상태 배열에서 특정 메타데이터 인덱스를 가져옵니다.
emit: 주어진 폼을 현재 출력 코드에 추가합니다.
with-meta: 폼에 메타데이터를 추가합니다.
state-array: 비동기 코드 실행을 위한 상태 배열을 생성합니다.
state-machine: 상태 머신을 생성하고 실행합니다.
매크로 설명:

go: 비동기 블록을 만들고, 채널을 반환합니다. 블록 내부의 코드는 즉시 실행되지 않으며, 스레드 풀에서 실행됩니다.
go-loop: go와 비슷하지만, loop처럼 반복적인 실행을 지원합니다.
thread: 독립적인 스레드에서 실행될 비동기 블록을 만듭니다.
thread-call: 특정 함수를 새로운 스레드에서 실행하고, 결과를 담은 채널을 반환합니다


상호 작용에 대한 간략한 설명:
ioc_macros.clj의 코드는 상태 배열, 상태 머신, 코드 변환 등을 사용하여 비동기 매크로를 처리합니다.
비동기 매크로는 코드를 상태 머신으로 변환하고, 이 상태 머신은 상태 배열을 사용하여 실행됩니다.
이 과정에서 async-custom-terminators가 사용되어 비동기 함수들의 내부 처리를 수행합니다.

1. ioc_macros.clj는 core.async의 매크로 구현에 대한 내부 지원을 제공합니다. 이 파일에서 정의된 매크로와 함수들은 상호 작용하여 비동기 코드를 생성하고 변환하는 데 사용됩니다. 주요 상호 작용을 요약하면 다음과 같습니다:
2. go 및 go-loop 매크로: 이 매크로들은 비동기 코드 블록을 받아들이고, 상태 기계 코드를 생성하고 실행하기 위해 ioc-macros 내부의 다른 함수들을 호출합니다.
3. build-state-machine: go 및 go-loop 매크로에서 호출되며, 주어진 비동기 코드를 분석하여 상태 기계를 생성합니다. 이 함수는 코드를 분석하여 :binds, :finally, :catch, <!, >! 및 alts!와 같은 특수한 연산자들을 처리합니다.
4. try-state-machine: 비동기 코드에서 예외 처리를 담당합니다. 코드의 try, catch, finally 블록을 처리하고 적절한 예외 처리를 위해 상태 기계를 생성합니다.
5. <!, >!, alts! 매크로: 이들 매크로는 비동기 코드에서 채널에서 값을 읽거나 쓰는 동작을 처리합니다. 이들은 상태 기계 코드에 포함되며, 해당 연산이 완료될 때까지 상태 기계의 실행을 일시 중단시킵니다.
6. async-custom-terminators: ioc_macros.clj 파일 내에서 사용되는 함수와 매크로의 집합입니다. 이들은 상태 기계 생성 과정에서 특수한 연산자들을 처리하는 데 사용됩니다.
7. ioc-macros 내의 다양한 유틸리티 함수들은 상태 기계 생성 및 코드 변환을 지원하는 데 사용됩니다.

요약하면, ioc_macros.clj 내의 매크로와 함수들은 상호 작용하여 주어진 비동기 코드를 상태 기계 코드로 변환하고,
채널에서 읽기 및 쓰기를 처리하며, 예외 처리를 수행합니다.
이 변환된 코드는 core.async 라이브러리가 제공하는 다양한 비동기 프리미티브를 지원하게 됩니다.


ioc_macro.clj 맨 위에 주석이 있다.
;; State monad stuff, used only in SSA construction
이 주석은 상태 모나드(State Monad) 관련 코드에 대한 설명입니다.
상태 모나드는 함수형 프로그래밍에서 상태 변화를 추적하고 관리하기 위한 구조로 사용됩니다.
이 경우, 상태 모나드는 SSA(Static Single Assignment) 구조 생성을 위한 것으로 사용되며, 비동기 코드를 상태 기계로 변환하는 과정에서 사용됩니다.

SSA 구조는 각 변수에 값이 오직 한 번만 할당되는 형태를 가지며, 이러한 형태는 코드 최적화 및 분석에 유용합니다.
core.async의 경우, 상태 모나드를 이용해 코드를 분석하고, SSA 형태로 변환하여 상태 기계를 구성하는 데 사용합니다.
이 과정에서 상태 모나드를 통해 코드의 상태 변화를 추적하고 관리할 수 있습니다.


go 블록은 뭘까? 고루틴과 비슷한 개념으로 비동기 작업을 동시에 실행할 수 있게 도와준다.


core.async의 go와 go-loop 매크로는 비동기 코드 블록을 받아들이고 상태 기계 코드를 생성하고 실행합니다.
  이 작업은 ioc_macros.clj 내부의 함수들을 사용하여 수행됩니다.
  이 두 매크로의 작동 방식을 이해하려면, 상태 기계 생성, 코드 변환 및 비동기 실행 과정을 이해해야 합니다.

1. 상태 기계 생성:
  go와 go-loop 매크로는 코드 블록을 받아들여 상태 기계로 변환합니다.
  상태 기계는 주어진 비동기 코드 블록의 실행을 제어하고 관리하는데 사용됩니다.
  상태 기계는 AtomicReferenceArray를 사용하여 상태, 함수, 값 등의 정보를 저장하고 관리합니다.

2. 코드 변환:
  ioc_macros.clj의 함수들은 비동기 코드 블록을 상태 기계 코드로 변환하는 작업을 수행합니다.
  이 변환 과정에서 일부 매크로 및 함수가 상태 기계와 관련된 함수로 대체되며, 원래의 비동기 코드 블록이 실행될 수 있는 상태로 변경됩니다.
  예를 들어, <!, >! 및 alts!와 같은 비동기 작업을 수행하는 매크로는 상태 기계 코드로 변환되어 실행됩니다.

3. 비동기 실행:
  상태 기계 코드가 생성되면, core.async는 이를 비동기적으로 실행합니다.
  이 과정에서 여러 go 블록이 동시에 실행되고, 채널을 통해 데이터를 전달하거나 받게 됩니다.
  즉, 상태 기계 코드는 다양한 비동기 작업을 관리하고 실행하는 역할을 수행합니다.

---------------
go와 go-loop 매크로는 비동기 코드 블록을 상태 기계로 변환하고 실행하는 역할을 합니다.
 이 과정에서 상태 기계는 비동기 코드 블록의 실행을 관리하고, 채널 간의 데이터 전달을 처리합니다.
 이를 위해 상태 기계는 AtomicReferenceArray를 사용하여 상태, 함수, 값 등의 정보를 저장하고 관리합니다.

ioc_macros.clj 파일은 이 변환과 실행 과정을 위한 여러 함수들을 포함하고 있습니다.
 이러한 함수들은 비동기 코드 블록을 상태 기계 코드로 변환하고,
 채널과 관련된 매크로와 함수를 적절한 상태 기계 코드로 대체하는 역할을 수행합니다.

이해를 돕기 위해 다음 과정을 따라 연구해보세요:

1. ioc_macros.clj 파일에서 state-machine-fn 함수를 살펴보세요. 이 함수는 비동기 코드 블록을 상태 기계 코드로 변환하는 역할을 합니다.
2. 상태 기계 코드를 생성한 후에는, go와 go-loop 매크로를 통해 비동기적으로 실행됩니다. 이 매크로들이 상태 기계 코드를 실행하고 관리하는 방식을 이해하려면, 이 매크로들이 어떻게 상호작용하는지 살펴보세요.
3. 상태 기계 코드가 실행되는 동안 채널과의 상호 작용은 어떻게 처리되는지 이해하려면, <!, >! 및 alts! 등의 매크로가 어떻게 상태 기계 코드로 변환되어 실행되는지 연구해보세요.

더 깊게 들어가기 전에, core.async의 기본 개념과 원리를 이해하는 것이 중요합니다. 그런 다음 ioc_macros.clj의 함수들과 상호작용을 이해하는데 집중하세요. 이 과정에서 질문이 생기면 언제든지 도움을 요청하세요!

core.async 기본설명.

core.async는 Clojure와 ClojureScript에서 사용할 수 있는 비동기 프로그래밍 라이브러리입니다. 이 라이브러리는 동시성(concurrency)을 관리하고, 비동기 코드를 더 간결하고 명확하게 작성할 수 있도록 도와줍니다. core.async의 핵심 개념은 다음과 같습니다.

1. 채널(Channel): 채널은 데이터를 전달하는 통로로 생각할 수 있습니다. 채널을 통해 고루틴 사이에서 데이터를 전송할 수 있습니다. 채널은 버퍼가 있을 수도 있고 없을 수도 있으며, 버퍼가 없는 경우 "시뮬레이션된 블로킹"을 사용하여 고루틴이 데이터를 받을 때까지 기다릴 수 있습니다.
2. 고루틴(Go-routine): 비동기적으로 실행되는 코드 블록입니다. 고루틴은 동시에 여러 작업을 처리할 수 있으며, 각각의 작업은 독립적으로 실행됩니다. 고루틴은 자체 스레드를 사용하지 않고, 스레드 풀을 사용하여 여러 스레드에 분산될 수 있습니다. 이를 통해 고루틴이 가볍고 효율적으로 동작합니다.
3. go와 go-loop 매크로: 이 매크로들은 고루틴을 생성하고 실행하는 데 사용됩니다. go 매크로는 주어진 코드 블록을 비동기적으로 실행하며, go-loop 매크로는 주어진 코드 블록을 반복적으로 비동기적으로 실행합니다.
4. <!, >! 및 alts! 매크로: 이 매크로들은 채널에서 데이터를 읽거나 쓰기 위한 연산을 제공합니다. <!는 채널에서 값을 읽어옵니다. >!는 채널에 값을 씁니다. alts!는 여러 채널 중 하나에서 데이터를 읽거나 쓸 수 있는 선택적 연산을 제공합니다.
5. 스레드 풀 및 Executor: core.async는 고루틴을 실행하기 위해 스레드 풀을 사용합니다. 이 스레드 풀은 고루틴 사이의 작업을 나누고, 리소스를 효율적으로 관리하여 동시성을 처리합니다.
core.async의 기본 개념과 원리를 이해하고 나면, 비동기 코드를 더욱 쉽고 간결하게 작성할 수 있습니다. 또한, core.async를 사용하여 여러 동시 작업을 독립적으로 관리하고 조율할 수 있습니다. 이를 통해 복잡한 비동기 프로그래밍 작업을 더욱 효율적으로 수행할 수 있습니다.

6. 에러 처리: core.async는 고루틴 내에서 발생하는 에러를 처리하기 위한 메커니즘을 제공합니다. 에러 처리를 위해 채널을 사용하거나, 채널에서 발생하는 에러를 처리하는 고루틴을 만들 수 있습니다.
7. 타임아웃 채널: core.async는 일정 시간이 경과한 후 값을 생성하는 타임아웃 채널을 제공합니다. 이를 통해 고루틴이 특정 시간 내에 값을 받지 못한 경우 타임아웃을 처리할 수 있습니다.

core.async를 사용하면 비동기 프로그래밍의 복잡성을 추상화하고, 동시성을 명확하게 관리할 수 있습니다.
이 라이브러리를 사용하면 동시에 실행되는 작업들을 더 쉽게 조율하고 관리할 수 있어, 개발자들이 더욱 생산적으로 작업할 수 있습니다.

이제 core.async의 기본 개념을 이해했으니, 이 라이브러리를 사용하여 실제 프로젝트에서 비동기 작업을 처리하는 방법에 대해 살펴보는 것이 좋습니다.
공식 문서나 튜토리얼을 참조하여 실습을 진행하면서 core.async의 사용법을 숙지하고, 여러 가지 동시성 문제를 해결하는 데 도움이 되는 기술을 익히세요.
