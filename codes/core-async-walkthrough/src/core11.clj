(ns core11)
;; https://clojure.github.io/tools.analyzer.jvm/spec/quickref.html
(defn mark-transitions
  ;; 여기서부터 시작함. 얘가 `:transition`을 넣으면 그때부터 임.
  {:pass-info {:walk :post :depends #{} :after an-jvm/default-passes}}
  [{:keys [op fn] :as ast}]
  (let [transitions (-> (env/deref-env) :passes-opts :mark-transitions/transitions)]
    ;; 중요한 것은 op == invoke and (:op fn) :var 인 경우야.
    ;; invoke 표현식
    ;; 그리고 이 표현식에서 :fn은 An AST node representing the function to invoke
    ;; 다시 트리형티인듯. 이 형식이 :var 면 Node for a var symbol
    ;; 함수인데 symbol이란 뜻, 이제 여기서 var-name가 env/deref-env 안에 {:passesp-opts {:mark-transition/transitions}}
    ;; 로 존재하면
    ;; {:op :transition
    ;;  :name (get transition (var-name (:var fn)))}
    ;;  으로 바꿔버림... (함수호출을 제거하고, transition에 넣어버리는데
    ;;  없으면 그냥 ast를 던짐.
    ;;  이 :mark-transition/transitions는 state-machine에서 옵션을넣을 때 user-transitions를 값으로 넣음.
    ;; :user-transitions
    ;;   {clojure.core.async/<! clojure.core.async.impl.ioc-macros/take!,
    ;;    clojure.core.async/>! clojure.core.async.impl.ioc-macros/put!,
    ;;    clojure.core.async/alts! clojure.core.async/ioc-alts!,
    ;;    :Return clojure.core.async.impl.ioc-macros/return-chan}
    ;;    이거면 다 바꾸는 건가봄. 자기 코드면 바꾼다! 이건가봄.
    ;;    이것은 ioc-macro에 있고 다시 따로 정의되어 있음 그것을 go 매크로에서 사용 중.
    (comment (def async-custom-terminators
               {'clojure.core.async/<! `take!
                'clojure.core.async/>! `put!
                'clojure.core.async/alts! 'clojure.core.async/ioc-alts!
                :Return `return-chan}))
    ;;;; 위 코드는 무시.
    (if (and (= op :invoke)
             (= (:op fn) :var)
             (contains? transitions (var-name (:var fn))))
      (merge ast
             {:op   :transition
              :name (get transitions (var-name (:var fn)))})
      ast)))

(defn propagate-transitions
  {:pass-info {:walk :post :depends #{#'mark-transitions}}}
  [{:keys [op] :as ast}]
  (if (or (= op :transition)
          (some #(or (= (:op %) :transition)
                     (::transform? %))
                (ast/children ast)))
    (assoc ast ::transform? true)
    ast))

(defn propagate-recur
  {:pass-info {:walk :post :depends #{#'annotate-loops #'propagate-transitions}}}
  [ast]
  (if (and (= (:op ast) :loop)
           (::transform? ast))
    ;; If we are a loop and we need to transform, and
    ;; one of our children is a recur, then we must transform everything
    ;; that has a recur
    (let [loop-id (:loop-id ast)]
      (ast/postwalk ast #(if (contains? (:loops %) loop-id)
                           (assoc % ::transform? true)
                           %)))
    ast))
(comment
In the given code, the propagate-recur function is designed to propagate the
transformation requirement across the entire Abstract Syntax Tree (AST) that
contains a recur within a loop.

The reason behind transforming everything that has a recur is to ensure the
correct handling of control flow and state in the presence of recur expressions.
The core.async library generates Static Single Assignment (SSA) form to simplify
analysis and optimizations. In SSA form, each variable is assigned exactly once,
and the control flow is represented using basic blocks connected in a graph.

When a recur is present within a loop, it introduces complexities in control
flow and state management. In order to correctly handle these complexities and
maintain the semantics of the loop and recur constructs, the transformation
needs to be applied to the entire sub-tree containing the recur expression.

By transforming everything that has a recur, the generated SSA form will be
consistent, and the control flow and state management can be correctly handled
during analysis and optimizations.
  ;;
  )

(def passes (into (disj an-jvm/default-passes #'warn-on-reflection)
                  #{#'propagate-recur
                    #'propagate-transitions
                    #'mark-transitions}))

This code defines three Clojure functions (mark-transitions, propagate-transitions, and propagate-recur)
that are used as passes in an abstract syntax tree (AST) transformation process.
These passes are performed on the AST after certain pre-defined passes have been executed.
The purpose of these functions is to mark specific transitions, propagate those marks, and propagate recur marks.

mark-transitions: This function marks the transitions in the AST
if the function being invoked is present in the transitions set.
The function adds the :op :transition and :name key-value pairs to the AST node.

propagate-transitions: This function propagates the marked transitions in the AST.
If the :op of the AST is :transition or if any child of the AST has the :op :transition or ::transform?,
the function adds ::transform? true to the AST node.

propagate-recur: This function propagates the recur marks in the AST.
If the :op of the AST is :loop and it has the ::transform? mark,
the function propagates the ::transform? true mark to all the nodes in the loop that have a recur mark.

The passes variable at the end combines the default passes with the custom passes defined in the code.
It removes the warn-on-reflection pass and adds the three custom passes
(propagate-recur, propagate-transitions, and mark-transitions).


이 코드는 추상 구문 트리(AST) 변환 프로세스에서 패스로 사용되는 세 가지 Clojure 함수(마크-전환, 전파-전환, 전파-재귀)를 정의합니다.
이러한 패스는 사전 정의된 특정 패스가 실행된 후 AST에서 수행됩니다.
이러한 함수의 목적은 특정 트랜지션을 마킹하고, 해당 마크를 전파하고, 재귀 마크를 전파하는 것입니다.

마크 트랜지션: 이 함수는 호출되는 함수가 트랜지션 세트에 있는 경우 AST의 트랜지션을 표시합니다.
이 함수는 :op :transition 및 :name 키-값 쌍을 AST 노드에 추가합니다.

propagate-transitions: 이 함수는 AST에서 표시된 트랜지션을 전파합니다.
AST의 :op이 :transition이거나 AST의 자식에 :op :transition 또는 ::transform? 이 있는 경우,
이 함수는 AST 노드에 ::transform? true를 추가합니다.

전파-재귀: 이 함수는 AST에서 재귀 마크를 전파합니다. AST의 :op이 :loop이고 ::transform? 마크가 있는 경우,
이 함수는 리커 마크가 있는 루프의 모든 노드에 ::transform? true 마크를 전파합니다.

마지막에 있는 passes 변수는 기본 패스와 코드에 정의된 사용자 지정 패스를 결합합니다.
이 함수는 반사 시 경고 패스를 제거하고 세 가지 사용자 정의 패스(전파-재반복, 전파-전환, 마크-전환)를 추가합니다.
