(ns core01)
;; clojure.core.async namespace
;; 1. chan : create a new channel. you can provide optional buffer or buffer size.
;;           channel is primary mechanism for communication between go blocks.
;;
;; 2. buffer : creates a fixed-size buffer for a channel, allowing the channel to hold a specified number of items before blocking.
;; 3. dropping-buffer : creates a dropping buffer for a channel. When the buffer is full, new items added to the channel will be silently dropped.
;; 4. sliding-buffer : creates a sliding buffer for a channel. when the buffer is full, adding a new item will cause the oldest item to be removed.
;; 5. >!, <! >!! <!! : functions to put and take values from a channel.
;;                     >!, <! are used inside go blocks and will park the block if the operation cannot be completed immediately.
;;                     >!!, <!! are used outside go blocks and will block the calling thread if the operation cannot be completed immediately
;;
;; 6. alts! alts!! : functions to perform non-deterministic operations on multiple channels.
;;                   `alts!` used inside go blocks and will park the block if none of the operations can be completed immediately.
;;                   `alts!!` ... (same concept with above)
;;
;; 7. timeout : creates a channel that will close after a specified duration, allowing you to implement timeout and delays in your code.
;; 8. close! : close a channel, indicating that no more values will be put into it.
;;             once a channel is closed, any pending or future takes will receive a special sentinel value.
;;
;; 9. `go` : macro to create a lightweight, asynchronous go block. the body of the go block is transformed into a state machine,
;;          allowing it to be paused and resumed without blocking the main thread.
;;
;; 10. `trhead` : macro to create a new thread that runs the provided body of code. unlike go blocks, threads are heavyweight and have a higher overhead.
;;

;; 1. channels
;; https://github.com/clojure/core.async/blob/master/src/main/clojure/clojure/core/async/impl/channels.clj
;; 이렇게 생김
;; buf (buffer): specify a buffer for the channel. `buffer`(fixed-size buffer), `dropping-buffer` (drops new items when full), and `sliding-buffer` (removes oldest item when full)
;;              if no buffer is specified, the channel will be unbuffered, meaning it can only hold one item and will block if it's full
;; xform (transform) : this argument is a transducer function that can be applied to the channel. a transducer is a higher-order function that transforms a reducing function,
;;                     allowing you to apply transformations like mapping, filtering, and reducing to the values passing through the channel. When transducer is applied to a channel,
;;                     it will transform the values as they're put into the channel. Not that this feature is only available when using buffered
;; exh (Exception handler) : This argument is an optional exception handler function that can be used to handle exceptions thrown by the transducer (xform).
;;                          The function should take an exception as its argument and can decide what to do with it.
;;                          This could include logging the exception, re-throwing it, or ignoring it.
;;                          If no exception handler is provided and an exception occurs in the transducer,
;;                          the exception will propagate to the putter, and the channel will be closed.
(comment
  (defn chan
    ([buf] (chan buf nil))
    ([buf xform] (chan buf xform nil))
    ([buf xform exh]
     (ManyToManyChannel.
      (LinkedList.) (LinkedList.) buf (atom false) (mutex/mutex)
      (let [add! (if xform (xform impl/add!) impl/add!)]
        (fn
          ([buf]
           (try
             (add! buf)
             (catch Throwable t
               (handle buf exh t))))
          ([buf val]
           (try
             (add! buf val)
             (catch Throwable t
               (handle buf exh t)))))))
     )))

;; example
(comment
  (require '[clojure.core.async :as async])

  (defn exception-handler [ex]
    (println "Exception occured:" ex))

  (def xf (map inc)) ;; transducer that increments each value by 1

  (def ch (async/chan (async/buffer 10) xf exception-handler))

  (async/>!! ch "!")  ;; good exception occur!
  (async/close! ch)
  ;;
  )

;; ManyToManyChannel
;; ManyToManyChannel은 core.async 라이브러리의 채널을 표현하는 방식임.
;; 이 record는 채널의 내부 상태를 관리하는데 사용한다. (버퍼 포함)
;; 또한 보류 중인 put들과 take들 그리고 closed 여부 등 모두 상태 관련된건 다 관리함.
;; 인자는
;; takes - LinkedList이며, 채널에서 take 오퍼레이션을 요청했지만 가져올 수 있는 항목이 없는 경우
;;          take 오퍼레이션이 이 리스트에 추가된다. item을 사용할 수 있게 되면, list 맨 위에 있는 take 오퍼레이션이
;;          dequeue 되고 완료됨.
;; puts - 이 필드는 보류 중인 put operation의 LinkedList입니다.
;;          채널에 값을 넣었지만 대기 중인 사용 가능한 take operation이 없으면 넣기 작업이 이 목록에 추가됩니다.
;;          테이크 작업을 사용할 수 있게 되면 목록 헤드의 넣기 작업이 대기열에서 제거되고 완료됩니다.
;; buf - 이 필드는 Queue 구현체임.
;; closed : AtomicBoolean 이라고함.(나중에 구현봐야할듯)
;; mutex : Lock 타입임. 이 필드는 ManyToManyChannel의 상태에 대한 액세스 동기화를 하는데 사용되는 Lock
;;          동시 작업이 스레드로부터 안전(thread-safe)한지 확인한다.
;; add! : 이 필드는 채널에 새 값이 추가될 때 호출되는 optional 함수. 디버깅, 모니터링 목적에 유용.
;;

;; ManyToManyChannel은 deftype으로 여러 protocol을 구현함.
;; MMC: 내부 프로토콜 두 개의 메소드를 정의 : `cleanup`, `abort` 채널의 내부 상태를 관리하기 위함
;; `impl/WritePort` : `put!` 메소드를 정의. 채널에 쓰기
;; `impl/ReadPort` : `take!` 메소드를 정의. 채널에서 읽기
;; `impl/Channel` : `closed?`, `close!` 채널의 닫혀짐여부 상태확인
;;
;; 이런 내부 상태는 이제 인자들로 관리할 수 있음.
;; takes: A LinkedList of pending take operations.
;; puts: A LinkedList of pending put operations.
;; buf: A buffer that can store values when the channel is full or there are no pending takes.
;; closed: An atom that tracks whether the channel is closed.
;; mutex: A Lock to manage concurrent access to the channel.
;; add!: A function to add items to the buffer.
;;
;; The ManyToManyChannel implementation manages the coordination of multiple concurrent reads and writes to the channel.
;; It is designed to be efficient and handle contention among multiple threads.
;; Here are some key points of the implementation:
;;
;; 1. `cleanup` 함수는 `put!`, `take!` 메서드가 시작될 때 호출되어 `takes` ,`puts` 리스트에서 비활성화(즉, 취소된) 작업을 제거한다.
;; 2. `put!` 은 여러 시나리오를 핸들링함. pending take가 있을 때, 버퍼가 찼을 때 등등. 또한 버퍼를 업데이트하고 실행할 콜백을 예약함.
;; 3. `take!` 도 비슷함. 버퍼가 비었을 때, pending put이 있을 때, 똑같이 버퍼 업데이트하고 콜백 예약.
;; 4. `close!` 메서드는 채널을 닫고 보류 중인 테이크 작업을 알리는 역할을 함. 필요한 경우 버퍼도 지움.
;;
;;
;; MMC cleanup, abort 프로토콜 (작업 정리 및 중단을 담당함)
;; 1. cleanup (takes와 puts를 각각 확이낳ㅁ)
;;  - `takes` 리스트경우:
;;    - `(when-not (.isEmpty takes)`  `takes` 가 비었는지 확인 (안 비었을 때만 돔)
;;      - `(loop [taker (.next iter)]` iterator로 `takes` 순회 (각 요소는 `taker` 가 바인딩)
;;        - `(when-not (impl/active? taker)`루프에서 현재 `taker` 가 활성 상태인지 확인
;;          - 활성상태가 아니면 `(.remove iter)` 로 제거
;;  - `puts` 리스트 경우:;;    - (when-not (.isEmpty puts)  `puts` 가 비었는지 확인 (안 비어있을 때만 돔)
;;     - (let [iter (.iterator puts) 루프돌기 위해 이터레이터 구현체 가져옴.(링크드 리스트니까)
;;      - (loop [[putter] (.next iter)]
;;
;;
;; `impl/ReadPort` protocol
;;
;; `take!` 함수를 정의하고, 채널에서 값을 가져오는(take) 하는 역할을 가짐.
;; 일단 코드를 보자.
(comment
  (deftype ManyToManyChannel [^LinkedList takes ^LinkedList puts ^Queue buf closed ^Lock mutex add!]
    impl/ReadPort
    (take!
   [this handler]
   (.lock mutex)  ;; step1, Lock the mutex ensure that only one operation (take or put) can access the channel's internal state at a time, avoiding race conditions
   (cleanup this)  ;; step2, removes inactive (committed or aborted) handlers from the takes and puts linked lists. This helps to keep the data structures clean and efficient.
   (let [^Lock handler handler
         ;;
         commit-handler (fn []
                          ;; 핸들러를 락함. 스레드 하나에서만 쓰려고.
                          (.lock handler)
                          ;; take-cb 변수가 정의됨. it will be assigned the result of the committed take operation if the handler is active
                          ;; impl/commit 은 커밋하기 위함.
                          ;; handler는 `take` operation을 위한 상태를 다루는 객체이다. Lock 인스턴스이며, Handler 프로토콜을 구현함.
                          ;; take-cb : take operation을 수행하는 콜백함수, take operation이 커밋될 때 실행되는 함수.(나중에 호출됨)
                          (let [take-cb (and (impl/active? handler) (impl/commit handler))]
                            ;; 핸들러를 언락. take operation이 커밋된 이후
                            (.unlock handler)

                            take-cb))]
     (if (and buf (pos? (count buf)))  ;; step3, check if the buffer has a value. there's at least one value in it, proceed with the next step. Otherwise jump to step 6
       (if-let [take-cb (commit-handler)] ;; step4, commit the take handler and get the value. `if-let [take-cb (commit-handler)]` - commit the handler and store the callback function
                                          ;; in `take-cb`. if the handler is active and committed, proceed to the next step. Otherwise, release the mutex and return `nil`.

         ;; step 5
         (let [val (impl/remove! buf) ;; step5, remove the value from the buffer and handle puts:
               iter (.iterator puts)  ;; puts 링크드 리스트 순회하기
               [done? cbs]
               (when (and (not (impl/full? buf)) (.hasNext iter)) ;; check if the buffer is not full and if there are any waiting put handlers:
                 ;; Loop through the waiting put handlers
                 (loop [cbs []  ;; initialize the loop with an empty list of callbacks `cbs`
                        [^Lock putter val] (.next iter)]  ;; and next put handler `putter`
                   (.lock putter) ;; Lock the put handler to prevent race conditions. 일단 락 걸고 확인.
                   (let [cb (and (impl/active? putter) (impl/commit putter))] ;; check if the put handler is active, commit it and store the callback function in `cb`
                     (.unlock putter) ;; put handler를 런락하고
                     (.remove iter)   ;; 대기 iterator에서 제거(?)
                     ;; update the list of callbacks and the `done?` flag
                     (let [cbs (if cb (conj cbs cb) cbs)  ;; if there's a callback function, add it to the `cbs` list.
                           ;; done?은 reduced? 함수를 버퍼와 값에 대한 add! 호출 결과에 적용한 결과로 설정함.
                           ;; core.async 맥락에서 reduced value는 버퍼가 한계에 도달했으며 더 이상 값을 받아들이지 않아야 함을 나타낸다.
                           ;; cb는 put 핸들러의 커밋 콜백이다. 존재하지 않으면 done?은 nil로 설정되며, "완료되지 않음"을 의미함.
                           ;; add! 는 buf에 room이 있다면 item `val` 을 버퍼에 넣고 버퍼를 리턴한다. (called under chan mutex)
                           ;; reduced는 기본 버퍼 타입에서는 사용하지 않음. 아마도 커스텀 버퍼를 위한 것일 수있다고함. (커스텀으로 무한 버퍼를 만들었다거나?, 크기를 알 수 없다면...? 루프 안도는 건가?)
                           done? (when cb (reduced? (add! buf val)))]  ;; set the `done?` flag if the callback is present and the buffer becomes reduced (reduced? what it means)
                       ;; continue the loop if the buffer isn't full, the `done?` flag is not set, and there are more put handlers:
                       (if (and (not done?) (not (impl/full? buf)) (.hasNext iter)) ;; if the conditions are met,
                         (recur cbs (.next iter))  ;;  recur with the updated list of callbacks and the next put handler
                         [done? cbs])))))]
           (when done?  ;; call abort if the `done?` flag is set:
             (abort this))  ;; abort function to close the channel.
           (.unlock mutex) ;; unlock the mutex to allow other operations to access the channel's internal state
           ;; call the committed put handler's callbacks with `true`:
           (doseq [cb cbs] ;; iterate through the callbacks in the `cbs` list and execute each callback with the value `true`, indicating that the put operation was successful.
             (dispatch/run #(cb true)))
           (box val)) ;; return the taken value (boxed) - wrapped in a `box` to ensure the value is returned as an object
         (do (.unlock mutex)
             nil))

       ;; STEP 6. check for waiting puts(버퍼가 없이 pending put이 있는 경우인가?)
       ;; handle cases when there are pending put handlers
       ;; if there are any waiting put handlers, proceed with the next step. Otherwise, jump 9
       (let [iter (.iterator puts) ;; `puts` 링크드리스트에서 iterator 꺼냄, pending put handlers 들임.
             [take-cb put-cb val] ;; there variables will store the matched take and put handler callbacks and the value from the matched put handler
             (when (.hasNext iter) ;; take-cb, put-cb, val 변수들은 세개가 서로 매칭되는 take/put핸들러와 put-handler의 값을 저장.
               (loop [[^Lock putter val] (.next iter)]
                 ;; Lock the putter and handler in ascending order based on their lock IDs to avoid deadlock
                 ;; lock-id를 이용하여 오름차순으로 락을 건다. (데드락 방지)
                 (if (< (impl/lock-id handler) (impl/lock-id putter))
                   (do (.lock handler) (.lock putter))
                   (do (.lock putter) (.lock handler)))
                 ;; 두 핸들러가 active한지 보고 그러하면 commit
                 (let [ret (when (and (impl/active? handler) (impl/active? putter))
                             [(impl/commit handler) (impl/commit putter) val])]
                   ;; 락을 품.
                   (.unlock handler)
                   (.unlock putter)
                   ;; handler matched take and put handlers
                   (if ret
                     (do ;; `ret` 이 nil이 아니면, 매칭되는 put handler를 `puts` 링크드리스트에서 제거하고 `take-cb`, `put-cb`, `val` 에 콜백과 value를 저장한다.
                       (.remove iter)
                       ret)
                     (when-not (impl/active? putter) ;; put 핸들러가 active 하지 않으면 `puts` 에서 제거한다. 그리고 다음 한들러로 간다. (recur로)
                       (.remove iter)
                       (when (.hasNext iter)
                         (recur (.next iter))))))))]
         ;; check if both `take-cb`, `put-cb` are present (예, match was found)
         (if (and put-cb take-cb)
           ;; 둘다 존재한다면, 매칭하는 put핸들러를 찾았다는 말임.
           (do
             (.unlock mutex) ;; 이것으로 다른 오퍼레이션이 채널의 내부상태에 엑세스 가능하도록함. (?? 이미 커밋하고 작업을 했다는 건가? take-cb, put-cb, val 나왔을 때 끝난 듯?)
             (dispatch/run #(put-cb true)) ;; put-handler에 `true` 와 함께 호출함. 이 말은 `put-cb` 가 성공이라는 것을 지칭함.
             (box val))                    ;; 이제 취해진 값을 리턴함.
           ;;check if channel is closed
           (if @closed
             (do
               ;; if there is a buffer, add an `end-of-buffer` marker to it
               (when buf (add! buf)) ;; 이게 end-of-buffer 마커인가?
               ;; determine if there is avalue left in the buffer, 버퍼에 값이 있는지 확인.
               (let [has-val (and buf (pos? (count buf)))]
                 ;; if the handler's commit callback is present, remove the remaining value from the buffer (if any)
                 ;; 핸들러의 커밋콜백이 있으면, 지우고 버퍼에 남은 값은 남겨둠.
                 (if-let [take-cb (commit-handler)]
                   (let [val (when has-val (impl/remove! buf))]
                     ;; 뮤텍스 언락
                     (.unlock mutex)
                     ;; 값을 리턴
                     (box val))
                   ;; 핸들러의 커밋콜백이 없다면 mutex를 언락하고 nil리턴
                   (do
                     (.unlock mutex)
                     nil))))
             ;; 채널이 클로저 되지 않았다면
             (do
               ;; 현재 take 하려는 거니까 take-handler와 매칭되는 put핸들러를 찾아본다.
               ;; take handler가 `blockable` takes의 사이즈가 puts의 사이즈가 맥스를 넘지 않았는지 본다.
               (when (impl/blockable? handler)
                 (assert-unlock mutex
                                (< (.size takes) impl/MAX-QUEUE-SIZE)
                                (str "No more than " impl/MAX-QUEUE-SIZE
                                     " pending takes are allowed on a single channel."))
                 ;; 그리고 take handler를 takes에 넣는다. (takes 대기열에 넣는다는 말인듯)
                 (.add takes handler))
               ;; 언락 뮤텍스
               (.unlock mutex)
               ;; 닐 리턴.
               nil)))))))
    ;;
    )
  ;; 1. 정리하면 mutex로 락을 걸로
  ;; 2. takes, puts 리스트에 비활성 오퍼레이션을 클리닝 하고
  ;; 3. 버퍼에 값이 있으면, handler 커밋 후 버퍼에서 값을 가져오고, 필요한 경우 보류 중인 put(pending puts)를 처리하고, 콜백을 실행하는 작업을 진행
  ;; 4. 버퍼에 값이 없으면, pending put operation을 찾아서, put 및 take 핸들러를 모두 커밋하고 put 콜백을 실행.
  ;; 5. 채널이 닫힌 경우 take 핸들러를 커밋하고, 사용 가능한 경우 값을 반환하고, 그렇지 않으면 nil을 반환한다.
  ;; 6. 위의 조건 중 어느 것도 충족되지 않으면 take handler를 `takes` 리스트에 넣고, `nil` 반환
  ;; 7. mutex 언락
  ;;
  ;; `handler` argument:
  ;; 이 `handler` 는 유저가 제공하는 콜백이며, 채널에 사용할 수 있는 값이 있을 때 호출되는 함수.
  ;; `handler` 는 `Lock` 구현체로 예상되며, `impl/Handler` 프로토콜을 따른다.
  ;; `handler` 는 콜백이 활성화되어 있는지, 콜백을 차단하거나 커밋할 수 있는지 확인하는데 사용됨.
  ;;
  ;; `commit` 이란
  ;; 핸들러를 커밋한다는 것은 해당 작업(take, put)이 진행될 것임을 확인하고, 콜백을 호출해야 함을 의미한다.
  ;; 핸들러가 커밋되면 "사용됨(used)" 로 간주되며 다른 어떤 연산도 동일한 핸들러를 커밋할 수 없다.
  ;; ReadPort 프로토콜 구현의 맥락에서 핸들러를 커밋한다는 것은 채널에서 값을 가져올 수 있음을 의미하며, 가져온 값으로 핸들러의 콜백이 호출된다.
  ;;
  (defrecord SimpleHandler [callback active? blockable?]
    (impl/Handler
      (active? [this] @active?)
      (blockable? [this] @blockable?)
      (commit [this]
              (if @active?
                (do
                  (reset! active? false)
                  (callback)
                  nil)))))
  ;; simple take handler
  (def take-handler (->SimpleHandler
                     (fn [val] (println "Value take from the cannel:" val))
                     (atom true)
                     (atom true)))

  ;; 아! handler는 이것을 구현한 것임.

(defprotocol Handler
  (active? [h] "returns true if has callback. Must work w/o lock")
  (blockable? [h] "returns true if this handler may be blocked, otherwise it must not block")
  (lock-id [h] "a unique id for lock acquisition order, 0 if no lock")
  (commit [h] "commit to fulfilling its end of the transfer, returns cb. Must be called within lock"))
  )

;; 보니까 여기에 handler구현체가 있다. https://github.com/clojure/core.async/blob/master/src/main/clojure/clojure/core/async/impl/runtime.clj
(defn- fn-handler
  [f]
  (reify
    Lock
    (lock [_]) ;; gpt4 는 간단한 기능에는 lock이 기능할 필요가 없다고 함.
    (unlock [_])

    impl/Handler
    (active? [_] true)
    (blockable? [_] true)
    (lock-id [_] 0)
    (commit [_] f)))
;; impl/ReadPort 구현체 읽다가 안 끝남...

;; add! 에 대해 알기 위해 protocols 를 다시 봄
(defn add!
  ([b] b)
  ([b itm]
   (assert (not (nil? itm)))
   (add!* b itm)))

(defprotocol Buffer
  (full? [b] "returns true if buffer cannot accept put")
  (remove! [b] "remove and return next item from buffer, called under chan mutex")
  (add!* [b itm] "if room, add item to the buffer, returns b, called under chan mutex")
  (close-buf! [b] "called on chan closed under chan mutex, return ignored"))

;; refactoring
(defrecord ManyToManyChannel [^Lock put-lock ^Lock take-lock puts takes buf]
  impl/Channel
  (close! [this]
    (.lock put-lock)
    (.lock take-lock)
    (doseq [put-handler (seq puts)]
      (impl/close! put-handler))
    (doseq [take-handler (seq takes)]
      (impl/close! take-handler))
    (impl/close-buf! buf)
    (.unlock take-lock)
    (.unlock put-lock))

  impl/ReadPort
  (take! [this fn-handler]
    (let [commit-handler (fn []
                            (.lock take-lock)
                            (let [take-cb (and (impl/active? fn-handler) (impl/commit fn-handler))]
                              (.unlock take-lock)
                              take-cb))]
      (commit-handler)
      (impl/take! buf fn-handler)))

  impl/WritePort
  (put! [this val fn-handler]
    (let [commit-handler (fn []
                            (.lock put-lock)
                            (let [put-cb (and (impl/active? fn-handler) (impl/commit fn-handler))]
                              (.unlock put-lock)
                              put-cb))]
      (commit-handler)
      (impl/put! buf val fn-handler)))

  impl/ChannelFactory
  (create [this]
    (ManyToManyChannel. (ReentrantLock.) (ReentrantLock.) (impl/ensure-buffer buf))))


(require '[clojure.core.async :as async]
         '[clojure.core.async.impl.protocols :as impl]
         '[clojure.core.async.impl.channels :as channels])
