(ns core08)

;;; go macro
;;; https://github.com/clojure/core.async/blob/master/src/main/clojure/clojure/core/async.clj#L443

(require '[clojure.core.async :as async])

async/go

(comment
(defmacro go
  [& body]
  ;; `crossing-env` 현재 환경 (&env)의 키를 gensym에서 생성한 새 심볼로 `zipmap` 하여 이름 충돌을 방지하기 위해 수행된다.
  (let [crossing-env (zipmap (keys &env) (repeatedly gensym))]
    ;; `c#` 은 버퍼사이즈 1인 채널이다. 이 채널은 body가 완료될 때, 결과를 수신하는데 사용된다.
    `(let [c# (chan 1)
           ;; `captured-bindings#` 현재 스레드의 바인딩 프레임을 캡처한다.
           ;; 스레드 간에 전환할 때 매크로가 바인딩을 보존할 수 있으므로 동적 바인딩을 처리할 때 유용하다.
           captured-bindings# (Var/getThreadBindingFrame)]
       ;; `dispatch/run`을 사용하여 상태 머신을 설정하고 실행하는 함수를 실행한다.
       ;; 실행함수를 go 매크로의 body를 비동기적으로 실행하는 역할을 한다.
       ;; dispatch/run 안에 executor가 있음 거기다가 일을 보냄.
       (dispatch/run
         (^:once fn* []
          ;; 함수 내부에서 crossing-env는 캡처된 환경의 변수를 새로운 익명 함수에 리바인딩하는 새로운 let 포맷을 만드는데 사용됨.
          (let [~@(mapcat (fn [[l sym]] [sym `(^:once fn* [] ~(vary-meta l dissoc :tag))]) crossing-env)
                ;; `f#` : ioc-macro에서 state-machine 함수를 호출하고 몇 가지 추가 인수와 함께 go 매크로의 본문을 전달한다.
                ;; 상태머신함수는 본문을 상태머신으로 변환한 다음 비동기 코드를 실행하는데 사용된다.
                f# ~((requiring-resolve 'clojure.core.async.impl.ioc-macros/state-machine)
                     `(do ~@body) 1 [crossing-env &env] ioc/async-custom-terminators)
                ;; `state#` : 특정 인덱스를 설정하여 상태 머신의 상태 객체를 초기화한다.
                state# (-> (f#)
                           ;; ioc/USER-START-IDX 결과 채널 c#을 이곳에 저장한다.
                           ;; ioc/BINDINGS-IDX 캡처링된 스레드 바인딩을 이곳에 저장한다.
                           (ioc/aset-all! ioc/USER-START-IDX c#
                                          ioc/BINDINGS-IDX captured-bindings#))]
            ;; 매크로는 마지막으로 초기화된 상태 객체로 ioc/run-state-machine-wrapped를 호출한다.
            ;; 이 함수는 상태 머신을 실행하고 여러 상태 간의 전환을 관리한다.
            (ioc/run-state-machine-wrapped state#))))
       c#)))
  ;; 요약하면 , go 매크로는 제공된 본문의 비동기 실행을 처리하기 위해 상태 머신을 설정한다.
  ;; 현재 환경과 동적 바인딩을 캡처하고 필요한 정보로 상태 머신을 초기화하여, run-state-machine-wrapped 함수를 사용하여 상태 머신을 실행한다. 실행이 완료되면
  ;; 매크로가 반환하는 결과 채널로 결과를 전송한다.
  ;;
  )
;; ex1
(require '[clojure.core.async :refer [go <! >! chan timeout]])

(defn async-example []
  (let [input (chan)
        output (chan)]
    (go
      (let [val (<! input)]
        (>! output (* 2 val))))

    (go (>! input 5)
        (println "Doubled value:" (<! output)))))

(async-example)

;; 아래 코드는 동작하지는 않지만 컨셉을 보기 좋다.

;; c__123 은 result 채널임.
(let [c__123 (chan 1)
      ;; current thread's binding frame is captured
      captured-bindings__124 (clojure.core/Var-getThreadBindingFrame)]
  (clojure.core.async.impl.dispatch/run ;; state machine을 동작시키기 위해 executor에 실행됨.
    (^:once
     fn* []
     (let [f__125
           ((clojure.core/async.impl.ioc-macros/state-machine)
            '(do
               (let [val (<! input)]
                 (>! output (* 2 val))))
            1
            [crossing-env &env]
            clojure.core.async.impl.ioc-macros/async-custom-terminators)
           state__126
           (-> (f__125)
               (clojure.core.async.impl.ioc-macros/aset-all!
                clojure.core.async.impl.ioc-macros/USER-START-IDX c__123
                clojure.core.async.impl.ioc-macros/BINDINGS-IDX captured-bindings__124))]
       ;; clojure.core.async.impl.ioc-macros/run-state-machine-wrapped 얘가 상태머신의 실행을 관장함.
       (clojure.core.async.impl.ioc-macros/run-state-machine-wrapped state__126))))
  c__123)
;;이렇게 해야 확장됨.. 이해는 안되지만..

(macroexpand
 '(let [input (chan)
        output (chan)]
    (go
      (let [
            val (<! input)]
        (>! output (* 2 val))))

    (go (>! input 5)
        (println "Doubled value:" (<! output)))))


(go
  (let [input (chan)
        output (chan)
        val (<! input)]
    (>! output (* 2 val))))

;; state machine을 사용하는 이유는 무엇인가?
;; multitasking and non-blocking 동작이 가능하게 하기 위함.
;; 이를 통해 <! >! 같은 블로킹 연산을 만나면 OS 스레드를 소모하지 않고 코드 블록의 실행을 '파킹'할 수 있다.
;; 대신 고정 스레드 풀에 의존하여 여러 개의 바둑 블록을 관리한다. 이렇게 하면
;; 라이브러리는 각각 작업에 대해 새 스레드를 생성하지 않고도 비동기 작업을 동시에 효율적으로 할 수 있다.

;; plain clojure를 쓰면 take!, put! 같은 블로킹 연산을 사용하면 전용 스레드를 생성해야 해서
;; 확장성이 떨어지고, 많은 수의 동시 작업에 과도한 리소스를 사용할 수 있다.

;; 상태 머신을 사용하면 core.async 가 `go` 블록의 실행을 더 작은 단계로 나누고 필요에 따라
;; 실행을 재개하고 파킹할 수 있다. 이는 바둑 블록의 코드를 채널 연산에 따라
;; 다양한 상태와 전환을 가진 상태 머신으로 변환함으로써 달성된다.

;; 요약하자면 상태 머신을 사용하는 것은 core.async 설계의 핵심 구성 요소로,
;; 고정된 스레드 풀로 많은 동시 작업을 효율적으로 관리하고 바둑 블록 내에서 채널 작업에 대한
;; 비차단 동작을 제공한다.
;; 유사한 메커니즘을 구현하지 않고 일반 Clojure코드로 동일한 수준의 효율성과 동시성을 달성할 수 없습니다.


(require '[clojure.core.async :as async :refer [<! >! go chan close!]])

(def input (chan))
(def output (chan))

; Producer go block
;; 0~4 까지 숫자를 생성하고 Thread/Sleep 으로 숫자를 입력 채널에 넣는 프로듀서
;; 모든 숫자를 생성하고 채널을 닫음.
(go
  (dotimes [n 5]
    (Thread/sleep 500)                  ; simulate work
    (>! input n))
  (close! input)) ; close the input channel when done

;; Consumer go block
;; 입력채널에서 값을 읽고 Thread/sleep 이후 값을 2배로 만듬.
(go
  (loop []
    (let [val (<! input)]
      (if (nil? val)
        (println "Input channel closed")
        (do
          (Thread/sleep 500)            ; simulate work
          (>! output (* 2 val))
          (recur))))))

;; Print the output
;; 출력 채널에서 값을 읽고 콘솔에 인쇄하는 프린터
;; 더이상 수신이 안될 때까지 (nil체크) 처리를 계속함.
(go
  (loop []
    (let [val (<! output)]
      (if (nil? val)
        (println "Done")
        (do
          (println "Output:" val)
          (recur))))))

;; 이 예제는 메인 스레드를 차단하지 않고 동시에 실행할 수 있는 가벼운 비동기 작업을 생성하는데
;; go 매크로가 사용됨.
;; 상태 머신은 go 매크로에 의해 암시적으로 사용되어 go 블록의 실행 흐름을 관리하므로
;; 효율적이고 블로킹 없는 채널작업이 가능함.
;;
;;core.async 없이 만들려면 스레드, 프로미스, 에이전트 같은 동시성 프리미티브를 사용해야한다.


;;;; parking
;; parking은 go 블록의 실행을 지연시키는 방법이다.
;; 파킹은 빈 채널ㅇ을 읽으려고 하거나 가득 찬 채널에 쓰는 것과 같이 차단할 수 있는 채널 연산이 있을 때
;; go 블록의 실행을 일시 중단하는 방법이다.
;;
;; 상태머신은 이러한 동작을 구현하는데 핵심적인 역할을 한다.
;; go 매크로가 사용되면 내부적으로 go block의 본문으로 상태머신으로 변환한다.
;; 상태머신은 go block의 실행흐름을 일련의 상태와 해상 상태간의 전환으로 나타낸다.
;; 각 상태는 코드의 세그먼트에 해당하며, 상태 머신은 채널 작업 및 준비 상태에 따라 상태 간에 전환할 수 있다.
;;
;; 차단 될 수 있는 채널 작업이 발생하면 상태 머신은 실행을 일시 중단하고
;; 현재 상태를 저장하여 실행을 "파킹"한다.
;; 이 시간 동안에 OS 스레드를 소비하지 않는다.
;; 채널 연산이 차단 해제되면 (값을 읽을 수 있거나 쓸 수 있는 공간이 확보되는 경우) 상태 머신은
;; 저장된 상태로부터 다시 실행을 재개한다.
;;
;; 다음은 상태 머신으로 작업을 주차(park)하고 재개(resuming) 하는 방법에 대한 개요.
;; 1. go 매크로는 코드의 세그먼트를 나타내는 상태가 있는 상태 머신으로 go블록의 body를 변환
;; 2. blocking channel operation을 만나면, 상태머신은 현재 상태를 저장하고 실행을 파킹한다.
;; 3. 파킹된 바둑 블록은 채널 작업을 대기 중인 작업 대기열에 추가된다.
;; 4. 채널 오퍼레이션에 대한 차단이 해제되면 상태 머신은 저장된 상태에서 실행을 재개하고 다음 상태 전환을 계속한다.
;; 5. execution of the `go` block은 끝에 도달할 때까지 진행하며, 이 시점에 상태 머신이 종료된다.

;; simplified version of go
(go
  (let [val (<! input)]
    (>! output (* 2 val))))

State 0:
  - Read from input channel
  - If successful, transition to State 1
  - If not, park and wait for input channel to have a value

State 1:
  - Compute the result (* 2 val)
  - Transition to State 2

Staet 2:
  - Write the result to output channel
  - If successful, transition to State 3 (end)
  - If not, park and wait for output channel to have space.


;; 파킹과 재개에 대해 알아보자.
;; 이것은 상태머신과 core.async.runtime에서 관리함.
;; 파킹은 채널 오퍼레이션 (<! >!) 를 진행할 수 없을 때 발생한다.
;; clojure.core.async.impl.runtime/take! , put! 이 그러하다.
;; 이 함수는 상태 머신을 파킹하고 각 채널의 큐에 등록하는 역할을 담당한다.
;;
;; 재개는 채널 작업을 진행할 수 있을 때 발생한다. core.async.runtime은 채널 조건이 충족되면
;; 파킹된 상태 머신을 재개하는 역할을 한다.
(macroexpand-1 '(go
                  (let [val (<! input)]
                    (>! output (* 2 val)))))
;; => (clojure.core/let
;;     [c__20168__auto__
;;      (clojure.core.async/chan 1)
;;      captured-bindings__20169__auto__
;;      (clojure.lang.Var/getThreadBindingFrame)]
;;     (clojure.core.async.impl.dispatch/run
;;      (fn*
;;       []
;;       (clojure.core/let
;;        [f__20170__auto__
;;         (clojure.core/fn
;;          state-machine__17236__auto__
;;          ([]
;;           (clojure.core.async.impl.runtime/aset-all!
;;            (java.util.concurrent.atomic.AtomicReferenceArray. 7)
;;            0
;;            state-machine__17236__auto__
;;            1
;;            1))
;;          ([state_23514]
;;           (clojure.core/let
;;            [old-frame__17237__auto__
;;             (clojure.lang.Var/getThreadBindingFrame)
;;             ret-value__17238__auto__
;;             (try
;;              (clojure.lang.Var/resetThreadBindingFrame
;;               (clojure.core.async.impl.runtime/aget-object state_23514 3))
;;              (clojure.core/loop
;;               []
;;               (clojure.core/let
;;                [result__17239__auto__
;;                 (clojure.core/case
;;                  (clojure.core/int
;;                   (clojure.core.async.impl.runtime/aget-object state_23514 1))
;;                  1
;;                  (clojure.core/let
;;                   [inst_23505 input]
;;;;;;;;;;;;;;;;;;;; input 채널에서 읽으려고 한다. State 1에서 파킹된다?
;;                   (clojure.core.async.impl.runtime/take!
;;                    state_23514
;;                    2
;;                    inst_23505))
;;                  2
;;                  (clojure.core/let
;;                   [inst_23507
;;                    (clojure.core.async.impl.runtime/aget-object state_23514 2)
;;                    inst_23508
;;                    inst_23507
;;                    val
;;                    inst_23508
;;                    inst_23509
;;                    output
;;                    val
;;                    inst_23508
;;                    inst_23510
;;                    (. clojure.lang.Numbers (multiply 2 val))]
;;;;;;;;;;;;;;;;;;;;;  output 채널에 쓰기를 하는 곳. 상태 2에서 파킹된다.
;;                   (clojure.core.async.impl.runtime/put!
;;                    state_23514
;;                    3
;;                    inst_23509
;;                    inst_23510))
;;                  3
;;                  (clojure.core/let
;;                   [inst_23512
;;                    (clojure.core.async.impl.runtime/aget-object state_23514 2)]
;;                   (clojure.core.async.impl.runtime/return-chan
;;                    state_23514
;;                    inst_23512)))]
;;                (if
;;                 (clojure.core/identical? result__17239__auto__ :recur)
;;                 (recur)
;;                 result__17239__auto__)))
;;              (catch
;;               java.lang.Throwable
;;               ex__17240__auto__
;;               (clojure.core.async.impl.runtime/aset-all!
;;                state_23514
;;                2
;;                ex__17240__auto__)
;;               (if
;;                (clojure.core/seq
;;                 (clojure.core.async.impl.runtime/aget-object state_23514 4))
;;                (clojure.core.async.impl.runtime/aset-all!
;;                 state_23514
;;                 1
;;                 (clojure.core/first
;;                  (clojure.core.async.impl.runtime/aget-object state_23514 4)))
;;                (throw ex__17240__auto__))
;;               :recur)
;;              (finally
;;               (clojure.core.async.impl.runtime/aset-object
;;                state_23514
;;                3
;;                (clojure.lang.Var/getThreadBindingFrame))
;;               (clojure.lang.Var/resetThreadBindingFrame
;;                old-frame__17237__auto__)))]
;;            (if
;;             (clojure.core/identical? ret-value__17238__auto__ :recur)
;;             (recur state_23514)
;;             ret-value__17238__auto__))))
;;         state__20171__auto__
;;         (clojure.core/->
;;          (f__20170__auto__)
;;          (clojure.core.async.impl.runtime/aset-all!
;;           clojure.core.async.impl.runtime/USER-START-IDX
;;           c__20168__auto__
;;           clojure.core.async.impl.runtime/BINDINGS-IDX
;;           captured-bindings__20169__auto__))]
;;;;;;;;;;;;; 상태 머신은 매크로로 확장된 코드의 끝에 있는 run-state-machiine-wrapped함수에 의해 다시 시작된다.
;;;;;;;;;;;;; 채널 작업이 수행될 준비가 되면 core.async.rintime은 업데이트된 상태로 상태머신함수를 다시 호출한다.
;;;;;;;;;;;;; 상태머신은 마지막으로 라킹된 상태부터 실행을 계쏙하여 효과적으로 계산을 재개한다.
;;;;;;;;;;;;; 파킹/재개 매커니즘은 core.async.runtime과 긴밀하게 결합되어 있으며, 그 구현 세부사항은 런타인 함수
;;;;;;;;;;;;; (take!, put!, run-state-machine-wrapped) 뒤에 숨겨져 있다는 점을 유의하자.
;;        (clojure.core.async.impl.runtime/run-state-machine-wrapped
;;         state__20171__auto__))))
;;     c__20168__auto__)

;; 좋아 take! put!에 대해서 알아보자.

(comment
  ;; 채널 c에서 값을 가져오려고 시도한다.
  ;; 성공하면, 수신된 값과 실행할 다음 상태(blk)로 상태를 업데이트 한다.
  ;; 즉시 가져올 수 없으면? impl/take! 를 이용하여 채널에 콜백함수를 등록한다. (fn-handler인듯)
  ;; 채널에서 값을 사용할 수 있게 되면 이 콜백 함수가 실행되어 상태를 업데이트하고 상태머신을 재개한다.
  (defn take! [state blk c]
    (if-let [cb (impl/take! c (fn-handler
                               (fn [x]
                                 (aset-all! state VALUE-IDX x STATE-IDX blk)
                                 (run-state-machine-wrapped state))))]
      (do (aset-all! state VALUE-IDX @cb STATE-IDX blk)
          :recur)
      nil))

  ;; 채널 c에 값 val을 넣으려고 시도한다.
  ;; 성공하면 결과와 실행할 다음 상태(blk)로 업데이트한다.
  ;; 그런 다음 실행 상태 머신 래핑 함수를 호출하여 상태 머신을 재개한다.
  ;; put! 연산을 바로 수행할 수 없는 경우 채널에 콜백을 등록한다.
  ;; 채널에 값에 대한 공간이 있으면 이 콜백 함수가 실행되어 상태를 업데이트하고 상태 머신을 재개한다.
  ;;
  (defn put! [state blk c val]
    (if-let [cb (impl/put! c val (fn-handler (fn [ret-val]
                                               (aset-all! state VALUE-IDX ret-val STATE-IDX blk)
                                               (run-state-machine-wrapped state))))]
      (do (aset-all! state VALUE-IDX @cb STATE-IDX blk)
          :recur)
      nil))

  ;; 예외가 발생하면 다시 던지기 전에 관련 채널(USER-START-IDX에 저장됨)이 닫혀있는지 확인한다.
  ;; 이 함수는 채널 조건이 충족되면 파킹된 상태 머신의 실행을 재개하는 역할을 가진다.
  (defn run-state-machine-wrapped [state]
    (try
      (run-state-machine state)
      (catch Throwable ex
        (impl/close! (aget-object state USER-START-IDX))
        (throw ex))))

  ;; 이 함수는 상태 객체를 받아 상태 머신 함수를 실행한다. (상태 객체의 FN-IDX 위치에 저장됨)
  ;; 기본적으로 주어진 상태로 상태 머신 함수를 호출한다.
  (defn run-state-machine [state]
    ;; 상태객체의 FN-IDX을 가져와서 state를 넣으면서 수행?
    ((aget-object state FN-IDX) state))
  )






;;; timers 예시
(require '[clojure.core.async :as async])

(defn print-message []
  (println "Message printed!"))

(defn schedule-message [timeout-ms]
  (async/go
    ;; 이 함수는 지정된 지정된 시간 후에 닫히는 채널을 반환함.
    ;; 그러므로 기다리다가 2초후에 닫히니까 출력하는거.
    ;; 시간 초과 이벤트 추적을 위해 DelayQueue, ConcurrentSkipListMap을 사용함.
    (let [timeout-chan (timeout timeout-ms)]
      (async/<! timeout-chan)
      (print-message))))

;; 2초 뒤에 옴.
(schedule-message 2000)
