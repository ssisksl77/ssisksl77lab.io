(ns core03)

;;;; dispatch file in core.async provides an abstraction over different dispatch mechanisms (like thread pools or javascript event loop)
;;;; that are used to run tasks asynchrously. It enables core.async to work in both clojure and ClojureScript environments with minimal changes.
;;;;

(ns ^{:skip-wiki true}
    clojure.core.async.impl.dispatch
    (:require [clojure.core.async.impl.protocols :as impl]
              [clojure.core.async.impl.exec.threadpool :as tp]))

(set! *warn-on-reflection* true)

;; 현재 스레드가 core.async dispatch pool의 부분인지 알기 위한 flag
(defonce ^:private in-dispatch (ThreadLocal.))
;; get, set, initialValue, remove, withInitial 이라는 메소드가 있음.

;; tp/thread-pool-executor를 이용해서 스레드 풀 executor를 생성하는 녀석.
;; `#(.set ^ThreadLocal in-dispatch true)` 는 이 executor가 생성한 스레드에 대해 `in-dispatch` 플래그를 `true`로 바꾼다.
;;   즉, 이 람다가 `init-fn` 인자임. 즉, 스레드풀에 있는 인자들만 `true`를 가지게 될 것. (나중에 어떻게 쓰이는지는 궁금하구만?)
;;
(defonce executor
  (delay (tp/thread-pool-executor #(.set ^ThreadLocal in-dispatch true))))

;; 현재 스레드가 core.async 디스패치 pool의 일부인지 확인하는 기능이다.
;; ThreadLocal 변수에서 `.get`을 호출하고 결과를 boolean으로 캐스팅한다.
;; 풀에 있는 스레드만 스레드 로컬에 true가 들어가 있을 거임.
(defn in-dispatch-thread?
  "Returns true if the current thread is a go block dispatch pool thread"
  []
  (boolean (.get ^ThreadLocal in-dispatch)))

(defn check-blocking-in-dispatch
  "If the current thread is a dispatch pool thread, throw an exception"
  []
  (when (.get ^ThreadLocal in-dispatch)
    (throw (IllegalStateException. "Invalid blocking call in dispatch thread"))))

;; 인자로 받은 ^Runnable r을 아까 생성한 `executor` 에서 실행한다. 이 호출은 `impl/exec` 에서함.
;; impl/exec는 정확한 구현은 모르지만 프로토콜에서 구현체가 있긴함.
;; 이것은 tp/thread-pool-executor 에서 구현하고 있음. `reify`를 이용해서!
(defn run
  "Runs Runnable r in a thread pool thread"
  [^Runnable r]
  (impl/exec @executor r))

;; 이게 중요함. dispatch/run이 뭐하나 했더니 executor를 사용하는 거네.
;; in-dispatch (ThreadLocal.) 을 이용해서 스레드별로 생성하는 건가?

;;;; thread pool
(ns clojure.core.async.impl.exec.threadpool
  (:require [clojure.core.async.impl.protocols :as impl]
            [clojure.core.async.impl.concurrent :as conc])
  (:import [java.util.concurrent Executors]))

(set! *warn-on-reflection* true)

;; clojure.core.async.pool-size system property로 수정하고 있음.
;; 1. command line으로 수정하기 : java -Dclojure.core.async.pool-size=16 -jar my-clojure-app.jar
;; 2. 프로그램으로 설정하기 :
;;  (System/setProperty "clojure.core.async.pool-size" "16")
;;  (require '[clojure.core.async :as async])
;;  core.async를 사용하기 전에 세팅을 미리 해야 한다!
;; 어떻게 문자열에서 `Long/getLong` 을 하면 동작한다는 거지?
;; 아래 링크를 보니까 Long/getLong은 system property를 가져오기 아이템이 맞음.
;; link - https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/Long.html#getLong(java.lang.String)
(def ^:private pool-size
  "Value is set via clojure.core.async.pool-size system property; defaults to 8; uses a
   delay so property can be set from code after core.async namespace is loaded but before
   any use of the async thread pool."
  (delay (or (Long/getLong "clojure.core.async.pool-size") 8)))

;; 스레드풀 executor의 인스턴스를 생성하고 반환함.
;; init-fn을 받는데 각 스레드가 초기화 할 때 호출하는 함수를 옵셔널로 받음.
;; Executor는 newFixedThreadPool을 받음.
(defn thread-pool-executor
  ([]
   (thread-pool-executor nil))
  ([init-fn]
   (let [executor-svc (Executors/newFixedThreadPool
                       @pool-size
                       (conc/counted-thread-factory "async-dispatch-%d"
                                                    true
                                                    {:init-fn init-fn}))]
     (reify impl/Executor
       (impl/exec [_ r]
         (.execute executor-svc ^Runnable r))))))

;;;;
;;;; protocol을 다 사용하니 가져와야겠다.

(ns ^{:skip-wiki true}
  clojure.core.async.impl.protocols)


;; ^:const - 이걸 넣으면 클로저 컴파일러가 최적화함.
;; ^{:tag 'int} - 타입힌트 넣는거임.
(def ^:const ^{:tag 'int} MAX-QUEUE-SIZE 1024)

;; 왜 자바꺼를 바로 쓰지 않는가?
;; 추상화 - core.async는 미래에 다른 구현이 존재할 수 있다. 그럴 때 기존 protocol을 바라보는 구현을 바뀌지 않도록 추상화됨
;; 유연성 - protocol로 Executor 기능을 지원하는 여러 타입을 만들 수 있다. 이 말은 다른 타입과 클래스를 위한 구현을 만들고 core.async code를 다른 실행 모델에서 재사용할 수 있다.
;; Clojure idiomatic : 프로토콜을 이용하는 것인 클로저 방식이다.
(defprotocol Executor
  (exec [e runnable] "execute runnable asynchronously"))

;; Defines a buffer that will never block (return true to full?)
(defprotocol UnblockingBuffer)


;;;; concurrent
;;;; https://github.com/clojure/core.async/blob/master/src/main/clojure/clojure/core/async/impl/concurrent.clj
(ns ^{:skip-wiki true}
  clojure.core.async.impl.concurrent
  (:import [java.util.concurrent ThreadFactory]))

(set! *warn-on-reflection* true)

;; 커스텀 스레드 팩토리임.
;; 자바 ThreadFactory를 확장함. (스레드에 이름을 붙이려면 이거 확장해야 하는 것으로 암)
;; name-format : 스레드 prefix 같은거.
;; daemon : `true` 면 스레드는 데몬스레드가 된다. 이 말은 메인 스레드가 완료될 때, JVM이 종료되는 것을 방해하지 않는다. (우선순위가 낮다는 말)
;;          `false` 면 스레드가 사용자 스레드가 되므로, JVM은 스레드가 오나료될 때까지 기다렸다가 종료한다.
;; opts : `:init-fn` 만 지원 중 새로 생성된 컨텍스트에서 호출되는 함수
(defn counted-thread-factory
  "Create a ThreadFactory that maintains a counter for naming Threads.
     name-format specifies thread names - use %d to include counter
     daemon is a flag for whether threads are daemons or not
     opts is an options map:
       init-fn - function to run when thread is created"
  ([name-format daemon]
   (counted-thread-factory name-format daemon nil))
  ([name-format daemon {:keys [init-fn] :as opts}]
   ;; 생성을 할 때마다 counter를 실행시켜서 name-format과 counter를 더함.
   ;; name-format은 어떤 포맷이름이 있어야 하고 아마 %d가 있어야 할 것 처럼 보인다. 실제로 위 사용사례를 보면 "async-dispatch-%d" 를 이용함.
   ;;
   (let [counter (atom 0)]
     (reify
       ThreadFactory
       (newThread [_this runnable]
         ;; init-fn 가 있으면 init-fn을 실행시키고 runnable을 .run으로 실행시키는 함수를 만들고, 없으면 runnable을 그냥 수행한다.
         (let [body (if init-fn
                      (fn []
                        (init-fn)
                        ;; start()를 하지 않는 이유는? start는 새로운 스레드를 생성한다.
                        ;; 그러나 start()를 하려면 `new Thread(runnable);` 처럼 들어간 후에 하는 것이다.
                        (.run ^Runnable runnable))
                      runnable)
               t (Thread. ^Runnable body)]
           (doto t
             (.setName (format name-format (swap! counter inc)))
             (.setDaemon daemon))))))))

(defonce
  ^{:doc "Number of processors reported by the JVM"}
  processors (.availableProcessors (Runtime/getRuntime)))

;;;;
