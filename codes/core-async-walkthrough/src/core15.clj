(ns core15)

;; clojure

;; deep walking macro
;; https://www.youtube.com/watch?v=HXfDK1OYpco&ab_channel=TimothyBaldridge


(defmulti parse-item (fn [form ctx]
                       (cond
                        (seq? form) :seq
                        (integer? form) :int
                        (symbol? form) :symbol
                        (nil? form) :nil)))

(defmulti parse-sexpr (fn [[sym & rest] ctx]
                        sym))

(defmethod parse-sexpr 'if
  [[_ test then else] ctx]
  {:type :if
   :test (parse-item test ctx)
   :then (parse-item then ctx)
   :else (parse-item else ctx)})

(defmethod parse-sexpr 'do
  [[_ & body] ctx]
  {:type :do
   :body (doall (map (fn [x] (parse-item x ctx))
                     body))})

(defmethod parse-sexpr :default
  [[f & body] ctx]
  {:type :call
   :fn (parse-item f ctx)
   :args (doall (map (fn [x] (parse-item x ctx))
                     body))})

(defmethod parse-item :seq
  [form ctx]
  (let [form (macroexpand form)]
    (parse-sexpr form ctx)))

(defmethod parse-item :int
  [form ctx]
  (swap! ctx inc)
  {:type :int
   :value form})

(defmethod parse-item :symbol
  [form ctx]
  {:type :symbol
   :value form})

(defmethod parse-item :nil
  [form ctx]
  {:type :nil})


(defmacro to-ast [form]
  (pr-str (parse-item form (atom 0))))

(defn r-assoc [k v m]
  (assoc m k v))

(def add-me
  (partial r-assoc :name "ME"))

(add-me {})
;; {:name "ME"}


(defn thread-it [& fns]
  (fn [initial]
    (reduce
     (fn [acc f]
       (f acc))
     initial
     fns)))

(defn add-personal-info []
  (thread-it (partial r-assoc :name "ME")
             (partial r-assoc :last-name "LAST")
             (partial r-assoc :age 30)))

(defn add-job-info []
  (thread-it (partial r-assoc :job "DEV")))

;; functional pure way with having worring about perhaps mutating state
((thread-it (add-personal-info)
            (add-job-info))
 {})


(require '[clojure.core.async.impl.ioc-macros :refer [assoc-in-plan gen-plan all get-plan]])

((assoc-in-plan [:test] "Hello") {})
;; => ["Hello" {:test "Hello"}]

((gen-plan
  ;; associate into the context {:test "HELLO"}
  ;; assoc-v (vlaue) => "HELLO"
  [assoc-v (assoc-in-plan [:test] "HELLO")]
  (str "value is " assoc-v))
 {})
;; => ["value is HELLO" {:test "HELLO"}]

#_((gen-plan
  ;; all -> seq of function -> single function
  [assoc-v (for [x (range 5)]
                   (assoc-in-plan [:test]
                                  "HELLO"))]
  (str "value is " assoc-v))
 {})
;; LazySeq cannot be cast to clojure.lang.IFn

((gen-plan
  ;; all -> seq of function -> single function
  [assoc-v (all (for [x (range 5)]
                  (assoc-in-plan [:test] "HELLO")))]
  (str "value is " assoc-v))
 {})
;; => ["value is [\"HELLO\" \"HELLO\" \"HELLO\" \"HELLO\" \"HELLO\"]" {:test "HELLO"}]


(-> (gen-plan
     ;; all -> seq of function -> single function
     [assoc-v (all (for [x (range 5)]
                     (assoc-in-plan [:test] (str "HELLO" x))))]
     (str "value is " assoc-v))
    get-plan)
;; => ["value is [\"HELLO0\" \"HELLO1\" \"HELLO2\" \"HELLO3\" \"HELLO4\"]" {:test "HELLO4"}]

;; ssa-format
(require '[clojure.core.async.impl.ioc-macros :refer [assoc-in-plan gen-plan all get-plan
                                                      parse-to-state-machine]])
(require '[clojure.core.async :refer [>!]])
;; state-machine
(-> (parse-to-state-machine '[(if (= x 1) :true :false)] {})
    (clojure.pprint/pprint))
(comment [inst_20351
          {:bindings {:terminators ()},
           :block-id 1,
           :blocks
           {1
            [{:ast [(if (= x 1) :true :false)], :locals nil, :id inst_20350}
             {:value inst_20350, :id inst_20351}]},
           :block-catches {1 nil},
           :start-block 1,
           :current-block 1}]

         {1 {id1 (= x 1)
             condjmp id1 2 3}
          2 {jmp 4 :value :true}
          3 {jmp 4 :value false}
          4 {return :value}})
;; 예시처럼 안됨 이제 core.async는 이제 jvm/analyzer를 사용하므로 이렇게 해서는 알 수 없음.
(-> (parse-to-state-machine '[(loop [x 0]
                                (if (= x 5)
                                  :done
                                  (do (>! c x)
                                      (recur (inc)))))] {})
    (clojure.pprint/pprint))

(comment
  ;; loop에 대한 상태머신 추상화.
  ;; #1
  x = 0
  jmp #2

  ;; #2 phi node says look at what you came from. and firgure out the value of x1 based upon previous value
  ;; #1 에서 왔으면 x #5에서 왔으면 x2
  x1 = phi {#1 x #5 x2}
  cmpv = cmp x1 5
  condjmp cmpv #3 #4

  ;; #3
  return :done

  ;; #4
  ;; 커스텀.
  put!-term 5 c x1 ;; x1은 phi임. 어디까지 진행되었는지 알 수 있음. 5도 필요한 숫자겠지? 5는 #5같음. put하고 inc하는 거니까 하지만 강의 코드 그대로 쓰겠음.

  ;; #5
  x2 = inc x1
  jmp #2
  ;; 이렇게 나눌 수 있으면 어떤 시점에도 멈추고 시작할 수 있다고 한다.
  ;; 어떤 경우에도 트래킹해야 하는 값은 무엇인가? 아래 3개다.
  [x x1 x2]
  ;; 이것들을 다시 연결하는 거다.
  (loop []
    (let [result
          (case (:block-id state)
            1 (do (aset state :x 0)
                  (aset state :block-ie 2)
                  :recur)
            2 (......block 2......))]
      (if (identical? result :recur)
        (recur)
        result)))
  ;; 40:13
  (loop [x 0]
    (if (= x 5)
      :done
      (do (>! c x)
          (recur (inc)))))
  ;; --->
  [inst_2746
   {:current-block 3
    :start-block 1
    :block-catches {1 nil 2 nil 3 nil 4 nil 5 nil 6 nil}
    :blocks {6
             [{:values :clojure.core.async.impl.ioc-macro/value, :id inst_2743}
              {:value inst_2743 :block 3 :id inst_2744}]
             5
             ;; 3.1 inst_2734값을 c에 넣음. 리턴값은 inst_2739 인가봄.
             ;; 3.2 리턴값 inst_2739를 inc
             ;; 3.3 inc값을 inst_2740에 저장.
             ;; 3.4 재귀수행(recur) recur-nodes=inst_2734는 x를 말함.
             ;; 3.5 ids는 마지막 값인듯. 그리고 이 리턴값을 inst_2741이라고 칭하는 듯
             ;; 3.6 이후 block 2로 감.
             [{:refs [>! c inst_2734] :id inst_2739}
              {:refs [inc] :id inst_2740}
              {:recur-nodes [inst_2734] :ids [inst_2740] :id inst_2741}
              {:value nil :block 2 :id inst_2738}]
             4 [{:value :done :block 6 :id inst_2738}]
             3 [{:value :clojure.core.async.impl.ioc-macros/value :id inst_2745}
                {:value inst_2745 :id inst_2746}]
             ;; 2.1 (= x 5) 값을 inst_2736에 저장.
             ;; 2.2 test inst_2736 -> then-block 4번, else-block 5번 중에 감.
             2 [{:refs [= inst_2734 5] :id inst_2736}
                {:test inst_2736 :then-block 4 :else-block 5 :id inst_2737}]
             ;; 1. 여기서 시작.
             ;; 1.1 inst_2734(x) = 0
             ;; 1.2 jmp 2
             1 [{:value 0 :id inst_2734}
                {:value nil :block 2 :id inst_2735}]}
    :block-id 6
    :bindings {:recur-nodes (), :recur-point () :terminators () :locals ()}}]
  )
;; item-to-ssa 를 보는 것은 재미 있을 것이다.
;; 특히 :list,
;; sexper-to-ssa 'if (이거 버전업 하면서 사라진듯.. jvm-analyzer 쓰는 버전으로 바뀜)
;; 대신 item-to-ssa :if 가 동일한 일을 함.

(comment

  (defmethod -item-to-ssa :if
    [{:keys [test then else]}]
    (gen-plan
     ;; 무엇을 하든 먼저 context가 생길 것이다. 어딘가에 있을 것이다.
     ;; 현재 위치를 블록으로 만든다. 그리고 그 블록을 context에 넣는다.
     ;; test를 item-to-ssa로 parse하고 그 표현식에 대한 ID를 리턴받아서 test-id를 리턴한다.
     [test-id (item-to-ssa test)
      ;;블록만 일단 만드는 것이다.
      then-blk (add-block)
      else-blk (add-block)
      final-blk (add-block)
      ;; 조건절에 block들을 넣는다.
      ;; 이것을 인스트럭션에 넣는다는 뜻인듯. 이게 컨텍스트에 들어가나봄..?
      _ (add-instruction (->CondBr test-id then-blk else-blk))
      ;; then 블록을 현재 블록으로 세팅.
      _ (set-block then-blk)
      ;; then표현식을 파싱하고 then-id리턴.
      then-id (item-to-ssa then)
      ;; ::terminated 는 뭘까?
      ;; ->Jmp는 final-blk로 가는 것인데...
      ;; (recur) 재귀가 끝나면 ::terminated가 리턴되는 것 같다.
      _ (if (not= then-id ::terminated)
          (gen-plan
           [_ (add-instruction (->Jmp then-id final-blk))]
           then-id)
          (no-op))

      _ (set-block else-blk)
      else-id (item-to-ssa else)
      _ (if (not= else-id ::terminated)
          (gen-plan
           [_ (add-instruction (->Jmp else-id final-blk))]
           then-id)
          (no-op))

      ;; add-instruction은 블록에 인스트럭션을 추가하는 것이다.
      _ (set-block final-blk)
      val-id (add-instruction (->Const ::value))]
     val-id))
  ;;
  )

;; Core Async Go Macros - Part 2
;; https://www.youtube.com/watch?v=SI7qtuuahhU&ab_channel=TimothyBaldridge
;; test에 대해 알아보자.
;; clojure.core.async.ioc-macros-test
;; 저자가 배열의 의미를 설명한다. 귀하다
(def ^:const FN-IDX 0) ;; state machine function 자체를 저장함.
(def ^:const STATE-IDX 1) ;; next state. start with 1 그리고 다른 숫자들로 바뀐다. block id와 같고, 어디로 jmp할지를 결정한다.
(def ^:const VALUE-IDX 2) ;; result of recently run expression.
(def ^:const BINDINGS-IDX 3) ;; 바인딩 var-binding.
(def ^:const EXCEPTION-FRAMES 4)  ;; 방식이 바꼈다고함. 예전방식은 EXCEPTION을 받아서 그냥 스택을 버리고 예외를 핸들링
(def ^:const CURRENT-EXCEPTION 5) ;; 요즘 방식은 예외를 다시 state machine으로 만들어서 state machine 안에 state machine을 넣음.
(def ^:const USER-START-IDX 6)
;; you can define when you create staet macihne
;; how many slot available in your implementation.
;; (ioc/state-machine body 0 &env terminators)
;; this is a user defined area.
;; go macro has set to 1 in that slot and stores a return channel.
;; and go write to in that slot
;; [f# ~(ioc/state-machine `(do ~@body) 1 [crossing-env &env] ioc/async-custom-terminators)]



(defn pause-run [state blk val]
  ;; state is an mutable array of objects
  ;; block to continue to
  ;; value
  ;; aset-all! is special type function of all
  ;; ioc/STATE-IDX :
  ;;
  (ioc/aset-all! state ioc/STATE-IDX blk ioc/VALUE-IDX val)
  :recur)

(defmacro runner
  "Creates a runner block. The code inside the body of this macro will be translated
  into a state machine. At run time the body will be run as normal. This transform is
  only really useful for testing."
  [& body]
  (let [terminators {'pause `pause-run}
        crossing-env (zipmap (keys &env) (repeatedly gensym))]
    ;; 바인딩 생성.
    `(let [captured-bindings# (clojure.lang.Var/getThreadBindingFrame)
           ~@(mapcat (fn [[l sym]] [sym `(^:once fn* [] ~l)]) crossing-env)
           ;; state machine을 만든다.
           ;; &env 는 매크로에서 사용된 모든 바인딩을 담고있다.
           ;; terminators를 새로 넣는 것.
           ;; state-machine은 함수 하나를 리턴함. (emit-state-machine) 이 하는 일임.
           state# (~(ioc/state-machine `(do ~@body) 0 [crossing-env &env] terminators))]
       (ioc/aset-all! state#
                      ~ioc/BINDINGS-IDX
                      captured-bindings#)
       ;; 이렇게 state-machine을 실행함.
       (ioc/run-state-machine state#)
       (ioc/aget-object state# ioc/VALUE-IDX))))

(defn- emit-state-machine [machine num-user-params custom-terminators]
  (let [index (index-state-machine machine)
        state-sym (with-meta (gensym "state_")
                    {:tag 'objects})
        local-start-idx (+ num-user-params USER-START-IDX)
        state-arr-size (+ local-start-idx (count-persistent-values index))
        local-map (atom {::next-idx local-start-idx})
        block-catches (:block-catches machine)]
    `(fn state-machine#
       ([] (aset-all! (AtomicReferenceArray. ~state-arr-size)
                      ~FN-IDX state-machine#
                      ~STATE-IDX ~(:start-block machine)))
       ([~state-sym]
        ;; everytime we enter in. we save old frame.

        (let [old-frame# (clojure.lang.Var/getThreadBindingFrame)
              ;; and run the body
              ;; but before we do reset Var all the binding state
              ;; (clojure.lang.Var/resetThreadBindingFrame (aget-object ~state-sym ~BINDINGS-IDX))
              ret-value# (try
                           ;; this is pretty low level
                           ;;when you start go block in one thread. and it will be executed later on the another thread.
                           ;; but it will use the original thread's Var
                           (clojure.lang.Var/resetThreadBindingFrame (aget-object ~state-sym ~BINDINGS-IDX))
                           (loop []
                             (let [result# (case (int (aget-object ~state-sym ~STATE-IDX))
                                             ~@(mapcat
                                                (fn [[id blk]]
                                                  [id `(let [~@(concat (build-block-preamble local-map index state-sym blk)
                                                                       (build-block-body state-sym blk))
                                                             ~@(build-new-state local-map index state-sym blk)]
                                                         ~(terminate-block (last blk) state-sym custom-terminators))])
                                                (:blocks machine)))]
                               (if (identical? result# :recur)
                                 (recur)
                                 result#)))
                           (catch Throwable ex#
                             (aset-all! ~state-sym ~VALUE-IDX ex#)
                             (if (seq (aget-object ~state-sym ~EXCEPTION-FRAMES))
                               (aset-all! ~state-sym ~STATE-IDX (first (aget-object ~state-sym ~EXCEPTION-FRAMES)))
                               (throw ex#))
                             :recur)
                           (finally
                             (aset-object ~state-sym ~BINDINGS-IDX (clojure.lang.Var/getThreadBindingFrame))
                             ;; when we are done. we put back the old frame.
                             (clojure.lang.Var/resetThreadBindingFrame old-frame#)))]
          (if (identical? ret-value# :recur)
            (recur ~state-sym)
            ret-value#))))))


(defn run-state-machine [state]
  ;; state는 배열인듯.
  ;; FN-IDX 안에 상태함수가 있다는 것을 말함.
  ((aget-object state FN-IDX) state))


;; 이것에 대해 알아보자.
(defprotocol IInstruction
  (reads-from [this] "Returns a list of instructions this instruction reads from")
  (writes-to [this] "Returns a list of instructions this instruction writes to")
  (block-references [this] "Returns all the blocks this instruction references"))

(defprotocol IEmittableInstruction
  (emit-instruction [this state-sym] "Returns the clojure code that this instruction represents"))

(defprotocol ITerminator
  (terminator-code [this] "Returns a unique symbol for this instruction")
  (terminate-block [this state-sym custom-terminators] "Emites the code to terminate a given block"))


(defn- emit-state-machine [machine num-user-params custom-terminators]
  (let [index (index-state-machine machine)
        state-sym (with-meta (gensym "state_")
                    {:tag 'objects})
        local-start-idx (+ num-user-params USER-START-IDX)
        state-arr-size (+ local-start-idx (count-persistent-values index))
        local-map (atom {::next-idx local-start-idx})
        block-catches (:block-catches machine)]
    `(fn state-machine#
       ([] (aset-all! (AtomicReferenceArray. ~state-arr-size)
                      ~FN-IDX state-machine#
                      ~STATE-IDX ~(:start-block machine)))
       ([~state-sym]
          (let [old-frame# (clojure.lang.Var/getThreadBindingFrame)
                ret-value# (try
                             (clojure.lang.Var/resetThreadBindingFrame (aget-object ~state-sym ~BINDINGS-IDX))
                             (loop []
                               (let [result# (case (int (aget-object ~state-sym ~STATE-IDX))
                                               ;; preamble (아래 링크 확인)
                                               ~@(mapcat
                                                  (fn [[id blk]]
                                                    [id `(let [~@(concat (build-block-preamble local-map index state-sym blk)
                                                                         (build-block-body state-sym blk))
                                                               ~@(build-new-state local-map index state-sym blk)]
                                                           ~(terminate-block (last blk) state-sym custom-terminators))])
                                                  (:blocks machine)))]
                                 (if (identical? result# :recur)
                                   (recur)
                                   result#)))
                             (catch Throwable ex#
                               (aset-all! ~state-sym ~VALUE-IDX ex#)
                               (if (seq (aget-object ~state-sym ~EXCEPTION-FRAMES))
                                 (aset-all! ~state-sym ~STATE-IDX (first (aget-object ~state-sym ~EXCEPTION-FRAMES)))
                                 (throw ex#))
                               :recur)
                             (finally
                               (aset-object ~state-sym ~BINDINGS-IDX (clojure.lang.Var/getThreadBindingFrame))
                               (clojure.lang.Var/resetThreadBindingFrame old-frame#)))]
            (if (identical? ret-value# :recur)
              (recur ~state-sym)
              ret-value#))))))

;; emit-state-machine 에 build-block-body...
(let [x1 (aget-object state 42) ;; preamble

      x2 (inc x1) ;; body

      state (aset-all! state 43 x2) ;; new state
      ]
  (do (aset-all! state STATE-IDX 4)
      :recur))


(defn add-instruction
  "Appends an instruction to the current block. "
  [inst]
  ;; 블록에 인스트럭션을 추가할 때마다 유니크아이디를 추가한다.
  (let [inst-id (with-meta (gensym "inst_")
                  {::instruction true})
        ;; 이처럼 instruction에 :id를 추가한다.
        inst (assoc inst :id inst-id)]
    ;; 추가하고 gen-plan을 다시해(?) 현재 블록을 만들어서
    (
    (gen-plan
     [blk-id (get-block)
      _ (update-in-plan [:blocks blk-id] (fnil conj []) inst)]
     inst-id))))

(defrecord Call [refs]
  IInstruction
  (reads-from [this] refs)
  (writes-to [this] [(:id this)])
  (block-references [this] [])
  IEmittableInstruction
  (emit-instruction [this state-sym]
    ;; this의 id가 유니크한 id를 말하는 듯.
    `[~(:id this) ~(seq refs)]))

(defmethod -item-to-ssa :invoke
  [{f :fn args :args}]
  (gen-plan
   [arg-ids (all (map item-to-ssa (cons f args)))
    ;; add-instruction이 id를 추가해준다.
    inst-id (add-instruction (->Call arg-ids))]
   inst-id))

(defrecord CustomTerminator [f blk values meta]
  IInstruction
  (reads-from [this] values)
  (writes-to [this] [])
  (block-references [this] [])
  ITerminator
  (terminate-block [this state-sym _]
    ;; call this function
    ;; state-sym 배열, block here
    ;; value 여러개로 slice함.
    (with-meta `(~f ~state-sym ~blk ~@values)
      meta)))


;; 이거 특이하다고 함.
(defrecord Fn [fn-expr local-names local-refs]
  IInstruction
  (reads-from [this] local-refs)
  (writes-to [this] [(:id this)])
  (block-references [this] [])
  IEmittableInstruction
  (emit-instruction [this state-sym]
    `[~(:id this)
      ;; 함수를 실행할 때 어떤 로컬 바인디잉 있는지 모른다.
      ;; 모든 것들을 가져와서 let에 연결함.
      ;; analyzer로 하면 let에 모든 것을 넣지 않고, 필요한 것만 넣을 수 있을 것 같다.
      ;; (지금 그렇게 하는 듯...?)
      (let [~@(interleave local-names local-refs)]
        ~@fn-expr)]))

;; idx : which one is written to another block
(defn- build-block-preamble [local-map idx state-sym blk]
  (let [args (->> (mapcat reads-from blk)
                  (filter instruction?)
                  (filter (partial persistent-value? idx))
                  set
                  vec)]
    (if (empty? args)
      []
      (mapcat (fn [sym]
                `[~sym (aget-object ~state-sym ~(id-for-inst local-map sym))])
              args))))

;; does this value live outside of the block
;; all only live inside the block
(defn persistent-value?
  "Returns true if this value should be saved in the state hash map"
  [index value]
  ;; 이런 인덱스도 있는 거구만?
  ;; {:read-in #{:a :b :c} :written-in #{:a :b :c}} 이런 형태인듯??
  (or (not= (-> index value :read-in)
            (-> index value :written-in))
      ;; 블록하나 이상에 있는 거라면? presistent-value 인 듯.
      (-> index value :read-in count (> 1))))

;; 31:00
(defn id-for-inst [m sym] ;; m :: symbols -> integers
  ;; 심볼이 이미 m에 있으면
  (if-let [i (get @m sym)]
    i
    ;; 없으면 idx를 올리고
    ;; 새로운 인덱스를 추가한다. ::next-idx를 증가시키고
    ;; 해당 심볼을 새로운 인덱스에 세팅함.
    (let [next-idx (get @m ::next-idx)]
      (swap! m assoc sym next-idx)
      (swap! m assoc ::next-idx (inc next-idx))
      next-idx)))


(defn- build-new-state [local-map idx state-sym blk]
  (let [results (->> blk
                     (mapcat writes-to)
                     (filter instruction?)
                     (filter (partial persistent-value? idx))
                     set
                     vec)
        results (interleave (map (partial id-for-inst local-map) results) results)]
    (if-not (empty? results)
      [state-sym `(aset-all! ~state-sym ~@results)]
      [])))
