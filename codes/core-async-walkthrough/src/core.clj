(ns core)

;; Define a simple channel structure
(defrecord Channel [buffer])

;; Create a new channel
(defn chan
  []
  (->Channel (atom [])))

;; Put a value into a channel
(defn >!
  [ch value]
  (swap! (:buffer ch) conj value))

;; Take a value from a channel
(defn <!
  [ch]
  (loop []
    (let [buf @(:buffer ch)]
      (if (seq buf)
        (do
          (swap! (:buffer ch) subvec 1)
          (first buf))
        (recur)))))

;; Simulate go blocks using futures
(defmacro go
  [& body]
  `(future ~@body))

(defn -main
  [& args]
  (let [c (chan)]
    (go
      (println "Putting 42 into channel")
      (>! c 42))

    (go
      (println "Taking value from channel")
      (Thread/sleep 1000)
      (let [val (<! c)]
        (println "Reading from channel:" val)))
    (Thread/sleep 2000)))


;;;
;;;
