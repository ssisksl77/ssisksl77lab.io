(ns demo1.portal
  (:require [portal.api :as inspect]))

(comment
  (inspect/open)
  (inspect/tap)
  (tap> {:accounts [{:name "jen" :email "jen@jen.com"} {:name "sara" :email "sara@sara.com"}]})

  (inspect/clear)
  (inspect/close)
  ;;
  )
