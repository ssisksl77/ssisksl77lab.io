(ns demo1.system
  (:require
   [demo1.db :as db :refer [->SqliteReviewRepository]]
   [demo1.server :as server]
   [demo1.router :as router]
   [integrant.core :as ig]
   [io.pedestal.http :as http]
   [next.jdbc :as jdbc]))

;;;; DB 초기 세팅 BEGIN
(def db
  {:connection-uri "jdbc:sqlite:db/review.db"})

(defn create-db
  "create db and table"
  []
  (jdbc/execute! db
             ["create table reviews (content text)"]))

(defn init-test-data
  []
  (jdbc/execute-one! db
                     ["insert into reviews(content) values ('number 1'),
                                                           ('number 2'),
                                                           ('number 3')"]))

(defn print-all-reviews
  []
  (jdbc/execute! db ["select rowid, * from reviews"]))

(comment
  (init-test-data))
;;;; DB 초기 세팅 END

;;; integrant
(def config
  {::data-source        db
   ::get-greet-handler  {}
   ::get-review-handler {:review-repository (ig/ref ::review-repository)}
   ::review-repository  {:data-source (ig/ref ::data-source)}
   ::router             {:route->handler {:get-review (ig/ref ::get-review-handler)
                                          :greet (ig/ref ::get-greet-handler)}}
   ::http-server        {:router (ig/ref ::router)
                         :port    8080}})

(defmethod ig/init-key ::get-greet-handler [_ system]
  server/greet)

(defmethod ig/init-key ::data-source [_ system]
  system)

(defmethod ig/init-key ::review-repository [_ {:keys [data-source]}]
  (->SqliteReviewRepository data-source))

(defmethod ig/init-key ::get-review-handler [_ {:keys [review-repository]}]
  #(server/get-review review-repository %))

(defmethod ig/init-key ::router [_ {:keys [route->handler]}]
  (router/router route->handler))

(defmethod ig/init-key ::http-server [_ {:keys [router port]}]
  (println ":http-server init " router)
  (let [server (http/create-server {::http/routes router
                                    ::http/type :jetty
                                    ::http/port port
                                    ::http/join? false})]
    (http/start server)
    server))

(defmethod ig/halt-key! ::http-server [_ server]
  (println "halt :http-server" server)
  (http/stop server))


(comment
  (def running-system (ig/init config))

  (ig/halt! running-system)

  @server-atom
  (init)
  (stop)
  ;;

  (require '[clojure.inspector :as inspector])

  (inspector/inspect-tree
   {:star-wars
    {:characters
     {:jedi ["obiwan kenobi" "Yoda" "Master Wendoo"]
      :sith ["Palpatine" "Count Dukoo"]}}})
  )
