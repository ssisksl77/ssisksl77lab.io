(ns demo1.router
  (:require
   [io.pedestal.http.route :as route]))

(defn router [route->handler]
  (route/expand-routes
   #{["/greet" :get (route->handler :greet) :route-name :greet]
     ["/reviews/:id" :get (route->handler :get-review) :route-name :reviews]}))

(comment
  (route/try-routing-for routes :prefix-tree "/greet" :get)
  (route/try-routing-for routes :prefix-tree "/greet" :post)
  ;;
  )
