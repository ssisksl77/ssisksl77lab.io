(ns demo1.review-repository)

(defprotocol ReviewRepository
  (get-review-by-id [_ id]))
