(ns demo1.db
  (:require [next.jdbc :as jdbc]
            [demo1.review-repository :refer [ReviewRepository]]))

(defrecord SqliteReviewRepository [data-source]
  ReviewRepository
  (get-review-by-id [_ id]
    (let [result (jdbc/execute-one! data-source ["select * from reviews where rowid=?" id])]
      result)))

(comment
  (SqliteReviewRepository. {})
  ;;
  )
