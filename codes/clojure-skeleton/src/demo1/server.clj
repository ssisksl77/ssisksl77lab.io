(ns demo1.server
  (:require [demo1.review-repository :as review-repository]
            [io.pedestal.http :as http]))

(defn greet
  [req]
  {:status 200 :body "Hello, world!"})

(defn get-review
  [review-repository req]
  (let [id (-> req :path-params :id)]
    {:status 200 :body (str (review-repository/get-review-by-id review-repository id))}))
