(ns demo1.db-test
  (:require [demo1.db :as sut]
            [clojure.test :as t]))

(def db-fixture [{:id 1 :content "TEST_ID_1"}
                 {:id 2 :content "TEST_ID_2"}])

(t/deftest get-article-by-id
  (t/testing
      (let [expected (first db-fixture)]
        (t/is expected
              (sut/get-article-by-id 1)))))

(comment
  (t/run-tests)
  ;;
  )
