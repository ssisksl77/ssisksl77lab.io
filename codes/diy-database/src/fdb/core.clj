(ns fdb.core
  (:use [fdb.storage]
        [fdb.constructs])
  (:require [clojure.set :as CS]))

(defn- next-ts [db]
  (let [res (inc (:curr-time db))]
    (println "next-ts " res)
    res))

(defn- next-id
  "returns a pair composed of the id to use for the given entity and the next free running id in the database

  ent - 주어진 entity에 새로운 id를 부여하기 위함. 
  (:id ent)가 :db/no-id-yet 이면 새로운 아이디를 바로 넣어준다. 
  :db/no-id-yet 이 아니면 기존 ent-id를 리턴한다. 
  
  top-id, increased-id는 기존 db의 가장 최근 ID"
  [db ent]
  (let [top-id (:top-id db)
        ent-id (:id ent)
        increased-id (inc top-id)]
    (if (= ent-id :db/no-id-yet)
      [(keyword (str increased-id)) increased-id]
      [ent-id top-id])))

(defn- update-attr-value
  "updating the attribute value based on the kind of the operation, the cardinality defined for this attribute and the given value

  attribute 값만 변경(시간은 다른 곳에서 함)
  "
  [attr value operation]
  (cond
    ;; attr의 메타의 cardinarity가 :db/single 이면 값 하나만을 저장하는 것.
    (single? attr)    (assoc attr :value #{value})
    ;; now we're talking about an attribute of multiple values
    ;; 각 operation에 따라서 덮어쓰기도, 더하기도, 없애기도 한다. 
    (= :db/reset-to operation)  (assoc attr :value value)
    (= :db/add operation)        (assoc attr :value (CS/union (:value attr)  value))
    (= :db/remove operation)  (assoc attr :value (CS/difference (:value attr) value))))

(defn- update-creation-ts
  "updates the timestamp value of all the attributes of an entity to the given timestamp

  entity의 모든 attribute의 :ts(timestamp)를 모두 ts-val로 덮어버린다."
  [ent ts-val]
  (reduce #(assoc-in %1 [:attrs %2 :ts] ts-val)
          ent
          (keys (:attrs ent))))

(defn- update-entry-in-index
  [index path _]  ;; _는 operation이지만 현재 사용X
  (let [update-path (butlast path)  ;; 마지막만 뺌(eav에서는 v를 뺌)
        update-value (last path)    ;; eav의 마지막 값은 value
        to-be-updated-set (get-in index update-path #{})]  ;; update할 곳의 기존 데이터를 가져온다.
    ;; 미리 가져온 데이터에 새로운 value를 conj 한다. (없으면 새로 만들어짐)
    (assoc-in index update-path (conj to-be-updated-set update-value))))

(defn update-attr-in-index
  "엔티티의 ID에서 인덱스 경로를 구축한다. "
  [index ent-id attr-name target-val operation]
  (let [colled-target-val (collify target-val) ;; collection이 아니면 감싸줌. reduce
        ;; index를 변경하는 함수.
        update-entry-fn (fn [indx vl]
                          (update-entry-in-index
                           indx
                           ;; eav는 다음처럼 만들어짐 (make-index #(vector %1 %2 %3) #(vector %1 %2 %3) always)
                           ;; %1=entity-id, %2=attr-name, %3=value 로 벡터를 만든다.
                           ;; 즉, [entity-id attr-name value] 형태의 벡터로만 만듬 (즉, path가 되는 것)
                           ((from-eav index) ent-id attr-name vl)
                           operation))]
    ;; target-val 모두 index를 변경한다.
    (reduce update-entry-fn index colled-target-val)))

(defn- add-entity-to-index [ent layer ind-name] 
  (let [ent-id (:id ent)              ;; entity-id 추출
        index (ind-name layer)        ;; :VAET 와 같은 것을 말함. 
        all-attrs (vals (:attrs ent)) ;; entity의 모든 attribute
        relevant-attrs (filter #((usage-pred index) %) all-attrs) ;; 인덱스로 사용중인 attributes
        ;; eav 인덱스를 반영한다. 
        add-in-index-fn (fn [ind attr] (update-attr-in-index ind
                                                             ent-id
                                                             (:name attr)
                                                             (:value attr)
                                                             :db/add))
        ;; 모든 attribute를 인덱스에 반영한다.
        ;; layer에 새로운 인덱스를 추가한다.
        new-index (reduce add-in-index-fn index relevant-attrs)]
    (assoc layer ind-name new-index)))

(defn- fix-new-entity [db ent]
  (let [[ent-id next-top-id] (next-id db ent)
        new-ts (next-ts db)]
    [(update-creation-ts (assoc ent :id ent-id) new-ts) next-top-id]))

;; - entity에 ID, timestamp 추가
;; - storage에 배치
;; - 필요에 따라 인덱스 업데이트
(defn add-entity [db ent]
  (let [[fixed-ent next-top-id] (fix-new-entity db ent)
        ;; layer의 마지막 db의 storage만 추가한 상태. 기존 storage에 덮어버리면 문제가 생기지 않나?
        ;; 싶지만 기존 layer는 불변이다 그곳에다가 새로운 entity를 더한 것이고, 이것을 이제 마지막 코드에서
        ;; 기존 layer에 더하는 행위를 할 것이다. 
        layer-with-updated-storage (update-in (last (:layers db)) [:storage] ;; db의 마지막 layer의 :starage에
                                              ;; (write-entity [storage entity] (assoc storage (:id entity) entity))
                                              write-entity 
                                              fixed-ent)  ;; 새로운 entity를 추가한다.
        add-entity-to-index-fn (partial add-entity-to-index fixed-ent)
        ;; (indices) - [:VAET :AVET :VEAT :EAVT]
        ;; 모든 인덱스를 돌면서 entity를 추가함.
        new-layer (reduce add-entity-to-index-fn layer-with-updated-storage (indices))]
    ;; layers에 새로운 layer를 추가한다. (conj (:layers db) new-layer)
    (assoc db :layers (conj (:layers db) new-layer) :top-id next-top-id)))

(defn add-entities [db ents-seq] (reduce add-entity db ents-seq))

;; update
(defn- update-attr-modification-time
  "
  시간 업데이트
  기존 attr의 :ts를 prev-ts로 설정하고 
  새로운 ts로 덮어버린다."
  [attr new-ts]
  (assoc attr :ts new-ts :prev-ts (:ts attr)))

(defn- update-attr [attr new-val new-ts operation]
  {:pre [(if (single? attr)
           (contains? #{:db/reset-to :db/remove} operation)
           (contains? #{:db/reset-to :db/add :db/remove} operation))]}
  (-> attr
      (update-attr-modification-time new-ts)
      (update-attr-value new-val operation)))

;; remove
(defn- remove-entry-from-index [index path]
  (let [path-head (first path)
        path-to-items (butlast path)
        val-to-remove (last path)
        old-entries-set (get-in index path-to-items)]
    (cond
      (not (contains? old-entries-set val-to-remove)) index
      (= 1 (count old-entries-set)) (update-in index [path-head] dissoc (second path))
      :else (update-in index path-to-items disj val-to-remove))))

(defn- remove-entries-from-index [ent-id operation index attr]
  (if (= operation :db/add)
    index
    (let [attr-name (:name attr)
          datom-vals (collify (:value attr))
          paths (map #((from-eav index) ent-id attr-name %) datom-vals)]
      (reduce remove-entry-from-index index paths))))

(defn- update-index [ent-id old-attr target-val operation layer ind-name]
  (if-not ((usage-pred (get-in layer [ind-name])) old-attr)
    layer
    (let [index (ind-name layer)
          cleaned-index (remove-entries-from-index ent-id operation index old-attr)
          updated-index (if (= operation :db/remove)
                          cleaned-index
                          (update-attr-in-index cleaned-index
                                                ent-id
                                                (:name old-attr)
                                                target-val
                                                operation))]
      (assoc layer ind-name updated-index))))

(defn- put-entity [storage e-id new-attr]
  (assoc-in (get-entity storage e-id) [:attrs (:name new-attr)] new-attr))

(defn- update-layer
  [layer ent-id old-attr updated-attr new-val operation]
  (let [storage (:storage layer)
        ;; index종류별로 인덱스 갱신.
        new-layer (reduce (partial update-index ent-id old-attr new-val operation)
                          layer
                          (indices))
        
        new-entity (put-entity storage ent-id updated-attr)
        storage+new-entity (write-entity storage new-entity)]

    (assoc new-layer :storage storage+new-entity)))

(defn update-entity
  ([db ent-id attr-name new-val]
   (update-entity db ent-id attr-name new-val :db/reset-to))
  ([db ent-id attr-name new-val operation]
   (let [update-ts (next-ts db) ;; 최근 timestamp 가져오기
         layer (last (:layers db)) ;; add-entity처럼 마지막 layer가져오기 
         attr (attr-at db ent-id attr-name) ;;  :curr-time의 entity/attr-name에 있는 값가져오기.
         
         updated-attr (update-attr attr new-val update-ts operation) ;; 다음 ts에 새로운 attr 값을 추가한다.
         fully-updated-layer (update-layer layer ent-id attr updated-attr new-val operation)]
     (update-in db [:layers] conj fully-updated-layer))))

(defn remove-entity-from-index [ent layer ind-name]
  (let [ent-id (:id ent)
        index (ind-name layer)
        all-attrs (vals (:attrs ent))
        relevant-attrs (filter #((usage-pred index) %) all-attrs)
        ;; 관련 attribute 모두 제거하기.
        remove-from-index-fn (partial remove-entries-from-index ent-id :db/remove)
        new-index (reduce remove-from-index-fn index relevant-attrs)]
    (assoc layer ind-name new-index)))

(defn reffing-to
  "특정 엔티티를 참조하는 다른 엔티티들을 찾는 함수이다.
  entity id와 layer를 인자로 받아서, VAET 인덱스에서 해당 엔티티의 ID를 키로 가지는 모든 값을 반환한다.
  VAET는 엔티티의 속성과 값을 키로 하여, 해당 속성과 값을 가지는 엔티티들의 ID를 값으로 하는 맵이다.
  reffing-to 함수는 VAET 인덱스에서 찾은 값들을 [entity-id, attr-name] 형태로 바꾸어, 
  제거된 엔티티를 참조하는 엔티티들의 정보를 담은 시퀀스를 리턴한다.

  예를들어, 다음과 같은 VAET 인덱스가 있다고 가정해보자.
  {:VAET 
    {'ALICE {:name #{:1}}
     'Bob {:name #{:2}}
     20   {:age #{:1 :2}}
     :1    {:friend #{:2}}
     :2    {:friend #{:1}}}}

  여기서 엔티티 1을 제거하려고 하면, reffing-to 함수는 다음과 같은 결과를 리턴할 것이다.
  ([2 :friend])

  이것은 엔티티 2가 friend라는 속성으로 엔티티 1을 참조하고 있다는 것을 보여줌
  이것을 구하기는 참 쉬운데 그냥 VAET에서 첫번째 인덱스로 땡겨오기만 하면 됨.
  e-id 는 키워드로 만들었기 때문에 (혹은 아니어도 함수처럼 쓸 수만 있으면됨) 그대로 vaet에서 e-id로 가져오면.
  곧바로 attr와 entity-id set이 나옴.

  이것으로 `update-entity` 로 각 삼중항(EAV)에 제거된 엔티티를 참조하는 속성을 업데이트한다. 
  
  "
  [e-id layer]
  (let [vaet (:VAET layer)]
    (for [[attr-name reffing-set] (e-id vaet)
          reffing reffing-set]
      [reffing attr-name])))

;; `remove-back-refs` 의 마지막 단계는 참조 정보를 저장하는 유일한 인덱스이므로, index 특히 vaet 인덱스에서
;; 참조 자체를 지우는 것이다.
(defn- remove-back-refs [db e-id layer]
  (let [reffing-datoms (reffing-to e-id layer)  ;; e-id를 바라보는 ([entity-id attr-name] ,,, )
        remove-fn (fn [d [e a]] (update-entity d e a e-id :db/remove))  ;; db에서 전부 제거하기
        clean-db (reduce remove-fn db reffing-datoms)]
    ;; 마지막에 더해진 layer를 리턴.
    (last (:layers clean-db))))

(defn remove-entity
  [db ent-id]
  (let [ent (entity-at db ent-id)
        ;; ent-id를 참조하는 다른 entity가 제거된 레이어 리턴.
        layer (remove-back-refs db ent-id (last (:layers db)))
        ;; :VAET 에 이제 ent-id를 가지는 인덱스 자체를 제거.
        retimed-layer (update-in layer [:VAET] dissoc ent-id)
        ;; storage에서 entity제거.
        no-ent-layer (assoc retimed-layer :storage (drop-entity (:storage retimed-layer) ent))
        ;; 이제 모든 인덱스에서 entity 제거
        new-layer (reduce (partial remove-entity-from-index ent) no-ent-layer (indices))]
    ;; 생성된 레이어 추가
    (assoc db :layers (conj (:layers db) new-layer))))

;; transaction
;; 로우레벨 API의 각 작업은 단일 엔티티에 대해 작동한다.
;; 그러나 거의 모든 데이터베이스는 여러 작업을 단일 트랜잭션으로 수행할 수 있는 방법이 있다.
;; 1. 배치작업은 단일 원자 작업으로 간주하여 함께 성공하거나 실패한다.
;; 2. 데이터베이스는 트랜잭션 전후에 유효한 상태이다.
;; 3. 일괄 업데이트는 격리된 것처럼 보이며, 다른 쿼리는 일부 작업만 적용된 데이터베이스 상태가 표시되지 않아야 한다.

;; 우리가 원하는 작업은 하나의 단일 레이어에 하나의 작업이 추가되는 것이지만, 현재 로우 API는 하나씩만 가능하다.
;; 그렇다면 우리가 원하는 것은 N개의 작업이 일괄 처리되면 N개의 새 레이어가 추가되는 것을 볼 수 있는데
;; 우리는 최상위 레이어만을 원한다.
;; 핵심은 최상위 레이어만 가져와서 초기 데이터베이스에 넣는다.  그리고 모든 작업이 완료된 이후에 timestamp를 업데이트한다.

;; All this is done is the `transact-on-db` function, which receives the initial value of the database and the
;; batch of operations to perform, and returns its updated value:
;; 갱신된 DB값을 리턴한다.
;; 첨언 : transact-on-db는 ops를 loop돌면서 하나씩 수행하고, layer를 생성한다.
;;        그리고 initial-layer에 마지막 layer를 더하면서 끝낸다.
(defn transact-on-db [initial-db ops]
  (loop [[op & rst-ops] ops
         transacted initial-db]
    (if op
      (recur rst-ops (apply (first op) transacted (rest op)))
      (let [initial-layer  (:layers initial-db)
            new-layer (last (:layers transacted))]
        (assoc initial-db
               :layers (conj initial-layer new-layer)
               :curr-time (next-ts initial-db)
               :top-id (:top-id transacted))))))

;; 여기서 갱신된 DB `값` 이라고 말한 것은 오로지 호출자만 변경된 DB를 바라볼 수 있기 때문이다.
;; 다른 사용자는 접근할 수 없다. 이때 Atom을 사용해서 간접적으로 참조해야 한다.
;; 가장 간단하고 명확한 API를 제공하기위해 사용자가 ATOM과 list of operations를 제공하면, 이 입력을
;; 적절한 트랜잭션으로 변환하도록 하고 싶다. 그 트랜잭션은 다음 트랜잭션 호출 체인안에서 발생한다.
;; `transact -> _transact -> swap! -> transact-on-db`

;; `_transact` 는 `swap!` 을 호출할 준비를 한다. `swap!` 로 시작하는 list를 생성한다.
;; 그 뒤에 Atom, transact-on-db, batch of operations 가 이어진다.
(defmacro _transact [db op & txs]
  (when txs
    (loop [[frst-tx# & rst-tx#] txs
           res# [op db `transact-on-db]
           accum-txs# []]
      (if frst-tx#
        (recur rst-tx# res# (conj accum-txs# (vec frst-tx#)))
        (list* (conj res# accum-txs#))))))

;; 유저들은 Atom(i.e., the connection) 과 operations to perform 를 인자로 `transact` 를 호출한다.
;; 이 트랜잭션은 입력을 `_transact` 로 전달하며, 여기에 Atom을 업데이트하는 함수의 이름(swap!)을 추가한다.
(defmacro transact [db-conn & txs] `(_transact ~db-conn swap! ~@txs))
;; transact 는 아래 comment에 예시를 보면 이해가 쉬움.
;; transact은 _transact 에게 txs와 함께 swap! 함수를 함께 보냄.
;; _transact은 그것을 op로 받아서 맨처음에 [op db `transaction-on-db`] 의 초기 벡터를 만들고
;; 그 위에다가 txs를 하나씩 나열함. 그러면 나중에 `list*` 를 이용해서 (op db transaction-on-db tx1 tx2) 가 되며
;; op는 swap! 이므로, (swap! db transaction-on-db tx1 tx2) 가 된다. 

;; 혹시 수정은 하지않고 어떻게 변하는지만 알고 싶은 what if(만약) 이라는 질의문도 만들 수 있지 않을까?
;; 이것또한 비슷한 콜 체인을 가진다. what-if -> _transact -> _what-if -> transact-on-db

;; what-if는 swap! 대신 _what-if를 이용.
;; _what-if 는 단순히 함수를 수행하고 끝이다.
(defn- _what-if
  "Operates on the db with the given transactions, but without eventually updating it."
  [db f ops] (f db ops))

(defmacro what-if [db & ops] `(_transact ~db _what-if ~@ops))

(defn evolution-of
  "The sequence of the values of an entity's attributes, as changed through time"
  [db ent-id attr-name]
  (loop [res [] ts (:curr-time db)]
    (if (= -1 ts)
      (reverse res)
      (let [attr (attr-at db ent-id attr-name ts)]
        (recur (conj res {(:ts attr) (:value attr)})
               (:prev-ts attr))))))



(comment
  (reffing-to :1 {:VAET {20 {:age #{1 2}}
                         "green" {:hair-color #{1}}
                         "blue"  {:hair-color #{2}}
                         "black" {:hair-color #{3}}
                         :1 {:boss #{2}
                             :friend #{3}}}})
  ;; => ([2 boss] [3 friend])

  (def index (with-meta {:a {:b #{"c"}
                             :e #{"d" "e"}}} {:from-eav #(vector %1 %2 %3)}))
  (update-entry-in-index index [:a :c "C"] :add)
  (update-attr-in-index index :a :b "D" :update)
  (remove-entry-from-index index [:a :e "d"])



  (let [db (fdb.constructs/make-db)
        entity (-> (fdb.constructs/make-entity)
                   (fdb.constructs/add-attr (fdb.constructs/make-attr :age 20 :int)))]
    (fix-new-entity @db entity))
  ;; => [{:id :1, :attrs {:age {:name :age, :value 20, :ts 1, :prev-ts -1}}} 1]

  (let [db (fdb.constructs/make-db)
        entity (-> (fdb.constructs/make-entity)
                   (fdb.constructs/add-attr (fdb.constructs/make-attr :age 20 :int))
                   (fdb.constructs/add-attr (fdb.constructs/make-attr :name "Mike" :string)))
        new-db (add-entity @db entity)
        layer (-> new-db :layers last)

        entity2 (-> (fdb.constructs/make-entity)
                    (fdb.constructs/add-attr (fdb.constructs/make-attr :age 40 :int))
                    (fdb.constructs/add-attr (fdb.constructs/make-attr :name "Paul" :string)))
        new-db2 (add-entity new-db entity2)]
    #_(put-entity (:storage layer) :1 (fdb.constructs/make-attr :age 40 :int))
    #_(update-entity new-db :1 :age 48)
    #_(->> (:layers new-db2)
           (map :storage)
           #_(map :EAVT)
           #_(clojure.pprint/pprint))
    (->> (:layers new-db2)
         (map :EAVT))
    ;; layers 변화 추이.(EAVT 인덱스로 보는.)
    ;; ({}
    ;;  {:1 {:age #{20}, :name #{"Mike"}}}
    ;;  {:1 {:age #{20}, :name #{"Mike"}}, :2 {:age #{40}, :name #{"Paul"}}})
    )

  ;; db 일반적인 형태.
  ;; => {:layers
  ;;     [{:storage {}, :VAET {}, :AVET {}, :VEAT {}, :EAVT {}}
  ;;      {:storage
  ;;       {:1 {:id :1, :attrs {:age {:name :age, :value 20, :ts 1, :prev-ts -1}}}},
  ;;       :VAET {},
  ;;       :AVET {:age {20 #{:1}}},
  ;;       :VEAT {20 {:1 #{:age}}},
  ;;       :EAVT {:1 {:age #{20}}}}],
  ;;     :top-id 1,
  ;;     :curr-time 0}
  (meta (fdb.constructs/make-attr :age 30 :int))
  (update-attr (fdb.constructs/make-attr :age 30 :int) 31 2 :db/reset-to)



  (let [db (fdb.constructs/make-db)
        entity1 (-> (fdb.constructs/make-entity)
                    (fdb.constructs/add-attr (fdb.constructs/make-attr :name "Mike" :string)))
        entity2 (-> (fdb.constructs/make-entity)
                    (fdb.constructs/add-attr (fdb.constructs/make-attr :name "Tom" :string)))
        entity3 (-> (fdb.constructs/make-entity)
                    (fdb.constructs/add-attr (fdb.constructs/make-attr :name "Kim" :string)))]
    (transact db
              (add-entity entity1)
              (add-entity entity2)
              (add-entity entity3)))
  ;; => {:layers
  ;;     [{:storage {}, :VAET {}, :AVET {}, :VEAT {}, :EAVT {}}
  ;;      {:storage
  ;;       {:1
  ;;        {:id :1,
  ;;         :attrs
  ;;         {:name {:name :name, :value "Mike", :ts 1, :prev-ts -1}}},
  ;;        :2
  ;;        {:id :2,
  ;;         :attrs {:name {:name :name, :value "Tom", :ts 1, :prev-ts -1}}},
  ;;        :3
  ;;        {:id :3,
  ;;         :attrs
  ;;         {:name {:name :name, :value "Kim", :ts 1, :prev-ts -1}}}},
  ;;       :VAET {},
  ;;       :AVET {:name {"Mike" #{:1}, "Tom" #{:2}, "Kim" #{:3}}},
  ;;       :VEAT
  ;;       {"Mike" {:1 #{:name}}, "Tom" {:2 #{:name}}, "Kim" {:3 #{:name}}},
  ;;       :EAVT
  ;;       {:1 {:name #{"Mike"}},
  ;;        :2 {:name #{"Tom"}},
  ;;        :3 {:name #{"Kim"}}}}],
  ;;     :top-id 3,
  ;;     :curr-time 1}
  ;;

  (macroexpand  '(transact db
                           (add-entity entity1)
                           (add-entity entity2)
                           (add-entity entity3)))
  ;; => (clojure.core/swap!
  ;;     db
  ;;     fdb.core/transact-on-db
  ;;     [[add-entity entity1] [add-entity entity2] [add-entity entity3]])

  (let [db (fdb.constructs/make-db)
        entity1 (-> (fdb.constructs/make-entity)
                    (fdb.constructs/add-attr (fdb.constructs/make-attr :name "Mike" :string)))
        entity2 (-> (fdb.constructs/make-entity)
                    (fdb.constructs/add-attr (fdb.constructs/make-attr :name "Tom" :string)))
        entity3 (-> (fdb.constructs/make-entity)
                    (fdb.constructs/add-attr (fdb.constructs/make-attr :name "Kim" :string)))]
    (macroexpand  '(what-if db
                            (add-entity entity1)
                            (add-entity entity2)
                            (add-entity entity3))))
  ;; => (fdb.core/_what-if
  ;;     db
  ;;     fdb.core/transact-on-db
  ;;     [[add-entity entity1] [add-entity entity2] [add-entity entity3]])


  ;; evolution-of  & transaction
  (let [db (fdb.constructs/make-db)
        entity1 (-> (fdb.constructs/make-entity)
                    (fdb.constructs/add-attr (fdb.constructs/make-attr :name "Mike" :string)))
        _ (transact db (add-entity  entity1))
        _ (transact db (update-entity :1 :name "Name"))
        _ (transact db (update-entity :1 :name "Kim"))]
    (evolution-of @db :1 :name))
  )




