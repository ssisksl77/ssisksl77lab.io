(ns fdb.query
  (:require [fdb.constructs :refer [from-eav indx-at]]))

'{:find [?nm ?bd ]
  :where [
          [?e  :likes "pizza"]
          [?e  :name  ?nm]
          [?e  :speak "English"]
          [?e  :bday (bday-mo? ?bd)]]}


(defn variable?
  "A predicate that accepts a string and checks whether it describes a datalog variable (either starts with ? or it is _)

  ?, _ 인지 알려줌."
  ([x] (variable? x true))
  ([x accept_?] ; intentionally accepts a string and implemented as function and not a macro so we'd be able to use it as a HOF
   (or (and accept_? (= x "_")) (= (first x) \?))))

(comment
  (variable? "_")
  (variable? "?a")
  
  (variable? "1")  ;; t
  ;; aceept가 false이면 _ 는 검사안함.
  
  (variable? "1" false) ;; f
  
  ;;
  )
(defmacro clause-term-meta
  "Finds the name of the variable at an item of a datalog clause element. If no variable, returning nil

  변수명을 찾아러 리턴한다."
  [clause-term]
  (cond
    ;; the item is an s-expression, need to treat it as a coll, by going over it and returning the name of the variable.
    (coll? clause-term) (first (filter #(variable? % false) (map str clause-term)))
    ;; the item is a simple variable
    (variable? (str clause-term) false) (str clause-term)
    ;; the item is a value and not a variable
    :no-variable-in-clause nil))

(comment
  (clause-term-meta [?nm ?bd])
  (clause-term-meta [?e :name "pizza"])
  (clause-term-meta [a b])
  (clause-term-meta ["A"])
  ;;
  )

(defmacro clause-term-expr
  "Create a predicate for each element in the datalog clause

  각 절을 받아서 predicate 함수를 만듬."
  [clause-term]
  (cond
    ;; simple one, e.g., ?a
    ;; 1 ?a 만 있으면 그냥 통과.
    (variable? (str clause-term)) (do (println "1 variable? ")
                                      #(= % %))
    ;; simple value given, e.g., :likes
    ;; 2 인자 값과 일치하는지 검사하는 함수 리턴
    (not (coll? clause-term)) (do (println "2 (not coll?) ")
                                  `#(= % ~clause-term))
    ;; an unary predicate, e.g., (pos? ?a)
    ;; 3 단항연산자는 해당 연산자를 적용하는 함수 리턴.
    (= 2 (count clause-term)) (do (println "3 단항연산자")
                                  `#(~(first clause-term) %))
    ;; a binary predicate, the variable is the first argument, e.g., (> ?a 42)
    ;; 4 이항 연산자 변수(?a)가 앞에 있는 경우 해당 변수를 받는 함수를 리턴한다. 
    (variable? (str (second clause-term)))
    (do (println "이항연산자가 앞에 있는 경우...")
        `#(~(first clause-term) % ~(last clause-term)))
    ;; a binary perdicate, the variable is the second argument, e.g., (> 42 ?a)
    ;; 5 마찬가지로 이항 연산자 변수가 뒤에 있는 경우 해당 변수를 받는 함수를 리턴한다.
    (variable? (str (last clause-term)))
    (do (println "이항연산자가 뒤에 있는 경우...")
        `#(~(first clause-term) ~(second clause-term) %))))

(comment
  ;; 1
  ((clause-term-expr ?e) 1)

  ;; 2
  ((clause-term-expr :likes) :likes)
  ((clause-term-expr >) >)
  ((clause-term-expr 42) 42)
  ;; 3
  ((clause-term-expr [pos? :e]) -1)
  ((clause-term-expr [pos? :e]) 1)

  ;; 4
  ((clause-term-expr [> ?a 42]) 2)
  ((clause-term-expr [> ?a 42]) 43)

  ;; 5
  ((clause-term-expr [> 42 ?a]) 2)
  ((clause-term-expr [> 42 ?a]) 43)

  ;;
  )


(defmacro pred-clause
  "Builds a predicate-clause from a query clause (a vector with three elements describing EAV).
  A predicate clause is a vector of predicates that would operate on an index, 
  and set for that vector's metadata to be the names of the variables that user assigned for each item in the clause

  predicate-clause(EAV를 설명하는 세 가지 요소로 구성된 vector) 를 생성한다. 
  predicate-clause 는 index에서 작동하는 vector이며, 해당 vector의 메타데이터는 사용자가 clause의 각 항목에 할당하는 변수의 이름이 되도록 설정.
  "
  [clause]
  (loop [[trm# & rst-trm#] clause
         exprs# []
         metas# []]
    (if trm#
      (recur rst-trm#
             (conj exprs# `(clause-term-expr ~trm#))  ;; 조건절을 함수 predicate로 변환은 clause-term-expr 로 이관
             (conj metas# `(clause-term-meta ~trm#)))  ;; 조건절에 있는 변수들을 meta에 추가?
      (with-meta exprs# {:db/variable metas#}))))

(comment
  (pred-clause [> ?a 42]) ;; [(clause-term-expr >) (clause-term-expr ?a) (clause-term-expr 42)]
  (let [[p1 p2 p3] (pred-clause [> ?a 42])]
    (println (p1 >))
    (println (p2 "무조건 true"))
    (println (p3 42)))
  
  (println (meta (pred-clause [> ?a 42])))  ;; #:db{:variable [nil ?a nil]}
  
  ;;
  )

(defmacro q-clauses-to-pred-clauses
  "create a vector of predicate clauses to operate on indices, based on the given vector of clauses
  
  주어진 조건절로 인덱스에 적용하기 위한 predicate 절 vector를 만든다. "
  [clauses]
  (loop [[frst# & rst#] clauses
         preds-vecs# []]
    (println frst#)
    (if-not frst#
      preds-vecs#
      (recur rst# `(conj ~preds-vecs# (pred-clause ~frst#))))))
(comment
  
  (pred-clause [> ?a 42])
  (println (q-clauses-to-pred-clauses [[> ?a 42]
                                       [pos? ?b]]))
  ;; => [[#function[fdb.query/eval10675/fn--10676] #function[fdb.query/clause-term-expr/fn--10364] #function[fdb.query/eval10675/fn--10678]]
  ;;     [#function[fdb.query/eval10675/fn--10680] #function[fdb.query/clause-term-expr/fn--10364]]]
  
  (println (map meta (q-clauses-to-pred-clauses [[> ?a 42]
                                                 [pos? ?b]]))) ;; (#:db{:variable [nil ?a nil]} #:db{:variable [nil ?b]})
  
  ;;
  )


(defn filter-index
  "Function that accepts an index and a path-predicate (which is a tripet of predicates to apply on paths in an index).
  For each path predicates it creates a result path (a triplet representing one path in the index)
  and returns a seq of result paths.

  인덱스와 path-predicate에 적용할 함수이다.
  각 경로 술어에 대해 결과 경로를 생성하고 결과 경로의 시퀀스를 반환."
  [index predicate-clauses]
  (for [pred-clause predicate-clauses
        ;; predicates for the first and second level of the index, also keeping the path to later use its meta
        :let [[lvl1-prd lvl2-prd lvl3-prd] (apply (from-eav index) pred-clause)]
        ;; keys and values of the first level
        [k1 l2map] index
        ;; filtering to keep only the keys and the vals of the keys that passed the first level predicate
        :when (try (lvl1-prd k1) (catch Exception e false))
        ;; keys and values of the second level
        [k2 l3-set] l2map
        ;; feep from the set at the third level only the items that passed the predicate on them
        :when (try (lvl2-prd k2) (catch Exception e false))
        ;; keep from the set at the third level only the items that passed the predicate on them
        :let [res (set (filter lvl3-prd l3-set))]]
    ;; constructed result clauses, while keeping the meta of the query to use it later when extracting variables
    ;; YNAM : 여기서 에러 나는 중.
    (with-meta [k1 k2 res] (meta pred-clause))))

(defn items-that-answer-all-conditions
  [items-seq num-of-conditions]
  (->> items-seq
       (map vec)
       (reduce into [])
       (frequencies)
       (filter #(<= num-of-conditions (last %)))
       (map first)
       (set)))



(defn index-of-joining-variable
  "A joining variable is the variable that is found on all of the query clauses
  joining variable은 모든 쿼리 절에서 발견되는 변수를 말함."
  [query-clauses]
  ;; :db/variale 은 위에서 계산하듯. meta 데이터 안의 where 절 안에서 사용하는 변수들을 갖고 있는 곳임.
  ;; all the metas (which are vectors) for the query
  (let [metas-seq (map #(:db/variable (meta %)) query-clauses)
        ;; going over the vectors, collapsing each onto another, term by term, keeping a term only if the two terms are equal
        collapsing-fn (fn [accV v] (map #(when (= %1 %2) %1) accV v))
        ;; using the above fn on the metas, eventually get a seq with one item who is not null, this is the joining variable
        collaped (reduce collapsing-fn metas-seq)]
    ;; returning the index of the first element that is a variable (there's only one)
    (println "index-of-joining-variable metas-seq" metas-seq)
    (println "index-of-joining-variable collaped" collaped)
    (first (keep-indexed #(when (variable? %2 false) %1) collaped))))

(defn query-index
  "A query plan that is based on querying a single index"
  [index pred-clauses]
  
  (println "query-index index=" index ", pred-clauses" pred-clauses)
  (let [result-clauses (filter-index index pred-clauses)]
    (println "query-index/result-clauses " result-clauses)))


(defn single-index-query-plan
  "A query plan that is based on quering a single index"
  [query indx db]
  ;; indx-at은 시점에 따른 index를 가져옴, 시점이 없으면 :curr-time 값 사용.
  (let [q-res (query-index (indx-at db indx) query)]
    #_(bind-variables-to-query q-res (indx-at db indx))))

(defn build-query-plan
  "Upon receiving a database and query clauses, this function responsible to deduce on
  which index in the db it is best to perform the query clauses, and then return a query plan,
  which is a function that accepts a database executes the plan on it."
  [query]
  ;; index-of-joing-variable 이 리턴하는 벡터를 보니, 각 벡터의 순서에 따라 인덱스를 리턴하도록 만듬.
  ;; 첫번째(0)가 Entity를 찾는 거 같음. 그래서 AVET를 리턴함.
  (let [term-ind (index-of-joining-variable query) ;; 여기서 리턴하는 값을 기반으로 인덱스를 선택.
        ind-to-use (case term-ind
                     0 :AVET
                     1 :VEAT
                     2 :EAVT)]
    (println "index-of-joining-variable query " term-ind)
    (println "ind-to-use " ind-to-use)
    (partial single-index-query-plan query ind-to-use)))

(defmacro q
  "querying the database using datalog queries built in a map structure
  ({:find [variables*] :where [ [e a v]* ]}). (after the where there are clauses)
  At the moment support only filtering queries, no joins is also assumed."
  [db query]
  `(let [ ;; transforming the clauses of the query to an internal representation structure called query-clauses
         pred-clauses# (q-clauses-to-pred-clauses ~(:where query)) ;; returns vector of functions
         ;; extracting from the query the variables that needs to be reported out as a set
         ;; find로 조회할 값을 set으로 변환함.
         needed-vars# (symbol-col-to-set ~(:find query))
         ;; extracting a query plan based on the query-clauses
         query-plan#  (build-query-plan pred-clauses#)
         ;; executing the plan on the database
         query-internal-res# (query-plan# ~db)
         ]
     #_(println pred-clauses#)
     (println needed-vars#)
     (quote pred-clauses#)))

(q {:layers [{:AVET {:likes {"pizza" 1}}} {:AVET {:likes {"pizza" #{1}}
                                                  :speak {"English" #{1}}}}]
    :curr-time 1}
   {:find [?nm ?bd ]
    :where [
            [?e  :likes "pizza"]
            [?e  :name  ?nm]
            [?e  :speak "English"]]})



(comment
  ;;
  )

;; where 는 중첩된 vector 구조를 가짐.
#_(defmacro clause-term-expr [clause-term]
    (cond
      (variable? (str clause-term))     ;variable
      #(= % %) 
      (not (coll? clause-term))         ;constant 
      `#(= % ~clause-term) 
      (= 2 (count clause-term))         ;unary operator
      `#(~(first clause-term) %) 
      (variable? (str (second clause-term))) ;binary operator, 1st operand is variable
      `#(~(first clause-term) % ~(last clause-term))
      (variable? (str (last clause-term))) ;binary operator, 2nd operand is variable
      `#(~(first clause-term) ~(second clause-term) %)))



;; 1. Transformation
;;   The `:find` part of the query is transformed into a set of the given variable names:
;;   The `:where` part of the query retains its nested vector structure.
;;    However, each of the terms in each of the clauses is replaced with a predicate according to Table 10.2
;;    example:
;;    Query Clause               |      Predicate Clause                      |  Meta Cluase
;;    [?e  :likes "pizza"]       |  [#(= % %) #(= % :likes) #(= % "pizza")]   | ["?e" nil nil]
;;    [?e  :name  ?nm]           |  [#(= % %) #(= % :name) #(= % %)]	      | ["?e" nil "?nm"]
;;    [?e  :speak "English"]     |  [#(= % %) #(= % :speak) #(= % "English")] | ["?e" nil nil]
;;    [?e  :bday (bday-mo? ?bd)] |  [#(= % %) #(= % :bday) #(bday-mo? %)]     | ["?e" nil "?bd"]
;;
;; 2. Making a Plan
;; 일반적으로 적절한 index(Table 10.4)를 선택하고, 함수 형태로 plan을 구성한다.
;; 여기서는 single joining variable을 기반으로 index를 선택한다.
;; Joining variable operates on (조인할 변수) | Index to use
;;     Entity IDs                           |  AVET
;;     Attribute names                      |  VEAT
;;     Attribute values                     |  EAVT
;; <Table 10.4 - Index Selection>
;; 여기서 핵심은 조인 변수가 작동하는 요소를 포함하는 인덱스를 선택한다는 것이다.
;; (조인하고자 하는 변수가 가장 마지막에 있는 index를 사용한다. 그래야 그 값을 볼 수 있으니까)
;; 조인 변수의 인덱스를 찾는 것은 index-of-joining-variable 로 수행함.
;;
;; 쿼리에서 각 절의 메타데이터를 추출하는 것으로 시작한다.
;; 추출된 메타데이터를 3-element vector로 구성되며, 각 요소는 변수 이름 또는 nil이다.(위에서 테스트해봄)
;; (해당 벡터에는 변수 이름이 하나 이상 있을 수 없습니다.) 벡터가 추출되면 변수 이름 또는 nil 중 하나의 값을 생성함.
;;  변수 이름이 생성되면 동일한 인덱스의 모든 메타데이터 벡터에 나타난다. (이것이 조인변수)
;;  따라서 위에서 설명한 매핑을 기반으로 조인 변수와 관련된 인덱스를 사용하도록 선택할 수 있다.
;;  인덱스가 일단 선택되면, plan을 생성한다, plan은 함수이며 query와 index이름을 갖고 필요한 작업을 실행하는 함수를 리턴한다.
;; `build-query-plan` 이 그 일을 하는 듯.
;; 이 예제에서 선택한 인덱스는 조인변수가 entity id에 작용하므로 avet 인덱스임. 아래는 우리가 사용할 쿼리 예제.
'{:find [?nm ?bd ]
 :where [
         [?e  :likes "pizza"]
         [?e  :name  ?nm]
         [?e  :speak "English"]
         [?e  :bday (bday-mo? ?bd)]]}

;; Phase 3 : Execution of the Plan
;; 이전단계에서 `single-index-query-plan` 을 호출하면 쿼리 계획이 끝나는 것을 보았다. 이 함수는
;; 1. 인덱스에 각 술어 절을 적용한다(각 술어는 해당 인덱스 수준에서)
;; 2. 결과에 대해 AND 연산을 수행한다.
;; 3. 결과를 더 간단한 데이터 구조로 병합한다.
(comment
  (defn single-index-query-plan [query indx db]
    (let [q-res (query-index (indx-at db indx) query)]
      (bind-variables-to-query q-res (indx-at db indx)))))
;; 프로세스를 더 잘 설명하기 위해 아래처럼 인테테가 있다고 가정하자.

;; Entity ID	Attribute Name	Attribute Value
;; 1	         :name          USA
;;               :likes         Pizza
;;               :speak         English
;;               :bday          July 4, 1776
;; 2	         :name          France
;;               :likes         Red wine
;;               :speak         French
;;               :bday	        July 14, 1789
;; 3	         :name          Canada
;;               :likes         Snow
;;               :speak         English
;;               :bday	        July 1, 1867
;;  <Table 10.5 - Example entities>
;; 이제 `query-index` 함수를 할펴보자. 쿼리가 결과를 산출하기 시작하는 곳.
(comment
  (defn query-index [index pred-clauses]
    (let [result-clauses (filter-index index pred-clauses)
          relevant-items (items-that-answer-all-conditions (map last result-clauses)
                                                           (count pred-clauses))
          cleaned-result-clauses (map (partial mask-path-leaf-with-items
                                               relevant-items)
                                      result-clauses)]
      (filter #(not-empty (last %)) cleaned-result-clauses))))
;; 이 함수는 이전에 선택한 인덱스에 predicate clauses(술어절)을 적용하는 것으로 시작함.
;; 인덱스에 술어절을 적용하고 결과절을 각각 반환함.
;; 결과의 주요 특징은 다음과 같다.
;;  1. 인덱스는 각기 다른 레벨에 있는 세 개의 항목으로 구성되며, 각 항목은 각각의 술어를 통과한다.
;;  2. 항목의 순서는 인덱스의 수준 구조와 일치한다(술어 절은 항상 EAV순서)
;;     재정렬은 술어 절에 인덱스의 FROM-EAV를 적용할 때 수행된다.
;;  3. 술어절의 메타데이터가 첨부된다.
;;  이 모든 작업은 filter-index 에서 수행된다.
(comment
  (defn filter-index [index predicate-clauses]
    (for [pred-clause predicate-clauses
          :let [[lvl1-prd lvl2-prd lvl3-prd] (apply (from-eav index) pred-clause)] 
          [k1 l2map] index  ; keys and values of the first level
          :when (try (lvl1-prd k1) (catch Exception e false))

          [k2  l3-set] l2map  ; keys and values of the second level
          :when (try (lvl2-prd k2) (catch Exception e false))
          :let [res (set (filter lvl3-prd l3-set))] ]
      (with-meta [k1 k2 res] (meta pred-clause))))
  ;;
  )
;; 쿼리아 7월 4일에 실행되었다고 가정하고, 위의 데이터에 대해 쿼리를 실행한 결과를 표 10.6에 보여줌.
;; Entity ID	Attribute Name	Attribute Value
;; 1	         :name          USA
;;               :likes         Pizza
;;               :speak         English
;;               :bday          July 4, 1776
;; 2	         :name          France
;;               :likes         Red wine
;;               :speak         French
;;               :bday	        July 14, 1789
;; 3	         :name          Canada
;;               :likes         Snow
;;               :speak         English
;;               :bday	        July 1, 1867
;;  <Table 10.5 - Example entities>
'{:find [?nm ?bd ]
  :where [
          [?e  :likes "pizza"]
          [?e  :name  ?nm]
          [?e  :speak "English"]
          [?e  :bday (bday-mo? ?bd)]]}
;; Result Clause	         | Result Meta
;; [:likes Pizza #{1}]	         | ["?e" nil nil]
;; [:name USA #{1}]	         | ["?e" nil "?nm"]
;; [:speak "English" #{1, 3}]	 | ["?e" nil nil]
;; [:bday "July 4, 1776" #{1}]	 | ["?e" nil "?bd"]
;; [:name France #{2}]           | ["?e" nil "?nm"]
;; [:bday "July 14, 1789" #{2}]	 | ["?e" nil "?bd"]
;; [:name Canada #{3}]           | ["?e" nil "?nm"]
;; [:bday "July 1, 1867" {3}]    | ["?e" nil "?bd"]
;; <Table 10.6 - Query results>
;; 위 데이터에 따르면 1,2,3 은 엔티티 id를 말하는 듯.
;; 일단 모든 result clauses를 생성하면 결과 절 사이에 AND 연산을 수행해야 한다.
(comment
  (defn items-that-answer-all-conditions [items-seq num-of-conditions]
    (->> items-seq ; take the items-seq
         (map vec) ; make each collection (actually a set) into a vector
         (reduce into []) ;reduce all the vectors into one vector
         (frequencies) ;count for each item in how many collections (sets) it was in
         (filter #(<= num-of-conditions (last %))) ;items that answered all conditions
         (map first) ; take from the duos the items themselves
         (set))) ; return it as set
  )
;; 조건을 패스하지 않는 아이템은 패스한다.
(comment
  (defn mask-path-leaf-with-items [relevant-items path]
    (update-in path [2] CS/intersection relevant-items)))
;; 마지막으로, 비어있는(즉, 마지막 항목이 비어있는) 모든 결과 절을 제거한다.
;; Table 10.7 - Filtered query results
;; 결과를 보고할 준비가 되어있다. 결과 절 구조는 이러한 목적에 적합하지 않으므로
;; 이를 인덱스와 유사한 구조(map of maps) 로 변환할 것이다.
;; 
