(ns diy-database2)

(defrecord Database [layers top-id curr-time])
(defrecord Layer [storage VAET AVET VEAT EAVT])
(defrecord Entity [id attrs])
(defrecord Attr [name value ts prev-ts])
(defprotocol Storage
  (get-entity [storage e-id])
  (write-entity [storage entity])
  (drop-entity [storage entity]))


(defn make-entity
  ([] (make-entity :db/no-id-yet))
  ([id] (Entity. id {})))

(defn make-attr
  ([name value type ;; these ones are required
    & {:keys [cardinality] :or {cardinality :db/single}}] ;; default
   {:pre [(contains? #{:db/single :db/multiple} cardinality)]} ;; Dbc precondition
   (with-meta
     (Attr. name value -1 -1) ; creation
     {:type type :cardinality cardinality}))) ; metadata

(defn add-attr [ent attr]
  (let [attr-id (keyword (:name attr))]
    (assoc-in ent [:attrs attr-id] attr)))

;; data model - accessors

(defn entity-at
  ([db ent-id] (entity-at db (:curr-time db) ent-id))
  ([db ts ent-id] (get-entity (get-in db [:layers ts :storage]) ent-id)))

(defn attr-at
  ([db ent-id attr-name] (attr-at db ent-id attr-name (:curr-time db)))
  ([db ent-id attr-name ts] (get-in (entity-at db ts ent-id) [:attrs attr-name])))

(defn value-of-at
  ([db ent-id attr-name] (:value (attr-at db ent-id attr-name)))
  ([db ent-id attr-name ts] (:value (attr-at db ent-id attr-name ts))))

(defn index-at
  ([db kind] (index-at db kind (:curr-time db)))
  ([db kind ts] (kind ((:layers db) ts))))


;; indexing - why
;; the database accumulates fact
;;  - many of them
;; need to provide mechanisms to ask questions about these facts
;;  - graph query APIs
;;  - Datalog query language APIs
;; That mechanism of insights extraction must be efficient
;; This is what indexes are all about

;; A fact can be identified by the triplet entityId, attributeName and value
;;  - at a specific time
;;  - a Datom
;; Datoms are indexed

;; indexing - how
;; An index is a three leveled structure:
;;  - First level : map from key to a second level map
;;  - Second level : map from a key to a third level set
;;  - Third level : a set
;; Represent the datom in an index structure
;;  - Each level represents different kind of items in it
;;  - Either entityId, attributeName or value

;; Indxing - how
;; The name of the index is derived from the kind of items found in each levels
;;  - EAVT {entityId {attributeName #{value}}}
;;  - VEAT {value    {entityId      #{attributeName}}}
;;  - AVET {attributeName {value    #{entityId}}}
;;  - VAET {value    {attributeName #{entityId}}}


(defn make-index [from-eav to-eav usage-pred]
  (with-meta {}
    {:from-eav from-eav ;; takes a triplet in the canonical EAV order and rearranges it in the index order
     :to-eav to-eav ;; takes an index triplet and rearrange it in the canonical eav order
     :usage-pred usage-pred}))  ;; decides for a given datom whether is should be indexed in this index



















