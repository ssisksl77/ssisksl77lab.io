(ns yhnam.diy-database
  (:require [clojure.set :as CS :only [union difference intersection]])
  (:gen-class))

(defrecord Database [layers top-id curr-time])
;; Value, Attribute, Entity, Time
(defrecord Layer [storage VAET AVET VEAT EAVT])

(defrecord Entity [id attrs])
(defn make-entity
  ([] (make-entity :db/no-id-yet))
  ([id] (Entity. id {})))

(defrecord Attr [name values ts prev-ts])

(defn make-attr
  "name value type - required"
  ([name value type & {:keys [cardinality] :or {cardinality :db/single}}]
   {:pre [contains? #{:db/single :db/multiple} cardinality]}))

;; attribute은 Entity의 부분이 되어야 의미가 있다. entity에 attr를 추가해주자.
(defn add-attr [ent attr]
  (let [attr-id (keyword (:name attr))]
    (assoc-in ent [:attrs attr-id] attr)))

;; 저장할 곳.
;; 간단한 프로토콜을 통해 스토리지에 액세스하여
;; 데이터베이스 소유자가 선택할 수 있는 추가 스토리지 제공업체를 정의할 수 있습니다.
(defprotocol Storage
  (get-entity [storage e-id])
  (write-entity [storage entity])
  (drop-entity [storage entity]))

(defrecord InMemory [] Storage
           (get-entity [storage e-id] (e-id storage))
           (write-entity [storage entity] (assoc storage (:id entity) entity))
           (drop-entity [storage entity] (dissoc storage (:id entity))))


;; Indexing the Data
(defn make-index [from-eav to-eav usage-pred]
  (with-meta {} {:from-eav from-eav :to-eav to-eav :usage-pred usage-pred}))

(defn from-eav [index] (:from-eav (meta index)))
(defn to-eav [index] (:to-eav (meta index)))
(defn usage-pred [index] (:usage-pred (meta index)))

(defn indices[] [:VAET :AVET :VEAT :EAVT])

(defn ref? [attr] (= :db/ref (:type (meta attr))))

(defn always [& more] true)

(defn make-db []
  (atom
   (Database. [(Layer.
                (InMemory.)
                ;; (make-index from-eav to-eav usage-pred)
                (make-index #(vector %3 %2 %1) #(vector %3 %2 %1) #(ref? %)) ; VAET
                (make-index #(vector %2 %3 %1) #(vector %3 %1 %2) always) ; AVET
                (make-index #(vector %3 %1 %2) #(vector %2 %3 %1) always) ; VEAT
                (make-index #(vector %1 %2 %3) #(vector %1 %2 %3) always) ; EAVT
                )]
              0  ;; top id
              0  ;; current time
              )))
;; basic accessor
(defn entity-at
  ([db ent-id] (entity-at db (:curr-time db) ent-id))
  ([db ts ent-id] (get-entity (get-in db [:layers ts :storage]) ent-id)))

(defn attr-at
  ([db ent-id attr-name] (attr-at db ent-id attr-name (:curr-time db)))
  ([db ent-id attr-name ts] (get-in (entity-at db ts ent-id) [:attrs attr-name])))

(defn value-of-at
  ([db ent-id attr-name] (:value (attr-at db ent-id attr-name)))
  ([db ent-id attr-name ts] (:value (attr-at db ent-id attr-name ts))))

(defn indx-at
  ([db kind] (indx-at db kind (:curr-time db)))
  ([db kind ts] (kind ((:layers db) ts))))


;; evolution
;; "read-into-the-past" 과거 읽기.
;; prev-ts property로 해당 레이어를 찾아보고 속성의 값이 시간에 따라 어떻게 변화했는지 알 수 있음.
(defn evolution-of [db ent-id attr-name]
  (loop [res [] ts (:cur-time db)]
    (if (= -1 ts) (reverse res)
        (let [attr (attr-at db ent-id attr-name ts)]
          (recur (conj res {(:ts attr) (:value attr)})
                 (:prev-ts attr))))))

(defn- next-ts [db] (inc (:curr-time db)))  ;; 다음 시간을 리턴.
(defn- update-creation-ts [ent ts-val])  ;; 생성시간을 생성하기.

(defn- next-id [db ent]
  (let [top-id (:top-id db)  ;; 가장최근 id
        ent-id (:id ent)     ;; entity id 져오기
        increased-id (inc top-id)]  ;; 생성한 id
    (if (= ent-id :db/no-id-yet)  ;; entity id가 아직 생성안된 경우.
          [(keyword (str increased-id)) increased-id] ;; 새로운 아이디와 db에 증가한 값 리턴.
          [ent-id top-id]))) ;; 아니면 그대로 변화없이 리턴.

(defn- fix-new-entity [db ent]
  (let [[ent-id next-top-id] (next-id db ent) ;; next-id에서 가져온 새로운 아이디와 db top-id 사용.
        new-ts (next-ts db)
        new-entity (-> ent
                       (assoc :id ent-id)
                       update-creation-ts next-top-id)]
    [new-entity next-top-id]))

;; 인덱스 업데이트하기
;; add-entity-to-index
;; 1. 인덱싱해야 하는 속성을 찾는다.`(filter #((usage-pred index %) all-attrs`
;; 2. 엔티티의 ID에서 인덱스 경로를 작성한다.(Build an index-path from the entity's ID
;;    (add-entity에 (partial add-entity-to-index ,,,) 참고.
;; 3. 인덱스에 해당 경로를 추가 (update-entiry-in-index)

(defn collify [x] (if (coll? x) x [x]))  ;; collection화 시키는함수.

;; ?? operation은 사용안함.
(defn- update-entry-in-index [index path operation]
  (let [update-path (butlast path) ;; (butlast [1 2 3] -> (1 2)
        update-value (last path)   ;; (last [1 2 3]) -> 3
        to-be-updated-set (get-in index update-path #{})] ;; 이전까지 index 가져오기.
    ;; 해당 path에 새로운 값을 conj 한다.
    (assoc-in index update-path (conj to-be-updated-set update-value))))

(defn- update-attr-in-index [index ent-id attr-name target-val opration]
  "add-entity-to-index 보조 함수

인덱싱이 필요한 속성을 받아서 index를 추가하는 partial 함수를 생성한다."
  (let [colled-target-val (collify target-val) ;; reduce에 넣으려고 일괄 collection 행
        update-entry-fn (fn [ind vl]          ;; ind: index, vl: value
                           (update-entry-in-index
                            ind
                            ;; index에서 eav meta가져옴.
                            ((from-eav index) ent-id attr-name vl)
                            ;; 사유인듯 add-entity-to-idex는 :db/add
                           :db/add))]
    (reduce update-entry-fn index colled-target-val)))

(defn- add-entity-to-index [ent layer ind-name]
  "ind-name : 여러 인덱스 종류중 하나에 대한 기능"
  (let [ent-id (:ent)
        index (ind-name layer)
        all-attrs (vals (:attrs ent))
        relevant-attrs (filter #((usage-pred index) %) all-attrs) ;; 인덱스가 필요한 속성찾기.
        ;; layer는 새로운 layer가 추가되고 엔티티도 추가된 것.
        add-in-index-fn (fn [ind attr]
                          (update-attr-in-index ind
                                                ent-id
                                                (:name attr)
                                                (:value attr)
                                                :db/add))]
    ;; layer의 index에 인덱스가 필요한 것을 추가한다.
    (assoc layer ind-name (reduce add-in-index-fn index relevant-attrs))))

;; Adding Entity
;; 1. prepare the entity for addition (by giving it an ID and timestamp)
;; 2. place the entity in storage
;; 3. update indexes as necessary
;; fix-new-entity로 엔티티 생성.
;; 인테테를 스토리지에 추가하려면 데이터베이스에서 가장 최근 레이어를 찾아
;; 해당 레이어의 스토리지를 새 레이어로 업데이트하고, 그 결과는
;; layer-with-updated-storage 에 저장된다.
;; 그 이후 마지막으로 인덱스를 업데이트한다.
(defn add-entity [db ent]
  (let [[fixed-ent next-top-id] (fix-new-entity db ent)
        layer-with-updated-storage (update-in (last (:layers db)) [:storage] write-entity fixed-ent)
        add-fn (partial add-entity-to-index fixed-ent)
        ;; 인덱스 리뉴얼?
        new-layer (reduce add-fn layer-with-updated-storage (indices))]
    ;; DB에 layer가 추가된 값이 리턴되는 듯.
    (assoc db :layers (conj (:layers db) new-layer) :top-id next-top-id)))

;; 여러개 넣을 때.
(defn add-entities [db ents-seq] (reduce add-entity db ents-seq))

;; Removing an Entity
;; 1. 엔티티 자체 제거.
;; 2. 참조하는 다른엔티티 속성 업데이트
;; 3. 인덱스에서 엔티티 지우기.
;; add-entity와 유사함.
(defn- reffing-to [e-id layer]
  "e-id에 연결된 모든 attribute를 조회한다.(?)"
  (let [vaet (:VAET layer)
        relevant-attributes (e-id vaet)]
    (for [[attr-name reffing-set] relevant-attributes
          reffing reffing-set]
      [reffing attr-name])))

(defn single? [attr] (= :db/single (:cardinality (meta attr))))

(defn- update-attr-modification-time
  [attr new-ts]
  (assoc attr :ts new-ts :prev-ts (:ts attr)))

(defn update-attr-value [attr value operation]
  (cond
    (single? attr) (assoc attr :value #{value})
    ;; multiple values
    (= :db/reset-to operation)
    (assoc attr :value value)
    (= :db/add operation)
    (assoc attr :value (CS/union (:value attr) value))
    (= :db/remove operation)
    (assoc attr :value (CS/difference (:value attr) value))
    ))

(defn- update-attr [attr new-val new-ts operation]
  {:pre [(if (single? attr)
           (contains? #{:db/reset-to :db/remove} operation)
           (contains? #{:db/reset-to :db/add :db/remove} operation))]}
  (-> attr
      (update-attr-modification-time new-ts)
      (update-attr-value new-val operation)))

(defn remove-entry-from-index [index path]
  (let [path-head (first path)
        path-to-items (butlast path)
        val-to-remove (get-in index path-to-items)
        old-entries-set (get-in index path-to-items)]
    (cond
      (not (contains? old-entries-set val-to-remove)) index
      (= 1 (count old-entries-set)) (update-in index [path-head] dissoc (second path))
      :else (update-in index path-to-items disj val-to-remove))))

(defn remove-entries-from-index [ent-id operation index attr]
  (if (= operation :db/add)
    index
    (let [attr-name (:name attr)
          datom-vals (collify (:value attr))
          paths (map #(from-eav index ent-id attr-name %) datom-vals)]
      (reduce remove-entry-from-index index paths))))

(defn update-index [ent-id old-attr target-val operation layer ind-name]
  (if-not ((usage-pred (get-in layer [ind-name])) old-attr)
    layer
    (let [index (ind-name layer)
          cleaned-index (remove-entries-from-index ent-id operation index old-attr)
          updated-index (if (= operation :db/remove)
                          cleaned-index
                          (update-attr-in-index
                           cleaned-index ent-id (:name old-attr target-val operation)))]
      (assoc layer ind-name updated-index))))

(defn- put-entity [storage e-id new-attr]
  (assoc-in (get-entity storage e-id) [:attrs (:name new-attr)] new-attr))

(defn- update-layer
  [layer ent-id old-attr updated-attr new-val operation]
  (let [storage (:storage layer)
        new-layer (reduce (partial update-index ent-id old-attr new-val operation)
                          layer
                          (indices))]
    (assoc new-layer :storage (write-entity storage
                                            (put-entity storage ent-id updated-attr)))))

(defn update-entity
  ([db ent-id attr-name new-val]
   (update-entity db ent-id attr-name new-val :db/reset-to))
  ([db ent-id attr-name new-val operation]
   (let [update-ts (next-ts db)
         layer (last (:layers db))
         attr (attr-at db ent-id attr-name)
         updated-attr (update-attr attr new-val update-ts operation)
         fully-updated-layer (update-layer layer ent-id attr updated-attr new-val operation)]
     (update-in db [:layers] conj fully-updated-layer))))

(defn- remove-back-refs [db e-id layer]
  "참조하는 곳 수정."
  (let [reffing-datoms (reffing-to e-id layer) ;; 연결된datoms를 가져옴.
        remove-fn (fn [d [e a]] (update-entity db e a e-id :db/remove))
        clean-db (reduce remove-fn db reffing-datoms)]
    (last (:layers clean-db))))

(defn remove-entity-from-index [ent layer ind-name]
  (let [ent-id (:id ent)
        index (ind-name layer)
        all-attrs (vals (:attrs ent))
        relevant-attrs (filter #((usage-pred index) %) all-attrs)
        remove-from-index-fn (partial remove-entries-from-index ent-id :db/remove)]
    (assoc layer ind-name (reduce remove-from-index-fn index relevant-attrs))))

(defn remove-entity [db ent-id]
  (let [ent (entity-at db ent-id) ;; curr-time의 entity
        layer (remove-back-refs db ent-id (last (:layers db)))
        no-ref-layer (update-in layer [:VAET] dissoc ent-id)
        no-ent-layer (assoc no-ref-layer :storage
                            (drop-entity (:storage no-ref-layer)
                                         ent))
        new-layer (reduce (partial remove-entity-from-index ent)
                          no-ent-layer (indices))]
    (assoc db :layers (conj (:layers db) new-layer))))


;;; transaction
(defn transact-on-db [initial-db ops]
  (loop [[op & rst-ops] ops
         transacted initial-db]
    (if op
      ;; 재귀로 다 db에 operation을 넣는듯? 일단 op 함수의 구현이 뭔지 알아야함.
      (recur rst-ops (apply (first op) transacted (rest op)))
      (let [initial-layer (:layers initial-db)
            new-layer (last (:layers transacted))]
        (assoc initial-db
               :layers (conj initial-layer new-layer)
               :curr-time (next-ts initial-db)
               :top-id (:top-id transacted))))))

;; To have the simplest and clearest API,
;; 우리는 사용자가 Atom operation리스트만 제공하면 데이터베이스가 사용자입력을
;; 적절한 트랜잭션으로 변환하도록 하고자 한다.

;; transact -> _transact -> swap! -> transact-on-db

(defmacro transact [db-conn & txs] `(_transact ~db-conn swap! ~@txs))

(defmacro  _transact [db exec-fn & ops]
  (when ops
    (loop [[frst-op# & rst-ops#] ops
           res#  [exec-fn db `transact-on-db]
           accum-ops# []]
      (if frst-op#
        (recur rst-ops# res#  (conj  accum-ops#  (vec frst-op#)))
        (list* (conj res# accum-ops#))))))

(defn- _what-if
  "Operates on the db with the given transactions, but without eventually updating it."
  [db f ops] (f db ops))

(defmacro what-if [db & opts] `(_transact ~db _what-if ~@ops))
(defmacro transact [db-conn & ops] `(_transact ~db-conn swap~ ~@ops))



(defn greet
  "Callable entry point to the application."
  [data]
  (println (str "Hello, " (or (:name data) "World") "!")))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (greet {:name (first args)}))
