// Simplest malloc implementations maintain a linked-list of free block of memory that
// can be partitioned and given out as needed. When a user requests a chunk of memory, a block of the right size is removed from list and returned.

// Each chunk of memory in the free list begins with a header describing the block.
// Our header will contain two fields,
// one indicating the size of the chunk and the second pointing to the next free block of memory:
typedef struct header {
  unsigned int size;    // chunk size
  struct header *next;  // next free block of memory
} header_t;
