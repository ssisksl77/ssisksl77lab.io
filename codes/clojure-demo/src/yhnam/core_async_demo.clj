(ns yhnam.core-async-demo
  (:require [clojure.core.async :as async :refer [<!! >!!]]))

;; https://tbaldridge.pivotshare.com/media/coreasync-episode-1-pipelines/9025/feature
(defn pipeline< [desc c]
  (let [p (partition 2 desc)]
    (reduce
     (fn [prev-c [n f]]
       (-> (for [_ (range n)]
             (async/map< f prev-c))
           async/merge))
     c
     p)))
;; map< 는 deprecated 됨. transducer를 권장함.

(let [c (async/chan 10)]
  (>!! c 42)
  (<!! (pipeline< [4 inc
                   1 inc
                   2 dec
                   3 str]
                  c)))
