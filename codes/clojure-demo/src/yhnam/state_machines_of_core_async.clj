(ns yhnam.state-machines-of-core-async
  (:require [clojure.core.async :as async :refer [<!! >!! go <! >! timeout]]))

;; http://hueypetersen.com/posts/2013/08/02/the-state-machines-of-core-async/
;; core.async는 synchronous/blocking 코드를 비동기/non-blocking 코드로 변환하는 상태 머신을 만듭니다.
(defn fake-search [kind]
  (fn [c query]
    (go
      (<! (timeout (rand-int 100)))
      (>! c [kind query]))))

;; 매크로확장을 하면 state machine 이 보인다.
(comment
  (fn state-machine
    ([]
     (ioc-macros/aset-all! (java.util.concurrent.atomic.AtomicReference. 6)
                           0 state-machine
                           1 1))
    ([state_3730]
     (let [old-frame__2202__auto__ (clojure.lang.Var/getThreadBindingFrame)]
       (try
         (clojure.lang.Var/resetThreadBindingFrame
          (ioc-macros/aget-object satte_3730 3))

         (loop []
           (let [result (case (int (ioc-macro/aget-object state_3730 1))
                          3 (let [inst_3728 (ioc-macros/aget-object state_3730 1)
                                  satte_3730 state_3730]
                              (ioc-macros/return-chan state_3730 inst_3728))
                          2 (let [inst_3725 (ioc-macros/aget-object state_3730 2)
                                  inst_3726 (vector kind query)
                                  state_3730 (ioc-macros/aset-all! state_3730 5 inst_3725)]
                              (ioc-macros/put! state_3730 3 c inst_3726))
                          1 (let [inst_3722 (rand-int 100)
                                  inst_3723 (timeout inst_3722)
                                  state_3730 state_3730]
                              (ioc-macros/take! state_3730 2 inst_3723)))]
             (if (identical? result :recur)
               (recur)
               result)))
         (finally
           (clojure.lang.Var/resetThreadBindingFrame
            old-frame__2202__auto__))))))

  ;;
  ;; 좀더 이쁘게 바꿔보기 
    (fn state-machine
    ([]
     (ioc-macros/aset-all! (java.util.concurrent.atomic.AtomicReference. 6)
                           0 state-machine
                           1 1))
    ([state]
     (let [old-frame (clojure.lang.Var/getThreadBindingFrame)]
       (try
         (clojure.lang.Var/resetThreadBindingFrame
          (ioc-macros/aget-object state 3))

         (loop []
           (let [result (case (int (ioc-macro/aget-object state 1))
                          3 (let [instant (ioc-macros/aget-object state 1)
                                  state state]
                              (ioc-macros/return-chan state instant))
                          2 (let [instant1 (ioc-macros/aget-object state 2)
                                  instant2 (vector kind query)
                                  state (ioc-macros/aset-all! state 5 instant1)]
                              (ioc-macros/put! state 3 c instant2))
                          1 (let [radom-100 (rand-int 100)
                                  timeout-100 (timeout random-100)
                                  state state]
                              (ioc-macros/take! state 2 timeout-100)))]
             (if (identical? result :recur)
               (recur)
               result)))
         (finally
           (clojure.lang.Var/resetThreadBindingFrame
            old-frame))))))
  ;;
  )
;; 1
;; java.util.concurrent.atomic.AtomicReferenceArray. state machine이 저장되는 곳.
;; 0 FN-IDX : state machine function 상태머신함수
;; 1 STAETE-IDX : 현재상태
;; 2 VALUE-IDX : go 블록의 표현식이 평가할 값. (the value the expression in the go block evaluate to)
;; 3 BINDINGS-IDX : 캡처링된 바인딩들
;; 4 USER-START-IDX : go 블록이 반환활 채널
;; aset-all! 함수는 index/value 쌍을 변경. aget-object 는 인덱스로 값을 가져온다.

;; 2
;; 상태를 받아들이거나, 인자를 받지않고 상태를 초기화한다.
(comment
  ([]
   (ioc-macros/aset-all! (java.util.concurrent.atomic.AtomicReferenceArray. 6)
                         0 state-machine  ;; 상태머신함수
                         1 1))  ;; 현재상태는 1로 시작
  ;;
  )
;; 상태배열이 제공되면 현재 상태값에서 case문을 수행한다.
(comment

  (let [result (case (int (ioc-macros/aget-object state_3730 1))
                 3 (let [inst_3728 (ioc-macros/aget-object state_3730 2)
                         state_3730 state_3730]
                     (ioc-macros/return-chan state_3730 inst_3728))
                 2 (let [inst_3725 (ioc-macros/aget-object state_3730 2)
                         inst_3726 (vector kind query)
                         state_3730 (ioc-macros/aset-all! state_3730 5 inst_3725)]
                     (ioc-macros/put! state_3730 3 c inst_3726))
                 1 (let [inst_3722 (rand-int 100)
                         inst_3723 (timeout inst_3722)
                         state_3730 state_3730]
                     (ioc-macros/take! state_3730 2 inst_3723)))]
    ;; 상태머신에 1,2,3 상태가 있다.
    ;; 초기 상태가 1이었음을 기억하자.
    ;; 실행되면 가장먼저  (ioc-macros/take! state_3730 2 inst_3723) 가 실행될 것이다.
    )
  ;;
  )
;; 이렇게 상태함수가 만들어졌지만 해야할일이 더 있다.

(comment
  (let [f__2332__auto__ (fn state-machine
                                        ; ... see above ...
                          )
        state__2333__auto__ (-> (f__2332__auto__)
                                (ioc-macros/aset-all!
                                 ioc-macros/USER-START-IDX c__2330__auto__
                                 ioc-macros/BINDINGS-IDX captured-bindings__2331__auto__))]
    (ioc-macros/run-state-machine state__2333__auto__))
  ;;
  )
;; 1. (fn__2332__auto__) 는 상태머신함수에서 인자없이 호출하여 초기화를 시키는 것이다. (START_IDX 1 과 배열 초기화)
;; 2. 그다음 상태머신의 두 인덱스를 변경한다.
;; ioc-macros/USER-START-IDX는 4를 말한다. (위에 설명있음)
;; 그리고 c__2330__auto 는 go 블록이 반환할 채널이다. (go 매크로는 channel의 마지막 값을 반환하는 expression이다)
;; ioc-macros/BINDINGS-IDX는 3이다.
;; captured-bindings__2331__auto__ 는 Var/getThreadBindingFrame 을 이용하여 코드 외부에서 캡처된 바인딩을 설정한다.
;; 이렇게 바인딩을 잡아서 저장할 수 있다는 것은 참으로 멋진 일이다.

;; 현재 시점에서 상태배열에 저장된 값이다.
;; 0 : FN-IDX :              , the state machine function
;; 1 : STATE-IDX : 1         , the current state
;; 2 : VALUE-IDX : nil       , the value the expression in the go block evaluate to
;; 3 : BINDINGS-IDX :        , the captured-bindings
;; 4 : USER-START-IDX :      , the channel that will be returned by the go block

;; 이제 우리는 상태머신배열을 run-state-machine에게 보낸다.
(defn run-state-machine [state]
  ((aget-object state FN-IDX) state))

;; take!
;; 이제 첫번쨰 상태(1)인 take!로 돌아간다.
(comment
  (let [inst_3722 (rand-int 100)
        inst_3723 (timeout inst_3722)
        state_3730 state_3730]
    (ioc-macro/take! state_3730 2 inst_3723))
  ;; original cade : (<! (timeout (rand-int 1000)))
  )
;; state_3730 는 mutable한 상태머신배열이다.
;; inst_3722 는 (timeout (rand-int 100)) 로 인한 채널이다.
;; 중앙에 있는 인자 2는 다음 상태이다.
;; take! 구현을 살펴보자.
(comment
  (def take! [state blk c]
    (if-let [cb (impl/take! c (fn-handler
                               (fn [x]
                                 (aset-all! state VALUE-IDX x STATE-IDX blk)
                                 (run-state-machine state))))]
      (do (aset-all! state VALUE-IDX @cb STATE-IDX blk)
          :recur)
      nil))
  ;;
  )
;; take! 는 두 가지가 있다.
;; 1. channel 용 take, imple/take! (함수 안에 있는 호출)
;; 2. 바로 위 함수전체 상태머신용 take를 말한다.
;; 상태머신용 take 사용을 할 때 두가지 상황이 있다.
;; - 채널에 dequeue할 값이 있다.
;; - 채널에 dequeue할 값이 없다.
;; 값이 없으면 impl/take! 는 nil을 리턴한다. 그러면 바깥에 있는 if-let이 돌지 않는다. 이러면 전체 take! 표현식 평가는 nil이 된다.
;; 값이 있으면 그 값을 boxed해서 리턴한다. (즉, 값을 deref 하기 위해 boxed해서 참조할 주소를 만들어서 역참조를 해야함)
;; 상태머신배열의 VALUE-IDX를 채널에서 얻은 것으로 업데이트한다. 그리고 STATE-IDX를 다음 상태(인자로 받은거)로 바꾼다(이 예제에서는 1에서 2로 간다)
;; 그리고 :recur를 리턴하다.
;; :recur가 무엇을 하는지는 이전 감싸는 이전함수를 봐야함.
(comment
  (loop []
    (let
        [result (case (int (ioc-macros/aget-object state_3730 1))
                  3 (let [inst_3728 (ioc-macros/aget-object state_3730 2)
                          state_3730 state_3730]
                      (ioc-macros/return-chan state_3730 inst_3728))
                  2 (let [inst_3725 (ioc-macros/aget-object state_3730 2)
                          inst_3726 (vector kind query)
                          state_3730 (ioc-macros/aset-all! state_3730 5 inst_3725)]
                      (ioc-macros/put! state_3730 3 c inst_3726))
                  1 (let [inst_3722 (rand-int 100)
                          inst_3723 (timeout inst_3722)
                          state_3730 state_3730]
                      (ioc-macros/take! state_3730 2 inst_3723)))]
      (if (identical? result :recur)
        (recur)
        result)))

  ;;
  )
;; 리턴값이 nil(nothing to dequeue) 이거나 :recur(we did dequeue something) 중 하나다.
;; nil이면 loop가 끝나고 nil(result)를 리턴한다.
;; :recur면 다음상태를 머금으면 loop가 돈다.
;; 이렇게 상태머신의 큰 그림을 확인했다.
;; 마지막으로 누락된 것은 queue에서 제거할 것이 없을 때, 가짜 블록을 만들고 싶을 수도 있지만(아마 기다리는 것을 말하는 듯, 가짜로 블로킹한다느낌), 여기서는 상태머함수가 일반종하고 스레드를 해제한다.
;; 값이 채널에 도착할 때 이 상머신을 다시 작동시키는 것은 다른 무언가에게 달려있다.
;;
;; 그 무언가가 이 채널이다.
(comment
  (impl/take! c (fn-handler
                 (fn [x]
                   (aset-all! state VALUE-IDX x STATE-IDX blk)
                   (run-state-machine state))))
  ;;
  )
;; impl/take! 를 호출할 때, channel에서 단일 인수를 취하는 콜백을 포함한 Handler를 제공함.
;; 위 경우 콜백은 두가지 일을 한다.
;; - 큐에서 꺼낼 값이 있다면, 상태머신배열을 바꾼다. (VALUE-IDX x) 그리고 상태머신은 다음 상태로 넘어간다.
;; - 상태머신을 재시동한다. (re-run the state machine)
;;
;; 이것이 상태 머신의 뒤에서 일어나는 마법과 넌블로킹이 어떻게 이루어지는지를 설명한다.
;; 이것들은 사실 모두 콜백이지만 channels/library 가 콜백헬을 사람 대신 매니징해주는 것이다.
;; 따라서 간단한 경우 상태머신은 스레드를 포기하지 않고 처음부터 끝까지 실행할 수 있다.
;; 모든 채널은 취하거나(take) 놓을 준비(put)된 것이 있으며, 우리는 끝날 때까지 상태머신을 곧바로 재귀한다.
;; 다른 경우(간단하지 않은 경우인듯?)는 상태머신이 종료되고, 스레드가 해제되어 다른 작업을 수행하고,
;; 값이 대기열에서 제거될(dequeue) 준비가 되면 채널의 재량에 따라 상태 머신을 다시 시작한다.
;;
;; 상태가 완료된 후 (즉시 또는 콜백을 통햬) 상태머신배열은 이제 다음과 같다.
;; 0 : FN-DX          : 상태머신함수
;; 1 : STATE-IDX      : 2, 현재상태
;; 2 : VALUE-IDX      : `timeout` 채널에게 받은 값 the value received from `timeout` channel (`nil` since it choses on timeout)
;; 3 : BINDINGS-IDX   : 캡처링된 바인딩들 the captured-bindings
;; 4 : USER-START-IDX : go 블록에서 리턴될 채널 the channel that will be returned by the go block
;;

;; put!
(comment
  (let [inst_3725 (ioc-macros/aget-object state_3730 2)
        inst_3726 (vector kind query)
        state_3730 (ioc-macros/aset-all! state_3730 5 inst_3725)]
    (ioc-macros/put! state_3730 3 c inst_3726))
  ;;
  )
;; 상기시키자면, state_3730은 상태머신의 수정간으 배열이다.
;; 여기서 3 은 다음에 수행해야할 상태를 말한다.
;; inst_3726 은 `(vector kind query)` 의 evalution(평가) 이다.
;; 인자 c는 뭘까? 원래 코드를 보자.
(comment
  (defn fake-search [kind]
    (fn [c query]
      (go
        (<! (timeout (rand-int 100)))
        (>! c [kind query]))))
  ;;
  )
;; c 는 이 함수의 채널이다.
;; 상태머신이 실행될 때마다 모든 변수가 함수가 기대하는 변수가인지 확인해야한다.
;; 따라서 `put!` 의 첫번째 부분은 상태머신배열과 다음 상태이다.
;; 다음은 채널 c, 그리고 그 채널에 넣고 싶은(put!) 값

(comment
  (defn put! [state blk c val]
    (if-let [cb (impl/put! c val (fn-handler (fn []
                                               (aset-all! state VALUE-IDX nil STATE-IDX blk)
                                               (run-state-machine state))))]
      (do (aset-all! state VALUE-IDX @cb STATE-IDX blk)
          :recur)
      nil))
  ;;
  )
;; 생김새가 어디서 본 것이다.
;; take! 와 다른 점이라고는 Handler 안에 있는 콜백함수가 인자를 받고 있지 않고 있다는 점이다.
;; 그리고 상태머신에 넣는 값이 nil이다. (aset-all! state VALUE-IDX nil STATE-IDX blk) 이거 말하는듯
;; take! 의 경우 채널에서 값을 dequeue 했었다.

;; 따라서, 우리는 곧바로 채널에 값을 성공적으로 넣고(put) `:recur` 또는 가짜블록(fake block)을 만들어 리턴한다.
;; 채널은 콜백을 실행할 책일을 가진다. (채널이 값을 enqueue 할 준비가 되었을 때!)

;; core.async를 처음 읽었을 때 채널에 접속하는 것도 blocking 이라는 것을 바로 알 수 없었다.
(comment
  (let [c (chan)]
    (go
      (>! c :foo)
      (prn "this never runs!")))
  ;;
  )
;; 이 go 루틴은 끝나지 않을 것이다. 아무도 c에서 뭘 가져갈 수 없다.
;; 여기서 필요한 것이 buffer 다. buffer에 배치할 공간이 있으면 block 되지 않는다.
;; 하지만 디폴트 채널은 버퍼가 없고, 다른 스레드가 채널에서 가져올 때까지 넣기가 차단된다.


;; return-chan
마침내, 마지막 상태에 도달했다.
go 언어와 core.async의 한가지 차이점은 `go` 키워드가 clojure 에서는 expression이라는 점이다.
이 expression은 채널을 리턴한다. 이 채널은 마지막 표현식의 값이 추가되고 즉시 닫힌다. (닫힌 채널을 리턴하는 것)
이 잡이 우리의 마지막 상태다.

(comment
  (let [inst_3728 (ioc-macros/aget-object state_3730 2)
        state_3730 state_3730]
    (ioc-macros/return-chan state_3730 inst_3728))
  ;;
  )
;; 상태머신배열의 VALUE-IDX(2)는 상태머신을 통과할 때 마지막 값을 추적하고 있으며,
;; USER-START-IDX(4)에는 go expression에서 반환된 채널이 있다.
;; `return-chan`이 나머지를 처리한다.
;; 위 경우 inst_3728은 상태배열의 값(마지막 표현식이 `put` 이므로 nil)이고
;; state_3730은 알다시피 상태머신배열이다.

(comment
  (defn return-chan [state value]
    (let [c (aget-object state USER-START-IDX)]
      (when-not (nil? value)
        (impl/put! c value (fn-handler (fn [] nil))))
      (impl/close! c)
      c))
  ;;
  )
;; 꽤나 직관적인 함수다.
;; 상태머신배열에서 채널을 가져온다.
;; 채널에 nil을 넣을 수 없기 때문에 nil여부를 확인한다.
;; nil이 아닌경우 채널에 입력하고 `noop` 콜백을 제공한다. (noop은 (fn [] nil) 을 말하는 듯)
;; 모든 케이스에서 우리는 채널을 닫고 반환한다.


;; do-alts!
;; take!, puts!, 그리고 return-chan 은 간단한 상태머신을 커버함.
;; 한가지 우리에게 남은 것은 `alts`를 사용하여 여러 채널에서 selection하는 것이다.

(comment
  (go
    (let [[v ch] (alts! [c1 c2])]
      (println "Read" v "from" ch)))
  ;;
  )
;; 이것은 아주 간단한 고루틴이다. 한 쌍의 채널에서 선택하고 메시지를 출력하는 고루틴이다.
;; 매크로를 확장하면 첫번째 상태는 다음과 같다.

(comment
  (let [inst_3786 (vector c1 c2)
        state_3793 state_3793]
    (ioc-alts! state_3793 2 inst_3786))
  ;;
  )
;; ioc-alts! 는 상태머신배열, 다음 상태, 채널벡터를 인자로 받는다.

(comment
  (defn ioc-alts! [state cont-block ports & {:as opts}]
    (ioc/aset-all! state ioc/STATE-IDX cont-block)
    (when-let [cb (clojure.core.async/do-alts
                   (fn [val]
                     (ioc/aset-all! state ioc/VALUE-IDX val)
                     (ioc/run-state-machine state))
                   ports
                   opts)]
      (ioc/aset-all! state ioc/VALUE-IDX @cb)
      :recur))

  ;; put! 과 비교하기
  (defn put! [state blk c val]
    (if-let [cb (impl/put! c val (fn-handler (fn []
                                               (aset-all! state VALUE-IDX nil STATE-IDX blk)
                                               (run-state-machine state))))]
      (do (aset-all! state VALUE-IDX @cb STATE-IDX blk)
          :recur)
      nil))
  )
;; take! put! 과는 코드가 좀 다르지만 하는 일은 비슷하다.
;; 콜백대신 상태머신의 상태를 즉시 앞으로 나아가게 한다.
;; 그 다음 콜백은 상태머신 리턴값설정을 처리한다.
;; impl/take! impl/put! 을 통해 채널을 직접 처리하는 대신 do-alts! 에게 위임한다. (do-alts 에게 콜백을 제공한다)
;; nil을 반환하면 표현식은 nil 평가되고, 그렇지 않으면 `:recur` 를 얻고 상태머신을 계속 진행시킨다.
;; 마지막 부분에서는 `do-alts`를 살펴본다.
;;
(comment
  (defn do-alts
    "returns derefable [val port] if immediate, nil if enqueued"
    [fret ports opts]
    (let [flag (alt-flag)
          n (count ports)
          ^ints idxs (random-array n)
          priority (:priority opts)
          ret
          (loop [i 0]
            (when (< i n)
              (let [idx (if priority i (aget idxs i))
                    port (nth ports idx)
                    wport (when (vector? port) (port 0))
                    vbox (if wport
                           (let [val (port 1)]
                             (impl/put! wport val (alt-handler flag #(fret [nil wport]))))
                           (impl/take! port (alt-handler flag #(fret [% port]))))]
                (if vbox
                  (channels/box [@vbox (or wport port)])
                  (recur (inc i))))))]
      (or
       ret
       (when (contains? opts :default)
         (.lock ^Lock flag)
         (let [got (and (impl/active? flag) (impl/commit flag))]
           (.unlock ^Lock flag)
           (when got
             (channels/box [(:default opts) :default]))))))))
;; 여기에도 put!, take! 와 마찬가지로 크게 두가지 경우가 있다.
;; 1. there IS something ready to dequeue or enqueue
;; 2. there IS NOT something ready to dequeue or enqueue.
;; 큐에 넣고뺼 준비가 되있거나 안되있거나
;;
;; let을 보면 `(alt-flag)`를 사용하여 플래그를 만든다.
;;
;; At a high level you can think of a flag as a shared lock / coordinator between all of the potential callback’s we are going to give to each channel in the alts! expression.
;; 높은 수준에서 보면, flag를 alts! 표현식 안에 있는 각 채널에 제공할 모든 잠재적 콜백 간의 shared lock / coordinator 로 볼 수 있다.
;; 여기서는 c1과 c2가 있고, c1에서 가져올 때 c2에서도 가져오지 않도록 해야한다.
;; flag 는 우리를 위 이 조정(coordination)을 처리한다.
;; 이 작업을 수행하는 방법은 매우훌륭하며 이후 게시물에서 이를 다룰 수 있으면 좋겠다.
;;
;; The first case we want to check for is the case in which a channel IS ready to enqueue or dequeue a value.
;; 우리가 확인하고자 하는 첫번째 경우는 채널이 값을 대기열에 넣거나 대기열에서 뺄 준비가 된 경우다.
;; This is done by looping over all of the channels.
;; 이것은 모든 채널을 루프도는 것으로 수행된다.
;; The order this is done depends on whether a priority flag is passed which would make the order the channels are given in the vector the order they are checked
;; 이것이 수행되는 순서는 우선순위 플래그 (priority flag)가 전달되서 채널이 확인되는 순서대로 vector에 제공되는지 여부에 따라 다르다.
;; 그렇지 않으면 무작위로 수행된다. loop expression은 이 검사를 수행하기 위함이다.
(comment 
  (loop [i 0]
    (when (< i n)
      (let [idx (if priority i (aget idxs i))
            port (nth ports idx)
            wport (when (vector? port) (port 0))
            vbox (if wport
                   (let [val (port 1)]
                     (impl/put! wport val (alt-handler flag #(fret [nil wport])))) ;; 채널이 vector
                   (impl/take! port (alt-handler flag #(fret [% port]))))] ;; 채널이 vector 아님
        (if vbox
          (channels/box [@vbox (or wport port)])
          (recur (inc i)))))))
;; 0에서 시작해서 i가 n(채널 수)보다 작은 동안 루프를 돈다.
;; 각 반복에서 우리가 검사하는 채널(port)은 index value i 혹은 i-th random integer of `idxs` array (idxs배열의 i번째 임의의정수) 이다.
;; `(idx (if (priority i (aget idxs i))))`

;; 채널이 vector이면 [c3 "foo"] 같이 [채널 값] 쌍을 사용하여 put 작업이다.
;; 그렇지 않으면 take 이고 c1과 같이 채널이다.
;; 

;; Once the channel is sorted we attempt the put! or take! operation on the channel.
;; 채널이 일단 정렬되면 put! take! 오퍼레이션을 시도한다.
;; 시도해서 nil을 리턴하면 아직 enqueue, dequeue 할 준비가 안됐거고, boxed value가 오면 준비가 된 것이다.
;; The Handler given to both put! and take! is not an fn-handler but an alt-handler which you should notice also takes the flag as well as a callback function.
;; 여기서 put! take! 에게 주어진 Handler는 fn-handler 가 아니라 alt-handler임을 알 수 있다. 하지만 alt-handler 또한 flag와 콜백함수를 취한다는 점을 유의하자.
;; Again, this flag is required for coordination.
;; 다시 말하면, flag는 coordination에 필요하다.

;; So if we get a value from take! or put! we are done – a channel operation was ready and completed.
;; 따라서 take! 로 값을 가져오거나 put! 을 했다면 우린 다끝냈다. 채널 operation은 준비완료했다.
;; We break from the loop and return the pair of value and channel (which is boxed) . Otherwise we advance the index and loop again.
;; 우린 루프에서 벗어나 값과 채널쌍(boxed 된 값)을 반환한다. 그렇지 않으면 인덱스를 진행하고 다시 루프를 돈다.
;;  If we loop through all of the channels and none are ready then the loop evaluates to nil.
;; 모든 채널을 loop 돌고 준비된 채널이 없으면 루프는 nil로 평가된다.

(comment
  
  (or
   ret
   (when (contains? opts :default)
     (.lock ^Lock flag)
     (let [got (and (impl/active? flag) (impl/commit flag))]
       (.unlock ^Lock flag)
       (when got
         (channels/box [(:default opts) :default])))))
  )
;; Here ret is the value of the loop expression.
;; 여기서 ret 심볼은 위에 있던 loop 표현식의 값이다.
;; nil인경우 채널 작업이 완료될 때까지 기다리거나 기본값이 전달되었다면 그걸 반환한다.

;; 기본값이 있으면 플래그를 잠그고 활성 상태인지 확인한 다음 플래그를 커밋한다.
;; 활성 상태인지 확인하려면 플래그를 잠가야한다.
;; 이것은 우리가 다중 스레드 환경이고 마지막으로 채널을 확인한 이후로 스레드 중 하나가 take, put operation을 완료할 수도 있기 때문이다.
;; 이 경우 flag가 inactive 로 바뀐다.
;; 하지만 flag가 계속 active 라면! 채널은 여전히 대기 중이고 콜백이 호출되지 않았으므로 기본값을 반환한다.

;; Since we are going to return a default value we want to cancel all of the callback's waiting on the channels
;; 우리는 기본값을 반환할 것이기 때문에 채널에서 대기 중인 모든 콜백을 취소하려한다.
;; This is where commit comes in.
;; 여기서 commit 이 등장한다.
;; When we commit the flag we make it inactive which also makes all of the callback’s on the channels from this alts! expression inactive fulfilling the flag’s coordination responsibility
;; 플래그를 커밋할 때 플래그를 비활성화하여 alts! 표현식의 채널에 있는 모든 콜백을 비활성화하여 플래그의 조정 책임을 수행한다.
;; 채널은 비활성 핸들러에 콜백을 실행하지 않는다.

;; Finally we unlock the flag and return the default value (again, boxed) .
;; 드디어 flag의 잠금을 해제하고 기본값(boxed)을 반환한다.
;; If there is no default value we simply return nil and all of the callback’s on each channel remain active.
;; 기본값이 없으면 단순히 nil을 반환하고 각 채널의 모든 콜백은 활성 상태로 유지된다.
;; 콜백함수는 다음과 같다.

(comment 
  (fn [val]
    (ioc/aset-all! state ioc/VALUE-IDX val)
    (ioc/run-state-machine state))
  )

;; The state machine’s state was already advanced, so it just updates the value and runs the machine.
;; 상태 머신의 상태는 이미 앞으로 나아가있다, 그러므로 값을 업데이트하고 머신을 실행한다.
;; 마지막 디테일은 alts! 표현식의 일부인 콜백이 채널에서 커밋되면 
;; 이것 또한 플래그를 커밋한다. 그리고 나머지 콜백은 비활성화된다.
;; 이것이 우리가 각각 의 변경을 보장하는 방법이다.
;; 표현식은 단일 채널에서만 넣거나 가져온다.

;; Conclusion
;; 
