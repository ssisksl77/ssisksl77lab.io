(ns yhnam.core-async-diy
  (:import
   (java.util LinkedList Queue)
   (java.util.concurrent.locks Lock)
   (clojure.lang Counted)))


(defn gen-random-number []
  (rand-int 10))

(defn random-indices [n k]
  (->> (range 3 n)
       (shuffle)
       (take k)
       (sort)))

(defn replace-with-condition [numbers indices]
  (reduce (fn [result idx]
           (assoc result idx (nth result (- idx 3))))
         numbers
         indices))

(defn generate-numbers-with-matches [n k]
  (let [numbers (vec (take n (repeatedly n gen-random-number)))
        indices (random-indices n k)]
    (replace-with-condition numbers indices)))

(defn count-matching-pairs [numbers]
  (->> numbers
       (map-indexed (fn [idx num] [idx num]))
       (drop 3)
       (filter (fn [[idx num]] (= num (nth numbers (- idx 3)))))
       (count)))

(defn calculate-matching-ratio [numbers]
  (let [matching-pairs (count-matching-pairs numbers)]
    (/ matching-pairs (count numbers))))

(->> (repeatedly #(generate-numbers-with-matches 200 (+ 30 (rand-int 11))))
     #_(filter #(< 0.2 (calculate-matching-ratio %)))
     (take 10)
     (map calculate-matching-ratio))
