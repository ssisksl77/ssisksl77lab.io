(ns ants)

;dimensions of square world
(def dim 80)
;number of ants = nants-sqrt^2
(def nants-sqrt 7)
;number of places with food
(def food-places 35)
;range of amount of food at a place
(def food-range 100)
;scale factor for pheromone drawing
(def pher-scale 20.0)
;scale factor for food drawing
(def food-scale 30.0)
;evaporation rate
(def evap-rate 0.99)

(def animation-sleep-ms 100)
(def ant-sleep-ms 40)
(def evap-sleep-ms 1000)

(def running true)

(defstruct cell :food :pher) ;may also have :ant and :home

;world is a 2d vector of refs to cells
(def world
     (apply vector
            (map (fn [_]
                   (apply vector (map (fn [_] (ref (struct cell 0 0)))
                                      (range dim))))
                 (range dim))))

;; place helper function은 만들어서 2차원의 특정 셀을 전달함으로써 world의 한 지점, 특정 셀을 찾을 수 있게 해줄것.
(defn place [[x y]]
  (-> world (nth x) (nth y)))


;; ants has a direction, always
;; ;; 0 is north, and then clockwise through 8
(defstruct ant :dir) ;; may also have :food

;; ants are going to live in the world, and that is really where their data is going to be.
;; so creating an ant means placing an ant in one of these cells!
;; 개미를 만든다는 것은 특정 셀에 ant를 만들어서 넣는다는 것이다.
(defn create-ant
  "create an ant at the location, returning an ant agent on the location"
  [loc dir]
  ;; anything that is inside the parens of this wync is going to be inside this transaction.
  ;; The nil is a place holder arguments,
  ;; and eventually I think I got rid of these by making a macro down here.
  ;; I got tired of writing sync nil, so I wrote a dosync.
  ;; and eventually that will be part of Clojure
  (sync nil
        (let [p (place loc)
              a (struct ant dir)]
          (alter p assoc :ant a)
          ;; an agent per ant.
          ;; agents are these asynchronous buys that are going to do work.
          ;; their state is going to be the location where their ant is.
          ;; so there is a presumption here that no two ants in the same place. Not allowed.
          ;; no two ants can occupy the same space.
          (agent loc))))

;; Now that would be potentially a hard thing to maintain if you were to write this manually with locks and everything like that.
;; You would have to make sure that no two ants can be in the same place.
;; That is going to happen in this system.
;; And these agents are going to be in charge of making sure that that is the case.

;; there is an offset for where home is. and range.
(def home-off (/ dim 4))
;; what i need is a littel square in part of the world where the ants start,
;; and they all have to be in unique spaces, therefore look how convenient this is to have
;; counted the ants by their square root.
;; 개미를 담을 수 있을 만큼 큰 정사각형이 생김.
(def home-range (range home-off (+ nants-sqrt home-off)))


(defn setup
  "place initial food and ants, returns seq of ant agents"
  []
  (sync nil
        ;; 음식을 세팅
        (dotimes [i food-places]
          (let [p (place [(rand-int dim) (rand-int dim)])]
            (alter p assoc :food (rand-int food-range))))
        (doall
         ;; home 을 세팅
         (for [x home-range y home-range]
           (do
             (alter (place [x y])
                    assoc :home true)

             (create-ant [x y] (rand-int 8)))))))

;; 경계를 넘어가면 경계 반대편으로 이동하게 해주는 함수
(defn bound
  "returns n wrapped into range 0-b"
  [b n]
  (let [n (rem n b)]
    (if (neg? n)
      (+ n b)
      n)))

;; wrand is weighted random
;; 시스템이나 시뮬레이션을 할 때 난수가 필요하다고 무작위로 선택할 때
;; 선택하는 항목들이 가중치가 다르다면 가중치에 따라서 선택할 수 있도록 해주는 함수가 필요하다.
(defn wrand
  "given a vector of slice sizes, returns the index of a slice given a
  random spin of a roulette wheel with compartments proportional to
  slices."
  [slices]
  (let [total (reduce + slices)
        r (rand total)]
    (loop [i 0 sum 0]
      ;; r은 가준치를 갖는
      (if (< r (+ (slices i) sum))
        i
        (recur (inc i) (+ (slices i) sum))))))


(def dir-delta {0 [0 -1]
                1 [1 -1]
                2 [1 0]
                3 [1 1]
                4 [0 1]
                5 [-1 1]
                6 [-1 0]
                7 [-1 -1]})

(defn delta-loc
  "returns the location one step in the given dir. Note the world is a torus"
  [[x y] dir]
  (let [[dx dy] (dir-delta (bound 8 dir))]
    [(bound dim (+ x dx)) (bound dim (+ y dy))]))

(defn turn
  "turns the ant at the location by the given amount

  location 에 있는 ant를 amt 만틈 회전시킨다."
  [loc amt]

  (dosync
   (let [p (place loc)
         ant (:ant @p)]
     ;; direction 을 바꾼다.
     (alter p assoc :ant (assoc ant :dir (bound 8 (+ (:dir ant) amt))))))
    loc)

(defn move
  "moves the ant in the direction it is heading.
  Must be called in a transaction that has verified the way is clear"
  [loc]
  (let [oldp (place loc)
        ant (:ant @oldp)
        ;; ant가 바라보는 방향으로 이동시킴
        newloc (delta-loc loc (:dir ant))
        p (place newloc)]
    ;; move the ant
    (alter p assoc :ant ant)
    (alter oldp dissoc :ant)
    ;; leave pheromone trail (페로몬 흔적 남기기)
    (when-not (:home @oldp)
      (alter oldp assoc :pher (inc (:pher @oldp))))
    newloc))

(defn take-food
  "Take one food from current location. Must be called in a transaction
  that has verified there is food available"
  [loc]
  (let [p (place loc)
        ant (:ant @p)]
    (alter p assoc
           :food (dec (:food @p))
           :and (assoc ant :food true))
    loc))

(defn drop-food
"Drops food at current location. Must be called in a
transaction that has verified the ant has food"
  [loc]
  (let [p (place loc)
        ant (:ant @p)]
    (alter p assoc
           :food (inc (:food @p))
           :ant (dissoc ant :food))))

(defn rank-by
  "returns a map of xs to their 1-based rank when sorted by keyfn"
  [keyfn xs]
  (let [sorted (sort-by (comp float keyfn) xs)]
    (reduce (fn [ret i] (assoc ret (nth sorted i) (inc i)))
            {} (range (count sorted)))))



(defn behave
  "the main function for the ant agent"
  [loc]
  ;; we are going to find all of our places
  (let [p (place loc)        ;; where are we?
        ;; 우리가 있는 곳에 ant는?
        ant (:ant @p)
        ;; 우리 앞에는 무엇이?
        ahead (place (delta-loc loc (:dir ant)))
        ;; 왼쪽에는/
        ahead-left (place (delta-loc loc (dec (:dir ant))))
        ;; 오른쪽에는?
        ahead-right (place (delta-loc loc (inc (:dir ant))))
        ;; [앞 왼 오] vector
        places [ahead ahead-left ahead-right]]
    (. Thread (sleep ant-sleep-ms))
    (dosync
     (when running
       (send-off *agent* #'behave))
     ;; 개미는 기본적으로 두가지를 하고 있다.
     ;; 먹이가 있어서 집으로 돌아가려고 하거나, 먹이를 찾고 있는 경우.
     ;; 개미가 먹이를 가지고 있다면 아무거나 리턴하고 그것은 ture, 없으면 nil=>false이다.
     (if (:food ant)
       ;; going home

       (cond

         ;; 집에와서 먹이를 떨어뜨리고 돌아선다.
         (:home @p)
         (-> loc drop-food (turn 4))

         ;; 우리 앞에 집이 있는데 개미가 없다면, 우리는 이동을 호출한다.
         ;; tthis is a requirement of move - it must be called in a transaction
         (and (:home @ahead) (not (:ant @ahead)))
         (move loc)

         ;; 그 외는 일종의 선택을 해야함.
         ;; 앞으로, 약간 왼쪽, 약간 오른쪽으로 돌아가는 것의 세 가지 가능성에 순위를 매겨야 한다.
         ;; 두 가지 방법으로 선택의 순위를 매긴다.
         :else
         (let [ranks (merge-with +
                                 ;; 우리 앞에 home이 있는가.
                                 (rank-by (comp #(if (:home %) 1 0) deref) places)
                                 ;; 우리 앞에 페로몬이 있는가.
                                 ;; 이 두 map을 +로 병합한다.
                                 ;; 이 두 가지 가중치의 점수를 합산하여 순위를 매기는데 사용할 것.
                                 (rank-by (comp :pher deref) places))]
           ;; 이제 세 가지 선택지의 순위를 매겼다.
           ;; vector를 호출할 것인데 maps가 functions of their keys 인 것처럼.
           ;; vectors는 functions of their indices 이다.
           ;; 3 가지 동작의 벡터가 생겼고, 이동, 좌회전, 우회전가능성이 있다.
           (([move #(turn % -1) #(turn % 1)]
             ;; 무작위로 룰렛을 돌려서 하나의 동작을 선택함. 여기에는 가중치만 넣고
             (wrand [(if (:ant @ahead) 0 (ranks ahead))
                     (ranks ahead-left)
                     (ranks ahead-right)]))
            loc)))
       ;; foraging (먹이 찾기)
       ;; no food
       (cond
         ;; 내가 있는 곳에 food가 있고 home에는 없는 상황이라면?
         ;; 음식을 들고, 집으로 돌아서야 한다.
         (and (pos? (:food @p)) (not (:home @p)))
         (-> loc take-food (turn 4))

         ;; 내 앞(ahead)에 음식이 있고, 집에는 없고, 또 내앞에 개미도 없다면?
         ;; 다음 턴에 음식을 가져갈것.
         (and (pos? (:food @ahead)) (not (:home @ahead)) (not (:ant @ahead)))
         (move loc)

         ;; 위 내용과 비슷하다.
         :else
         (let [ranks (merge-with +
                                 (rank-by (comp :food deref) places)
                                 (rank-by (comp :pher deref) places))]
           (([move #(turn % -1) #(turn % 1)]
             (wrand [(if (:ant @ahead) 0 (ranks ahead))
                     (ranks ahead-left) (ranks ahead-right)]))
            loc)))))))

;; 음식  함량, 페로몬 함량에 따라 내 앞에 있는 선택 항목의 순위를 매기는 것을 제외하면 같은 종류의 논리이다.
;; 그래서 난 집을 찾는 대신 음식을 찾고 있다.
;; 페로몬도 이론적으로 그 길을 좋은 일로 이어져야 하기 때문에 그 길에 머물려고 한다.
;; 그래서 모든 개미들이 그 위에 있는 것
;; 이것이 behave 이다. 가장 큰 기능이며 에이전트의 주요 루프인 개미 에이전트이다.


(defn evaporate
  "causes all the pheromones to evaporate a bit"
  []
  (dorun
   (for [x (range dim) y (range dim)]
     (dosync
      (let [p (place [x y])]
        (alter p assoc :pher (* evap-rate (:pher @p))))))))

;; 마지막으로 증발이라는 기능이 있다.
;; 페로몬은 어디에나 축적되지만 신선한 페로몬은 더 가치가 있다.
;; 그래서 페로몬은 시간이 지남에 따라 증발한다.
;; 증발은 큰 일이다.
;; 모든 cell은 순회해야함.
;; 이것은 전체 6400개를 돌아다니는 기능이다.
;; 작은 트랜잭션에서 셀당 하나씩, 한 장소에 있는 페로몬의 양을 0.99로 곱할 것이다.
;; 이것은 페로몬의 1%를 증발시킨다.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; UI ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(import
 '(java.awt Color Graphics Dimension)
 '(java.awt.image BufferedImage)
 '(javax.swing JPanel JFrame))

;pixels per world cell
(def scale 5)

(defn fill-cell [#^Graphics g x y c]
  (doto g
    (.setColor c)
    (.fillRect (* x scale) (* y scale) scale scale)))

(defn render-ant [ant #^Graphics g x y]
  (let [black (. (new Color 0 0 0 255) (getRGB))
        gray (. (new Color 100 100 100 255) (getRGB))
        red (. (new Color 255 0 0 255) (getRGB))
        [hx hy tx ty] ({0 [2 0 2 4]
                        1 [4 0 0 4]
                        2 [4 2 0 2]
                        3 [4 4 0 0]
                        4 [2 4 2 0]
                        5 [0 4 4 0]
                        6 [0 2 4 2]
                        7 [0 0 4 4]}
                       (:dir ant))]
    (doto g
      (.setColor (if (:food ant)
                  (new Color 255 0 0 255)
                  (new Color 0 0 0 255)))
      (.drawLine (+ hx (* x scale)) (+ hy (* y scale))
                (+ tx (* x scale)) (+ ty (* y scale))))))

(defn render-place [g p x y]
  (when (pos? (:pher p))
    (fill-cell g x y (new Color 0 255 0
                          (int (min 255 (* 255 (/ (:pher p) pher-scale)))))))
  (when (pos? (:food p))
    (fill-cell g x y (new Color 255 0 0
                          (int (min 255 (* 255 (/ (:food p) food-scale)))))))
  (when (:ant p)
    (render-ant (:ant p) g x y)))

(defn render [g]
  (let [v (dosync (apply vector (for [x (range dim) y (range dim)]
                                   @(place [x y]))))
        img (new BufferedImage (* scale dim) (* scale dim)
                 (. BufferedImage TYPE_INT_ARGB))
        bg (. img (getGraphics))]
    (doto bg
      (.setColor (. Color white))
      (.fillRect 0 0 (. img (getWidth)) (. img (getHeight))))
    (dorun
     (for [x (range dim) y (range dim)]
       (render-place bg (v (+ (* x dim) y)) x y)))
    (doto bg
      (.setColor (. Color blue))
      (.drawRect (* scale home-off) (* scale home-off)
                 (* scale nants-sqrt) (* scale nants-sqrt)))
    (. g (drawImage img 0 0 nil))
    (. bg (dispose))))

(def panel (doto (proxy [JPanel] []
                        (paint [g] (render g)))
             (.setPreferredSize (new Dimension
                                     (* scale dim)
                                     (* scale dim)))))

(def frame (doto (new JFrame) (.add panel) .pack .show))

(def animator (agent nil))

(defn animation [x]
  (when running
    (send-off *agent* #'animation))
  (. panel (repaint))
  (. Thread (sleep animation-sleep-ms))
  nil)

(def evaporator (agent nil))

(defn evaporation [x]
  (when running
    (send-off *agent* #'evaporation))
  (evaporate)
  (. Thread (sleep evap-sleep-ms))
  nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; use ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment
                                        ;demo
  (load-file "/Users/rich/dev/clojure/ants.clj")
  (def ants (setup))
  (send-off animator animation)
  (dorun (map #(send-off % behave) ants))
  (send-off evaporator evaporation)

)
