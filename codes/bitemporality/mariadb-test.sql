
DROP TABLE naite.test2;
create table naite.test2(
    id BIGINT UNSIGNED NOT NULL,
    json_data JSON,
    valid_from DATETIME NOT NULL DEFAULT NOW(),
    valid_to DATETIME NOT NULL DEFAULT '2038-01-19 03:14:07.999999',
    PRIMARY KEY(id, valid_to),
    PERIOD FOR application_time(valid_from, valid_to))
    ENGINE InnoDB character set = 'utf8mb4' collate = 'utf8mb4_general_ci'
WITH SYSTEM VERSIONING;

ALTER TABLE naite.test2
    ADD INDEX idx_valid_from(valid_from),
    ADD INDEX idx_valid_to(valid_to);


INSERT INTO naite.test2(id, json_data, valid_from, valid_to)
VALUES
    (JSON_UNQUOTE(JSON_EXTRACT('{"id": "1234567890123456", "name": "ynam", "age": 24, "address": "korea", "etc": ""}', '$.id')),
     '{"id": "1234567890123456", "name": "ynam", "age": 24, "address": "korea", "etc": ""}',
     NOW(),
     '9999-12-31'
);

UPDATE naite.test2 FOR PORTION OF application_time FROM NOW() TO '9999-12-31'
SET json_data = JSON_REPLACE(json_data, '$.age', 25)
WHERE id = 1234567890123456;



SELECT * FROM naite.test2 where now() between valid_from and valid_to;
SELECT * FROM naite.test2 WHERE NOW() > valid_to;
# properly deleting
UPDATE naite.test2
SET valid_to = NOW()
WHERE id = 1234567890123456 AND valid_to > NOW();



