(ns ssisksl77.diy-core.channel
  (:require [ssisksl77.diy-core.protocols :as impl]
            [ssisksl77.diy-core.dispatch :as dispatch]
            [ssisksl77.diy-core.mutex :as mutex]
            #_#_            [clojure.core.async.impl.dispatch :as dispatch]
            [clojure.core.async.impl.mutex :as mutex])
  (:import [java.util LinkedList Queue Iterator]
           [java.util.concurrent.locks Lock]))


(defmacro assert-unlock [lock test msg]
  `(when-not ~test
     (.unlock ~lock)
     (throw (new AssertionError (str "Assert failed: " ~msg "\n" (pr-str '~test))))))

(defn box [val]
  (reify clojure.lang.IDeref
    (deref [_] val)))

(defprotocol MMC
  (cleanup [_])
  (abort [_]))

;; takes - 다른 프로세스나 스레드로부터 값을 받을 준비가 되어 있는 작업. (메시지 큐에서 메시지 수신)
;; puts - 대기 중인 작업. 어떤 값을 다른 프로세스나 스레드에 전달하려는 작업. (메시지 큐에서 메시지 전송)
(deftype ManyToManyChannel [^LinkedList takes ^LinkedList puts ^Queue buf closed ^Lock mutex add!]
  MMC
  (cleanup
   [_]
    #_(tap> {:takes takes
           :puts puts
           :buf (count buf)})
    (when-not (.isEmpty takes)
      (let [iter (.iterator takes)]
        (loop [taker (.next iter)]
#_          (tap> {:c '(impl/active? taker)
                 :v (impl/active? taker)})
          (when-not (impl/active? taker)
            (.remove iter))
          (when (.hasNext iter)
            (recur (.next iter))))))
    
    (when-not (.isEmpty puts)
      (let [iter (.iterator puts)]
        (loop [[putter] (.next iter)]
          (when-not (impl/active? putter)
            (.remove iter))
          (when (.hasNext iter)
            (recur (.next iter)))))))

  (abort
   [this]
   (let [iter (.iterator puts)]
     (when (.hasNext iter)
       (loop [[^Lock putter] (.next iter)]
         (.lock putter)
         (let [put-cb (and (impl/active? putter) (impl/commit putter))]
           (.unlock putter)
           (when put-cb
             (dispatch/run (fn [] (put-cb true))))
           (when (.hasNext iter)
             (recur (.next iter)))))))
   (.clear puts)
   (impl/close! this))

  impl/WritePort
  (put!
   [this val handler]
   (when (nil? val)
     (throw (IllegalArgumentException. "Can't put nil on channel")))
   (.lock mutex)
    (tap> {:order 1
           :currentThread (.getName (Thread/currentThread))})
    (cleanup this)
   (if @closed
     (let [^Lock handler handler]
       (.lock handler)
       (when (impl/active? handler) (impl/commit handler))
       (.unlock handler)
       (.unlock mutex)
       (box false))
     (let [^Lock handler handler]
       (if (and buf (not (impl/full? buf)) (not (.isEmpty takes)))
         (do
           ;; takes에 기대하는 놈이 있어야 되는구만?
           (.lock handler)
           ;; 테스트 결과 impl/commit 은 기본적인 기능에서는 하는 일 없음...
           ;; 그냥 put!, take! 하면 콜백할 함수가 나옴.
           (let [put-cb (and (impl/active? handler) (impl/commit handler))]
             (.unlock handler)
             (tap> {:order 2
                    :buffer-count (count buf)
                    :currentThread (.getName (Thread/currentThread))})
             (if put-cb
               (let [done? (reduced? (add! buf val))]
                 (if (pos? (count buf))
                   (let [iter (.iterator takes)
                         take-cbs (loop [takers []]
                                    (if (and (.hasNext iter) (pos? (count buf)))
                                      (let [^Lock taker (.next iter)]
                                        (tap> {:order 3
                                               :buffer-count (count buf)
                                               :currentThread (.getName (Thread/currentThread))})
                                        (.lock taker)
                                        (let [ret (and (impl/active? taker) (impl/commit taker))]
                                          (.unlock taker)
                                          (tap> {:c '(.unlock taker)
                                                 :order 4
                                                 :currentThread (.getName (Thread/currentThread))}
                                                )
                                          (if ret
                                            (let [val (impl/remove! buf)]
                                              (.remove iter)
                                              (recur (conj takers (fn fn-1 [] (ret val)))))
                                            (recur takers))))
                                      takers))]
                     (if (seq take-cbs)
                       (do
                         (when done?
                           (abort this))
                         (.unlock mutex)
                         (doseq [f take-cbs]
                           (tap> {:c '(doseq f take-cbs)
                                  :take-cbs take-cbs
                                  :currentThread (.getName (Thread/currentThread))
                                  :f f})
                           (dispatch/run f)))
                       (do
                         (when done?
                           (abort this))
                         (.unlock mutex))))
                   (do
                     (when done?
                       (abort this))
                     (.unlock mutex)))
                 (box true))
               (do (.unlock mutex)
                   nil))))
         (let [iter (.iterator takes)
               [put-cb take-cb] (when (.hasNext iter)
                                  (loop [^Lock taker (.next iter)]
                                    (if (< (impl/lock-id handler) (impl/lock-id taker))
                                      (do (.lock handler) (.lock taker))
                                      (do (.lock taker) (.lock handler)))
                                    (let [ret (when (and (impl/active? handler) (impl/active? taker))
                                                [(impl/commit handler) (impl/commit taker)])]
                                      (.unlock handler)
                                      (.unlock taker)
                                      (if ret
                                        (do
                                          (.remove iter)
                                          ret)
                                        (when (.hasNext iter)
                                          (recur (.next iter)))))))]
           (if (and put-cb take-cb)
             (do
               (.unlock mutex)
               (dispatch/run (fn fn-2 [] (take-cb val)))
               (box true))
             (if (and buf (not (impl/full? buf)))
               (do
                 (.lock handler)
                 (let [put-cb (and (impl/active? handler) (impl/commit handler))]
                   (.unlock handler)
                   (if put-cb
                     (let [done? (reduced? (add! buf val))]
                       (when done?
                         (abort this))
                       (.unlock mutex)
                       (box true))
                     (do (.unlock mutex)
                         nil))))
               (do
                 (when (and (impl/active? handler) (impl/blockable? handler))
                   (assert-unlock mutex
                                  (< (.size puts) impl/MAX-QUEUE-SIZE)
                                  (str "No more than " impl/MAX-QUEUE-SIZE
                                       " pending puts are allowed on a single channel."
                                       " Consider using a windowed buffer."))
                   (.add puts [handler val]))
                 (.unlock mutex)
                 nil))))))))

  impl/ReadPort
  (take!
   [this handler]
    (.lock mutex)
   (cleanup this)
    (let [^Lock handler handler
          commit-handler (fn commit-handler []
                           (.lock handler)
                           (let [take-cb (and (impl/active? handler) (impl/commit handler))]
                             (.unlock handler)
                             take-cb))]
      (tap> {:where 'impl/ReadPort
             :order 1
             :buffer-count (count buf)
             :pred (and buf (pos? (count buf)))
             :commit-handler (commit-handler)})
      (if (and buf (pos? (count buf)))
        (if-let [take-cb (commit-handler)]
          (let [val (impl/remove! buf)
                _ (tap> {:order 1.2
                         :val val})
                iter (.iterator puts)
                _ (tap> {:order 1.3
                         :msg "버퍼가 남았는데 puts가 있는 경우."
                         :pred (and (not (impl/full? buf)) (.hasNext iter))})
                [done? cbs]
                (when (and (not (impl/full? buf)) (.hasNext iter))
                  (loop [cbs []
                         [^Lock putter val] (.next iter)]
                    (tap> {:where 'impl/ReadPort
                           :order 2
                           :buffer-count (count buf)})
                    (.lock putter)
                    (let [cb (and (impl/active? putter) (impl/commit putter))]
                      (.unlock putter)
                      (.remove iter)
                      (let [cbs (if cb (conj cbs cb) cbs)
                            done? (when cb (reduced? (add! buf val)))]
                        ;; buf에 자리가 있고,
                        ;; puts 에도 값이 남았고
                        ;; add! 가 바로 reduced가 안됨?
                        ;; 하나라도 들어가면 리턴하는 듯?
                        ;; 지금까지 테스트 중 cbs 항상 nil
                        (if (and (not done?) (not (impl/full? buf)) (.hasNext iter))
                          (recur cbs (.next iter))
                          [done? cbs])))))]
            (tap> 
             {:order 4
              :done? done?
              :cbs cbs
              :val val
              :currentThread (.getName (Thread/currentThread))})
            (when done?
              ;; buffer가 full인 상태에서 한번 순회해서 마친게 있으면 한번 abort한다?
              (abort this))
            (.unlock mutex)
            (doseq [cb cbs]
              (tap> {:order 1.4})
              (dispatch/run #(cb true)))
            (box val))
          (do (.unlock mutex)
              (tap> {:order 1.5})
              nil))
        
        (let [iter (.iterator puts)
              _ (tap> {:order 3})
              [take-cb put-cb val]
              (when (.hasNext iter)
                (loop [[^Lock putter val] (.next iter)]
          
                  (if (< (impl/lock-id handler) (impl/lock-id putter))
                    (do (.lock handler) (.lock putter))
                    (do (.lock putter) (.lock handler)))
                  (let [ret (when (and (impl/active? handler) (impl/active? putter))
                              [(impl/commit handler) (impl/commit putter) val])]
                    (.unlock handler)
                    (.unlock putter)
                    (if ret
                      (do
                        (.remove iter)
                        ret)
                      (when-not (impl/active? putter)
                        (.remove iter)
                        (when (.hasNext iter)
                          (recur (.next iter))))))))]
          {:order 5
           :val val
           :buffer-count (count buf)
           :currentThread (.getName (Thread/currentThread))}
          (if (and put-cb take-cb)
            (do
              (.unlock mutex)
              (dispatch/run #(put-cb true))
              (box val))
            (if @closed
              (do
                (when buf (add! buf))
                (let [has-val (and buf (pos? (count buf)))]
                  (if-let [take-cb (commit-handler)]
                    (let [val (when has-val (impl/remove! buf))]
                      (.unlock mutex)
                      (box val))
                    (do
                      (.unlock mutex)
                      nil))))
              (do
                (when (impl/blockable? handler)
                  (assert-unlock mutex
                                 (< (.size takes) impl/MAX-QUEUE-SIZE)
                                 (str "No more than " impl/MAX-QUEUE-SIZE
                                      " pending takes are allowed on a single channel."))
                  (.add takes handler))
                (.unlock mutex)
                nil)))))))

  impl/Channel
  (closed? [_] @closed)
  (close!
   [this]
   (.lock mutex)
   (cleanup this)
   (if @closed
     (do
       (.unlock mutex)
       nil)
     (do
       (reset! closed true)
       (when (and buf (.isEmpty puts))
         (add! buf))
       (let [iter (.iterator takes)]
         (when (.hasNext iter)
           (loop [^Lock taker (.next iter)]
             (.lock taker)
             (let [take-cb (and (impl/active? taker) (impl/commit taker))]
               (.unlock taker)
               (when take-cb
                 (let [val (when (and buf (pos? (count buf))) (impl/remove! buf))]
                   (dispatch/run (fn fn-3 [] (take-cb val)))))
               (.remove iter)
               (when (.hasNext iter)
                 (recur (.next iter)))))))
       (when buf (impl/close-buf! buf))
       (.unlock mutex)
       nil))))

(defn- ex-handler [ex]
  (-> (Thread/currentThread)
      .getUncaughtExceptionHandler
      (.uncaughtException (Thread/currentThread) ex))
  nil)

(defn- handle [buf exh t]
  (let [else ((or exh ex-handler) t)]
    (if (nil? else)
      buf
      (impl/add! buf else))))

(defn chan
  ([buf] (chan buf nil))
  ([buf xform] (chan buf xform nil))
  ([buf xform exh]
   (ManyToManyChannel.
    (LinkedList.) (LinkedList.) buf (atom false) (mutex/mutex)
    (let [add! (if xform (xform impl/add!) impl/add!)]
      (fn
        ([buf]
         (try
           (add! buf)
           (catch Throwable t
             (handle buf exh t))))
        ([buf val]
         (try
           (add! buf val)
           (catch Throwable t
             (handle buf exh t)))))))))



