(ns ssisksl77.diy-core.concurrent
  (:import [java.util.concurrent ThreadFactory]))

;; 이름을 커스텀한 스레드를 만들어줌.
(defn counted-thread-factory
  "Create a ThreadFactory that maintains a counter for naming Threads.
  name-format : specifies thread names - use %d to include counter
  daemon : is a flag for whether threads are daemons or not 
  opts : is an options map:
  init-fn : function to run when thread is created"
  ([name-format daemon]
   (counted-thread-factory name-format daemon nil))
  ([name-format daemon {:keys [init-fn] :as opts}]
   (let [counter (atom 0)]
     (reify
       ThreadFactory
       (newThread [this runnable]
         (let [body (if init-fn
                      (fn []
                        (init-fn)
                        (.run ^Runnable runnable))
                      runnable)
               t (Thread. ^Runnable body)]
           (doto t
             (.setName (format name-format (swap! counter inc)))
             (.setDaemon daemon))))))))
(defonce
  ^{:doc "Number of processors reported by the JVM"}
  processors (.availableProcessors (Runtime/getRuntime)))
