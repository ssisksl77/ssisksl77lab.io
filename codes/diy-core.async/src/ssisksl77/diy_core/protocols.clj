(ns ssisksl77.diy-core.protocols)


(defprotocol Buffer
  (full? [b] "returns true if buffer cannot accept put")
  (remove! [b] "remove and return next item from buffer, called under chan mutex")
  (add!* [b itm] "if room, add item to the buffer, returns b, called under chan mutex")
  (close-buf! [b] "called on chan closed under chan mutex, return ignored"))


;; 마커 인터페이스 같은 것. 블록이 일어나지 않는 버퍼
(defprotocol UnblockingBuffer)

(defprotocol ReadPort
  ;; port 값을 받아올 인자, fn1-handler 작업이 완료되었을 때 호출될 콜백함수.
  ;; 성공적이면 받은 값을 리턴, take작업이 대기열에 등록된 경우 nil 리턴. 
  (take! [port fn1-handler] "derefable val if taken, nil if taken was enqueued"))

(defprotocol WritePort
  ;; port 값을 넣을 대상, val 넣을 값, fn1-handler 작업이 완료되었을 때 호출될 콜백.
  (put! [port val fn1-handler] "derefable boolean (false iff already closed) if handled, nil if put was enqueued. Must throw on nil val."))


(defprotocol Handler
  (active? [h] "returns true if has callback. Must work w/o lock")
  (blockable? [h] "returns true if this handler may be blocked, otherwise it must not block")
  (lock-id [h] "a unique id for lock acquisition order, 0 if no lock")
  (commit [h] "commit to fulfilling its end of the transfer, returns cb. Must be called within lock"))

(defprotocol Executor
  (exec [e Runnable] "execute runnable asynchronously"))

(defprotocol Channel
  (close! [chan])
  (closed? [chan]))

(defn add!
  ([b] b)
  ([b itm]
   (assert (not (nil? itm)))
   (add!* b itm)))

(def ^:const ^{:tag 'int} MAX-QUEUE-SIZE 1024)
