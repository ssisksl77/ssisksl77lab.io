(ns ssisksl77.diy-core.threadpool
  (:require [ssisksl77.diy-core.protocols :as impl]
            [ssisksl77.diy-core.concurrent :as conc])
  (:import [java.util.concurrent Executors Executor]))


(def ^:private pool-size
  "설정 안하면 8로 설정됨."
  (delay (or (Long/getLong "clojure.core.async.pool-size") 8)))

(defn thread-pool-executor
  "pool-size 만큼 thread-pool을 채운다."
  ([]
   (thread-pool-executor nil))
  ([init-fn]
   (let [executor-svc (Executors/newFixedThreadPool
                       @pool-size
                       (conc/counted-thread-factory "async-dispatch-%d"  ;; name-format
                                                    true  ;; daemon
                                                    {:init-fn init-fn}))]
     (reify impl/Executor
       (impl/exec [this r]
         (.execute executor-svc ^Runnable r))))))
