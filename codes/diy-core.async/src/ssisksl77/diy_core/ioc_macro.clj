(ns ssisksl77.diy-core.ioc-macro
  (:refer-clojure :exclude [all])
  (:require [clojure.pprint :refer [pprint]]
            [clojure.tools.analyzer.ast :as ast]
            [clojure.tools.analyzer.env :as env]
            [clojure.tools.analyzer.passes :refer [schedule]]
            [clojure.tools.analyzer.passes.jvm.annotate-loops :refer [annotate-loops]]
            [clojure.tools.analyzer.passes.jvm.warn-on-reflection :refer [warn-on-reflection]]
            [clojure.tools.analyzer.jvm :as an-jvm]
            [ssisksl77.diy-core.protocols :as impl]
            [clojure.set :as set])
  (:import [java.util.concurrent.locks Lock]
           [java.util.concurrent.atomic AtomicReferenceArray]))

(defn debug [x]
  (pprint x)
  x)


(def ^{:const true :tag 'long} FN-IDX 0)
(def ^{:const true :tag 'long} STATE-IDX 1)
(def ^{:const true :tag 'long} VALUE-IDX 2)
(def ^{:const true :tag 'long} BINDINGS-IDX 3)
(def ^{:const true :tag 'long} EXCEPTION-FRAMES 4)
(def ^{:const true :tag 'long} USER-START-IDX 5)


(defn aset-object [^AtomicReferenceArray arr ^long idx o]
  (.set arr idx o))

(defn aget-object [^AtomicReferenceArray arr ^long idx]
  (.get arr idx))

(defmacro aset-all!
  [arr & more]
  (assert (even? (count more)) "Must give an even number of args to aset-all!")
  (let [bindings (partition 2 more)
        arr-sym (gensym "statearr-")]
    `(let [~arr-sym ~arr]
       ~@(map
          (fn [[idx val]]
            `(aset-object ~arr-sym ~idx ~val))
          bindings)
       ~arr-sym)))

;; State monad stuff, used only in SSA construction
(defmacro gen-plan
    "Allows a user to define a state monad binding plan.

  (gen-plan
    [_ (assoc-in-plan [:foo :bar] 42)
     val (get-in-plan [:foo :bar])]
    val)"
  [binds id-expr]
  (let [binds (partition 2 binds)
        psym (gensym "plan_")
        forms (reduce
               (fn [acc [id expr]]
                 (concat acc `[[~id ~psym] (~expr ~psym)]))
               []
               binds)]
    `(fn [~psym]
       (let [~@forms]
         [~id-expr ~psym]))))

(defn get-plan
  "Returns the final [id state] from a plan."
  [f]
  (f {}))

(defn push-binding
  "Sets the binding 'key' to value. This operation can be undone via pop-bindings.
   Bindings are stored in the state hashmap.

  plan 안의 {:binding {key [v1 v2]}}
  안에 value를 conj 한다."
  [key value]
  (fn [plan]
    [nil (update-in plan [:bindings key] conj value)]))

(defn push-alter-binding
  "Pushes the result of (apply f old-value args) as current value of binding key
  바인딩 된 데이터들 중에 [v1 v2 v3] 가장 최근 데이터 v1에 (f v1)을 수행하여 new-value를 만들어
  [new-value v1 v2 v3] 형태로 업데이트."
  [key f & args]
  (fn [plan]
    [nil (update-in plan [:bindings key]
                    #(conj % (apply f (first %) args)))]))

(defn get-binding
  "Gets the value of the current binding for key
  현재 key에 바인딩된 값"
  [key]
  (fn [plan]
    [(first (get-in plan [:bindings key])) plan]))

(defn pop-binding
  "Removes the most recent binding for key
  현재 key에 가장 최근 바인딩 된 값을 제거"
  [key]
  (fn [plan]
    [(first (get-in plan [:bindings key]))
     (update-in plan [:bindings key] pop)]))

(defn no-op
  "This function can be used inside a gen-plan when no operation is to be performed"
  []
  (fn [plan]
    [nil plan]))

(defn all
  "Assumes that itms is a list of state monad function results, 
  threads the state map through all of them. Returns a vector of all the result

  리턴값은 [[r1 r2 r3] plan] 형태"
  [itms]
  (fn [plan]
    (reduce
     (fn [[ids plan] f]  ;; 전부 수행하고 모든 리턴한 모든 result를 vector로 리턴함.
       (let [[id plan] (f plan)]
         [(conj ids id) plan]))
     [[] plan]  ;; initial value
     itms  ;; state monad function result
     )))

(defn assoc-in-plan
  [path val]
  (fn [plan]
    [val (assoc-in plan path val)]))

(defn update-in-plan
  [path f & args]
  (fn [plan]
    [nil (apply update-in plan path f args)]))

(defn get-in-plan
  [path]
  (fn [plan]
    (pprint plan)
    [nil plan]))

(defn print-plan []
  (fn [plan]
    (pprint plan)
    [nil plan]))

(defn set-block
  "Sets the current block begin wrtten to by the functions.
  The next add-instruction call will append to this block

  plan에 현재 block을 인자 block-id로 세팅하고 block-id 리턴.
  "
  [block-id]
  (fn [plan]
    [block-id (assoc plan :current-block block-id)]))

(defn get-block
  "Gets the current block"
  []
  (fn [plan]
    [(:current-block plan) plan]))


(defn add-block
  "Adds a new block, returns its id, but does not change the current block (does not call set-block)."
  []
  (gen-plan
   [_ (update-in-plan [:block-id] (fnil inc 0))  ;; block-id 생성
    blk-id (get-in-plan [:block-id]) ;; 새로 생성된 블록 가져오기
    cur-blk (get-block)  ;; 현재블록 가져오기
    _ (assoc-in-plan [:blocks blk-id] [])  ;; 새로운블록(blk-id)를 추가(assoc)하고 빈 리스트로 초기화
    catches (get-binding :catch)  ;; 현재 블록의 예외처리정보 가져오기.
    _ (assoc-in-plan [:block-catches blk-id] catches)  ;; 예외 처리 정보를 새로운 블록에 연결함.
    _ (if-not cur-blk  ;; 현재 작업 중인 블록이 없는 경우, 새로운 블록을 시작 블록으로 설정.
        (assoc-in-plan [:start-block] blk-id)
        (no-op))]
   blk-id))



(defn instruction? [x]
  (::instruction (meta x)))

(defn add-instruction
  "Appends an instruction to the current block. 
  현재 블록에 명령어추가하기."
  [inst]
  (let [inst-id (with-meta (gensym "inst_")
                  {::instruction true})
        inst (assoc inst :id inst-id)]
    (gen-plan
     [blk-id (get-block)  ;; 현재 블록 조회
      _ (update-in-plan [:blocks blk-id] (fnil conj []) inst)] ;; inst 추가(conj)
     inst-id)))

;; We're going to reduce Clojure expression to a ssa format,
;; and then translate the instructions for this
;; virtual-virtual-machine back into Clojure data.
;; ssa format으로 변형 후 아래 Instruction 프로토콜에 맞춘 객체로 바꾼다는 듯?
(defprotocol IInstruction
  ;; 이 명령어가 읽는 명령어목록을 반환.
  (reads-from [this] "Returns a list of instructions this instruction read from")
  ;; 명령어가 쓰려는 명령어목록 리턴.
  (writes-to [this] "Returns a list of instructions this instruction write to")
  ;; 명령어가 참조하는 모든 블록.
  (block-references [this] "Returns all the blocks this instruction references"))

;; 발생가능한 명령어 : 명령어가 나타내는 Clojure코드를 반환함.
(defprotocol IEmittableInstruction
  (emit-instruction [this state-sym] "Returns the clojure code that this instruction represents"))

(defprotocol ITerminator
  (terminator-code [this] "Returns a unique symbol for this instruction")
  (terminate-block [this state-sym custom-terminators] "Emites the code to terminate a given block"))

(defrecord Const [value]
  IInstruction
  (reads-from [_this] [value])
  (writes-to [this] [(:id this)])
  (block-references [_this] []))

(defrecord RawCode [ast locals]
  IInstruction
  (reads-from [_this]
    (for [local (map :name (-> ast :env :locals vals))  ;; locals 바인딩에서 모든 값을 가져온다.
          :when (contains? locals local)]  ;; locals에 읽고자 하는 local이 나올때까지 순회
      (get locals local)))
  (writes-to [this] [(:id this)])
  (block-references [_this] [])

  IEmittableInstruction
  (emit-instruction [this _state-sym]
    (if (not-empty (reads-from this))
      `[~@(into []
                (comp
                 (map #(select-keys % [:op :name :form]))
                 (filter (fn [local]
                           (contains? locals (:name local))))
                 (distinct)
                 (mapcat
                  (fn [local]
                    `[~(:form local) ~(get locals (:name local))])))
                (-> ast :env :locals vals))])))

(defrecord CustomTerminator [f blk values meta]
  IInstruction
  (reads-from [_this] values)
  (writes-to [_this] [])
  (block-references [_this] [])
  ITerminator
  (terminate-block [_this state-sym _]
    (with-meta `(~f ~state-sym ~blk ~@values)
      meta)))

(defn- emit-clashing-binds
  [recur-nodes ids clashes]
  ;; 충돌해결을위한 임시바인딩 생성.
  ;; 각 clash마다 (gensym "tmp") 를 매핑.
  (let [temp-binds (reduce
                    (fn [acc i]
                      (assoc acc i (gensym "tmp")))
                    {} clashes)]
    (concat
     ;; [tmpid1 clash1 tmpid2 clash2]
     (mapcat (fn [i]
               `[~(temp-binds i) ~i])
             clashes)
     (mapcat (fn [node id]
               `[~node ~(get temp-binds id id)])
             recur-nodes
             ids))))

(defrecord Recur [recur-nodes ids]
  IInstruction
  (reads-from [_this] ids)
  (writes-to [_this] recur-nodes)
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [_this _state-sym]
    (if-let [overlap (seq (set/intersection (set recur-nodes) (set ids)))]
      (emit-clashing-binds recur-nodes ids overlap)
      (mapcat (fn [r i]
                `[~r ~i]) recur-nodes ids))))


(defrecord Call [refs]
  IInstruction
  (reads-from [_this] refs)
  (writes-to [this] [(:id this)])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [this _state-sym]
    `[~(:id this) ~(seq refs)]))

(defrecord StaticCall [class method refs]
  IInstruction
  (reads-from [_this] refs)
  (writes-to  [this] [(:id this)])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [this _state-sym]
    `[~(:id this) (. ~class ~method ~@(seq refs))]))

(defrecord InstanceInterop [instance-id op refs]
  IInstruction
  (reads-from [_this] (cons instance-id refs))
  (writes-to [this] [(:id this)])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [this _state-sym]
    `[~(:id this) (. ~instance-id ~op ~@(seq refs))]))


(defrecord Case [val-id test-vals jmp-blocks default-block]
  IInstruction
  (reads-from [_this] [val-id])
  (writes-to [_this] [])
  (block-references [_this] [])
  ITerminator
  (terminate-block [_this state-sym _]
    `(do (case ~val-id
           ~@(concat (mapcat (fn [test blk]
                               `[~test (aset-all! ~state-sym ~STATE-IDX ~blk)])
                             test-vals jmp-blocks)
                     (when default-block
                       `[(do (aset-all! ~state-sym ~STATE-IDX ~default-block)
                             :recur)])))
         :recur)))


(defrecord Fn [fn-expr local-names local-refs]
  IInstruction
  (reads-from [_this] local-refs)
  (writes-to [this] [(:id this)])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [this _state-sym]
    `[~(:id this)
      (let [~@(interleave local-names local-refs)]
        ~@fn-expr)]))

(defrecord Dot [cls-or-instance method args]
  IInstruction
  (reads-from [_this] `[~cls-or-instance ~method ~@args])
  (writes-to [this] [(:id this)])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [this _state-sym]
    `[~(:id this) (. ~cls-or-instance ~method ~@args)]))

(defrecord Jmp [value block]
  IInstruction
  (reads-from [_this] [value])
  (writes-to [_this] [])
  (block-references [_this] [block])
  ITerminator
  (terminate-block [_this state-sym _]
    `(do (aset-all! ~state-sym ~VALUE-IDX ~value ~STATE-IDX ~block)
         :recur)))

(defrecord Return [value]
  IInstruction
  (reads-from [_this] [value])
  (writes-to [_this] [])
  (block-references [_this] [])
  ITerminator
  (terminator-code [_this] :Return)
  (terminate-block [this state-sym custom-terminators]
    (if-let [f (get custom-terminators (terminator-code this))]
      `(~f ~state-sym ~value)
      `(do (aset-all! ~state-sym ~VALUE-IDX ~value ~STATE-IDX ::finished)
           nil))))

(defrecord CondBr [test then-block else-block]
  IInstruction
  (reads-from [_this] [test])
  (writes-to [_this] [])
  (block-references [_this] [then-block else-block])
  ITerminator
  (terminate-block [_this state-sym _]
    `(do (if ~test
           (aset-all! ~state-sym ~STATE-IDX ~then-block)
           (aset-all! ~state-sym ~STATE-IDX ~else-block))
         :recur)))
(defrecord PushTry [catch-block]
  IInstruction
  (reads-from [_this] [])
  (writes-to [_this] [])
  (block-references [_this] [catch-block])
  IEmittableInstruction
  (emit-instruction [_this state-sym]
    `[~'_ (aset-all! ~state-sym ~EXCEPTION-FRAMES (cons ~catch-block (aget-object ~state-sym ~EXCEPTION-FRAMES)))]))
(defrecord PopTry []
  IInstruction
  (reads-from [_this] [])
  (writes-to [_this] [])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [_this state-sym]
    `[~'_ (aset-all! ~state-sym ~EXCEPTION-FRAMES (rest (aget-object ~state-sym ~EXCEPTION-FRAMES)))]))
(defrecord CatchHandler [catches]
  IInstruction
  (reads-from [_this] [])
  (writes-to [_this] [])
  (block-references [_this] (map first catches))
  ITerminator
  (terminate-block [_this state-sym _]
    (let [ex (gensym 'ex)]
      `(let [~ex (aget-object ~state-sym ~VALUE-IDX)]
         (cond
          ~@(for [[handler-idx type] catches
                  i [`(instance? ~type ~ex) `(aset-all! ~state-sym ~STATE-IDX ~handler-idx)]]
              i)
          :else (throw ~ex))
         :recur))))
(defrecord EndFinally [exception-local]
  IInstruction
  (reads-from [_this] [exception-local])
  (writes-to [_this] [])
  (block-references [_this] [])
  IEmittableInstruction
  (emit-instruction [_this _state-sym]
    `[~'_ (throw ~exception-local)]))


;;;;;;;;;;;;;; Dispatch clojure forms based on :op
(def -item-to-ssa nil) ;; for help in the repl
(defmulti -item-to-ssa :op)

(defmethod -item-to-ssa :default
  [ast]
  (gen-plan
   [locals (get-binding :locals)
    id     (add-instruction (->RawCode ast locals))]
   id))

(defn item-to-ssa [ast]
  (if (or (::transform? ast)
          (contains? #{:local :const :quote} (:op ast)))
    (-item-to-ssa ast)
    (gen-plan
     [locals (get-binding :locals)
      id (add-instruction (->RawCode ast locals))]
     id)))

(defmethod -item-to-ssa :invoke
  [{f :fn args :args}]
  (gen-plan
   [arg-ids (all (map item-to-ssa (cons f args)))
    inst-id (add-instruction (->Call arg-ids))]
   inst-id))

(defmethod -item-to-ssa :keyword-invoke
  [{f :keyword target :target}]
  (gen-plan
   [arg-ids (all (map item-to-ssa (list f target)))
    inst-id (add-instruction (->Call arg-ids))]
   inst-id))

(defmethod -item-to-ssa :protocol-invoke
  [{f :protocol-fn target :target args :args}]
  (gen-plan
   [arg-ids (all (map item-to-ssa (list* f target args)))
    inst-id (add-instruction (->Call arg-ids))]
   inst-id))

(defmethod -item-to-ssa :instance?
  [{:keys [class target]}]
  (gen-plan
   [arg-id (item-to-ssa target)
    inst-id (add-instruction (->Call (list `instance? class arg-id)))]
   inst-id))

(defmethod -item-to-ssa :prime-invoke
  [{f :fn args :args}]
  (gen-plan
   [arg-ids (all (map item-to-ssa (cons f args)))
    inst-id (add-instruction (->Call arg-ids))]
   inst-id))

(defmethod -item-to-ssa :instance-field
  [{:keys [instance field]}]
  (gen-plan
   [instance-id (item-to-ssa instance)
    inst-id (add-instruction (->InstanceInterop instance-id (symbol (str "-" field)) ()))]
   inst-id))

(defmethod -item-to-ssa :host-interop
  [{:keys [target m-or-f]}]
  (gen-plan
   [instance-id (item-to-ssa target)
    inst-id (add-instruction (->InstanceInterop instance-id m-or-f ()))]
   inst-id))

(defmethod -item-to-ssa :static-call
  [{:keys [class method args]}]
  (gen-plan
   [arg-ids (all (map item-to-ssa args))
    inst-id (add-instruction (->StaticCall class method arg-ids))]
   inst-id))

(defmethod -item-to-ssa :set!
  [{:keys [val target]}]
  (gen-plan
   [arg-ids (all (map item-to-ssa (list target val)))
    inst-id (add-instruction (->Call (cons 'set! arg-ids)))]
   inst-id))

(defn var-name [v]
  (let [nm (:name (meta v))
        nsp (.getName ^clojure.lang.Namespace (:ns (meta v)))]
    (symbol (name nsp) (name nm))))

(defmethod -item-to-ssa :var
  [{:keys [var]}]
  (gen-plan
   []
   (var-name var)))

(defmethod -item-to-ssa :const
  [{:keys [form]}]
  (gen-plan
   []
   form))

(defn let-binding-to-ssa
  "ssa로 변환한 init을 :locals에 바인딩을 푸시한다.
  name은 바인딩의 이름
  vary-meta - 메타데이터를 병합하면서 이름에 연결"
  [{:keys [name init form]}]
  (gen-plan
   [bind-id (item-to-ssa init)
    _ (push-alter-binding :locals assoc (vary-meta name merge (meta form)) bind-id)]
   bind-id))


(defmethod -item-to-ssa :let
  [{:keys [bindings body]}]
  (gen-plan
   [let-ids (all (map let-binding-to-ssa bindings)) ;; generate bindings ssa 
    _ (all (map (fn [_] (pop-binding :locals)) bindings)) ;; pop bindings from :locals

    local-ids (all (map (comp add-instruction ->Const) let-ids)) ;; 각 바인딩을 add-instruction이용하여 상수(Const)로 변환
    ;; locals에 
    _ (push-alter-binding :locals merge (into {} (map (fn [id {:keys [name form]}]
                                                        [name (vary-meta id merge (meta form))])
                                                      local-ids bindings)))

    body-id (item-to-ssa body)
    _ (pop-binding :locals)]
   body-id))

#_(defnmethod -item-to-ssa :loop
  [{:keys [body bindings] :as ast}]
  (gen-plan
   [local-val-ids (all (map let-binding-to-ssa bindings))
    _ (all (for [_ bindings]
             (pop-binding :locals)))
    local-ids (all (map (comp add-instruction ->Const)))]/))

;; ssa END
(defn- fn-handler
  [f]
  (reify
   Lock
   (lock [_])
   (unlock [_])

   impl/Handler
   (active? [_] true)
   (blockable? [_] true)
   (lock-id [_] 0)
   (commit [_] f)))

(defn run-state-machine [state]
  ((aget-object state FN-IDX) state))

(defn run-state-machine-wrapped [state]
  (try
    (run-state-machine state)
    (catch Throwable ex
      (impl/close! (aget-object state USER-START-IDX))
      (throw ex))))

(defn take!
  "<! 를 take! 로 변환하고, commit 시 fn-handler를 리턴
   fn-handler 리턴값(put에 들어온값)을 VALUE-IDX에 저장 STATE-IDX(다음에 갈 곳)은 block-id(blk)를 넣음. 
  아마 take가 있는 block일것..(확실치않음)
  해당 상태로 state machine 수행

  콜백으로 들어온 값을 VALUE-IDX에 넣고 다시 다음에 갈 STATE-IDX를 blk로 설정(??)
  이후 재귀."
  [state blk c]
  (if-let [cb (impl/take! c (fn-handler
                             (fn [x]
                               (aset-all! state VALUE-IDX x STATE-IDX blk)
                               (run-state-machine-wrapped state))))]
    (do (aset-all! state VALUE-IDX @cb STATE-IDX blk)
        :recur)
    nil))

(defn put! [state blk c val]
  (if-let [cb (impl/put! c val (fn-handler (fn [ret-val]
                                             (aset-all! state VALUE-IDX ret-val STATE-IDX blk)
                                             (run-state-machine-wrapped state))))]
    (do (aset-all! state VALUE-IDX @cb STATE-IDX blk)
        :recur)
    nil))

(defn return-chan [state value]
  (let [c (aget-object state USER-START-IDX)]
    (when-not (nil? value)
      (impl/put! c value (fn-handler (fn [_] nil))))
    (impl/close! c)
    c))

(def async-custom-terminators
    {'clojure.core.async/<! `take!
     'clojure.core.async/>! `put!
     'clojure.core.async/alts! 'clojure.core.async/ioc-alts!
     :Return `return-chan})

(defn mark-transitions
  {:pass-info {:walk :post :depends #{} :after an-jvm/default-passes}}
  [{:keys [op fn] :as ast}]
  (let [transitions (-> (env/deref-env) :passes-opts :mark-transitions/transitions)]
    ;; op=:invoke 이고 해당함수가 transition에 포함되어 있으면(기본적으로 async-custom-terminators임)
    ;; :transition으로 :op 를 표현한다.
    (if (and (= op :invoke)
             (= (:op fn) :var)
             (contains? transitions (var-name (:var fn))))
      (merge ast
             {:op :transition
              :name (get transitions (var-name (:var fn)))})
      ast)))

(defn propagate-transitions
  {:pass-info {:walk :post :depends #{#'mark-transitions}}}
  [{:keys [op] :as ast}]
  ;; mark-transitions 이후 (= op :transition) 이 있으면, 하위 노드로 전파한다.
  ;; 특정 노두가 :transition 이거나 
  (if (or (= op :transition)
          ;; 하위 노드 중 하나가 :transition or ::tranform? 이면 현재 노드에 ::transform? 추가
          (some #(or (= (:op %) :transition)
                     (::transform? %))
                (ast/children ast)))
    (assoc ast ::transform? true)
    ast))

(defn propagate-recur
  {:pass-info {:walk :post :depends #{#'annotate-loops #'propagate-transitions}}}
  [ast]
  (if (and (= (:op ast) :loop)
           (::transform? ast))
    ;; :loop 연산자가 ::transform? 플래그를 가지고 하위 노드 중 하나가 :recur 인 경우,
    ;; 해당 루프 ID를 가진 모든 노드에 ::transform? 플래그를 추가한다.
    ;; If we are a loop and we need to transform, and
    ;; one of our children is a recur, then we must transform everything
    ;; that has a recur
    (let [loop-id (:loop-id ast)]
      (ast/postwalk ast #(if (contains? (:loops %) loop-id)
                           (assoc % ::transform? true)
                           %)))
    ast))

(defn nested-go? [env]
  (-> env
      vals
      first
      map?))

(def passes (into (disj an-jvm/default-passes #'warn-on-reflection)
                  #{#'propagate-recur
                    #'propagate-transitions
                    #'mark-transitions}))

(def run-passes
  ;; ast에 대한 분석 패스들을 조직하고 적용하는 역할을 한다.
  ;; 이 함수는 각 패스가 어떻게 적용되어야 할지 설명하는 메타데이터를 사용하여 패스들을 순서대로 정렬하고
  ;; 의존성을 처리하며, 가능한 적은 트리 순회로 패스를 적용하려고 시도한다.
  ;; 주요인자
  ;; - passes : 적용할 패스들의 집합. 각 패스는 Var로 표현되고, 해당 Var의 메타데이터에 `:pass-info` 요소가 포함되어있어야 한다.
  ;; - opts : 스케줄링 동작을 제어하는 옵션 `:debug?` 같은 플래그 사용
  ;; - `:pass-info` 맵에는 다음과 같은 항목을 포함함
  ;;   - `:after`, `:before`, `:depends` : 패스 실행 순서와 의존성 제어
  ;;   - `walk` : 패스 트리 순회 방식 (`:none` `:post` `:pre` `any` 중 하나)
  ;;   - `affects` : 트리 순회에서 이 패스가 마지막으로 실행되어야할 패스들을 지정한다.
  ;;   - `:state` : 트리 순회를 걸쳐 상태를 유지할 수 있는 초기 값을 저장하는 아톰을 반환하는 함수.
  ;; 패스란?
  ;; 패스(pass)는 프로그래밍 언어의 소스 코드나 추상 구문 트리(AST)를 분석하거나 변환하는 단계를 의미.
  ;; 컴파일러나 인터프리터, 코드 분석 도구 등에서 사용되며, 특정 목적에 따라 소스 코드나 AST를 처리한다.

  ;; 패스는 다음과 같은 작업을 수행할 수 있다.
  ;; 분석 : 소스 코드나 AST를 읽어서 특정 정보를 수집합니다. 예를 들어, 타입 체크 패스는 변수의 타입이 올바른지 확인합니다.
  ;; 변환: 소스 코드나 AST를 읽어서 변경된 버전을 생성합니다. 예를 들어, 최적화 패스는 성능을 개선하기 위해 코드를 변환합니다.
  ;; 검증: 코드가 특정 규칙이나 제약 조건을 준수하는지 검사합니다. 이러한 패스는 코드의 품질을 향상시키거나 오류를 찾는 데 사용됩니다.
  ;; 패스는 보통 다른 패스와 함께 일련의 처리 파이프라인을 구성합니다. 각 패스는 특정 작업을 수행하며, 한 패스의 출력이 다음 패스의 입력이 될 수 있습니다.
  ;; clojure.tools.analyzer.passes의 schedule 함수와 같은 도구는 이러한 패스들을 조직하고 실행하는 데 사용됩니다.
  ;; 각 패스의 의존성과 순서, 실행 방식 등을 제어하면서, 전체 분석 또는 변환 프로세스를 효율적으로 관리합니다.

  ;; emit-form pass는 ast를 clojure 코드로 바꿔주고 validate pass는 유효성을 검사한다.
  ;; 결론 : pass는 AST의 노드들을 방문함녀서 어떤 작업을 수행하는 함수를 말한다.
  (schedule passes))

(defn emit-hinted [local tag env]
  ;; local 데이터의 tag를 가져오거나 인자로 직접 제공된 tag를 가져옴.
  (let [tag (or tag (-> local meta tag))
        init (list (get env local))]
    ;; 기본데이터타입 처리 `tag` 의 값이 특정 기본 데이터 타입에 해당하는지 확인.
    ;; 그에 따라 특정 함수를 사용
    (if-let [prim-fn (case (cond-> tag (string? tag) symbol)
                       int `int
                       long `long
                       char `char
                       float `float
                       byte `byte
                       short `short
                       boolean `boolean
                       nil)]
      ;; 기본데이터의 경우 (list prim-fn init) 으로 해당 값으로 전환.
      [(vary-meta local dissoc :tag) (list prim-fn init)]
      ;; 기본데이터가 아닌 경우 메타데이터에 tag를 추가 or 수정
      [(vary-meta local merge (when tag {:tag tag})) init])))

(defn state-machine [body num-user-params [crossing-env env] user-transitions]
  (binding [an-jvm/run-passes run-passes]
    (-> (an-jvm/analyze `(let [~@(if (nested-go? env)
                                   ;; go가 nested 하게 있다면?
                                   (mapcat (fn [[l {:keys [tag]}]]
                                             (emit-hinted l tag crossing-env))
                                           env)
                                   ;; 
                                   (mapcat (fn [[l ^clojure.lang.Compiler$LocalBinding lb]]
                                             (emit-hinted l (when (.hashJavaClass lb)
                                                              (some-> lb .getJavaClass .getName))
                                                          crossing-env))
                                           env))]
                           ~body)))))

;; temp


(-> (an-jvm/analyze
     `(let [a# 3]
        (+ 1 a#)))

    pprint)

(comment
  (def my-plan (gen-plan
                [_   (assoc-in-plan [:foo :bar] 42)
                 val (get-in-plan [:foo :bar])]
                val))
  (get-plan my-plan) ;;=> [nil {:foo {:bar 42}}]
  ;; =>
  ;; 이것을 보면 알 수 있듯이. vector의 두번째 값을 계속 plan이다.
  ;; 계속 plan이 리턴되어야 상태가 보존된다.
  (fn [plan]
    (let [[_ plan] ((assoc-in-plan [:foo :bar] 42) plan)
          [val plan] ((get-in-plan [:foo :bar]) plan)]
      [val plan]))


  ;;
  )



