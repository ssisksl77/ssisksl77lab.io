(ns ssisksl77.diy-core.mutex
  (:import [java.util.concurrent.locks Lock ReentrantLock]))

(defn mutex []
  (let [m (ReentrantLock.)]
    (reify
      Lock
      (lock [_] (.lock m))
      (unlock [_] (.unlock m)))))
