(ns ssisksl77.diy-core.tools_analyzer)


;; https://github.com/clojure/tools.analyzer
(require '[clojure.tools.analyzer :as ana])
(require '[clojure.tools.analyzer.env :as env])
(comment (defn analyze [form env]
           (binding [ana/macroexpand-1 macroexpand-1
                     ana/create-var create-var
                     ana/parse parse
                     ana/var? var?]
             (env/ensure (global-env)
                         (run-passes (-analyze form env))))))
;; Here, `-analyze` is a multimethod that defaults to `ana/analyze` and defines analysis methods for the JVM specific special forms,
;; `global-env` is a function that returns a global environment for the JVM analyzer
;; `run-passes` is a functino that takes an AST and applies a number of passes to it.

;; The `tools.analyzer.jvm` README contains more example on how the `analyze` function works as well as a
;; reference for all the nodes it can return

;; One of the most important features of `tools.analyzer` is the ability to walk generically through the AST nodes,
;; this has been immensely useful to write most of the passes used by the various analyzer.
;; The `tools.analyzer.ast` namespace provides a number of functions that implement various generic AST walking strategies.

;; The `children` function returns a vector of the children nodes of the current node
;; (the output has been elided and pretty-printed for clarity):



;; https://github.com/clojure/tools.analyzer.jvm#ast-quickref
(require '[clojure.tools.analyzer.jvm :as ana.jvm])
(pprint (ana.jvm/analyze 1))
(comment
  {:val 1,
   :type :number,
   :op :const,
   :env
   {:context :ctx/expr,
    :locals {},
    :ns ssisksl77.diy-core.tools_analyzer,
    :file "/Users/tom/IdeaProjects/ssisksl77.gitlab.io/codes/diy-core.async/src/ssisksl77/diy_core/tools_analyzer.clj"},
   :o-tag long,
   :literal? true,
   :top-level true,
   :form 1,
   :tag long}
  ;;
  )

;; To get a clojure form out of an AST, use the `emit-form`
(require '[clojure.tools.analyzer.passes.jvm.emit-form :as e])
(e/emit-form (ana.jvm/analyze '(let [a 1] a)))
(comment
  (let* [a 1] a)
  ;;
  )

;; output will be fully macroexpanded.
;; You can also get an hygienic form back, using the `emit-hygienic-form` pass.
(e/emit-hygienic-form (ana.jvm/analyze '(let [a 1 a a] a)))
(comment (let* [a__#0 1 a__#1 a__#0] a__#1))

;; As you can see the local names are renamed to resolve shadowing.
;; The `analyze` function can take an environment arg
;; (when not provided it uses the defualt empty-env)
;; which allows for more advanced usaged, like injecting locals from an outer scope.
(-> '(let [a a] a)
    (ana.jvm/analyze (assoc (ana.jvm/empty-env)
                            :locals '{a {:op :binding
                                         :name a
                                         :form a
                                         :local :let}}))
    e/emit-hygienic-form)
(comment
  (let* [a__#0 a] a__#0)
  ;;
  )

;; There's also an `analyze-eval` function that, as the name suggests,
;; evaluates the form after its analysis and stores the resulting value in the `:result` field of the AST,
;; this function should be used when analyzing multiple forms,
;; as the analysis of a clojure form might require the evaluation of a previous one to make sense.

;; This would not work using `analyze` but works find when using `analyze-eval` :
(-> (ana.jvm/analyze+eval '(defmacro x []))
    (select-keys [:op :top-level :form :result])
    pprint)
(comment {:op :do,
          :top-level true,
          :form
          (do (clojure.core/defn x ([&form &env])) (. #'x (setMacro)) #'x),
          :result #'ssisksl77.diy-core.tools_analyzer/x})

(-> (ana.jvm/analyze+eval '(x))
    pprint)
(comment
  {:val nil,
 :type :nil,
 :op :const,
 :env
 {:context :ctx/expr,
  :locals {},
  :ns ssisksl77.diy-core.tools_analyzer,
  :column 28,
  :line 76,
  :file
  "/Users/tom/IdeaProjects/ssisksl77.gitlab.io/codes/diy-core.async/src/ssisksl77/diy_core/tools_analyzer.clj"},
 :o-tag nil,
 :literal? true,
 :result nil,
 :top-level true,
 :form nil,
 :tag nil,
 :raw-forms ((x))})

;; To analyze a whole namespace, use analyze-ns which behaves like `analyze-eval` and puts the ASTs
;; for each analyzed form in a vector, in order.
#_(ana.jvm/analyze-ns 'clojure.string)
(comment
  [{:op        :do,
  :result    nil,
  :top-level true,
  :form      (do (clojure.core/in-ns (quote clojure.string)) )}
   ;;...
   ]
  ;;
  )


















