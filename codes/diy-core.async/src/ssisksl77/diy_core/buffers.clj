(ns ssisksl77.diy-core.buffers
  (:require [ssisksl77.diy-core.protocols :as impl])
  (:import [java.util LinkedList Queue]
           [clojure.lang Counted]))

;; size가 꽉차면 받지 않는다.
(deftype FixedBuffer [^LinkedList buf ^long n]
  impl/Buffer
  (full? [this]
    (>= (.size buf) n))
  (remove! [this]
    (.removeLast buf))
  (add!* [this itm]
    (.addFirst buf itm)
    this)
  (close-buf! [this])
  
  clojure.lang.Counted
  (count [this]
    (.size buf)))

(defn fixed-buffer [^long n]
  (FixedBuffer. (LinkedList.) n))

(deftype DroppingBuffer [^LinkedList buf ^long n]
  impl/UnblockingBuffer
  impl/Buffer
  (full? [this]
    ;; 항상 false
    false)
  (remove! [this]
    (.removeLast buf))
  (add!* [this itm]
    ;; 꽉찼으면 아예 넣지를 않음.
    (when-not (>= (.size buf) n)
      (.addFirst buf itm))
    this)
  (close-buf! [this])

  clojure.lang.Counted
  (count [this]
    (.size buf)))

(defn dropping-buffer [n]
  (DroppingBuffer. (LinkedList.) n))

(deftype SlidingBuffer [^LinkedList buf ^long n]
  impl/UnblockingBuffer
  impl/Buffer
  (full? [this]
    false)
  (remove! [this]
    (.removeLast buf))
  (add!* [this itm]
    (when (= (.size buf) n)
      (impl/remove! this))
    (.addFirst buf itm)
    this)
  (close-buf! [this])
  clojure.lang.Counted
  (count [this]
    (.size buf)))

(defn sliding-buffer [n]
  (SlidingBuffer. (LinkedList.) n))


;; 프로미스 버퍼는 한번만 값을 담을 수 이쓴ㄴ 버퍼이다.
;; 프로미스 객체와 유사한 역할을 하며, 한번 값이 설정되면 변경되지 않는ㄴ다.
;; NO-VAL 프로미스 버퍼에 값이 아직 전달되지 않았음을 나타낸다.
;; undelivered? 이 함수는 값이 전달되었는지 여부를 알려준다.
;; unsynchronized-mutable 메타데이터는 특정 필드가 동기화되지 않은 mutable 상태로
;; 변경될 수 있음을 나타낸다.
;; clojure는 일반적으로 불변이지만 특별하게 변경이 필요한 경우. 해당 메타를 추가하여,
;; JVM 기본 동작과 더 효율적으로 상호동작할 수 있다.
;; 이는 deftype, defrecord, defprotocol에서 사용ㅇ되며 특정 필드가 ㅁ변경가능하며 동기화되지 않는다는 것을 컴파일러에게 알린다.
;; 효과 :
;; 1. 성능최적화(동기화가 필요없으면, 락이 없이 필드에 접근하고 변경하므로 성능이 향상될 수 있다.
;; 2. 직접적인 상태변경
;; 프로미스버퍼는 값을 한번만 설정하고 그 후에는 변경되지 않으므로, 동기화가 필요없다고 판단되어 사용한 것으로 보임.
(defonce ^:private NO-VAL (Object.))
(defn- undelivered? [val]
  (identical? NO-VAL val))

(deftype PromiseBuffer [^:unsynchronized-mutable val]
  impl/UnblockingBuffer
  impl/Buffer
  (full? [_] false)
  (remove! [_] val)
  (add!* [this itm]
    (when (undelivered? val)
      (set! val itm))
    this)
  (close-buf! [_]
    (when (undelivered? val)
      (set! val nil)))
  clojure.lang.Counted
  (count [_]
    (if (undelivered? val)
      0
      1)))

(defn fixed-buffer [^long n]
  (FixedBuffer. (LinkedList.) n))

(deftype DroppingBuffer [^LinkedList buf ^long n]
  impl/UnblockingBuffer
  impl/Buffer
  (full? [_this]
    false)
  (remove! [_this]
    (.removeLast buf))
  (add!* [this itm]
    (when-not (>= (.size buf) n)
      (.addFirst buf itm))
    this)
  (close-buf! [_this])
  Counted
  (count [_this]
    (.size buf)))

(defn dropping-buffer [n]
  (DroppingBuffer. (LinkedList.) n))

(deftype SlidingBuffer [^LinkedList buf ^long n]
  impl/UnblockingBuffer
  impl/Buffer
  (full? [_this]
    false)
  (remove! [_this]
    (.removeLast buf))
  (add!* [this itm]
    (when (= (.size buf) n)
      (impl/remove! this))
    (.addFirst buf itm)
    this)
  (close-buf! [_this])
  Counted
  (count [_this]
    (.size buf)))

(defonce ^:private NO-VAL (Object.))
(defn- undelivered? [val]
  (identical? NO-VAL val))

(deftype PromiseBuffer [^:unsynchronized-mutable val]
  impl/UnblockingBuffer
  impl/Buffer
  (full? [_]
    false)
  (remove! [_]
    val)
  (add!* [this itm]
    (when (undelivered? val)
      (set! val itm))
    this)
  (close-buf! [_]
    (when (undelivered? val)
      (set! val nil)))
  Counted
  (count [_]
    (if (undelivered? val) 0 1)))

(defn promise-buffer []
  (PromiseBuffer. NO-VAL))
