(ns ssisksl77.diy-core.async
  (:require [ssisksl77.diy-core.protocols :as impl]
            [ssisksl77.diy-core.buffers :as buffers]
            [ssisksl77.diy-core.channel :as channels]
            [ssisksl77.diy-core.dispatch :as dispatch]
            [ssisksl77.diy-core.ioc-macro :as ioc])
  (:import [java.util.concurrent.locks Lock]
           [clojure.lang Var])
  (:gen-class))

(alias 'core 'clojure.core)

;; handler에 대해 이해해야함.
;; 기본적인 구현인듯.
(defn fn-handler
  ([f]
   (fn-handler f true))
  ([f blockable]
   (reify
     Lock
     (lock [_])
     (unlock [_])

     impl/Handler
     (active? [_] true)
     (blockable? [_] blockable)
     (lock-id [_] 0)
     (commit [_] f))))

(defn buffer [n]
  (assert (pos? n) "fixed buffers must have size > 0")
  (buffers/fixed-buffer n))

(defn dropping-buffer
  "Returns a buffer of size n. When full, puts will complete but
  val will be dropped (no transfer)."
  [n]
  (buffers/dropping-buffer n))

(defn sliding-buffer
  [n]
  (buffers/sliding-buffer n))

(defn unblocking-buffer?
  [buff]
  (extends? impl/UnblockingBuffer (class buff)))

(defn chan
  ([] (chan nil))
  ([buf-or-n] (chan buf-or-n nil))
  ([buf-or-n xform] (chan buf-or-n xform nil))
  ([buf-or-n xform ex-handler]
   (when xform (assert buf-or-n "buffer must be supplied when transducer is"))
   (channels/chan (if (number? buf-or-n) (buffer buf-or-n) buf-or-n) xform ex-handler)))

(defn take!
  "Asynchronously takes a val from port, passing to fn1.
   Will pass nil if closed. If on-caller? (default true) is true, the value is immediately available, will call fn1 on calling thread.

  fn1 may be run in a fixed-size dispatch thread pool and should not perform blocking IO,
  including core.async blocking ops (those that end in !!).

  Returns nil"
  ([port fn1] (take! port fn1 true))
  ([port fn1 on-caller?]
   (let [ret (impl/take! port (fn-handler fn1))]
     (when ret
       (let [val @ret]
         (if on-caller?
           (fn1 val)
           (dispatch/run #(fn1 val)))))
     nil)))

(defn- nop [_])
(def ^:private fhnop (fn-handler nop))

(defn put!
  "Asynchronously puts a val into port, calling fn1 (if supplied) when
   complete, passing false iff port is already closed. nil values are
   not allowed. If on-caller? (default true) is true, and the put is
   immediately accepted, will call fn1 on calling thread.

   fn1 may be run in a fixed-size dispatch thread pool and should not
   perform blocking IO, including core.async blocking ops (those that
   end in !!).

   Returns true unless port is already closed."
  ([port val]
   (if-let [ret (impl/put! port val fhnop)]
     @ret
     true))
  ([port val fn1] (put! port val fn1 true))
  ([port val fn1 on-caller?]
   (if-let [retb (impl/put! port val (fn-handler fn1))]
     (let [ret @retb]
       (if on-caller?
         (fn1 ret)
         (dispatch/run #(fn1 ret)))
       ret)
     true)))

(defn close!
  [chan]
  (impl/close! chan))


(defmacro go
  [& body]
  ;; 바깥에 바인딩된 심볼들에 gensym 값을 만들어서 연결.
  (let [crossing-env (zipmap (keys &env) (repeatedly gensym))]
    `(let [c# (chan 1)
           captured-bindings (Var/getThreadBindingFrame)]
       (dispatch/run
         (^:once fn* []
          (let [~@(mapcat (fn [[l sym]] [sym `(^:once fn* [] ~(vary-meta l dissoc :tag))]) crossing-env)
                f# ~(ioc/state-machine `(do ~@body) 1 [crossing-env &env] ioc/async-custom-terminators)
                state# (-> (f#)
                           (ioc/aset-all! ioc/USER-START-IDX c#
                                          ioc/BINDINGS-IDX captured-bindings#))]
            (ioc/run-state-machine-wrapped state#))))
       c#)))



(defn greet
  "Callable entry point to the application."
  [data]
  (println (str "Hello, " (or (:name data) "World") "!")))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (greet {:name (first args)}))


