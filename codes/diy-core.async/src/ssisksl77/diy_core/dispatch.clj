(ns ssisksl77.diy-core.dispatch
  (:require [ssisksl77.diy-core.protocols :as impl]
            [ssisksl77.diy-core.threadpool :as tp]))

(defonce ^:private in-dispatch (ThreadLocal.))

;; ThreadLocal은 인스턴스가 하나지만
;; 각 스레드마다 독립적으로 값을 저장할 수 있다.
;; 값을 공유하지 않으며, ThreadLocal은 스레드한정 로컬 저장소 역할을 한다.
;; 이것으로 내부로직에서 디스패치 풀에 속한 스레드인지 아닌지 판단할 수 있다.
;; 디스패치 풀 스레드에서는 특정 작업(블로킹 호출 금지)를 수행하는 등 유용.
(defonce executor
  (delay (tp/thread-pool-executor #(.set ^ThreadLocal in-dispatch true))))

(defn in-dispatch-thread?
  []
  (boolean (.get ^ThreadLocal in-dispatch)))

;; core.async 내부 스레드면 블로킹연산 금지.
(defn check-blocking-in-dispatch
  []
  (when (.get ^ThreadLocal in-dispatch)
    (throw (IllegalStateException. "Invalid blocking call in dispatch thread"))))

(defn run
  [^Runnable r]
  (impl/exec @executor r))
