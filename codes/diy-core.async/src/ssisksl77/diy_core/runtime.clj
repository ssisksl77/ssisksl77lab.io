(ns ssisksl77.diy-core.runtime
  (:require [ssisksl77.diy-core.protocols :as impl])
  (:import [java.util.concurrent.locks Lock]
           [java.util.concurrent.atomic AtomicReferenceArray]))

;; state machine function 
(def ^:const FN-IDX 0)
;; maybe next(or current) block (to run)
(def ^:const STATE-IDX 1)
;; return value
(def ^:const VALUE-IDX 2)
(def ^:const BINDINGS-IDX 3)
(def ^:const EXCEPTION-FRAMES 4)
(def ^:const CURRENT-EXCEPTION 5)
(def ^:const USER-START-IDX 6)

(defn aset-object [^AtomicReferenceArray arr idx ^Object o]
  (.set arr idx o))

(defn aget-object [^AtomicReferenceArray arr idx]
  (.get arr idx))

(defmacro aset-all!
  [arr & more]
  (assert (even? (count more)) "Must give an even number of args to aset-all!")
  (let [bindings (partition 2 more)
        arr-sym (gensym "statearr-")]
    `(let [~arr-sym ~arr]
       ~@(map
          (fn [[idx val]]
            `(aset-object ~arr-sym ~idx ~val))
          bindings)
       ~arr-sym)))

;; unused fn
(defn finished?
  "Returns true if the machine is in a finished state"
  [state-array]
  (identical? (aget-object state-array STATE-IDX) ::finished))


(defn- fn-handler
  [f]
  (reify
   Lock
   (lock [_])
   (unlock [_])

   impl/Handler
   (active? [_] true)
   (blockable? [_] true)
   (lock-id [_] 0)
   (commit [_] f)))


(defn run-state-machine [state]
  ((aget-object state FN-IDX) state))

(defn run-state-machine-wrapped [state]
  (try
    (run-state-machine state)
    (catch Throwable ex
      ;; close channel
      (impl/close! (aget-object state USER-START-IDX))
      (throw ex))))

;; fn-handler는 impl/commit 을 할 때 리턴하는 녀석이다.
;; 
(defn take! [state blk c]
  (if-let [cb (impl/take! c (fn-handler
                             (fn [x]
                               (aset-all! state
                                 VALUE-IDX x
                                 STATE-IDX blk)
                               (run-state-machine-wrapped state))))]
    (do (aset-all! state
          VALUE-IDX @cb
          STATE-IDX blk)
        :recur)
    nil))

(defn return-chan [state value]
  (let [c (aget-object state USER-START-IDX)]
    (when-not (nil? value)
      (impl/put! c value (fn-handler (fn [] nil))))
    (impl/close! c)
    c))


#_(def async-custom-terminators
  {'ssisksl77.diy-core.asyncc/<! `take!
   'ssisksl77.dir-core.async/>! `put!
   'ssisksl77.dir-core.async/alts! 'ssisksl77.diy-core.async/ioc-alts!
   :Return `return-chan})
